import time
import serial
from termcolor import colored
import os

READING_ENABLED = True
# configure the serial connections (the parameters differs on the device you are connecting to)
#Communication speed 19200 Baud (8b,1 stop, no parity, no flow control).
ser = serial.Serial(
    port='/dev/ttyUSB0',
    baudrate=19200,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)

#ser.open()
ser.isOpen()


print colored('Enter your commands below.\r\nInsert "exit" to leave the application.', 'green')

input = 1
while 1:
    # get keyboard input
    input = raw_input(">> ")
        # Python 3 users
        # input = input(">> ")
    if input == 'exit':
        ser.close()
        exit()
    else:
        # send the character to the device
        # (note that I happend a \r\n carriage return and line feed to the characters - this is requested by my device)
        to_send = input + '\r'
        ser.write(to_send)
        print "This is the COMMAND-->#" + to_send + "#"
        out = ''
        # let's wait one second before reading output (let's give device time to answer)
        time.sleep(1)
        while ser.inWaiting() > 0:

            out += ser.read(1)

            if READING_ENABLED:
                print colored("Red-->@", 'green'), colored(str(out), 'red'), colored("@", 'blue')
                time.sleep(0.05)
                os.system('clear')

        print "FINISHED READING"
        if out != '':
            print ">>" + out
