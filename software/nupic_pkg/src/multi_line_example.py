#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib.animation as animation
import random
import math

# This example uses subclassing, but there is no reason that the proper
# function couldn't be set up and then use FuncAnimation. The code is long, but
# not really complex. The length is due solely to the fact that there are a
# total of 9 lines that need to be changed for the animation as well as 3
# subplots that need initial set up.
# Install for save http://fcorti.com/2014/04/22/ffmpeg-ubuntu-14-04-lts/
class SubplotAnimation(animation.TimedAnimation):
    def __init__(self, number_lvls):
        self.fig = plt.figure()
        self.color_array = ['black', 'red', 'green']
        self.number_lvls = number_lvls
        self.ax_array = []
        self.line_array = []

        self.fill_ax_array()
        self.fill_line_array()

        self.t = np.linspace(0, 80, 400)
        self.x = np.cos(2 * np.pi * self.t / 10.)
        self.y = np.sin(2 * np.pi * self.t / 10.)
        self.z = 10 * self.t

        self.ax_setup()

        animation.TimedAnimation.__init__(self, self.fig, interval=50, blit=True)


    def fill_ax_array(self):
        for lvl in range(1,self.number_lvls+1):
            num_rows = int(math.ceil(self.number_lvls/3.0))
            num_cols = min(self.number_lvls, 3)
            position = lvl
            ax = self.fig.add_subplot(num_rows, num_cols, position)
            self.ax_array.append(ax)

    def fill_line_array(self):
        for lvl in range(1, self.number_lvls+1):
            color = self.color_array[random.randint(0, len(self.color_array)-1)]
            line = Line2D([], [], color=color)
            self.line_array.append(line)

    def ax_setup(self):
        for idx, ax in enumerate(self.ax_array):
            ax.set_xlabel('x')
            ax.set_ylabel('y')

            ax.add_line(self.line_array[idx])

            ax.set_xlim(-2, 2)
            ax.set_ylim(-2, 2)

            ax.set_aspect('equal', 'datalim')

    def _draw_frame(self, framedata):
        i = framedata
        data_array = []
        head = i - 1
        head_len = 10
        head_slice = (self.t > self.t[i] - 1.0) & (self.t < self.t[i])

        self._drawn_artists = []

        for idx, line in enumerate(self.line_array):

            x = self.x[head_slice]
            y = self.y[head_slice]

            line.set_data(x, y)
            self._drawn_artists.append(line)

    def new_frame_seq(self):
        return iter(range(self.t.size))

    def _init_draw(self):
        for l in self.line_array:
            l.set_data([], [])

ani = SubplotAnimation(2)
mywriter = animation.FFMpegWriter()
ani.save('/home/rdaneel/Videos/test_sub.mp4',writer=mywriter)
plt.show()