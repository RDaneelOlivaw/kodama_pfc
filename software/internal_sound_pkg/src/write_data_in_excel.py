"""
This module is for writing data in an excel file
$sudo pip install xlwt
"""
__author__ = 'rdaneel'

import os
import xlwt
import math
from datetime import datetime

from xlutils.copy import copy # http://pypi.python.org/pypi/xlutils, pip install xlutils
from xlrd import open_workbook # http://pypi.python.org/pypi/xlrd
from xlwt import easyxf # http://pypi.python.org/pypi/xlwt


TEST_EXCEL_PATH = "/home/rdaneel/Kodama/kodama_project/src/recording_internal_sound/peak-detect/excel_file/test_excel_file.xls"
TEST_EXCEL_PATH2 = "/home/rdaneel/Kodama/kodama_project/src/recording_internal_sound/peak-detect/excel_file/data.xls"

def init_test():
    style0 = xlwt.easyxf('font: name Times New Roman, color-index red, bold on',
        num_format_str='#,##0.00')
    style1 = xlwt.easyxf(num_format_str='D-MMM-YY')

    wb = xlwt.Workbook()
    ws = wb.add_sheet('A Test Sheet')

    ws.write(0, 0, 1234.56, style0)
    ws.write(1, 0, datetime.now(), style1)
    ws.write(2, 0, 1)
    ws.write(2, 1, 3)
    ws.write(2, 2, xlwt.Formula("A3+B3"))

    wb.save('example.xls')

    return None

def write_sinus_into_existing_file(file_path):

    # 0 based (subtract 1 from excel row number)
    START_ROW = 0
    col_y_value = 1
    col_x_value = 0


    rb = open_workbook(file_path, formatting_info=True)
    # read only copy to introspect the file
    r_sheet = rb.sheet_by_index(0)
    # a writable copy (I can't read values out of this, only write to it)
    wb = copy(rb)
    # the sheet to write to within the writable copy
    w_sheet = wb.get_sheet(0)

    row_index = START_ROW + 1
    Fs = 150.0;  # sampling rate
    Ts = 1.0/Fs; # sampling interval
    t = Ts
    ff = 1;   # frequency of the signal
    time_recorded = 1
    while t < time_recorded:
        y = math.sin(2*math.pi*ff*t)
        print "Time="+str(t)
        print "Y value="+str(y)

        w_sheet.write(row_index, col_y_value, y)
        w_sheet.write(row_index, col_x_value, t)

        t += Ts
        row_index += 1



    print file_path
    print os.path.splitext(file_path)[-1]

    print "Saving file..."
    wb.save(file_path)
    print "Done"

class ExcelFileObject(object):
    def __init__(self, file_path):

        self._file_path = file_path
        try:
            self.rb = open_workbook(file_path, formatting_info=True)
            # read only copy to introspect the file
            self.r_sheet = self.rb.sheet_by_index(0)
            # a writable copy (I can't read values out of this, only write to it)
            self.wb = copy(self.rb)
            # the sheet to write to within the writable copy
            self.w_sheet = self.wb.get_sheet(0)
            print "Opened Existing File"
        except:
            self.wb = xlwt.Workbook()
            self.w_sheet = self.wb.add_sheet('Data Sheet')
            print "Created a new File"

        self.start_row = 0
        self.start_row = 0
        self.col_y_value = 1
        self.col_x_value = 0

        self.row_index_L = self.start_row
        self.row_index_R = self.start_row
        self.col_index = self.start_row

    def write_data(self, x, y, is_left):
        print "X in write ="+str(x)
        print "Y in write ="+str(y)
        if is_left:
            self.w_sheet.write(self.row_index_L, self.col_y_value, y)
            self.w_sheet.write(self.row_index_L, self.col_x_value, x)
            self.row_index_L += 1
        else:
            self.w_sheet.write(self.row_index_R, self.col_y_value + 3, y)
            self.w_sheet.write(self.row_index_R, self.col_x_value + 3, x)
            self.row_index_R += 1


    def save_data(self):

        self.wb.save(self._file_path)
        print "File Saved"



if __name__ == "__main__":

    s = None
    while not s == "exit":
        print "(1) Init Test (2)Write a sinus test, exit to finish (3)Write data step by step"
        s = raw_input("Select Option:\n")

        if s == "1":
            init_test()

        elif s == "2":
            write_sinus_into_existing_file(TEST_EXCEL_PATH)

        elif s == "3":
            e_o = ExcelFileObject(TEST_EXCEL_PATH2)
            x = 0
            while x < 1000:
                e_o.write_data(x = x, y = math.sqrt(x))
                x += 1
            e_o.save_data()

        else:
            None