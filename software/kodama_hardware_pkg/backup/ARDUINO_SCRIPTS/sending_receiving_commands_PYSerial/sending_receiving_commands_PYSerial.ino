// This is works with python script sending_receiving_commands_PYSerial.py
String servo_position = "0";
String MAX_POS = "180";
String MIN_POS = "0";
int i_MAX_POS = 180;
int i_MIN_POS = 0;

void setup() {
Serial.begin(9600); // set the baud rate
Serial.println("Ready"); // print "Ready" once

}

void loop() {
handleSerialCommunication();
}

void handleSerialCommunication(void){
  if (Serial.available())
  {
    char inByte = Serial.read();
    switch(inByte)
    {
    case 'r': //Data request
      serialPrintPos();
      break;

    case 'b': //Set to constant beer temperature
      int servo_position;
      numberFromSerial();
      Serial.println("MOVING");
      delay(1000);
      Serial.println("..");
      delay(1000);
      Serial.println("..");
      delay(1000);
      Serial.println("OK");
      break;

    default:
      Serial.println(inByte);
    }
    Serial.flush();
  }
}
 
void numberFromSerial(void)
{
  char numberString[8];
  unsigned char index=0;
  delay(10);
  while(Serial.available())
  {
    delay(10);
    numberString[index++]=Serial.read();
    if(index > 2)
    {
      break;
    }
  }
  numberString[index]=0;
  //return atoi(numberString);
  //return numberString;
  servo_position = servoLimits(atoi(numberString),numberString);
}

void serialPrintPos(void)
{
 Serial.println(servo_position);
 delay(1000);
   
}

String servoLimits(int i_number, String s_init_num)
{
 String s_num;

 if (i_number > i_MAX_POS)
 {
   s_num = MAX_POS;
 }
 else if (i_number < i_MIN_POS)
 {
   s_num = MIN_POS;
 }
 else
 {
   s_num = s_init_num;
 }
 return s_num;
}
