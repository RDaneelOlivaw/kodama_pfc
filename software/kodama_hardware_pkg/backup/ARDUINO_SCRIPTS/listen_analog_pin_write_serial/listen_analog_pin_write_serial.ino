/* Knock Poller
 * ----------------
 * We listen to an analog pin, sample the
 * signal and write it to the serial port.
 */

int knockSensor = 0;
long val = 0;

void setup() {
 Serial.begin(9600);
 Serial.println("Ready to READ ANALOG SIGNALS"); // print "Ready to Move PCB STARTER_KIT" once
}

void loop() {
    val = analogRead(knockSensor);
    Serial.println(val,DEC);
    delay(100);  // we have to make a delay to avoid overloading the serial port
}
