#!/usr/bin/env python

import rospy
from nupic_pkg.msg import htm_output
import random

PUBLISH_RATE = 10

def update_msg(msg):
    """
    Updates the values of the msg based on the values
    :param msg:
    :param ir_object:
    :return:
    """
    size = 4
    msg.sp_output_array = []
    msg.tp_prediction_array = []

    for _ in range(0,size):
        msg.sp_output_array.append(bool(random.randint(0,1)))
        msg.tp_prediction_array.append(bool(random.randint(0,1)))

    return msg

def talker():
    rospy.init_node('htm_output_publisher', anonymous=True)

    pub = rospy.Publisher("/htm_output", htm_output, queue_size=1)

    rate = rospy.Rate(PUBLISH_RATE)

    msg = htm_output()
    update_msg(msg)

    while not rospy.is_shutdown():

        update_msg(msg)
        rospy.loginfo(msg)
        pub.publish(msg)
        rate.sleep()



if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass