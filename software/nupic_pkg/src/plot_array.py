#!/usr/bin/env python
import rospy
from pylab import *
from internal_sound_pkg.msg import internal_sound_sdr
from nupic_pkg.msg import graph
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import math
import time
import random

X_MAX = 200
Y_MAX = 1
INTERVAL_T = 20
T_STEPS = INTERVAL_T/1000.0

ACCEPTABLE_ERROR_LIMIT = 0.2


class PlotGraph(object):
    def __init__(self, t_init):
        self._t_init = t_init
        self._t_now = 0

    def update_t_now(self, t_value):
        self._t_now = self._t_now + (t_value - self._t_init)

    def get_t_now(self):
        return self._t_now


def plot_array():

    rospy.init_node('graph_plotter', anonymous=True)

    msg = rospy.wait_for_message('graph_data', graph)
    #t_init = msg.t_value
    #t_init = time.time()
    t_init = rospy.get_time()
    plt_graph_object = PlotGraph(t_init)


    fig, ax = plt.subplots()

    fig.suptitle('bold figure suptitle', fontsize=14, fontweight='bold')
    line, = ax.plot([], [], color='black', lw=3)
    ax.set_ylim(0, Y_MAX)
    ax.set_xlim(0, X_MAX)
    ax.set_xlabel('xlabel')
    ax.set_ylabel('ylabel')
    ax.grid()
    xdata, ydata = [], []

    def update_t(t_value):
        return plt_graph_object.update_t_now(t_value)

    def run(*args):
        # update the data
        msg = rospy.wait_for_message('graph_data', graph)
        y = msg.y_value
        t_new = rospy.get_time()

        print "Y = "+str(y)
        print "t_new = "+str(t_new)

        update_t(t_new)

        t = plt_graph_object.get_t_now()

        print "Y == "+str(y)
        print "T == "+str(t)

        if y < ACCEPTABLE_ERROR_LIMIT:
            ax.plot([t], [y], 'o', color='red')

        xdata.append(t)
        ydata.append(y)
        xmin, xmax = ax.get_xlim()
        if t >= xmax:
            new_xmin = xdata[1]
            new_xmax = xdata[len(xdata)-1] + T_STEPS
            ax.set_xlim(new_xmin, new_xmax)
            xdata.pop(0)
            ydata.pop(0)
            print "XMAX =="+str(new_xmax)
            print "XMIN =="+str(new_xmin)
            print "LEN XDATA ="+str(len(xdata))
            print "LEN XDATA ="+str(len(ydata))
            ax.figure.canvas.draw()

        line.set_data(xdata, ydata)

        return line,

    ani = animation.FuncAnimation(fig, run, blit=True, interval=INTERVAL_T, repeat=False)
    plt.title('Center Title')
    plt.show()


if __name__ == '__main__':
    plot_array()