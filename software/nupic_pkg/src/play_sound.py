#!/usr/bin/env python

import rospy
import wave
import pygame
import os
from random import shuffle
import subprocess
from tools_sound_files import standarize_list, join_wav_files_list

WAV_TEST = "/home/rdaneel/Kodama/Media/03a01Fa.wav"
SOUND_DIR = "/home/rdaneel/playground/sound_tests/sounds/"

def play_wav_till_end(file_path):

    f = wave.open(file_path,"rb")
    Ts = f.getframerate()
    channels = f.getnchannels()

    pygame.mixer.init(frequency=Ts, channels=channels, buffer=4096)

    # If you want more channels, change 8 to a desired number. 8 is the default number of channel
    pygame.mixer.set_num_channels(7)
    # This is the sound channel
    voice = pygame.mixer.Channel(2)
    sound = pygame.mixer.Sound(file_path)
    voice.play(sound)
    while voice.get_busy():
        None

    return True


def get_sound_list_dir(path_dir):
    from os import listdir
    from os.path import isfile, join

    onlyfiles = [ os.path.join(path_dir,f) for f in listdir(path_dir) if isfile(join(path_dir,f)) ]

    return onlyfiles

def play_sound_list(sound_list):

    for song in sound_list:
        while not play_wav_till_end(song):
            None

    print "List Ended"

if __name__ == "__main__":

    anger_sound_dir = "/media/rdaneel/Data/Kodama_Project_Files/Predict/Anger/"
    boredom_sound_dir = "/media/rdaneel/Data/Kodama_Project_Files/Predict/Boredom/"
    disgust_sound_dir = "/media/rdaneel/Data/Kodama_Project_Files/Predict/Disgust/"
    fear_sound_dir = "/media/rdaneel/Data/Kodama_Project_Files/Predict/Fear/"
    happiness_sound_dir = "/media/rdaneel/Data/Kodama_Project_Files/Predict/Happiness/"
    neutral_sound_dir = "/media/rdaneel/Data/Kodama_Project_Files/Predict/Neutral/"
    sad_sound_dir = "/media/rdaneel/Data/Kodama_Project_Files/Predict/Sadness/"
    surprise_sound_dir = "/media/rdaneel/Data/Kodama_Project_Files/Predict/Surprise/"

    anger_sound_dir_std = "/media/rdaneel/Data/Kodama_Project_Files/Predict/Anger_std/"
    boredom_sound_dir_std = "/media/rdaneel/Data/Kodama_Project_Files/Predict/Boredom_std/"
    disgust_sound_dir_std = "/media/rdaneel/Data/Kodama_Project_Files/Predict/Disgust_std/"
    fear_sound_dir_std = "/media/rdaneel/Data/Kodama_Project_Files/Predict/Fear_std/"
    happiness_sound_dir_std = "/media/rdaneel/Data/Kodama_Project_Files/Predict/Happiness_std/"
    neutral_sound_dir_std = "/media/rdaneel/Data/Kodama_Project_Files/Predict/Neutral_std/"
    sad_sound_dir_std = "/media/rdaneel/Data/Kodama_Project_Files/Predict/Sadness_std/"
    surprise_sound_dir_std = "/media/rdaneel/Data/Kodama_Project_Files/Predict/Surprise_std/"



    s = raw_input("Which Database play:Anger(a),Boredom(b),Disgust(d),Fear(f),Happiness(h),Neutral(n),Sadness(s),Surprise(x)")

    if s == "a":
        sound_dir = anger_sound_dir
        base_dir_out_files = anger_sound_dir_std
    elif s == "b":
        sound_dir = boredom_sound_dir
        base_dir_out_files = boredom_sound_dir_std
    elif s == "d":
        sound_dir = disgust_sound_dir
        base_dir_out_files = disgust_sound_dir_std
    elif s == "f":
        sound_dir = fear_sound_dir
        base_dir_out_files = fear_sound_dir_std
    elif s == "h":
        sound_dir = happiness_sound_dir
        base_dir_out_files = happiness_sound_dir_std
    elif s == "n":
        sound_dir = neutral_sound_dir
        base_dir_out_files = neutral_sound_dir_std
    elif s == "s":
        sound_dir = sad_sound_dir
        base_dir_out_files = sad_sound_dir_std
    elif s == "x":
        sound_dir = surprise_sound_dir
        base_dir_out_files = surprise_sound_dir_std
    else:
        sound_dir = None
        base_dir_out_files = None

    lst_songs = get_sound_list_dir(sound_dir)
    shuffle(lst_songs)
    print lst_songs

    result = raw_input("Select operation: play(p), standarize (s), join(j)")

    if result == "p":
        wav_file_to_play = raw_input("Wav file Path to play")
        play_sound_list(sound_list=[str(wav_file_to_play)])
    elif result == "s":
        out_wav_files_list = standarize_list(lst_songs, base_dir_out_files)
    elif result == "j":
        result_output_wav_base_path = "/media/rdaneel/Data/Kodama_Project_Files/Predict/JOIN_DIR/"
        join_number = raw_input("Join Number =")
        file_name = str(s)+"-"+str(join_number)+".wav"
        result_output_wav_path = os.path.join(result_output_wav_base_path, file_name)
        print "out_path =="+str(result_output_wav_path)
        join_wav_files_list(lst_songs, result_output_wav_path)
    else:
        print "Nothing to do..."

