#!/usr/bin/env python
import os
import rospy
from internal_sound_pkg.msg import internal_sound_sdr
from nupic_pkg.msg import graph
from nupic_pkg.msg import htm_output
import time
from play_wav import get_sound_list_dir, PlayWav
import copy
import numpy
from nupic.research.TP import TP
from sp_tools import print_connected_synapses
from nupic.research.spatial_pooler import SpatialPooler
from encoding_tools import create_4_encoded_animal_categories
import timeit
import pickle
import easygui
import rospkg
from show_variable_publisher import BlackboardVariablePublisher
from clustering_tools import generate_patern_sequence_correspondance
from nupic.encoders import ScalarEncoder

# Time that will be inputing sound data encoded into csv file
LEARNING_TIME = 60
#SOUND_DIR = "/home/rdaneel/playground/sound_tests/sounds/"
PICKLE_HTM = "pickle_files/htm.p"
PICKLE_HTM_BACKUP = "pickle_files/htm_backup.p"

RATE_INIT_PUBLISH = 2
INIT_TIME = 5
INIT_I = INIT_TIME*RATE_INIT_PUBLISH

HTM_NAME_LVL1 = 'htm_lvl-1'
HTM_NAME_LVL2 = 'htm_lvl-2'

HTM_SOUND_NAME_LVL1 = 'htm_sound_lvl-1'
HTM_SOUND_NAME_LVL2 = 'htm_sound_lvl-2'

HTM_SOUND_NAME_LVLX = "htm_sound_lvl-"

SOUND_DIR = "/home/rdaneel/playground/sound_tests/sounds/"
LEARNING_ERROR_ACCEPTED = 0.05

HTMBUCKET_DEFAULT_NAME = "htm_bucket_default"

def get_nupic_pkg_path_to_pickle_data():
    # get an instance of RosPack with the default search paths
    rospack = rospkg.RosPack()
    # get the file path for rospy_tutorials
    pkg_path = rospack.get_path('nupic_pkg')
    path_to_htm_pickle = os.path.join(pkg_path, "src/pickle_files/")
    return path_to_htm_pickle

class HTM(object):
    def __init__(self,number_of_things_to_learn, inputDimensionsSP, numberOfColsTP, errors_to_bare_in_mind, htmlvl_name):

        rospy.loginfo("Start HTM INIT")

        self.sp = SpatialPooler(inputDimensions=(inputDimensionsSP,),
                   columnDimensions=(number_of_things_to_learn,),
                   potentialRadius=15,
                   numActiveColumnsPerInhArea=1,
                   globalInhibition=True,
                   synPermActiveInc=0.03,
                   potentialPct=1.0)

        # Now we see how each column is conected initialy in the SP.
        # we use xrange just for future efficiency in big ranges. Here we could use range.
        #print_connected_synapses(self.sp, clear=True)

        # Step 1: create Temporal Pooler instance with appropriate parameters
        self.tp = TP(numberOfCols=numberOfColsTP, cellsPerColumn=4,
                initialPerm=0.5, connectedPerm=0.5,
                minThreshold=3, newSynapseCount=10,
                permanenceInc=0.1, permanenceDec=0.0,
                activationThreshold=1,
                globalDecay=0, burnIn=1,
                checkSynapseConsistency=False,
                pamLength=10)

        self.output = numpy.zeros((number_of_things_to_learn,), dtype="int")
        self.ouput_dict = {}
        self.next_key_name_number = 0
        self.vis = Visualizations()
        self.predictedIntPreviousTimestep = numpy.zeros((number_of_things_to_learn,), dtype="int")
        self.anomaly_list = [1]*errors_to_bare_in_mind
        # We make windows that are a 10% of the total error memory
        self.window = errors_to_bare_in_mind/10
        self.ema_error = []
        self.average_error = 1

        path_to_htm_pickle_dir = get_nupic_pkg_path_to_pickle_data()

        path_to_htm_pickle = os.path.join(path_to_htm_pickle_dir, str(htmlvl_name))
        self._pickle_htm_name = path_to_htm_pickle+".p"
        self._pickle_htm_name_backup = path_to_htm_pickle+"_backup.p"

        rospy.loginfo("PICKLE FILE ==>"+str(self._pickle_htm_name))
        rospy.loginfo("PICKLE FILE ==>"+str(self._pickle_htm_name_backup))

        self.SEPARATOR = "-"


        rospy.loginfo("End HTM INIT")

    def learning_algorith(self,element,key,debug=False):
        """

        :param element:SDR input data
        :param key: Identifier for the patern
        :return:
        """
        # First learn SP
        rospy.loginfo("###### START #######")
        rospy.loginfo("KEY INPUT TO SP =>"+str(key))
        rospy.loginfo("ELEMENT INPUT TO SP =>"+str(element))

        self.sp.compute(element, learn=True, activeArray=self.output)
        if debug:
            print_connected_synapses(self.sp, clear=False)

        rospy.loginfo("DATA")
        found = False
        for value in self.ouput_dict.values():
            rospy.loginfo("OUTPUT SP =>"+str(self.output))
            rospy.loginfo("VALUE DICT==>"+str(value))

            c1 = (str(self.output) == str(value))
            rospy.loginfo("ARE THEY THE SAME? = "+str(c1))
            if c1:
                rospy.loginfo("This group is already in outputdict.")
                found = True
                break

        if not found:
            rospy.loginfo("NEW group")
            key_new = "G-"+str(self.next_key_name_number)
            self.ouput_dict[key_new] = copy.deepcopy(self.output)
            self.next_key_name_number += 1

        # The compute method performs one step of learning and/or inference. Note:
        # here we just perform learning but you can perform prediction/inference and
        # learning in the same step if you want (online learning).
        #self.tp.compute(self.output, enableLearn=True, computeInfOutput=False)
        self.tp.compute(self.output, enableLearn=True, computeInfOutput=True)


        actualInt = self.output
        predictedInt = self.tp.getPredictedState().max(axis=1)

        ema, av_err = self.error_calculations(actualInt, predictedInt)
        rospy.loginfo("EMA ==>"+str(ema))
        rospy.loginfo("AV ERROR =="+str(av_err))

        # This function prints the segments associated with every cell.$$$$
        # If you really want to understand the TP, uncomment this line. By following
        # every step you can get an excellent understanding for exactly how the TP
        # learns.
        if debug:
            self.tp.printCells()


    def error_calculations(self, actualInt, predictedInt):
        rospy.logdebug("ACTUAL INT ==>"+str(actualInt))
        rospy.logdebug("PREDICTED INT ==>"+str(predictedInt))

        compare = self.vis.compareArray(actualInt, self.predictedIntPreviousTimestep)
        anomaly = self.vis.calcAnomaly(actualInt, self.predictedIntPreviousTimestep)

        rospy.logdebug("ANOMALY ==>"+str(anomaly))
        rospy.logdebug("." . join(compare))
        rospy.logdebug(self.vis.hashtagAnomaly(anomaly))

        self.anomaly_list.pop(0)
        self.anomaly_list.append(anomaly)

        rospy.logdebug("ANOMALY LIST ==>"+str(self.anomaly_list))

        self.ema_error = self.ExpMovingAverage(values=self.anomaly_list, window=self.window)
        self.average_error = self.Average(self.ema_error)

        # We save it for the next one
        self.predictedIntPreviousTimestep = predictedInt

        return self.ema_error, self.average_error


    def learning_algorith_withoutsp(self,element):
        """

        :param element:SDR input data
        :param key: Identifier for the patern
        :return:
        """

        # The compute method performs one step of learning and/or inference. Note:
        # here we just perform learning but you can perform prediction/inference and
        # learning in the same step if you want (online learning).
        self.tp.compute(element, enableLearn=True, computeInfOutput=False)

        # This function prints the segments associated with every cell.$$$$
        # If you really want to understand the TP, uncomment this line. By following
        # every step you can get an excellent understanding for exactly how the TP
        # learns.
        #self.tp.printCells()

    def reset_tp(self):
        self.tp.reset()

    def print_ouput_dict(self):
        print self.ouput_dict
        print "Length =="+str(len(self.ouput_dict))
        print self.ouput_dict.keys()

    # Utility routine for printing the input vector
    def formatRow(self, x):
        s = ''
        for c in range(len(x)):
            if c > 0 and c % 10 == 0:
                s += ' '
            s += str(x[c])
        s += ' '
        return s

    def predict_test(self):
        for key, element in self.ouput_dict.items():
            print "\n\n--------"+str(key)+"-----------"
            print "Raw input vector\n", element


            # Send each vector to the TP, with learning turned off
            self.tp.compute(element, enableLearn=False, computeInfOutput=True)

            # This method prints out the active state of each cell followed by the
            # predicted state of each cell. For convenience the cells are grouped
            # 10 at a time. When there are multiple cells per column the printout
            # is arranged so the cells in a column are stacked together
            #
            # What you should notice is that the columns where active state is 1
            # represent the SDR for the current input pattern and the columns where
            # predicted state is 1 represent the SDR for the next expected pattern
            print "\nAll the active and predicted cells:"
            self.tp.printStates(printPrevious=False, printLearnState=False)

            # tp.getPredictedState() gets the predicted cells.
            # predictedCells[c][i] represents the state of the i'th cell in the c'th
            # column. To see if a column is predicted, we can simply take the OR
            # across all the cells in that column. In numpy we can do this by taking
            # the max along axis 1.
            print "\n\nThe following columns are predicted by the temporal pooler. This"
            print "should correspond to columns in the *next* item in the sequence."
            predictedCells = self.tp.getPredictedState()
            print self.formatRow(predictedCells.max(axis=1).nonzero())

    def predict(self,data):

        rospy.logdebug("DATA=="+str(data))
        self.sp.compute(data, learn=True, activeArray=self.output)

        rospy.logdebug("SP OUTPUT =="+str(self.output))
        # Send each vector to the TP, with learning turned off
        self.tp.compute(self.output, enableLearn=False, computeInfOutput=True)

        # This method prints out the active state of each cell followed by the
        # predicted state of each cell. For convenience the cells are grouped
        # 10 at a time. When there are multiple cells per column the printout
        # is arranged so the cells in a column are stacked together
        #
        # What you should notice is that the columns where active state is 1
        # represent the SDR for the current input pattern and the columns where
        # predicted state is 1 represent the SDR for the next expected pattern
        rospy.logdebug("\nAll the active and predicted cells:")
        self.tp.printStates(printPrevious=False, printLearnState=False)

        # tp.getPredictedState() gets the predicted cells.
        # predictedCells[c][i] represents the state of the i'th cell in the c'th
        # column. To see if a column is predicted, we can simply take the OR
        # across all the cells in that column. In numpy we can do this by taking
        # the max along axis 1.
        rospy.logdebug("\n\nThe following columns are predicted by the temporal pooler. This")
        rospy.logdebug("should correspond to columns in the *next* item in the sequence.")
        predictedCells = self.tp.getPredictedState()
        result_predicted = predictedCells.max(axis=1)
        predicted_group_index = self.formatRow(result_predicted.nonzero())
        rospy.loginfo("PREDICTED CELLS MAX AXIS 1=="+str(result_predicted))
        rospy.logdebug("PREDICTED CELLS ==>"+str(predictedCells))

        return self.output, result_predicted


    def predict_cluster(self, data):

        rospy.logdebug("DATA=="+str(data))
        self.sp.compute(data, learn=True, activeArray=self.output)

        rospy.logdebug("SP OUTPUT =="+str(self.output))
        # Send each vector to the TP, with learning turned off
        self.tp.compute(self.output, enableLearn=False, computeInfOutput=True)

        # This method prints out the active state of each cell followed by the
        # predicted state of each cell. For convenience the cells are grouped
        # 10 at a time. When there are multiple cells per column the printout
        # is arranged so the cells in a column are stacked together
        #
        # What you should notice is that the columns where active state is 1
        # represent the SDR for the current input pattern and the columns where
        # predicted state is 1 represent the SDR for the next expected pattern
        rospy.logdebug("\nAll the active and predicted cells:")
        self.tp.printStates(printPrevious=False, printLearnState=False)

        # tp.getPredictedState() gets the predicted cells.
        # predictedCells[c][i] represents the state of the i'th cell in the c'th
        # column. To see if a column is predicted, we can simply take the OR
        # across all the cells in that column. In numpy we can do this by taking
        # the max along axis 1.
        rospy.logdebug("\n\nThe following columns are predicted by the temporal pooler. This")
        rospy.logdebug("should correspond to columns in the *next* item in the sequence.")
        predictedCells = self.tp.getPredictedState()
        result_predicted = predictedCells.max(axis=1)
        predicted_group_index = self.formatRow(result_predicted.nonzero())
        rospy.loginfo("PREDICTED CELLS MAX AXIS 1=="+str(result_predicted))
        rospy.logdebug("PREDICTED CELLS ==>"+str(predictedCells))

        # The only case in which this active state could be various is when its bursting.
        col_cell_id_of_input = self.extract_column_cell_id_activeState(self.tp)
        predicted_col_cell_id_list = self.extract_column_cell_id_predictedState(self.tp)

        return self.output, result_predicted, col_cell_id_of_input, predicted_col_cell_id_list


    def extract_column_cell_id_activeState(self, tp):
        """
        Returns the column-cell iD of the activeState
        :param tp:
        :param element:
        :return:
        """
        col_cell_id = None

        print "\nInference Active state"
        var = tp.infActiveState['t']
        found = False
        for i in xrange(tp.cellsPerColumn):
            for c in range(tp.numberOfCols):
                if int(var[c, i]) == 1:
                    col_cell_id = self.convert_cell_name(c,i)
                    found = True
                if found:
                    break
            if found:
                break

        return col_cell_id


    def extract_column_cell_id_predictedState(self, tp):
        """
        Returns the column-cell iD of the predictedStates
        IMPORTANT: it can be more than ONE Ids given due to the possibility of
        predicting severan outcomes
        :param tp:
        :param element:
        :return:
        """
        predicted_col_cell_id_list = []

        print "\nInference Active state"
        var = tp.getPredictedState()
        found = False
        for i in xrange(tp.cellsPerColumn):
            for c in range(tp.numberOfCols):
                if int(var[c, i]) == 1:
                    col_cell_id = self.convert_cell_name(c,i)
                    predicted_col_cell_id_list.append(col_cell_id)

        return predicted_col_cell_id_list

    def convert_cell_name(self, c, i):
        return str(c)+self.SEPARATOR+str(i)


    def movingaverage(self,values,window):
        weigths = numpy.repeat(1.0, window)/window
        smas = numpy.convolve(values, weigths, 'valid')
        return smas # as a numpy array


    def ExpMovingAverage(self,values, window):
        """
        http://pythonprogramming.net/advanced-matplotlib-graphing-charting-tutorial/
        """
        weights = numpy.exp(numpy.linspace(-1., 0., window))
        weights /= weights.sum()
        a =  numpy.convolve(values, weights, mode='full')[:len(values)]
        a[:window] = a[window]
        return a

    def Average(self, values):
        return numpy.mean(values)


    def save_htm(self):
        pickle.dump(self, open(self._pickle_htm_name, "wb"))
        rospy.loginfo("HTM File Saved="+str(self._pickle_htm_name))

    def save_htm_backup(self):
        pickle.dump(self, open(self._pickle_htm_name_backup, "wb"))
        rospy.loginfo("HTM File Saved="+str(self._pickle_htm_name_backup))

    def get_pickle_path(self):
        return self._pickle_htm_name

    def get_pickle_path_backup(self):
        return self._pickle_htm_name_backup


class Visualizations:

  def calcAnomaly(self, actual, predicted):
    """
    Calculates the anomaly of two SDRs

    Uses the equation presented on the wiki:
    https://github.com/numenta/nupic/wiki/Anomaly-Score-Memo

    To put this in terms of the temporal pooler:
      A is the actual input array at a given timestep
      P is the predicted array that was produced from the previous timestep(s)
      [A - (A && P)] / [A]
    Rephrasing as questions:
      What bits are on in A that are not on in P?
      How does that compare to total on bits in A?

    Outputs 0 is there's no difference between P and A.
    Outputs 1 if P and A are totally distinct.

    Not a perfect metric - it doesn't credit proximity
    Next step: combine with a metric for a spatial pooler
    """
    combined = numpy.logical_and(actual, predicted)
    delta = numpy.logical_xor(actual,combined)
    delta_score = sum(delta)
    actual_score = float(sum(actual))
    return delta_score / actual_score


  def compareArray(self, actual, predicted):
    """
    Produce an array that compares the actual & predicted

    'A' - actual
    'P' - predicted
    'E' - expected (both actual & predicted
    ' ' - neither an input nor predicted
    """
    compare = []
    for i in range(actual.size):
      if actual[i] and predicted[i]:
        compare.append('E')
      elif actual[i]:
        compare.append('A')
      elif predicted[i]:
        compare.append('P')
      else:
        compare.append(' ')
    return compare


  def hashtagAnomaly(self, anomaly):
    """
    Basic printout method to visualize the anomaly score (scale: 1 - 50 #'s)
    """
    hashcount = '#'
    for i in range(int(anomaly / 0.02)):
      hashcount += '#'
    for j in range(int((1 - anomaly) / 0.02)):
      hashcount += '.'
    return hashcount


class LearningStatus(object):
    def __init__(self, num_levels, learning_time):
        self._learning_status = [False]*num_levels
        self._lvl_learning_now = 1
        self._reset_learning_tp = False
        self._start_learning_time = time.time()
        self._start_learning_time_lvl_list = [None]*num_levels
        self._start_learning_time_lvl_list[0] = self._start_learning_time
        self._learning_time = learning_time
        self._all_lvl_learned = False
        self._average_error_list = [1]*num_levels

    def update_learning_lvl_status(self, lvl, value):
        if lvl < 1:
            lvl = 1
        if lvl > len(self._learning_status):
            lvl = len(self._learning_status)

        self._learning_status[lvl-1] = bool(value)
        self.update_which_lvl_is_learning()

    def get_lvl_status(self, lvl):
        if lvl < 1:
            lvl = 1
        if lvl > len(self._learning_status):
            lvl = len(self._learning_status)

        return self._learning_status[lvl-1]

    def all_status(self):
        return self._learning_status

    def toggle_reset_learning_tp(self):
        self._reset_learning_tp = not self._reset_learning_tp

    def get_reset_learning_tp(self):
        return self._reset_learning_tp

    def end_learning_time(self):
        return time.time()-self._start_learning_time > self._learning_time

    def get_learning_time(self):
        return time.time()-self._start_learning_time

    def update_which_lvl_is_learning(self):
        i = 1
        for has_learned in self._learning_status:
            if has_learned:
                i += 1
            else:
                break
        self._lvl_learning_now = i
        print "Current Learning Level = "+str(self._lvl_learning_now)
        self.update_if_all_lvls_learned()

    def update_if_all_lvls_learned(self):
        self._all_lvl_learned = self._lvl_learning_now > len(self._learning_status)
        print "All lvls have learned? ==> "+str(self._all_lvl_learned)

    def get_if_all_lvls_learned(self):
        return self._all_lvl_learned

    def get_which_lvl_is_learning(self):
        return self._lvl_learning_now

    def restart_learning_chrono(self):
        self._start_learning_time = time.time()

    def update_learning_status_based_on_time(self):
        if self.end_learning_time():
            # If the time has finished
            i = self.get_which_lvl_is_learning()
            self.update_learning_lvl_status(i, True)
            self.restart_learning_chrono()

    def update_lvl_average_error(self, lvl, error_value):
        if lvl > 0 and lvl <= len(self._average_error_list):
            self._average_error_list[lvl-1] = error_value
        else:
            assert False, "lvl has to be between 1 and the number of levels"

    def get_lvl_average_error(self, lvl):
        return self._average_error_list[lvl-1]

    def update_all_average_errors(self, error_values_list):
        self._average_error_list = error_values_list

    def get_all_average_errors(self):
        return self._average_error_list


class HTMMacroObject():
    def __init__(self, num_levels, load_htm_from_files, number_of_things_to_learn_lvx_list, errors_to_bare_in_mind, length_input_encoded_array):
        self._num_levels = num_levels
        self._errors_to_bare_in_mind = errors_to_bare_in_mind
        self._length_input_encoded_array = length_input_encoded_array
        self._number_of_things_to_learn_lvx_list = number_of_things_to_learn_lvx_list
        self._htm_object_lvlx_list = []
        self._htm_name_lvlx_list = self.fill_htm_name_list()
        self._number_of_things_to_learn_dict = self.create_number_thing_to_learn_by_lvl()
        self.patern_sequence_correspondance_dict = {}

        if load_htm_from_files:
            rospy.loginfo("Loading from PIckled Files...")
            self.load_htm_pickled_files()
        else:
            rospy.loginfo("Creating New Htm objects...")
            self.create_new_htm_objects()

    def get_num_levels(self):
        return self._num_levels

    def load_htm_pickled_files(self):
        rospy.loginfo("Retrieving Memory HTM...")
        for lvl_name in self._htm_name_lvlx_list:
            self._htm_object_lvlx_list.append(get_saved_htm(lvl_name))

    def fill_htm_name_list(self):
        htm_name_lvlx_list = []
        for i in range(self._num_levels):
            name = HTM_SOUND_NAME_LVLX+str(i+1)
            htm_name_lvlx_list.append(name)
        return htm_name_lvlx_list

    def create_number_thing_to_learn_by_lvl(self):
        print self._htm_name_lvlx_list
        print self._number_of_things_to_learn_lvx_list
        return dict(zip(self._htm_name_lvlx_list, self._number_of_things_to_learn_lvx_list))


    def create_new_htm_objects(self):
        # We erase first the existing htm files
        rospy.loginfo("Erasing Memory HTM...")
        for lvl_name in self._htm_name_lvlx_list:
            clean_saved_htm(htm_lvl_name=lvl_name)

        for lvl_name in self._htm_name_lvlx_list:
            rospy.loginfo("Started Creating HTMs...")
            number_things_to_learn_lvlx = self._number_of_things_to_learn_dict.get(lvl_name)
            print "NUMBER OF THINGS TO LEARN ==> "+str(number_things_to_learn_lvlx)
            length_input_encoded_array = self._length_input_encoded_array
            errors_to_bare_in_mind = self._errors_to_bare_in_mind

            rospy.loginfo("Num things to Learn = "+str(number_things_to_learn_lvlx))
            rospy.loginfo("inputDimensionsSP = "+str(length_input_encoded_array))
            rospy.loginfo("numberOfColsTP = "+str(number_things_to_learn_lvlx))
            rospy.loginfo("lvl_name = "+str(lvl_name))

            htm_object_lvlx = HTM(number_of_things_to_learn=number_things_to_learn_lvlx,
                                  inputDimensionsSP=length_input_encoded_array,
                                  numberOfColsTP=number_things_to_learn_lvlx,
                                  errors_to_bare_in_mind=errors_to_bare_in_mind,
                                  htmlvl_name=lvl_name)

            self._htm_object_lvlx_list.append(htm_object_lvlx)

            # Now we have to set the next level input with the same dimension as the output of previous lvl.
            self._length_input_encoded_array = number_things_to_learn_lvlx

    def save_htm_lvls(self):
        rospy.loginfo("Saving Memory HTM...")
        for htm_object in self._htm_object_lvlx_list:
            htm_object.save_htm()

    def reset_tp_lvl(self, lvl):
        rospy.loginfo("@@@@ LEARNING LV RESET @@@@")
        self._htm_object_lvlx_list[lvl-1].reset_tp()

    def learn(self, lvl, element, debug):
        rospy.loginfo("@@@@ LEARNING LV "+str(lvl)+"@@@@")
        key = "LVL-"+str(lvl)+"-"+str(time.time())
        self._htm_object_lvlx_list[lvl-1].learning_algorith(element=element, key=key, debug=debug)
        return self._htm_object_lvlx_list[lvl-1].average_error

    def predict_lvl(self, lvl, input_data):
        sp_output = None
        predicted_data = None
        col_cell_id_of_input = None
        if lvl > 0:
            print "INPUT LV "+str(lvl)+" =="+str(input_data)
            sp_output, predicted_data = self._htm_object_lvlx_list[lvl-1].predict(input_data)
            print "OUTPUT LV "+str(lvl)+" =="+str(sp_output)

        return sp_output, predicted_data

    def predict_lvl_cluster(self, lvl, input_data):
        sp_output = None
        predicted_data = None
        col_cell_id_of_input = None
        if lvl > 0:
            print "INPUT LV "+str(lvl)+" =="+str(input_data)

            sp_output, predicted_data, col_cell_id_of_input, predicted_col_cell_id_list = self._htm_object_lvlx_list[lvl-1].predict_cluster(input_data)
            print "sp_output LV "+str(lvl)+" =="+str(sp_output)
            print "predicted_data "+str(predicted_data)
            print "col_cell_id_of_input =="+str(col_cell_id_of_input)
            print "predicted_col_cell_id_list "+str(predicted_col_cell_id_list)

            output_sequence = self.get_corresponding_ouput_sequence(lvl, col_cell_id_of_input)
            print "OUTPUT SEQUENCE ==>"+str(output_sequence)
            sdr_output_sequence = self.get_sdr_output_sequence(lvl, output_sequence)
            print "SDR OUTPUT SEQUENCE ==>"+str(sdr_output_sequence)

        return sp_output, predicted_data, col_cell_id_of_input

    def predict_till_lvl(self, lvl, input_data):
        # Predicts till one level before the designated level
        i = 1
        sp_output = None
        predicted_data = None
        if i < lvl:
            for htm_object in self._htm_object_lvlx_list:
                if i < lvl:
                    print "INPUT LV "+str(i)+" =="+str(input_data)
                    sp_output, predicted_data = htm_object.predict(input_data)
                    print "OUTPUT LV "+str(i)+" =="+str(sp_output)
                    i += 1
                    input_data = copy.deepcopy(sp_output)
                else:
                    print "Ended prediction levels..."
                    break
            return sp_output, predicted_data
        else:
            return input_data, predicted_data


    def get_lvlx_tp(self, lvl):
        """
        Returns the tp of the corresponding lvl
        :return:
        """
        return self._htm_object_lvlx_list[lvl-1].tp

    def clusterize(self, lvl, n_clusters):
        # We clusterise the htm lvl
        print "Clusterising..."

        lvlx_tp = self.get_lvlx_tp(lvl)
        pattern_seq_lvlx = generate_patern_sequence_correspondance(lvlx_tp, n_clusters=n_clusters)
        self.patern_sequence_correspondance_dict.update({str(lvl): pattern_seq_lvlx})

        print "PATTERN CORRESPONDENCE DICT => "+str(self.patern_sequence_correspondance_dict)

    def get_corresponding_ouput_sequence(self, lvl, col_cell_id_of_input):
        """
        Returns the sequence that the col_cell_id corresponds to based on previous clusterization.
        :param col_cell_id_of_input:
        :return:
        """
        return self.patern_sequence_correspondance_dict.get(lvl).get(col_cell_id_of_input)

    def get_sdr_output_sequence(self, lvl, output_sequence):
        """
        Encondes the output integer sequence number to a SDR
        :return:
        """

        lvlx_num_clusters = self.get_lvlx_num_clusters(lvl)

        enconder_max_val = lvlx_num_clusters-1
        enc = ScalarEncoder(n=lvlx_num_clusters, w=1, minval=0, maxval=enconder_max_val, clipInput=True, forced=True)
        sdr_output_sequence = enc.encode(int(output_sequence))
        print str(output_sequence)+" ===> "+ str(sdr_output_sequence)
        return sdr_output_sequence

    def get_lvlx_num_clusters(self, lvl):
        """
        Get the number of clusters available in the given htm lvl.
        :param lvl:
        :return:
        """
        return None


class HTMMacroObjectClusters():
    def __init__(self, num_levels, number_of_things_to_learn_lvx_list, number_of_sequences_to_learn_lvx_list, errors_to_bare_in_mind, length_input_encoded_array, htm_bucket_name):
        self._num_levels = num_levels
        self._errors_to_bare_in_mind = errors_to_bare_in_mind
        self._length_input_encoded_array = length_input_encoded_array
        self._number_of_things_to_learn_lvx_list = number_of_things_to_learn_lvx_list
        self._htm_object_lvlx_list = []
        self._htm_name_lvlx_list = self.fill_htm_name_list()
        self._number_of_things_to_learn_dict = self.create_number_thing_to_learn_by_lvl()
        self.patern_sequence_correspondance_dict = {}

        self._number_of_sequences_to_learn_lvx_list = number_of_sequences_to_learn_lvx_list
        self._number_of_sequences_to_learn_dict = self.create_number_sequences_to_learn_by_lvl()

        rospy.loginfo("Checking input output dimensions...")
        self.check_input_output_dimensions()

        self._sequence_encoders_dict = {}
        self.generate_sequence_encoders()

        self._htm_bucket_name = htm_bucket_name
        self._path_to_pickle = get_nupic_pkg_path_to_pickle_data()
        self._htm_bucket_file_path = os.path.join(self._path_to_pickle, str(self._htm_bucket_name)+".p")

        rospy.loginfo("Creating New Htm objects...")
        self.create_new_htm_objects()


    def check_input_output_dimensions(self):
        """
        Checks if the output_lvl == input_lvl+1
        :return:
        """
        msg = "Uncoeherent lengths"
        assert len(self._number_of_things_to_learn_lvx_list) == len(self._number_of_sequences_to_learn_lvx_list), msg
        for i in range(0, len(self._number_of_things_to_learn_lvx_list)):
            p = i+1
            if p < len(self._number_of_things_to_learn_lvx_list):
                msg = "Input of lvl+1 has to be == to output_sequence lvl"
                assert self._number_of_things_to_learn_lvx_list[p] == self._number_of_sequences_to_learn_lvx_list[i], msg

        rospy.loginfo("Correct input_output dimensions")

    def get_number_of_sequence_to_learn_list(self):
        return self._number_of_sequences_to_learn_lvx_list

    def get_bucket_name(self):
        return self._htm_bucket_name

    def get_number_thing_to_learn_dict(self):
        return self._number_of_things_to_learn_dict

    def generate_sequence_encoders(self):
        """
        Genereates all the encoders needed for each level with the corresponding bits and max levels
        :return:
        """
        for idx, number_clusters in enumerate(self._number_of_sequences_to_learn_lvx_list):
            enc = ScalarEncoder(n=number_clusters, w=1, minval=0, maxval=number_clusters-1, clipInput=True, forced=True)
            lvl = str(idx+1)
            self._sequence_encoders_dict.update({lvl: enc})

    def enconde_seq_lvl(self, lvl, seq_scalar):
        """
        Encodes the given seq_scalar ofr the lvl into an SDR.
        :param lvl:
        :param seq_scalar:
        :return:
        """
        return self._sequence_encoders_dict.get(str(lvl)).encode(int(seq_scalar))

    def get_num_clusters_for_lvlx(self, lvl):
        return self._number_of_sequences_to_learn_lvx_list[lvl-1]

    def get_num_levels(self):
        return self._num_levels

    def fill_htm_name_list(self):
        htm_name_lvlx_list = []
        for i in range(self._num_levels):
            name = HTM_SOUND_NAME_LVLX+str(i+1)
            htm_name_lvlx_list.append(name)
        return htm_name_lvlx_list

    def create_number_thing_to_learn_by_lvl(self):
        print self._htm_name_lvlx_list
        print self._number_of_things_to_learn_lvx_list
        return dict(zip(self._htm_name_lvlx_list, self._number_of_things_to_learn_lvx_list))

    def create_number_sequences_to_learn_by_lvl(self):
        print self._htm_name_lvlx_list
        print self._number_of_sequences_to_learn_lvx_list
        return dict(zip(self._htm_name_lvlx_list, self._number_of_sequences_to_learn_lvx_list))


    def create_new_htm_objects(self):

        for lvl_name in self._htm_name_lvlx_list:
            rospy.loginfo("Started Creating HTMs...")
            number_things_to_learn_lvlx = self._number_of_things_to_learn_dict.get(lvl_name)
            print "NUMBER OF THINGS TO LEARN ==> "+str(number_things_to_learn_lvlx)
            length_input_encoded_array = self._length_input_encoded_array
            errors_to_bare_in_mind = self._errors_to_bare_in_mind

            rospy.loginfo("Num things to Learn = "+str(number_things_to_learn_lvlx))
            rospy.loginfo("inputDimensionsSP = "+str(length_input_encoded_array))
            rospy.loginfo("numberOfColsTP = "+str(number_things_to_learn_lvlx))
            rospy.loginfo("lvl_name = "+str(lvl_name))

            htm_object_lvlx = HTM(number_of_things_to_learn=number_things_to_learn_lvlx,
                                  inputDimensionsSP=length_input_encoded_array,
                                  numberOfColsTP=number_things_to_learn_lvlx,
                                  errors_to_bare_in_mind=errors_to_bare_in_mind,
                                  htmlvl_name=lvl_name)

            self._htm_object_lvlx_list.append(htm_object_lvlx)

            # Now we have to set the next level input with the same dimension as the output of previous lvl.
            # TODO: here is the error, SP dimension in levels > 1 have the wrong dimension.
            output_dimension_lvlx = self._number_of_sequences_to_learn_dict.get(lvl_name)
            self._length_input_encoded_array = output_dimension_lvlx
            #self._length_input_encoded_array = number_things_to_learn_lvlx

    def reset_tp_lvl(self, lvl):
        rospy.loginfo("@@@@ LEARNING LV RESET @@@@")
        self._htm_object_lvlx_list[lvl-1].reset_tp()

    def learn(self, lvl, element, debug):
        rospy.loginfo("@@@@ LEARNING LV "+str(lvl)+"@@@@")
        key = "LVL-"+str(lvl)+"-"+str(time.time())
        self._htm_object_lvlx_list[lvl-1].learning_algorith(element=element, key=key, debug=debug)
        return self._htm_object_lvlx_list[lvl-1].average_error

    def predict_lvl(self, lvl, input_data):
        sp_output = None
        predicted_data = None
        col_cell_id_of_input = None
        if lvl > 0:
            print "INPUT LV "+str(lvl)+" =="+str(input_data)
            sp_output, predicted_data = self._htm_object_lvlx_list[lvl-1].predict(input_data)
            print "OUTPUT LV "+str(lvl)+" =="+str(sp_output)

        return sp_output, predicted_data


    def predict_lvl_cluster(self, lvl, input_data):
        sdr_output_sequence = None
        sdr_predicted_sequence = None
        current_sequence_label = None
        if lvl > 0:
            print "SP INPUT LV "+str(lvl)+" =="+str(input_data)
            sp_output, predicted_data, col_cell_id_of_input, predicted_col_cell_list = self._htm_object_lvlx_list[lvl-1].predict_cluster(input_data)
            print "SP OUTPUT LV "+str(lvl)+" =="+str(sp_output)
            print "Predicted DATA LV "+str(predicted_data)
            print "OUTPUT COL CELL ID of INPUT "+str(col_cell_id_of_input)
            print "OUTPUT COL CELL ID of PREDICTED LIST "+str(predicted_col_cell_list)

            # Current patern sequence belongs to
            sdr_output_sequence, output_sequence_list = self.get_sdr_from_col_id(lvl, [col_cell_id_of_input])
            if len(output_sequence_list) == 1:
                # There should be only ONE sequence for current patern
                current_sequence_label = str(output_sequence_list[0])
            print "SDR CURRENT SEQUENCE ==>"+str(sdr_output_sequence)

            # Predicted next sequence
            sdr_predicted_sequence, output_predicted_sequence_list = self.get_sdr_from_col_id(lvl, predicted_col_cell_list)
            print "SDR PREDICTED SEQUENCE ==>"+str(sdr_predicted_sequence)

        return sdr_output_sequence, sdr_predicted_sequence, current_sequence_label


    def predict_till_lvl_cluster(self, lvl, input_data):
        # Predicts till one level before the designated level
        i = 1
        sdr_output_sequence = None
        sdr_predicted_sequence = None
        # If lvl is 1 or less, makes no sense make the prediction.
        if i < lvl:
            for htm_object in self._htm_object_lvlx_list:
                if i < lvl:
                    print "INPUT LV "+str(i)+" =="+str(input_data)
                    sp_output, predicted_data, col_cell_id_of_input, predicted_col_cell_list = htm_object.predict_cluster(input_data)
                    print "col_cell_id_of_input LV "+str(i)+" =="+str(col_cell_id_of_input)
                    # TODO: Error in someplace around here
                    sdr_output_sequence, output_sequence_list = self.get_sdr_from_col_id(i, [col_cell_id_of_input])
                    sdr_predicted_sequence, predicted_sequence_list = self.get_sdr_from_col_id(i, predicted_col_cell_list)

                    i += 1
                    input_data = copy.deepcopy(sdr_output_sequence)
                else:
                    print "Ended prediction levels..."
                    break
            return sdr_output_sequence, sdr_predicted_sequence
        else:
            return input_data, sdr_predicted_sequence

    def predict_till_lvl(self, lvl, input_data):
        # Predicts till one level before the designated level
        i = 1
        sp_output = None
        predicted_data = None
        if i < lvl:
            for htm_object in self._htm_object_lvlx_list:
                if i < lvl:
                    print "INPUT LV "+str(i)+" =="+str(input_data)
                    sp_output, predicted_data = htm_object.predict(input_data)
                    print "OUTPUT LV "+str(i)+" =="+str(sp_output)
                    i += 1
                    input_data = copy.deepcopy(sp_output)
                else:
                    print "Ended prediction levels..."
                    break
            return sp_output, predicted_data
        else:
            return input_data, predicted_data


    def get_lvlx_tp(self, lvl):
        """
        Returns the tp of the corresponding lvl
        :return:
        """
        return self._htm_object_lvlx_list[lvl-1].tp

    def clusterize(self, lvl):
        # We clusterise the htm lvl
        print "Clusterising...lvl =="+str(lvl)
        n_clusters = self.get_num_clusters_for_lvlx(lvl=lvl)
        lvlx_tp = self.get_lvlx_tp(lvl)

        print "n_clusters =="+str(n_clusters)
        print "lvlx_tp =="+str(lvlx_tp)


        pattern_seq_lvlx = generate_patern_sequence_correspondance(lvlx_tp, n_clusters=n_clusters)

        print "pattern_seq_lvlx =="+str(pattern_seq_lvlx)

        self.patern_sequence_correspondance_dict.update({str(lvl): pattern_seq_lvlx})

        print "PATTERN CORRESPONDENCE DICT => "+str(self.patern_sequence_correspondance_dict)

    def get_corresponding_ouput_sequence(self, lvl, col_cell_id_of_input_list):
        """
        Returns the sequence that the col_cell_id corresponds to based on previous clusterization.
        If the given col_cell_id has no sequence asigned, give warning message that we are having paterns
        that havent been learnt.
        :param col_cell_id_of_input:
        :return:
        """
        sequence_list = []

        for element in col_cell_id_of_input_list:
            sequence = self.patern_sequence_correspondance_dict.get(str(lvl)).get(str(element))
            if sequence is not None:
                # We add only if there is value
                sequence_list.append(sequence)

        print "RESULT Sequence list =="+str(sequence_list)
        if not sequence_list:
            rospy.logwarn("Input Pattern =="+str(col_cell_id_of_input_list)+", has no sequence because it wasnt learned.")
            rospy.logwarn("Patern Correspondance of lvl="+str(lvl)+", "+str(self.patern_sequence_correspondance_dict.get(str(lvl))))
        return sequence_list

    def get_sdr_output_sequence(self, lvl, output_sequence_list):
        """
        Encondes the output integer sequence number to a SDR
        :return:
        """

        number_sequences_for_lvl = self._number_of_sequences_to_learn_lvx_list[lvl-1]

        # We creata a void container size the number of bits that encode the encoder of the lvl.
        final_sdr_output_sequence = numpy.zeros(number_sequences_for_lvl)

        if output_sequence_list:
            print "output_sequence_list === "+str(output_sequence_list)
            for output_sequence in output_sequence_list:
                sdr_output_sequence = self.enconde_seq_lvl(lvl=lvl, seq_scalar=output_sequence)
                final_sdr_output_sequence += sdr_output_sequence

            # We turn it intro binary
            print " final_sdr_output_sequence ==> "+ str(final_sdr_output_sequence)
            sdr_result_binary = (final_sdr_output_sequence>0).astype(int)
            print " sdr_result_binary ==> " + str(sdr_result_binary)

            return sdr_result_binary
        else:
            # Id no sequence given, output a zero sdr, which means not learned pattern with sequence detected.
            rospy.logwarn("Void input sequence...")
            return final_sdr_output_sequence

    def get_sdr_from_col_id(self, lvl, col_cell_id_of_input_list):
        """
        Given a col cell id list returns the sdr that encodes all the sequences that those patterns belong toguether
        :param lvl:
        :param col_cell_id_of_input_list:
        :return:
        """
        output_sequence_list = self.get_corresponding_ouput_sequence(lvl, col_cell_id_of_input_list)
        print "SEQUENCE ==>"+str(output_sequence_list)
        sdr_sequence = self.get_sdr_output_sequence(lvl, output_sequence_list)
        print "SEQUENCE SDR ==>"+str(sdr_sequence)

        return sdr_sequence, output_sequence_list


class HtmOutputMessages(object):
    def __init__(self):
        self._msg_lvl1 = htm_output()
        self._msg_lvl2 = htm_output()
        self._pub_lvl1 = rospy.Publisher("/htm_output_lvl1", htm_output, queue_size=1)
        self._pub_lvl2 = rospy.Publisher("/htm_output_lvl2", htm_output, queue_size=1)

    def update_htm_msg(self, lvl, sp_output, tp_output):
        if lvl == 1:
            self._msg_lvl1.sp_output_array = sp_output.astype(bool)
            self._msg_lvl1.tp_prediction_array = tp_output.astype(bool)
            rospy.loginfo("MSG SP OUT = "+str(self._msg_lvl1.sp_output_array))
            rospy.loginfo("MSG TP OUT = "+str(self._msg_lvl1.tp_prediction_array))
        elif lvl == 2:
            self._msg_lvl2.sp_output_array = sp_output.astype(bool)
            self._msg_lvl2.tp_prediction_array = tp_output.astype(bool)
            rospy.loginfo("MSG SP OUT = "+str(self._msg_lvl2.sp_output_array))
            rospy.loginfo("MSG TP OUT = "+str(self._msg_lvl2.tp_prediction_array))
        else:
            assert False, "lvl has to be between 1 or 2"

    def publish_htm_msg(self, lvl):
        if lvl == 1:
            self._pub_lvl1.publish(self._msg_lvl1)
        elif lvl == 2:
            self._pub_lvl2.publish(self._msg_lvl2)
        else:
            assert False, "lvl has to be between 1 or 2"


class HtmOutputLevelXMessages(object):
    def __init__(self, lvl_num):
        self._msg_lvl_list = []
        self._pub_lvl_list = []
        for i in range(lvl_num):
            self._msg_lvl_list.append(htm_output())
            topic_name = "/htm_output_lvl"+str(i+1)
            self._pub_lvl_list.append(rospy.Publisher(topic_name, htm_output, queue_size=1))

    def update_htm_msg(self, lvl, sp_output, tp_output):
        if lvl > 0:
            self._msg_lvl_list[lvl-1].sp_output_array = sp_output.astype(bool)
            self._msg_lvl_list[lvl-1].tp_prediction_array = tp_output.astype(bool)

            rospy.loginfo("MSG SP OUT = "+str(self._msg_lvl_list[lvl-1].sp_output_array))
            rospy.loginfo("MSG TP OUT = "+str(self._msg_lvl_list[lvl-1].tp_prediction_array))
        else:
            assert False, "lvl has to be > 0"

    def publish_htm_msg(self, lvl):
        if lvl > 0:
            self._pub_lvl_list[lvl-1].publish(self._msg_lvl_list[lvl-1])
        else:
            assert False, "lvl has to be > 0"


# Save and Load HTMBucket Objects
class HTMBucketManage(object):
    def __init__(self, htm_bucket_name):
        self._htm_bucket_name = htm_bucket_name


    def save_htm_bucket(self, htm_bucket_object):
        path_to_pickle = get_nupic_pkg_path_to_pickle_data()
        htm_bucket_file_path = os.path.join(path_to_pickle, str(self._htm_bucket_name)+".p")
        pickle.dump(htm_bucket_object, open(htm_bucket_file_path, "wb"))
        rospy.loginfo("HTM File Saved="+str(self._htm_bucket_name))


    def get_saved_htm_bucket(self):
        path_to_pickle = get_nupic_pkg_path_to_pickle_data()
        htm_bucket_file_path = os.path.join(path_to_pickle, str(self._htm_bucket_name)+".p")
        try:
            htm_bucket = pickle.load(open(htm_bucket_file_path, "rb"))
            rospy.loginfo("Pickle File FOUND, retrieving=="+str(htm_bucket_file_path))
            return htm_bucket
        except:
            rospy.loginfo("Pickle File not found=="+str(htm_bucket_file_path))
            open(htm_bucket_file_path, 'a').close()
            return None


    def clean_saved_htm_bucket(self):
        path_to_pickle = get_nupic_pkg_path_to_pickle_data()
        htm_bucket_file_path = os.path.join(path_to_pickle, str(self._htm_bucket_name)+".p")
        s = raw_input("Sure DELETE previous learned Data from"+str(htm_bucket_file_path)+"[y,n]")

        if s == "y":
            try:
                os.remove(htm_bucket_file_path)
                rospy.loginfo("HTM File cleaned="+str(htm_bucket_file_path))
            except:
                rospy.loginfo("Pickle File not found=="+str(htm_bucket_file_path))
        else:
            rospy.loginfo("Remove Canceled...")


def generate_category_dict(number_of_things_to_learn):

    id = "ID-"
    categories = (id+str(1),)
    for i in range(2,number_of_things_to_learn+1):
        categories = categories + (id+str(i),)
    encoded_categories_dict = dict(zip(categories, categories))
    return encoded_categories_dict


def get_saved_htm(htm_lvl_name):
    path_to_pickle = get_nupic_pkg_path_to_pickle_data()
    htm_file_path = os.path.join(path_to_pickle, str(htm_lvl_name)+".p")
    try:
        htm = pickle.load(open(htm_file_path, "rb"))
        rospy.loginfo("Pickle File FOUND, retrieving=="+str(htm_file_path))
        return htm
    except:
        rospy.loginfo("Pickle File not found=="+str(htm_file_path))
        open(htm_file_path, 'a').close()
        return None

def clean_saved_htm(htm_lvl_name):
    path_to_pickle = get_nupic_pkg_path_to_pickle_data()
    htm_file_path = os.path.join(path_to_pickle, str(htm_lvl_name)+".p")
    s = raw_input("Sure DELETE previous learned Data from"+str(htm_file_path)+"[y,n]")

    if s == "y":
        try:
            os.remove(htm_file_path)
            rospy.loginfo("HTM File cleaned="+str(htm_file_path))
        except:
            rospy.loginfo("Pickle File not found=="+str(htm_file_path))
    else:
        rospy.loginfo("Remove Canceled...")


def update_msg(msg, value):
    msg.y_value = value
    return msg


def start_error_ploting_engine(pub_lv1, pub_lv2):
    msg_error_lv1 = graph()
    msg_error_lv2 = graph()

    msg_error_lv1 = update_msg(msg=msg_error_lv1, value=0)
    msg_error_lv2 = update_msg(msg=msg_error_lv2, value=0)


    rate = rospy.Rate(RATE_INIT_PUBLISH)
    i = 0
    while not rospy.is_shutdown() and i<INIT_I:
        rospy.loginfo("Initialazing Plots of Error..."+str(i))
        msg_error_lv1 = update_msg(msg=msg_error_lv1, value=0)
        msg_error_lv2 = update_msg(msg=msg_error_lv2, value=0)
        pub_lv1.publish(msg_error_lv1)
        pub_lv2.publish(msg_error_lv2)
        rate.sleep()
        i += 1

    return msg_error_lv1, msg_error_lv2


def start_error_ploting_engine_levelx(lvl_num, pub_lvl_list):
    msg_error_lvl_list = []
    for i in range(1,lvl_num+1):
        msg = graph()
        msg = update_msg(msg=msg, value=0)
        msg_error_lvl_list.append(msg)


    rate = rospy.Rate(RATE_INIT_PUBLISH)
    counter = 0
    print "INIT_I LIMIT ="+str(INIT_I)
    while not rospy.is_shutdown() and counter<INIT_I:
        rospy.loginfo("Initialazing Plots of Error..."+str(counter))
        for index in range(lvl_num):
            print "Updating msg lvl"+str(index)
            msg_error_lvl_list[index] = update_msg(msg=msg_error_lvl_list[index], value=0)
            print "Creating publisher lvl"+str(index)
            pub_lvl_list[index].publish(msg_error_lvl_list[index])
        print "Sleep"
        rate.sleep()
        counter += 1

    return msg_error_lvl_list


def learnLV1_learnLV2(htm_name_lvl1, htm_name_lvl2, learning_iterations, plot_error=False, htm_lv1_load_from_file=False, htm_lv2_load_from_file=False):


    if plot_error:
        pub_lv1 = rospy.Publisher('graph_data_lv1', graph, queue_size=1)
        pub_lv2 = rospy.Publisher('graph_data_lv2', graph, queue_size=1)
        msg_error_lv1, msg_error_lv2 = start_error_ploting_engine(pub_lv1, pub_lv2)

    encoded_categories_dict, length_encoded_array = create_4_encoded_animal_categories()

    number_of_things_to_learn = 4
    length_encoded_categories_dict = len(encoded_categories_dict)
    errors_to_bare_in_mind = 30

    if not htm_lv1_load_from_file:
        htm_object = HTM(number_of_things_to_learn=number_of_things_to_learn,
                             inputDimensionsSP=length_encoded_array,
                             numberOfColsTP=length_encoded_categories_dict,
                             errors_to_bare_in_mind=errors_to_bare_in_mind,
                             htmlvl_name=htm_name_lvl1)
    else:
        htm_object = get_saved_htm(htm_name_lvl1)

    # We repeat the sequence 10 times
    for i in range(learning_iterations):

        # Send each letter in the sequence in order
        for _ in xrange(10):
            for key, element in encoded_categories_dict.items():

                htm_object.learning_algorith(element,key,debug=False)

                if plot_error:
                    update_msg(msg=msg_error_lv1, value=htm_object.average_error)
                    pub_lv1.publish(msg_error_lv1)

                if htm_object.average_error <= 0.05:
                    print "OUT of Loop 1"
                    break
            # The reset command tells the TP that a sequence just ended and essentially
            # zeros out all the states. It is not strictly necessary but it's a bit
            # messier without resets, and the TP learns quicker with resets.
            if htm_object.average_error <= 0.05:
                print "OUT of Loop 2"
                break
        htm_object.reset_tp()
        if htm_object.average_error <= 0.05:
            print "OUT of Loop 3"
            break

    htm_object.print_ouput_dict()
    htm_object.save_htm()

    encoded_categories_dict, length_encoded_array = create_4_encoded_animal_categories()
    length_encoded_array_lv2 = number_of_things_to_learn
    number_of_things_to_learn_lv2 = 2
    length_encoded_categories_dict_lv2 = number_of_things_to_learn_lv2

    if not htm_lv2_load_from_file:
        htm_object_lv2 = HTM(number_of_things_to_learn=number_of_things_to_learn_lv2,
                             inputDimensionsSP=length_encoded_array_lv2,
                             numberOfColsTP=length_encoded_categories_dict_lv2,
                             errors_to_bare_in_mind=errors_to_bare_in_mind,
                             htmlvl_name=htm_name_lvl2)
    else:
        htm_object_lv2 = get_saved_htm(htm_name_lvl2)

    # We repeat the sequence 10 times
    for i in range(learning_iterations):

        # Send each letter in the sequence in order
        for _ in xrange(10):
            for key, element in encoded_categories_dict.items():

                print "#### START PREDICTION LV 1 ###"
                print "KEY LV1 =="+str(key)
                print "INPUT LV1 =="+str(element)
                sp_output, predicted_data = htm_object.predict(element)
                print "#### END PREDICTION LV 1 ####"
                print "INPUT LV2 =="+str(sp_output)
                htm_object_lv2.learning_algorith(sp_output,key="Rubish",debug=False)

                if plot_error:
                    update_msg(msg=msg_error_lv2, value=htm_object_lv2.average_error)
                    pub_lv2.publish(msg_error_lv2)

                if htm_object_lv2.average_error <= 0.05:
                    print "OUT of Loop 1"
                    break
            # The reset command tells the TP that a sequence just ended and essentially
            # zeros out all the states. It is not strictly necessary but it's a bit
            # messier without resets, and the TP learns quicker with resets.
            if htm_object_lv2.average_error <= 0.05:
                print "OUT of Loop 2"
                break
        htm_object_lv2.reset_tp()
        if htm_object_lv2.average_error <= 0.05:
            print "OUT of Loop 3"
            break

    htm_object_lv2.print_ouput_dict()
    htm_object_lv2.save_htm()

    return htm_object, htm_object_lv2


def update_htm_msg(msg, sp_output, tp_output):

    msg.sp_output_array = sp_output.astype(bool)
    msg.tp_prediction_array = tp_output.astype(bool)

    rospy.loginfo("MSG SP OUT = "+str(msg.sp_output_array))
    rospy.loginfo("MSG TP OUT = "+str(msg.tp_prediction_array))

    return msg


def predictLV1_LV2(htm_name_lvl1, htm_name_lvl2):

    htm_object_lvl1 = get_saved_htm(htm_name_lvl1)
    htm_object_lvl2 = get_saved_htm(htm_name_lvl2)

    encoded_categories_dict, length_encoded_array = create_4_encoded_animal_categories()

    pub_lvl1 = rospy.Publisher("/htm_output_lvl1", htm_output, queue_size=1)
    pub_lvl2 = rospy.Publisher("/htm_output_lvl2", htm_output, queue_size=1)

    msg_lvl1 = htm_output()
    msg_lvl2 = htm_output()

    for key, element in encoded_categories_dict.items():
        raw_input("PRESS TO CONTINUE...")
        print "#### START PREDICTION LV 1 ###"
        print "KEY LV1 =="+str(key)
        print "INPUT LV1 =="+str(element)

        sp_data_lv1, predicted_data_lvl1 = htm_object_lvl1.predict(element)
        msg_lvl1 = update_htm_msg(msg_lvl1, sp_data_lv1, predicted_data_lvl1)
        pub_lvl1.publish(msg_lvl1)


        print "PREDICTED LV1 =="+str(predicted_data_lvl1)
        print "OUTPUT LV1 =="+str(sp_data_lv1)
        print "#### END PREDICTION LV 1 ####"
        print "INPUT LV2 =="+str(sp_data_lv1)


        sp_data_lv2, predicted_data_lvl2 = htm_object_lvl2.predict(sp_data_lv1)

        msg_lvl2 = update_htm_msg(msg_lvl2, sp_data_lv2, predicted_data_lvl2)
        pub_lvl2.publish(msg_lvl2)


        print "PREDICTED LV2 =="+str(predicted_data_lvl2)
        print "OUTPUT LV2 =="+str(sp_data_lv2)
        print "#### END PREDICTION ####"


def learn_predict_loop_basic():
    rospy.init_node('predict_learn_plot', anonymous=True)

    clean_saved_htm(htm_lvl_name=HTM_NAME_LVL1)
    clean_saved_htm(htm_lvl_name=HTM_NAME_LVL2)

    htm1, htm2 = learnLV1_learnLV2(htm_name_lvl1=HTM_NAME_LVL1,
                                  htm_name_lvl2=HTM_NAME_LVL2,
                                  learning_iterations=30,
                                  plot_error = True,
                                  htm_lv1_load_from_file=False,
                                  htm_lv2_load_from_file=False)

    raw_input("@@@@ Predict Loop LV1+LV2...")
    while not rospy.is_shutdown():
        predictLV1_LV2(htm_name_lvl1=HTM_NAME_LVL1,
                       htm_name_lvl2=HTM_NAME_LVL2)


def learner_with_sound_mng(number_of_things_to_learn, learning_data_dir, loop_data_times, reset=False):

    rospy.init_node('learn_htm', anonymous=True)

    lst_songs = get_sound_list_dir(learning_data_dir)
    assert len(lst_songs)>0, "Empty Sound Dir"
    pw = PlayWav(lst_songs)

    learning_time = time.time()

    msg = rospy.wait_for_message("sound_sdr", internal_sound_sdr)
    array_sdr = msg.sdr_array
    length_encoded_array = len(array_sdr)

    encoded_categories_dict = generate_category_dict(number_of_things_to_learn)
    length_encoded_categories_dict = len(encoded_categories_dict)

    if reset:
        rospy.loginfo("Erasing Memory HTM...")
        clean_saved_htm()
    else:
        rospy.loginfo("Retrieving Memory HTM...")
    htm_object = get_saved_htm()
    if not htm_object:
        # If ther isnt any data, we create a new one
        htm_object = HTM(number_of_things_to_learn, length_encoded_array, length_encoded_categories_dict)
    pw.play_next_song()

    def callback(data):
        #rospy.loginfo(rospy.get_caller_id() + "SDR = %s", data.sdr_array)
        element = numpy.asarray(data.sdr_array)

        htm_object.learning_algorith(element,key=str(time.time()))

        song_ended = pw.check_if_ended()
        print "SONG ENDED?=="+str(song_ended)

        if not song_ended:
            if pw.play_next_song():
                print "Playing Next Song"
            else:
                if pw.get_start_again_times() < loop_data_times:
                    pw.start_again()
                else:
                    htm_object.reset_tp()
                    htm_object.print_ouput_dict()
                    save_htm(htm_object)
                    easygui.msgbox("Start Predict Test", title="Kodama Learning")
                    htm_object.predict_test()
                    #save_htm_backup(htm_object)
                    easygui.msgbox("Learning Material Ended", title="Kodama Learning")
                    rospy.signal_shutdown("No more songs to learn")

        rospy.loginfo("Time learning ==> "+str(time.time()-learning_time))

        if time.time()-learning_time> LEARNING_TIME:
            rospy.loginfo("Time learning ==> "+str(time.time()-learning_time))
            htm_object.reset_tp()
            htm_object.print_ouput_dict()
            save_htm(htm_object)
            #save_htm_backup(htm_object)
            easygui.msgbox("Learning Time Ended", title="Kodama Learning")
            rospy.signal_shutdown("Passed the Record Time")


    rospy.Subscriber("sound_sdr", internal_sound_sdr, callback)
    rospy.loginfo("Start Learning...")
    rospy.spin()



def learnLV1_learnLV2_sound(number_of_things_to_learn_lv1, number_of_things_to_learn_lv2, learning_data_dir, htm_name_lvl1, htm_name_lvl2, learning_iterations, learning_global_time, learning_error_accepted, errors_to_bare_in_mind=30, erase_old_learned_data=False, plot_error=False, play_sound_files=True):

    if play_sound_files:
        lst_songs = get_sound_list_dir(learning_data_dir)
        assert len(lst_songs) > 0, "Empty Sound Dir"
        pw = PlayWav(lst_songs)

    msg = rospy.wait_for_message("sound_sdr", internal_sound_sdr)
    array_sdr = msg.sdr_array
    length_encoded_array = len(array_sdr)

    htm_object_lvl1 = None
    htm_object_lvl2 = None

    if erase_old_learned_data:
        rospy.loginfo("Erasing Memory HTM...")
        clean_saved_htm(htm_lvl_name=htm_name_lvl1)
        clean_saved_htm(htm_lvl_name=htm_name_lvl2)
    else:
        rospy.loginfo("Retrieving Memory HTM...")
        htm_object_lvl1 = get_saved_htm(htm_name_lvl1)
        htm_object_lvl2 = get_saved_htm(htm_name_lvl2)

    if not htm_object_lvl1 or not htm_object_lvl2:
        # If ther isnt any data, we create a new one
        rospy.loginfo("Started Creating HTMs...")
        encoded_categories_dict_lv1 = generate_category_dict(number_of_things_to_learn_lv1)
        length_encoded_categories_dict_lv1 = len(encoded_categories_dict_lv1)


        htm_object_lvl1 = HTM(number_of_things_to_learn=number_of_things_to_learn_lv1,
                             inputDimensionsSP=length_encoded_array,
                             numberOfColsTP=length_encoded_categories_dict_lv1,
                             errors_to_bare_in_mind=errors_to_bare_in_mind,
                             htmlvl_name=htm_name_lvl1)


        assert length_encoded_categories_dict_lv1 == length_encoded_categories_dict_lv1, "OutPut LV1 != Input LV2"

        encoded_categories_dict_lv2 = generate_category_dict(number_of_things_to_learn_lv2)
        length_encoded_categories_dict_lv2 = len(encoded_categories_dict_lv2)

        htm_object_lvl2 = HTM(number_of_things_to_learn=number_of_things_to_learn_lv2,
                             inputDimensionsSP=length_encoded_categories_dict_lv1,
                             numberOfColsTP=length_encoded_categories_dict_lv2,
                             errors_to_bare_in_mind=errors_to_bare_in_mind,
                             htmlvl_name=htm_name_lvl2)


    if plot_error:
        pub_lv1 = rospy.Publisher('graph_data_lv1', graph, queue_size=1)
        pub_lv2 = rospy.Publisher('graph_data_lv2', graph, queue_size=1)
        msg_error_lv1, msg_error_lv2 = start_error_ploting_engine(pub_lv1, pub_lv2)


    if play_sound_files:
        pw.play_next_song()


    l_status = LearningStatus(num_levels=2, learning_time=learning_global_time)
    print "Learning STATUS of ALL levels =="+str(l_status.all_status())


    def callback(data):

        if play_sound_files:
            song_ended = pw.check_if_ended()
            if song_ended:
                l_status.toggle_reset_learning_tp()
                if pw.play_next_song():

                    song_name = pw.get_current_song_name()
                    print "Playing Next Sound File = "+ str(song_name)
                else:
                    if pw.get_start_again_times() < learning_iterations:
                        pw.start_again()
                    else:
                        htm_object_lvl1.save_htm()
                        htm_object_lvl2.save_htm()
                        easygui.msgbox("Learning Material Ended", title="Kodama Learning")
                        rospy.signal_shutdown("No more sound files to learn")

            rospy.loginfo("Time learning ==> "+str(l_status.get_learning_time()))

            if l_status.end_learning_time():
                htm_object_lvl1.save_htm()
                htm_object_lvl2.save_htm()
                easygui.msgbox("Learning Time Ended", title="Kodama Learning")
                rospy.signal_shutdown("Finnished the learning Time")

        # TODO: Add lospy logdebig prints
        #rospy.loginfo(rospy.get_caller_id() + "SDR = %s", data.sdr_array)
        element = numpy.asarray(data.sdr_array)
        # Which level we are going to train


        if not l_status.get_lvl_status(1):
            if l_status.get_reset_learning_tp():
                print "@@@@ LEARNING LV 1 RESET @@@@"
                htm_object_lvl1.reset_tp()
                l_status.toggle_reset_learning_tp()
                assert not l_status.get_reset_learning_tp(), "After reset lv2, it should be False to avoid over reseting"

            print "@@@@ LEARNING LV 1 @@@@"
            patern_name_lvl1 = "LVL1-"+str(time.time())
            htm_object_lvl1.learning_algorith(element,key=patern_name_lvl1,debug=False)

            if plot_error:
                update_msg(msg=msg_error_lv1, value=htm_object_lvl1.average_error)
                pub_lv1.publish(msg_error_lv1)

            if htm_object_lvl1.average_error <= learning_error_accepted:
                print "LVL 1 ERROR < "+str(learning_error_accepted)
                l_status.update_learning_lvl_status(1, True)
                # We dont reset because this isnt the end of the sequence
                htm_object_lvl1.save_htm()
                pw.wait_till_finish_and_start_again()
                print "#### Restarted again sound file list ####"

            # TODO: Do a save function to avoid loosing all data learned after long periods of time.
            #htm_object_lvl1.save_htm()

        elif l_status.get_lvl_status(1) and not l_status.get_lvl_status(2):
            if l_status.get_reset_learning_tp():
                print "@@@@ LEARNING LV 2 RESET @@@@"
                htm_object_lvl2.reset_tp()
                l_status.toggle_reset_learning_tp()
                assert not l_status.get_reset_learning_tp(), "After reset lv2, it should be False to avoid over reseting"

            print "#### START PREDICTION LV 1 ###"
            print "INPUT LV1 =="+str(element)
            sp_output, predicted_data = htm_object_lvl1.predict(element)
            print "#### END PREDICTION LV 1 ####"
            print "INPUT LV2 =="+str(sp_output)
            patern_name_lvl2 = "LVL2-"+str(time.time())
            htm_object_lvl2.learning_algorith(sp_output, key=patern_name_lvl2, debug=False)

            if plot_error:
                update_msg(msg=msg_error_lv2, value=htm_object_lvl2.average_error)
                pub_lv2.publish(msg_error_lv2)

            if htm_object_lvl2.average_error <= learning_error_accepted:
                print "LVL 2 ERROR < "+str(learning_error_accepted)
                l_status.update_learning_lvl_status(2,True)
                htm_object_lvl2.save_htm()

            # TODO: Do a save function to avoid loosing all data learned after long periods of time.
            #htm_object_lvl2.save_htm()

        else:
            print "LVL 1 and LVL2 Trained..."
            easygui.msgbox("ALL LVLS have stabilised learning...", title="Kodama Learning")
            rospy.signal_shutdown("Learning Ended")


    rospy.Subscriber("sound_sdr", internal_sound_sdr, callback)
    rospy.loginfo("Start Learning...")
    rospy.spin()



def learnLV1_learnLV2_sound_with_equal_time(number_of_things_to_learn_lv1, number_of_things_to_learn_lv2, learning_data_dir, htm_name_lvl1, htm_name_lvl2, learning_iterations, learning_global_time, learning_error_accepted, errors_to_bare_in_mind=30, erase_old_learned_data=False, plot_error=False, play_sound_files=True):

    if play_sound_files:
        lst_songs = get_sound_list_dir(learning_data_dir)
        assert len(lst_songs) > 0, "Empty Sound Dir"
        pw = PlayWav(lst_songs)

    msg = rospy.wait_for_message("sound_sdr", internal_sound_sdr)
    array_sdr = msg.sdr_array
    length_encoded_array = len(array_sdr)

    htm_object_lvl1 = None
    htm_object_lvl2 = None

    if erase_old_learned_data:
        rospy.loginfo("Erasing Memory HTM...")
        clean_saved_htm(htm_lvl_name=htm_name_lvl1)
        clean_saved_htm(htm_lvl_name=htm_name_lvl2)
    else:
        rospy.loginfo("Retrieving Memory HTM...")
        htm_object_lvl1 = get_saved_htm(htm_name_lvl1)
        htm_object_lvl2 = get_saved_htm(htm_name_lvl2)

    if not htm_object_lvl1 or not htm_object_lvl2:
        # If ther isnt any data, we create a new one
        rospy.loginfo("Started Creating HTMs...")
        encoded_categories_dict_lv1 = generate_category_dict(number_of_things_to_learn_lv1)
        length_encoded_categories_dict_lv1 = len(encoded_categories_dict_lv1)


        htm_object_lvl1 = HTM(number_of_things_to_learn=number_of_things_to_learn_lv1,
                             inputDimensionsSP=length_encoded_array,
                             numberOfColsTP=length_encoded_categories_dict_lv1,
                             errors_to_bare_in_mind=errors_to_bare_in_mind,
                             htmlvl_name=htm_name_lvl1)


        assert length_encoded_categories_dict_lv1 == length_encoded_categories_dict_lv1, "OutPut LV1 != Input LV2"

        encoded_categories_dict_lv2 = generate_category_dict(number_of_things_to_learn_lv2)
        length_encoded_categories_dict_lv2 = len(encoded_categories_dict_lv2)

        htm_object_lvl2 = HTM(number_of_things_to_learn=number_of_things_to_learn_lv2,
                             inputDimensionsSP=length_encoded_categories_dict_lv1,
                             numberOfColsTP=length_encoded_categories_dict_lv2,
                             errors_to_bare_in_mind=errors_to_bare_in_mind,
                             htmlvl_name=htm_name_lvl2)


    if plot_error:
        pub_lv1 = rospy.Publisher('graph_data_lv1', graph, queue_size=1)
        pub_lv2 = rospy.Publisher('graph_data_lv2', graph, queue_size=1)
        msg_error_lv1, msg_error_lv2 = start_error_ploting_engine(pub_lv1, pub_lv2)


    if play_sound_files:
        pw.play_next_song()


    l_status = LearningStatus(num_levels=2, learning_time=learning_global_time)
    print "Learning STATUS of ALL levels =="+str(l_status.all_status())


    def callback(data):

        if play_sound_files:
            song_ended = pw.check_if_ended()
            if song_ended:
                l_status.toggle_reset_learning_tp()
                if pw.play_next_song():
                    song_name = pw.get_current_song_name()
                    print "Playing Next Sound File = "+ str(song_name)
                else:
                    pw.start_again()


        rospy.loginfo("Time learning ==> "+str(l_status.get_learning_time()))
        if l_status.end_learning_time():
            # If the time has finished

            lvl_learning_now = l_status.get_which_lvl_is_learning()
            easygui.msgbox("Finished time LVL ="+str(lvl_learning_now), title="Kodama Learning")
            l_status.update_learning_lvl_status(lvl_learning_now, True)

            if l_status.get_if_all_lvls_learned():
                htm_object_lvl1.save_htm()
                htm_object_lvl2.save_htm()
                easygui.msgbox("Learning Time Ended", title="Kodama Learning")
                rospy.signal_shutdown("Finnished the learning Time")
            else:
                l_status.restart_learning_chrono()
                pw.wait_till_finish_and_start_again()
                print "#### Restarted again sound file list ####"

        #rospy.loginfo(rospy.get_caller_id() + "SDR = %s", data.sdr_array)
        element = numpy.asarray(data.sdr_array)
        # Which level we are going to train


        if not l_status.get_lvl_status(1):
            if l_status.get_reset_learning_tp():
                print "@@@@ LEARNING LV 1 RESET @@@@"
                htm_object_lvl1.reset_tp()
                l_status.toggle_reset_learning_tp()
                assert not l_status.get_reset_learning_tp(), "After reset lv2, it should be False to avoid over reseting"

            print "@@@@ LEARNING LV 1 @@@@"
            patern_name_lvl1 = "LVL1-"+str(time.time())
            htm_object_lvl1.learning_algorith(element,key=patern_name_lvl1,debug=False)

            if plot_error:
                update_msg(msg=msg_error_lv1, value=htm_object_lvl1.average_error)
                pub_lv1.publish(msg_error_lv1)

        elif l_status.get_lvl_status(1) and not l_status.get_lvl_status(2):
            if l_status.get_reset_learning_tp():
                print "@@@@ LEARNING LV 2 RESET @@@@"
                htm_object_lvl2.reset_tp()
                l_status.toggle_reset_learning_tp()
                assert not l_status.get_reset_learning_tp(), "After reset lv2, it should be False to avoid over reseting"

            print "#### START PREDICTION LV 1 ###"
            print "INPUT LV1 =="+str(element)
            sp_output, predicted_data = htm_object_lvl1.predict(element)
            print "#### END PREDICTION LV 1 ####"
            print "INPUT LV2 =="+str(sp_output)
            patern_name_lvl2 = "LVL2-"+str(time.time())
            htm_object_lvl2.learning_algorith(sp_output, key=patern_name_lvl2, debug=False)

            if plot_error:
                update_msg(msg=msg_error_lv2, value=htm_object_lvl2.average_error)
                pub_lv2.publish(msg_error_lv2)


        else:
            print "LVL 1 and LVL2 Trained..."
            easygui.msgbox("ALL LVLS have stabilised learning...", title="Kodama Learning")
            rospy.signal_shutdown("Learning Ended")


    rospy.Subscriber("sound_sdr", internal_sound_sdr, callback)
    rospy.loginfo("Start Learning...")
    rospy.spin()



def learnLVX_sound_with_equal_time(num_levels, number_of_things_to_learn_lvx_list, learning_data_dir, learning_global_time, simultaneous_learning, errors_to_bare_in_mind=30, load_htm_from_files=False, plot_error=False, play_sound_files=True):

    if play_sound_files:
        lst_songs = get_sound_list_dir(learning_data_dir)
        assert len(lst_songs) > 0, "Empty Sound Dir"
        pw = PlayWav(lst_songs)

    msg = rospy.wait_for_message("sound_sdr", internal_sound_sdr)
    array_sdr = msg.sdr_array
    length_encoded_array = len(array_sdr)

    htm_bucket = HTMMacroObject(num_levels=num_levels,
                                load_htm_from_files=load_htm_from_files,
                                number_of_things_to_learn_lvx_list=number_of_things_to_learn_lvx_list,
                                errors_to_bare_in_mind=errors_to_bare_in_mind,
                                length_input_encoded_array=length_encoded_array)

    if plot_error:
        error_pub_lvl_list = []
        for i in range(1, num_levels+1):
            topic_name = "graph_data_lv"+str(i)
            pub_lvlx = rospy.Publisher(topic_name, graph, queue_size=1)
            error_pub_lvl_list.append(pub_lvlx)
            print "TOPIC ==>"+str(topic_name)

        print "PUB ERROR LIST ==>"+str(error_pub_lvl_list)
        msg_error_lvl_list = start_error_ploting_engine_levelx(lvl_num=num_levels,
                                                               pub_lvl_list=error_pub_lvl_list)
        print "MSG ERROR LIST ==>"+str(msg_error_lvl_list)

    if play_sound_files:
        pw.play_next_song()

    l_status = LearningStatus(num_levels=num_levels, learning_time=learning_global_time)
    print "Learning STATUS of ALL levels =="+str(l_status.all_status())


    def callback(data):

        if play_sound_files:
            song_ended = pw.check_if_ended()
            if song_ended:
                l_status.toggle_reset_learning_tp()
                if pw.play_next_song():
                    song_name = pw.get_current_song_name()
                    print "Playing Next Sound File = "+ str(song_name)
                else:
                    pw.start_again()


        rospy.loginfo("Time learning ==> "+str(l_status.get_learning_time()))
        if l_status.end_learning_time():
            # If the time has finished

            lvl_learning_now = l_status.get_which_lvl_is_learning()
            easygui.msgbox("Finished time LVL ="+str(lvl_learning_now), title="Kodama Learning")
            l_status.update_learning_lvl_status(lvl_learning_now, True)

            if l_status.get_if_all_lvls_learned():
                htm_bucket.save_htm_lvls()
                easygui.msgbox("Learning Time Ended", title="Kodama Learning")
                rospy.signal_shutdown("Finnished the learning Time")
            else:
                l_status.restart_learning_chrono()
                pw.wait_till_finish_and_start_again()
                print "#### Restarted again sound file list ####"

        #rospy.loginfo(rospy.get_caller_id() + "SDR = %s", data.sdr_array)
        input_data = numpy.asarray(data.sdr_array)
        # Which level we are going to train
        for lvl in range(1, htm_bucket.get_num_levels()+1):
            rospy.loginfo("LV = "+str(lvl))
            if not l_status.get_lvl_status(lvl):
                # The current level hasnt finished learning fase yet
                if l_status.get_reset_learning_tp():
                    print "@@@@ LEARNING LV 1 RESET @@@@"
                    htm_bucket.reset_tp_lvl(lvl)
                    l_status.toggle_reset_learning_tp()
                    assert not l_status.get_reset_learning_tp(), "It should be false to avoid overreseting."

                sp_output, predicted_data = htm_bucket.predict_till_lvl(lvl=lvl, input_data=input_data)
                average_error = htm_bucket.learn(lvl=lvl, element=sp_output, debug=False)

                if plot_error:
                    msg_error_lvl_list[lvl-1] = update_msg(msg=msg_error_lvl_list[lvl-1], value=average_error)
                    error_pub_lvl_list[lvl-1].publish(msg_error_lvl_list[lvl-1])
                break

    rospy.Subscriber("sound_sdr", internal_sound_sdr, callback)
    rospy.loginfo("Start Learning...")
    rospy.spin()


def learnLVX_sound_with_equal_time_cluster_version(num_levels, number_of_things_to_learn_lvx_list, number_of_sequences_to_learn_lvx_list, learning_data_dir, learning_global_time, simultaneous_learning, htm_bucket_name, errors_to_bare_in_mind=30, load_htm_from_files=False, plot_error=False, play_sound_files=True):

    if play_sound_files:
        lst_songs = get_sound_list_dir(learning_data_dir)
        assert len(lst_songs) > 0, "Empty Sound Dir"
        pw = PlayWav(lst_songs)

    msg = rospy.wait_for_message("sound_sdr", internal_sound_sdr)
    array_sdr = msg.sdr_array
    length_encoded_array = len(array_sdr)

    htm_bucket_manage = HTMBucketManage(htm_bucket_name)
    htm_bucket_manage.clean_saved_htm_bucket()

    htm_bucket = HTMMacroObjectClusters(num_levels=num_levels,
                                        number_of_things_to_learn_lvx_list=number_of_things_to_learn_lvx_list,
                                        number_of_sequences_to_learn_lvx_list=number_of_sequences_to_learn_lvx_list,
                                        errors_to_bare_in_mind=errors_to_bare_in_mind,
                                        length_input_encoded_array=length_encoded_array,
                                        htm_bucket_name=htm_bucket_name)

    if plot_error:
        error_pub_lvl_list = []
        for i in range(1, num_levels+1):
            topic_name = "graph_data_lv"+str(i)
            pub_lvlx = rospy.Publisher(topic_name, graph, queue_size=1)
            error_pub_lvl_list.append(pub_lvlx)
            print "TOPIC ==>"+str(topic_name)

        print "PUB ERROR LIST ==>"+str(error_pub_lvl_list)
        msg_error_lvl_list = start_error_ploting_engine_levelx(lvl_num=num_levels,
                                                               pub_lvl_list=error_pub_lvl_list)
        print "MSG ERROR LIST ==>"+str(msg_error_lvl_list)

    if play_sound_files:
        pw.play_next_song()

    l_status = LearningStatus(num_levels=num_levels, learning_time=learning_global_time)
    print "Learning STATUS of ALL levels =="+str(l_status.all_status())


    def callback(data):

        if play_sound_files:
            song_ended = pw.check_if_ended()
            if song_ended:
                l_status.toggle_reset_learning_tp()
                if pw.play_next_song():
                    song_name = pw.get_current_song_name()
                    print "Playing Next Sound File = "+ str(song_name)
                else:
                    pw.start_again()


        rospy.loginfo("Time learning ==> "+str(l_status.get_learning_time()))
        if l_status.end_learning_time():
            # If the time has finished

            lvl_learning_now = l_status.get_which_lvl_is_learning()
            easygui.msgbox("Finished time LVL ="+str(lvl_learning_now), title="Kodama Learning")
            l_status.update_learning_lvl_status(lvl_learning_now, True)

            # We cluster the level that has finished learning
            easygui.msgbox("Start Clustering LVL ="+str(lvl_learning_now), title="Kodama Learning")
            htm_bucket.clusterize(lvl=lvl_learning_now)

            rospy.loginfo("ALL LVLs Have Learned? =="+str(l_status.get_if_all_lvls_learned()))

            if l_status.get_if_all_lvls_learned():
                rospy.loginfo("Saving HTM Bucket")
                htm_bucket_manage.save_htm_bucket(htm_bucket)
                easygui.msgbox("Learning Time Ended", title="Kodama Learning")
                rospy.signal_shutdown("Finnished the learning Time")
            else:
                l_status.restart_learning_chrono()
                pw.wait_till_finish_and_start_again()
                print "#### Restarted again sound file list ####"

        #rospy.loginfo(rospy.get_caller_id() + "SDR = %s", data.sdr_array)
        input_data = numpy.asarray(data.sdr_array)
        # Which level we are going to train
        for lvl in range(1, htm_bucket.get_num_levels()+1):
            rospy.loginfo("LV = "+str(lvl))
            if not l_status.get_lvl_status(lvl):
                # The current level hasnt finished learning fase yet
                if l_status.get_reset_learning_tp():
                    print "@@@@ LEARNING LV 1 RESET @@@@"
                    htm_bucket.reset_tp_lvl(lvl)
                    l_status.toggle_reset_learning_tp()
                    assert not l_status.get_reset_learning_tp(), "It should be false to avoid overreseting."

                sdr_output_sequence, sdr_predicted_sequence = htm_bucket.predict_till_lvl_cluster(lvl=lvl, input_data=input_data)
                average_error = htm_bucket.learn(lvl=lvl, element=sdr_output_sequence, debug=False)

                if plot_error:
                    msg_error_lvl_list[lvl-1] = update_msg(msg=msg_error_lvl_list[lvl-1], value=average_error)
                    error_pub_lvl_list[lvl-1].publish(msg_error_lvl_list[lvl-1])
                break

    rospy.Subscriber("sound_sdr", internal_sound_sdr, callback)
    rospy.loginfo("Start Learning...")
    rospy.spin()


def learn_simultaneous_sound(num_levels, number_of_things_to_learn_lvx_list, learning_data_dir, learning_global_time, simultaneous_learning, errors_to_bare_in_mind=30, load_htm_from_files=False, plot_error=False, play_sound_files=True):

    if play_sound_files:
        lst_songs = get_sound_list_dir(learning_data_dir)
        assert len(lst_songs) > 0, "Empty Sound Dir"
        pw = PlayWav(lst_songs)

    msg = rospy.wait_for_message("sound_sdr", internal_sound_sdr)
    array_sdr = msg.sdr_array
    length_encoded_array = len(array_sdr)

    htm_bucket = HTMMacroObject(num_levels=num_levels,
                                load_htm_from_files=load_htm_from_files,
                                number_of_things_to_learn_lvx_list=number_of_things_to_learn_lvx_list,
                                errors_to_bare_in_mind=errors_to_bare_in_mind,
                                length_input_encoded_array=length_encoded_array)

    if plot_error:
        error_pub_lvl_list = []
        for i in range(1, num_levels+1):
            topic_name = "graph_data_lv"+str(i)
            pub_lvlx = rospy.Publisher(topic_name, graph, queue_size=1)
            error_pub_lvl_list.append(pub_lvlx)
            print "TOPIC ==>"+str(topic_name)

        print "PUB ERROR LIST ==>"+str(error_pub_lvl_list)
        msg_error_lvl_list = start_error_ploting_engine_levelx(lvl_num=num_levels,
                                                               pub_lvl_list=error_pub_lvl_list)
        print "MSG ERROR LIST ==>"+str(msg_error_lvl_list)

    if play_sound_files:
        pw.play_next_song()

    l_status = LearningStatus(num_levels=num_levels, learning_time=learning_global_time)
    print "Learning STATUS of ALL levels =="+str(l_status.all_status())


    def callback(data):

        if play_sound_files:
            song_ended = pw.check_if_ended()
            if song_ended:
                l_status.toggle_reset_learning_tp()
                if pw.play_next_song():
                    song_name = pw.get_current_song_name()
                    print "Playing Next Sound File = "+ str(song_name)
                else:
                    pw.start_again()


        rospy.loginfo("Time learning ==> "+str(l_status.get_learning_time()))
        if l_status.end_learning_time():
            # If the time has finished

            htm_bucket.save_htm_lvls()
            average_errors_list = l_status.get_all_average_errors()
            easygui.msgbox("Learning Time Ended, errors ="+str(average_errors_list), title="Kodama Learning")
            rospy.signal_shutdown("Finnished the learning Time")

        #rospy.loginfo(rospy.get_caller_id() + "SDR = %s", data.sdr_array)
        input_data = numpy.asarray(data.sdr_array)
        sp_output = None
        # Which level we are going to train
        for lvl in range(1, htm_bucket.get_num_levels()+1):
            rospy.loginfo("LV LEARNING = "+str(lvl))
            # The current level hasnt finished learning fase yet
            if l_status.get_reset_learning_tp():
                print "@@@@ LEARNING LV "+str(lvl)+" RESET @@@@"
                htm_bucket.reset_tp_lvl(lvl)
                l_status.toggle_reset_learning_tp()
                assert not l_status.get_reset_learning_tp(), "It should be false to avoid overreseting."

            if lvl != 1:
                input_data = copy.deepcopy(sp_output)

            average_error = htm_bucket.learn(lvl=lvl, element=input_data, debug=False)
            l_status.update_lvl_average_error(lvl=lvl, error_value=average_error)
            sp_output, predicted_data = htm_bucket.predict_lvl(lvl=lvl, input_data=input_data)

            if plot_error:
                msg_error_lvl_list[lvl-1] = update_msg(msg=msg_error_lvl_list[lvl-1], value=average_error)
                error_pub_lvl_list[lvl-1].publish(msg_error_lvl_list[lvl-1])


    rospy.Subscriber("sound_sdr", internal_sound_sdr, callback)
    rospy.loginfo("Start Learning...")
    rospy.spin()


def predict_sound_LV1_LV2(htm_name_lvl1, htm_name_lvl2):

    htm_object_lvl1 = get_saved_htm(htm_name_lvl1)
    htm_object_lvl2 = get_saved_htm(htm_name_lvl2)

    rospy.loginfo(htm_object_lvl1)
    rospy.loginfo(htm_object_lvl2)

    assert htm_object_lvl1 and htm_object_lvl2, "Prior to predict you need to learn."

    htm_out_msg = HtmOutputMessages()
    variable_out = BlackboardVariablePublisher()

    def callback(data):

        element = numpy.asarray(data.sdr_array)
        print "#### START PREDICTION LV 1 ###"
        print "INPUT LV1 =="+str(element)

        sp_data_lv1, predicted_data_lvl1 = htm_object_lvl1.predict(element)

        htm_out_msg.update_htm_msg(lvl=1, sp_output=sp_data_lv1, tp_output=predicted_data_lvl1)
        htm_out_msg.publish_htm_msg(lvl=1)

        print "PREDICTED LV1 =="+str(predicted_data_lvl1)
        print "OUTPUT LV1 =="+str(sp_data_lv1)
        print "#### END PREDICTION LV 1 ####"
        print "INPUT LV2 =="+str(sp_data_lv1)


        sp_data_lv2, predicted_data_lvl2 = htm_object_lvl2.predict(sp_data_lv1)

        htm_out_msg.update_htm_msg(lvl=2, sp_output=sp_data_lv2, tp_output=predicted_data_lvl2)
        htm_out_msg.publish_htm_msg(lvl=2)

        variable_out.publish_value(sp_data_lv2)

        print "PREDICTED LV2 =="+str(predicted_data_lvl2)
        print "OUTPUT LV2 =="+str(sp_data_lv2)
        print "#### END PREDICTION ####"


    rospy.Subscriber("sound_sdr", internal_sound_sdr, callback)
    rospy.loginfo("Start Learning...")
    rospy.spin()


def predict_lvlx_sound_LV1_LV2(num_levels,):

    htm_bucket = HTMMacroObject(num_levels=num_levels,
                                load_htm_from_files=True,
                                number_of_things_to_learn_lvx_list=[None]*num_levels,
                                errors_to_bare_in_mind=None,
                                length_input_encoded_array=None)

    htm_out_msg = HtmOutputLevelXMessages(lvl_num=htm_bucket.get_num_levels())
    variable_out = BlackboardVariablePublisher()

    def callback(data):

        input_data = numpy.asarray(data.sdr_array)
        sp_data = None
        print "#### START PREDICTION LV 1 ###"
        print "INPUT LVL =="+str(input_data)

        for lvl in range(1,htm_bucket.get_num_levels()+1):
            print "PREDICTING LVL =="+str(lvl)
            sp_data, predicted_data = htm_bucket.predict_lvl(lvl=lvl, input_data=input_data)
            htm_out_msg.update_htm_msg(lvl=lvl, sp_output=sp_data, tp_output=predicted_data)
            htm_out_msg.publish_htm_msg(lvl=lvl)

            input_data = copy.deepcopy(sp_data)

        variable_out.publish_value(sp_data)

    rospy.Subscriber("sound_sdr", internal_sound_sdr, callback)
    rospy.loginfo("Start Learning...")
    rospy.spin()


def predict_with_clusters_lvlx(num_levels, htm_bucket_name):

    htm_bucket_manage = HTMBucketManage(htm_bucket_name)
    htm_bucket = htm_bucket_manage.get_saved_htm_bucket()

    htm_out_msg = HtmOutputLevelXMessages(lvl_num=htm_bucket.get_num_levels())
    variable_out = BlackboardVariablePublisher()

    def callback(data):

        input_data = numpy.asarray(data.sdr_array)
        current_sequence_label = None
        print "#### START PREDICTION ###"
        print "INPUT LVL =="+str(input_data)

        for lvl in range(1,htm_bucket.get_num_levels()+1):
            print "PREDICTING LVL =="+str(lvl)
            # We are interested in the sequence that belongs to the input_data and the next sequence.
            sdr_output_sequence, sdr_predicted_sequence, current_sequence_label = htm_bucket.predict_lvl_cluster(lvl=lvl, input_data=input_data)
            htm_out_msg.update_htm_msg(lvl=lvl, sp_output=sdr_output_sequence, tp_output=sdr_predicted_sequence)
            htm_out_msg.publish_htm_msg(lvl=lvl)

            input_data = copy.deepcopy(sdr_output_sequence)

        print "Current Sequence Label =="+str(current_sequence_label)
        variable_out.publish_value(current_sequence_label)

    rospy.Subscriber("sound_sdr", internal_sound_sdr, callback)
    rospy.loginfo("Start Learning...")
    rospy.spin()

def cluster_htm_lvls(num_levels, htm_bucket_name):
    """
    We cluster the number of levels given
    :param num_levels:
    :return:
    """

    htm_bucket_manage = HTMBucketManage(htm_bucket_name)
    htm_bucket = htm_bucket_manage.get_saved_htm_bucket()

    for lvl in range(1,htm_bucket.get_num_levels()+1):
        print "Clusterising HTM TP lvl =="+str(lvl)
        htm_bucket.clusterize(lvl=lvl)



def learn_sound():
    rospy.init_node('learn_sound', log_level=rospy.INFO ,anonymous=True)

    #learning_time = LEARNING_TIME
    learning_time = 900

    #sound_dir = SOUND_DIR
    sound_dir = "/home/rdaneel/Kodama/Learning_DB/Learning/"


    learnLV1_learnLV2_sound(number_of_things_to_learn_lv1=40,
                            number_of_things_to_learn_lv2=8,
                            learning_data_dir=sound_dir,
                            htm_name_lvl1=HTM_SOUND_NAME_LVL1,
                            htm_name_lvl2=HTM_SOUND_NAME_LVL2,
                            learning_iterations=30,
                            learning_global_time=learning_time,
                            learning_error_accepted=LEARNING_ERROR_ACCEPTED,
                            errors_to_bare_in_mind=30,
                            erase_old_learned_data=True,
                            plot_error=True,
                            play_sound_files=True)

def learn_sound_time():
    rospy.init_node('learn_sound', log_level=rospy.INFO ,anonymous=True)

    #learning_time = LEARNING_TIME
    learning_time = 900

    #sound_dir = SOUND_DIR
    sound_dir = "/home/rdaneel/Kodama/Learning_DB/Learning/"


    learnLV1_learnLV2_sound_with_equal_time(number_of_things_to_learn_lv1=40,
                                            number_of_things_to_learn_lv2=8,
                                            learning_data_dir=sound_dir,
                                            htm_name_lvl1=HTM_SOUND_NAME_LVL1,
                                            htm_name_lvl2=HTM_SOUND_NAME_LVL2,
                                            learning_iterations=30,
                                            learning_global_time=learning_time,
                                            learning_error_accepted=LEARNING_ERROR_ACCEPTED,
                                            errors_to_bare_in_mind=30,
                                            erase_old_learned_data=True,
                                            plot_error=True,
                                            play_sound_files=True)



def learn_sound_time_lvlx():


    rospy.init_node('learn_sound_lvlx', log_level=rospy.INFO ,anonymous=True)

    learning_time = 900
    sound_dir = "/home/rdaneel/Kodama/Learning_DB/Learning/"
    number_of_things_to_learn_lvx_list = [160, 12, 4, 2]
    number_of_levels = len(number_of_things_to_learn_lvx_list)

    learnLVX_sound_with_equal_time(num_levels=number_of_levels,
                                   number_of_things_to_learn_lvx_list=number_of_things_to_learn_lvx_list,
                                   learning_data_dir=sound_dir,
                                   learning_global_time=learning_time,
                                   simultaneous_learning=False,
                                   errors_to_bare_in_mind=30,
                                   load_htm_from_files=False,
                                   plot_error=True,
                                   play_sound_files=True)

def learn_sound_with_clustering():


    rospy.init_node('learn_sound_lvlx_with_clusters', log_level=rospy.INFO ,anonymous=True)

    learning_time = 300
    sound_dir = "/home/rdaneel/Kodama/Learning_DB/Learning/"
    #number_of_things_to_learn_lvx_list = [160, 12, 4, 2]
    number_of_things_to_learn_lvx_list = [300, 50]
    number_of_sequences_to_learn_lvx_list = [50, 8]
    number_of_levels = len(number_of_things_to_learn_lvx_list)
    htm_bucket_name = HTMBUCKET_DEFAULT_NAME

    learnLVX_sound_with_equal_time_cluster_version(num_levels=number_of_levels,
                                                   number_of_things_to_learn_lvx_list=number_of_things_to_learn_lvx_list,
                                                   number_of_sequences_to_learn_lvx_list=number_of_sequences_to_learn_lvx_list,
                                                   learning_data_dir=sound_dir,
                                                   learning_global_time=learning_time,
                                                   simultaneous_learning=False,
                                                   htm_bucket_name=htm_bucket_name,
                                                   errors_to_bare_in_mind=30,
                                                   load_htm_from_files=False,
                                                   plot_error=True,
                                                   play_sound_files=True)




def learn_sound_time_simultaneous_lvlx():
    rospy.init_node('learn_sound_simultaneous', log_level=rospy.INFO ,anonymous=True)

    learning_time = 60
    sound_dir = "/home/rdaneel/Kodama/Learning_DB/Learning/"
    number_of_things_to_learn_lvx_list = [24, 12, 4, 2]
    number_of_levels = len(number_of_things_to_learn_lvx_list)

    learn_simultaneous_sound(num_levels=number_of_levels,
                                   number_of_things_to_learn_lvx_list=number_of_things_to_learn_lvx_list,
                                   learning_data_dir=sound_dir,
                                   learning_global_time=learning_time,
                                   simultaneous_learning=False,
                                   errors_to_bare_in_mind=30,
                                   load_htm_from_files=False,
                                   plot_error=True,
                                   play_sound_files=True)

def predict_sound():
    rospy.init_node('predict_sound', log_level=rospy.INFO, anonymous=True)

    rospy.loginfo("Started to predict")
    while not rospy.is_shutdown():
        predict_sound_LV1_LV2(htm_name_lvl1=HTM_SOUND_NAME_LVL1,
                              htm_name_lvl2=HTM_SOUND_NAME_LVL2)


def predict_lvlx_sound():
    rospy.init_node('predict_sound', log_level=rospy.INFO, anonymous=True)

    rospy.loginfo("Started to predict")
    while not rospy.is_shutdown():
        predict_lvlx_sound_LV1_LV2(num_levels=6)


def cluster_test():
    rospy.init_node('cluster_test', log_level=rospy.DEBUG, anonymous=True)

    rospy.loginfo("Started to cluster")
    cluster_htm_lvls(num_levels=1, htm_bucket_name=HTMBUCKET_DEFAULT_NAME)


def predict_with_clusters_test():
    rospy.init_node('predict_with_clusters', log_level=rospy.DEBUG, anonymous=True)

    rospy.loginfo("Started to predict")
    while not rospy.is_shutdown():
        predict_with_clusters_lvlx(num_levels=1, htm_bucket_name=HTMBUCKET_DEFAULT_NAME)


def htm_bucket_save_test():
    rospy.init_node('cluster_test', log_level=rospy.DEBUG, anonymous=True)

    rospy.loginfo("Create HTM Bucket")

    num_levels = 1
    number_of_things_to_learn_lvx_list = [8]
    number_of_sequences_to_learn_lvx_list = [3]
    errors_to_bare_in_mind = 30

    msg = rospy.wait_for_message("sound_sdr", internal_sound_sdr)
    array_sdr = msg.sdr_array
    length_encoded_array = len(array_sdr)

    htm_bucket_name = "test_bucket"

    htm_bucket_manage = HTMBucketManage(htm_bucket_name)
    htm_bucket_manage.clean_saved_htm_bucket()

    htm_bucket = HTMMacroObjectClusters(num_levels=num_levels,
                                        number_of_things_to_learn_lvx_list=number_of_things_to_learn_lvx_list,
                                        number_of_sequences_to_learn_lvx_list=number_of_sequences_to_learn_lvx_list,
                                        errors_to_bare_in_mind=errors_to_bare_in_mind,
                                        length_input_encoded_array=length_encoded_array,
                                        htm_bucket_name=htm_bucket_name)


    rospy.loginfo("Original HTM ############")
    print htm_bucket.get_bucket_name()
    print htm_bucket.get_number_thing_to_learn_dict()
    print htm_bucket.get_number_of_sequence_to_learn_list()

    rospy.loginfo("Saving HTM Bucket")
    htm_bucket_manage.save_htm_bucket(htm_bucket)

    rospy.loginfo("Retrieving HTM Bucket")
    new_htm_bucket = htm_bucket_manage.get_saved_htm_bucket()

    rospy.loginfo("NEW HTM ############")
    print new_htm_bucket.get_bucket_name()
    print new_htm_bucket.get_number_thing_to_learn_dict()
    print new_htm_bucket.get_number_of_sequence_to_learn_list()


if __name__ == '__main__':
    s = raw_input("Learn, LearnWithTime, LearnWithTimeXlevels, LearnSimulatneous, LearnByStepsWithClustering, Predict, PredictLevels, PredictLevelsWithClusters, ClusterTest, SaveHtmBucketTest [l,lt,lx,ls,lxc,p,px,pct,ct,save]")
    #s = "px"
    if s == "l":
        learn_sound()
    if s =="lt":
        learn_sound_time()
    if s == "p":
        predict_sound()
    if s == "px":
        predict_lvlx_sound()
    if s == "lx":
        learn_sound_time_lvlx()
    if s == "ls":
        learn_sound_time_simultaneous_lvlx()
    if s == "lxc":
        learn_sound_with_clustering()
    if s == "ct":
        cluster_test()
    if s == "pct":
        predict_with_clusters_test()
    if s == "save":
        htm_bucket_save_test()
    else:
        None
