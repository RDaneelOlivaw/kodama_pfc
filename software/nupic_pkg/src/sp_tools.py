import numpy
import os
import pprint
import random
import copy
from nupic.encoders.category import CategoryEncoder
#Spatial Pooler Example
from nupic.research.spatial_pooler import SpatialPooler


def learn_four_categories_example(number_of_learning_rounds):
    """
    Learns four categories without noise.
    :param number_of_learning_rounds:
    :return:
    """

    categories = ("cat", "dog", "monkey", "slow loris")
    # w is the number of bits that we want that encodes each category.
    # the bigger the number
    encoder = CategoryEncoder(w=3, categoryList=categories, forced=True)
    cat = encoder.encode("cat")
    dog = encoder.encode("dog")
    monkey = encoder.encode("monkey")
    loris = encoder.encode("slow loris")

    encoded_categories = (cat,)
    encoded_categories = encoded_categories + (dog,)
    encoded_categories = encoded_categories + (monkey,)
    encoded_categories = encoded_categories + (loris,)

    encoded_categories_dict = dict(zip(categories, encoded_categories))
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(encoded_categories_dict)

    print "cat =       ", encoded_categories_dict["cat"]
    print "dog =       ", encoded_categories_dict["dog"]
    print "monkey =    ", encoded_categories_dict["monkey"]
    print "slow loris =", encoded_categories_dict["slow loris"]

    number_of_things_to_learn = 4
    sp = SpatialPooler(inputDimensions=(len(cat),),
                   columnDimensions=(number_of_things_to_learn,),
                   potentialRadius=15,
                   numActiveColumnsPerInhArea=1,
                   globalInhibition=True,
                   synPermActiveInc=0.03,
                   potentialPct=1.0)

    # Now we see how each column is conected initialy in the SP.
    # we use xrange just for future efficiency in big ranges. Here we could use range.
    print_connected_synapses(sp, clear=True)
    #raw_input("Press any Key To Start Learning...>")

    # First output without any learn , expect from the one made when asked this ouput.
    output = numpy.zeros((number_of_things_to_learn,), dtype="int")
    sp.compute(cat, learn=True, activeArray=output)
    print output

    # Now we do the learning by input the patterns as many times as number_of_learning_rounds
    for _ in xrange(number_of_learning_rounds):
        sp.compute(cat, learn=True, activeArray=output)
        sp.compute(dog, learn=True, activeArray=output)
        sp.compute(monkey, learn=True, activeArray=output)
        sp.compute(loris, learn=True, activeArray=output)
        print_connected_synapses(sp, clear=True)

    return sp, output, encoded_categories_dict


def learn_four_categories_example_with_noise(number_of_learning_rounds):
    """
    Learns four categories WITH noise.
    :param number_of_learning_rounds:
    :return:
    """

    categories = ("cat", "dog", "monkey", "slow loris")
    # w is the number of bits that we want that encodes each category.
    # the bigger the number
    encoder = CategoryEncoder(w=3, categoryList=categories, forced=True)
    cat = encoder.encode("cat")
    dog = encoder.encode("dog")
    monkey = encoder.encode("monkey")
    loris = encoder.encode("slow loris")

    encoded_categories = (cat,)
    encoded_categories = encoded_categories + (dog,)
    encoded_categories = encoded_categories + (monkey,)
    encoded_categories = encoded_categories + (loris,)

    encoded_categories_dict = dict(zip(categories, encoded_categories))
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(encoded_categories_dict)

    print "cat =       ", encoded_categories_dict["cat"]
    print "dog =       ", encoded_categories_dict["dog"]
    print "monkey =    ", encoded_categories_dict["monkey"]
    print "slow loris =", encoded_categories_dict["slow loris"]

    number_of_things_to_learn = 4

    sp_noise = SpatialPooler(inputDimensions=(len(cat),),
                             columnDimensions=(number_of_things_to_learn,),
                             potentialRadius=15,
                             numActiveColumnsPerInhArea=1,
                             globalInhibition=True,
                             synPermActiveInc=0.03,
                             potentialPct=1.0)

    # Now we see how each column is conected initialy in the SP.
    # we use xrange just for future efficiency in big ranges. Here we could use range.

    print "#### WITH NOISE INIT ####"
    print_connected_synapses(sp_noise, clear=False)

    output = numpy.zeros((number_of_things_to_learn,), dtype="int")


    # Now we do the learning by input the patterns 200 times.
    for _ in xrange(number_of_learning_rounds):
        encoded_categories_dict_with_noise = input_noise_in_categories(encoded_categories_dict, extreme_noise=False)
        sp_noise.compute(encoded_categories_dict_with_noise["cat"], learn=True, activeArray=output)
        sp_noise.compute(encoded_categories_dict_with_noise["dog"], learn=True, activeArray=output)
        sp_noise.compute(encoded_categories_dict_with_noise["monkey"], learn=True, activeArray=output)
        sp_noise.compute(encoded_categories_dict_with_noise["slow loris"], learn=True, activeArray=output)
        print_connected_synapses(sp_noise, clear=True)


    pp = pprint.PrettyPrinter(indent=4)
    print "#######################"
    pp.pprint(encoded_categories_dict)
    print "#######################"
    pp.pprint(encoded_categories_dict_with_noise)
    print "#### WITH NOISE FINAL SP ####"
    print_connected_synapses(sp_noise, clear=False)


    output_categories_dict = correspondance_ouput_sp_categories_v2(sp_noise, encoded_categories_dict)

    # First we test with a model cat
    categ_model_encoded = monkey
    for categ, encoded_categ in encoded_categories_dict.items():
        if (categ_model_encoded == encoded_categ).all():
            model_name = categ
            print "MODEL NAME >>>" + str(model_name)

    print "#### MODEL "+model_name+" identify ####"
    sp_noise.compute(categ_model_encoded, learn=False, activeArray=output)
    print output
    category_name_model = get_category_pattern(output, output_categories_dict)
    print "Detected ==>" + str(category_name_model)

    category_name = copy.deepcopy(category_name_model)
    cat_slightly_noisy = copy.deepcopy(categ_model_encoded)
    # Now we add noise to cat encoding until it stops detecting cat
    number_of_degradations = 0
    while category_name == category_name_model and number_of_degradations <= len(cat_slightly_noisy):
        print "#### SLIGHTLY NOISY "+model_name+" identify ####"
        cat_slightly_noisy = input_degrade_to_numpy_array(cat_slightly_noisy, to_where=1)
        sp_noise.compute(cat_slightly_noisy, learn=False, activeArray=output)
        print output
        category_name = get_category_pattern(output, output_categories_dict)
        print "Detected ==>" + str(category_name)
        number_of_degradations += 1

    print "###### SUM UP #######"
    print ">"+model_name+" MODEL ENCODING ==> "
    print str(categ_model_encoded)
    print ">"+model_name+" DEGRADING THAT STOPPED BEING A "+model_name+" ==>"
    print str(cat_slightly_noisy)
    print "CATEGORY THAT IT CONFUSED WITH ==>" + str(category_name) + " after Degradations==" + str(number_of_degradations)

    value = encoded_categories_dict.get(category_name, "Nothing")
    print str(value)
    if value == "Nothing":
        raw_input("NOTHING FOUND... PRESS ENTER")

    print "SP LEARNED WITH NOISE"
    print_connected_synapses(sp_noise, clear=False)
    print "SP CATEGORIES ASOCIATED WITH EACH COLUMN"
    pp.pprint(output_categories_dict)
    print "###### ###### #######"

    return None


def spatial_pooler_example():
    """
    Learns Four categories with 200 learning rounds
    :return:
    """
    number_of_learning_rounds = 200

    sp, output, encoded_categories_dict = learn_four_categories_example(number_of_learning_rounds)

    print "###############"
    noisyCat = numpy.zeros((15,), dtype="uint32")
    noisyCat[3] = 1
    noisyCat[4] = 1
    # This is part of dog!
    noisyCat[6] = 1
    print noisyCat
    raw_input("Press any Key Input Noisy Cat...>")
    sp.compute(noisyCat, learn=False, activeArray=output)
    print output  # matches cat!
    correspondance_ouput_sp_categories(output, sp, encoded_categories_dict)

    print "###############"
    random_animal = numpy.random.randint(2, size=15)
    print "Random ANIMAL ==> " + str(random_animal)
    raw_input("Press any Key Input RANDOM ANIMAL...>")
    sp.compute(random_animal, learn=False, activeArray=output)
    print output
    correspondance_ouput_sp_categories(output, sp, encoded_categories_dict)


def learn_with_noise_example():
    """
    Learns without and with noise the same 4 categories
    :return:
    """
    number_of_learning_rounds = 200
    #We first learn without noise to be able to campare.
    learn_four_categories_example_with_noise(number_of_learning_rounds)

def correspondance_ouput_sp_categories(output, sp, encoded_categories_dict):
    """
    Prints the category that corresponds to the output given based on the representation in the SP.
    :param output: NumpyArray with dimension columnDimension of the Sp
    :param sp:Spatial Pooler with the knowledge
    :param encoded_categories_dict: Dictionary that connects names to the encoding
    :return:
    """
    categories_detected = []
    # We fetch for the column that gives 1, is active
    itemindex = numpy.where(output == 1)
    #We iterate in the first dimension because all of our inputs are 1 dimension for the moment
    print "### Encoded DB ###"
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(encoded_categories_dict)

    for index in itemindex[0]:
        connected = numpy.zeros((sp.getInputDimensions()[0],), dtype="int")
        # Access to the element number index in the sp
        sp.getConnectedSynapses(index, connected)

        # We search for that encoded array in connected in the dictionary of categories.
        for categ, encoded_categ in encoded_categories_dict.iteritems():
            # We compare .all() because numpy Arrays have the array pluss the dtype
            print "Conected synaps ==" + str(connected)
            print "Encoded synaps ==" + str(encoded_categ)
            if (encoded_categ == connected).all():
                print "Knowledge SP =====" + str(connected)
                categories_detected.append(categ)
            else:
                print "NO"

    print "Categories Detected ==> " + str(categories_detected)
    return categories_detected


def correspondance_ouput_sp_categories_v2(sp, encoded_categories_dict):
    """
    Based on the observation that we need someway of linking the model patterns to the
    ones memorised by the pool, we need to iterate in sp with the model patterns and
    generate a correspondance between the outputs of sp.compute(pattern) and the model patterns.
    Because the patterns in th sp aren't equal to the model patterns.They are only equal when
    the data feeded is exclusevely models, without any kind of noise.
    :param sp:
    :param encoded_categories_dict:
    :return:output_categories_dict
    """
    output = numpy.zeros((sp.getColumnDimensions(),), dtype="int")
    output_categories_dict = {}

    for categ, encoded_categ in encoded_categories_dict.iteritems():
        print "Model Data Feeding:"
        print "Name Category dict ==>" + str(categ)
        print "Encoding Category dict ==>" + str(encoded_categ)
        sp.compute(encoded_categ, learn=False, activeArray=output)
        itemindex = numpy.where(output == 1)
        print itemindex[0][0]
        output_categories_dict[itemindex[0][0]] = categ

    print "Output Category Mapping"
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(output_categories_dict)

    return output_categories_dict


def get_category_pattern(output, output_categories_dict):
    """
    Returns the category name in the output_categories_dict by the index of the 1 in ouput.
    :param output:
    :param output_categories_dict:
    :return:
    """
    itemindex = numpy.where(output == 1)
    print "ITEM INDEX IN GET CATEGORY PATTERN==>"+str(itemindex)
    assert (len(itemindex[0]) == 1), "Something went wrong in the itemindex get category pattern"
    # Because it has the structure numpy array, so we have to get the list array and then the element in it.
    item = itemindex[0][0]
    print "ITEM ==>"+str(item)

    value = output_categories_dict.get(item, "NoCategory")

    return value


def all_indices(value, qlist):
    """
    Use --> all_indices("foo", ["foo","bar","baz","foo"])
    :param value:
    :param qlist:
    :return:
    """
    indices = []
    idx = -1
    while True:
        try:
            idx = qlist.index(value, idx+1)
            indices.append(idx)
        except ValueError:
            break
    return indices


def print_connected_synapses(sp, clear):
    """
    Prints the state of all the columns of the SpatialPooler.
    :param sp:Spatial Pooler Object
    :return:
    """
    if clear:
        os.system('clear')

    for column in xrange(sp.getColumnDimensions()[0]):
        connected = numpy.zeros((sp.getInputDimensions()[0],), dtype="int")
        sp.getConnectedSynapses(column, connected)
        numpy.set_printoptions(threshold=numpy.nan)
        print connected

    return None


def getAllConnectedSynapses(sp):
    """
    Returns All conected sinapses
    :param sp:
    :return:
    """
    conected_list = []
    for column in xrange(sp.getColumnDimensions()[0]):
        connected = numpy.zeros((sp.getInputDimensions()[0],), dtype="int")
        sp.getConnectedSynapses(column, connected)
        conected_list.append(connected)

    return conected_list


def input_noise_example(rounds, extreme_noise):
    """
    Inpuyts noise in four categories
    :param rounds:
    :return:
    """

    categories = ("cat", "dog", "monkey", "slow loris")
    # w is the number of bits that we want that encodes each category.
    # the bigger the number
    encoder = CategoryEncoder(w=3, categoryList=categories, forced=True)
    cat = encoder.encode("cat")
    dog = encoder.encode("dog")
    monkey = encoder.encode("monkey")
    loris = encoder.encode("slow loris")

    encoded_categories = (cat,)
    encoded_categories = encoded_categories + (dog,)
    encoded_categories = encoded_categories + (monkey,)
    encoded_categories = encoded_categories + (loris,)

    encoded_categories_dict = dict(zip(categories, encoded_categories))
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(encoded_categories_dict)

    print "cat =       ", encoded_categories_dict["cat"]
    print "dog =       ", encoded_categories_dict["dog"]
    print "monkey =    ", encoded_categories_dict["monkey"]
    print "slow loris =", encoded_categories_dict["slow loris"]

    for _ in xrange(rounds):
        encoded_categories_dict_with_noise = input_noise_in_categories(encoded_categories_dict, extreme_noise)
        pp.pprint(encoded_categories_dict_with_noise)


def input_noise_in_categories(encoded_categories_dict, extreme_noise):
    """
    Inputs Noise in an encoded categories dict
    :param encoded_categories_dict:
    :param extreme_noise: Boolean that indicates if we want only a few bits change or totaly random change.
    :return:encoded_categories_dict_with_noise
    """
    encoded_categories_dict_with_noise = copy.deepcopy(encoded_categories_dict)

    for element in encoded_categories_dict_with_noise:
        array_element = encoded_categories_dict_with_noise[element]
        print "#### "+element+" ####"
        print array_element
        print "NOISE INPUT"
        if extreme_noise:
            print "EXTREME NOISE"
            random_number = random.randint(0, len(array_element)-1)
        else:
            # We only change a quarter of the bits.
            random_number = (len(array_element)-1) / 4

        print "How many bit will change == " + str(random_number)

        for _ in xrange(random_number):
            random_number = random.randint(0, len(array_element)-1)
            random_binary_number = random.randint(0, 1)
            array_element[random_number] = random_binary_number

        encoded_categories_dict_with_noise[element] = array_element
        print encoded_categories_dict_with_noise[element]
        print "########"

    return encoded_categories_dict_with_noise


def input_noise_to_numpy_array(numpy_array_element, bits_changed):
    """
    Inverts a number of bits in random positions, number of which depend on bits_changed.
    :param bits_changed:
    :return:
    """
    print "How many bit will change == " + str(bits_changed)
    assert (len(numpy_array_element) >= bits_changed), "Asking to change more bits than length of array"
    print "OLD ARRAY ==>" + str(numpy_array_element)

    for _ in xrange(bits_changed):
        random_number = random.randint(0, len(numpy_array_element)-1)
        random_binary_number = random.randint(0, 1)
        old_value = numpy_array_element[random_number]
        if old_value == 1:
            new_value = 0
        else:
            new_value = 1

        numpy_array_element[random_number] = new_value

    print "NEW ARRAY ==>" + str(numpy_array_element)

    return numpy_array_element


def input_degrade_to_numpy_array(numpy_array_element, to_where=1):
    """
    Degrades an array by changing one zero to a one or viceversa, depending on the variable to_where.
    if to_where = 1, then all will tend to be 1.
    :param numpy_array_element: Only the array, no type
    :param to_where:
    :return:
    """
    i = 0
    not_change_bit = True
    print "OLD ARRAY ==>" + str(numpy_array_element)
    while i < len(numpy_array_element) and not_change_bit:
        old_value = numpy_array_element[i]
        # If the value is not what you ask, you change it.
        if old_value != to_where:
            numpy_array_element[i] = to_where
            not_change_bit = False
        i += 1

    print "NEW ARRAY ==>" + str(numpy_array_element)
    return numpy_array_element
