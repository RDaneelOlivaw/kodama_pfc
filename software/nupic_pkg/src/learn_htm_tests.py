#!/usr/bin/env python
import os
import rospy
from internal_sound_pkg.msg import internal_sound_sdr
from nupic_pkg.msg import graph
import time
from play_wav import get_sound_list_dir, PlayWav
import copy
import numpy
from nupic.research.TP import TP
from sp_tools import print_connected_synapses
from nupic.research.spatial_pooler import SpatialPooler
from encoding_tools import create_4_encoded_animal_categories
import timeit
import pickle
import easygui
import rospkg


# Time that will be inputing sound data encoded into csv file
LEARNING_TIME = 300
SOUND_DIR = "/home/rdaneel/playground/sound_tests/sounds/"
PICKLE_HTM = "pickle_files/htm.p"
PICKLE_HTM_BACKUP = "pickle_files/htm_backup.p"

RATE_INIT_PUBLISH = 2
INIT_TIME = 15
INIT_I = INIT_TIME*RATE_INIT_PUBLISH

class HTM(object):
    def __init__(self,number_of_things_to_learn, inputDimensionsSP, numberOfColsTP, errors_to_bare_in_mind, htmlvl_name):

        rospy.loginfo("Start HTM INIT")

        self.sp = SpatialPooler(inputDimensions=(inputDimensionsSP,),
                   columnDimensions=(number_of_things_to_learn,),
                   potentialRadius=15,
                   numActiveColumnsPerInhArea=1,
                   globalInhibition=True,
                   synPermActiveInc=0.03,
                   potentialPct=1.0)
        # Now we see how each column is conected initialy in the SP.
        # we use xrange just for future efficiency in big ranges. Here we could use range.
        #print_connected_synapses(self.sp, clear=True)

        # Step 1: create Temporal Pooler instance with appropriate parameters
        self.tp = TP(numberOfCols=numberOfColsTP, cellsPerColumn=4,
                initialPerm=0.5, connectedPerm=0.5,
                minThreshold=3, newSynapseCount=10,
                permanenceInc=0.1, permanenceDec=0.0,
                activationThreshold=1,
                globalDecay=0, burnIn=1,
                checkSynapseConsistency=False,
                pamLength=10)

        self.output = numpy.zeros((number_of_things_to_learn,), dtype="int")
        self.ouput_dict = {}
        self.next_key_name_number = 0
        self.vis = Visualizations()
        self.predictedIntPreviousTimestep = numpy.zeros((number_of_things_to_learn,), dtype="int")
        self.anomaly_list = [1]*errors_to_bare_in_mind
        # We make windows that are a 10% of the total error memory
        self.window = errors_to_bare_in_mind/10
        self.ema_error = []
        self.average_error = 1

        # get an instance of RosPack with the default search paths
        rospack = rospkg.RosPack()
        # get the file path for rospy_tutorials
        pkg_path = rospack.get_path('nupic_pkg')
        path_to_htm_pickle = os.path.join(pkg_path, "src/pickle_files/"+str(htmlvl_name))
        self._pickle_htm_name = path_to_htm_pickle+".p"
        self._pickle_htm_name_backup = path_to_htm_pickle+"_backup.p"

        rospy.loginfo("PICKLE FILE ==>"+str(self._pickle_htm_name))
        rospy.loginfo("PICKLE FILE ==>"+str(self._pickle_htm_name_backup))

        rospy.loginfo("End HTM INIT")

    def learning_algorith(self,element,key,debug=False):
        """

        :param element:SDR input data
        :param key: Identifier for the patern
        :return:
        """
        # First learn SP
        rospy.loginfo("###### START #######")
        rospy.loginfo("KEY INPUT TO SP =>"+str(key))
        rospy.loginfo("ELEMENT INPUT TO SP =>"+str(element))

        self.sp.compute(element, learn=True, activeArray=self.output)
        if debug:
            print_connected_synapses(self.sp, clear=False)

        rospy.loginfo("DATA")
        found = False
        for value in self.ouput_dict.values():
            rospy.loginfo("OUTPUT SP =>"+str(self.output))
            rospy.loginfo("VALUE DICT==>"+str(value))

            c1 = (str(self.output) == str(value))
            rospy.loginfo("ARE THEY THE SAME? = "+str(c1))
            if c1:
                rospy.loginfo("This group is already in outputdict.")
                found = True
                break

        if not found:
            rospy.loginfo("NEW group")
            key_new = "G-"+str(self.next_key_name_number)
            self.ouput_dict[key_new] = copy.deepcopy(self.output)
            self.next_key_name_number += 1

        # The compute method performs one step of learning and/or inference. Note:
        # here we just perform learning but you can perform prediction/inference and
        # learning in the same step if you want (online learning).
        #self.tp.compute(self.output, enableLearn=True, computeInfOutput=False)
        self.tp.compute(self.output, enableLearn=True, computeInfOutput=True)


        actualInt = self.output
        predictedInt = self.tp.getPredictedState().max(axis=1)

        ema, av_err = self.error_calculations(actualInt, predictedInt)
        rospy.loginfo("EMA ==>"+str(ema))
        rospy.loginfo("AV ERROR =="+str(av_err))

        # This function prints the segments associated with every cell.$$$$
        # If you really want to understand the TP, uncomment this line. By following
        # every step you can get an excellent understanding for exactly how the TP
        # learns.
        if debug:
            self.tp.printCells()


    def error_calculations(self, actualInt, predictedInt):
        rospy.logdebug("ACTUAL INT ==>"+str(actualInt))
        rospy.logdebug("PREDICTED INT ==>"+str(predictedInt))

        compare = self.vis.compareArray(actualInt, self.predictedIntPreviousTimestep)
        anomaly = self.vis.calcAnomaly(actualInt, self.predictedIntPreviousTimestep)

        rospy.logdebug("ANOMALY ==>"+str(anomaly))
        rospy.logdebug("." . join(compare))
        rospy.logdebug(self.vis.hashtagAnomaly(anomaly))

        self.anomaly_list.pop(0)
        self.anomaly_list.append(anomaly)

        rospy.logdebug("ANOMALY LIST ==>"+str(self.anomaly_list))

        self.ema_error = self.ExpMovingAverage(values=self.anomaly_list, window=self.window)
        self.average_error = self.Average(self.ema_error)

        # We save it for the next one
        self.predictedIntPreviousTimestep = predictedInt

        return self.ema_error, self.average_error


    def learning_algorith_withoutsp(self,element):
        """

        :param element:SDR input data
        :param key: Identifier for the patern
        :return:
        """

        # The compute method performs one step of learning and/or inference. Note:
        # here we just perform learning but you can perform prediction/inference and
        # learning in the same step if you want (online learning).
        self.tp.compute(element, enableLearn=True, computeInfOutput=False)

        # This function prints the segments associated with every cell.$$$$
        # If you really want to understand the TP, uncomment this line. By following
        # every step you can get an excellent understanding for exactly how the TP
        # learns.
        #self.tp.printCells()

    def reset_tp(self):
        self.tp.reset()

    def print_ouput_dict(self):
        print self.ouput_dict
        print "Length =="+str(len(self.ouput_dict))
        print self.ouput_dict.keys()

    # Utility routine for printing the input vector
    def formatRow(self, x):
        s = ''
        for c in range(len(x)):
            if c > 0 and c % 10 == 0:
                s += ' '
            s += str(x[c])
        s += ' '
        return s

    def predict_test(self):
        for key, element in self.ouput_dict.items():
            print "\n\n--------"+str(key)+"-----------"
            print "Raw input vector\n", element


            # Send each vector to the TP, with learning turned off
            self.tp.compute(element, enableLearn=False, computeInfOutput=True)

            # This method prints out the active state of each cell followed by the
            # predicted state of each cell. For convenience the cells are grouped
            # 10 at a time. When there are multiple cells per column the printout
            # is arranged so the cells in a column are stacked together
            #
            # What you should notice is that the columns where active state is 1
            # represent the SDR for the current input pattern and the columns where
            # predicted state is 1 represent the SDR for the next expected pattern
            print "\nAll the active and predicted cells:"
            self.tp.printStates(printPrevious=False, printLearnState=False)

            # tp.getPredictedState() gets the predicted cells.
            # predictedCells[c][i] represents the state of the i'th cell in the c'th
            # column. To see if a column is predicted, we can simply take the OR
            # across all the cells in that column. In numpy we can do this by taking
            # the max along axis 1.
            print "\n\nThe following columns are predicted by the temporal pooler. This"
            print "should correspond to columns in the *next* item in the sequence."
            predictedCells = self.tp.getPredictedState()
            print self.formatRow(predictedCells.max(axis=1).nonzero())

    def predict(self,data):

        rospy.logdebug("DATA=="+str(data))
        self.sp.compute(data, learn=True, activeArray=self.output)

        rospy.logdebug("SP OUTPUT =="+str(self.output))
        # Send each vector to the TP, with learning turned off
        self.tp.compute(self.output, enableLearn=False, computeInfOutput=True)

        # This method prints out the active state of each cell followed by the
        # predicted state of each cell. For convenience the cells are grouped
        # 10 at a time. When there are multiple cells per column the printout
        # is arranged so the cells in a column are stacked together
        #
        # What you should notice is that the columns where active state is 1
        # represent the SDR for the current input pattern and the columns where
        # predicted state is 1 represent the SDR for the next expected pattern
        rospy.logdebug("\nAll the active and predicted cells:")
        self.tp.printStates(printPrevious=False, printLearnState=False)

        # tp.getPredictedState() gets the predicted cells.
        # predictedCells[c][i] represents the state of the i'th cell in the c'th
        # column. To see if a column is predicted, we can simply take the OR
        # across all the cells in that column. In numpy we can do this by taking
        # the max along axis 1.
        rospy.logdebug("\n\nThe following columns are predicted by the temporal pooler. This")
        rospy.logdebug("should correspond to columns in the *next* item in the sequence.")
        predictedCells = self.tp.getPredictedState()
        result_predicted = predictedCells.max(axis=1)
        predicted_group_index = self.formatRow(result_predicted.nonzero())
        rospy.loginfo("PREDICTED CELLS MAX AXIS 1=="+str(result_predicted))
        rospy.logdebug("PREDICTED CELLS ==>"+str(predictedCells))

        return self.output, result_predicted


    def movingaverage(self,values,window):
        weigths = numpy.repeat(1.0, window)/window
        smas = numpy.convolve(values, weigths, 'valid')
        return smas # as a numpy array


    def ExpMovingAverage(self,values, window):
        """
        http://pythonprogramming.net/advanced-matplotlib-graphing-charting-tutorial/
        """
        weights = numpy.exp(numpy.linspace(-1., 0., window))
        weights /= weights.sum()
        a =  numpy.convolve(values, weights, mode='full')[:len(values)]
        a[:window] = a[window]
        return a

    def Average(self, values):
        return numpy.mean(values)


    def save_htm(self):
        pickle.dump(self, open(self._pickle_htm_name, "wb"))
        rospy.loginfo("HTM File Saved="+str(self._pickle_htm_name))
        return self._pickle_htm_name

    def save_htm_backup(self):
        pickle.dump(self, open(self._pickle_htm_name_backup, "wb"))
        rospy.loginfo("HTM File Saved="+str(self._pickle_htm_name_backup))

    def get_saved_htm(self):
        try:
            htm = pickle.load(open(self._pickle_htm_name, "rb"))
            rospy.loginfo("Pickle File FOUND, retrieving=="+str(self._pickle_htm_name))
            return htm
        except:
            rospy.loginfo("Pickle File not found=="+str(self._pickle_htm_name))
            open(self._pickle_htm_name, 'a').close()
            return None

    def clean_saved_htm(self):
        try:
            os.remove(self._pickle_htm_name)
            rospy.loginfo("HTM File cleaned="+str(self._pickle_htm_name))
        except:
            rospy.loginfo("Pickle File not found=="+str(self._pickle_htm_name))

class Visualizations:

  def calcAnomaly(self, actual, predicted):
    """
    Calculates the anomaly of two SDRs

    Uses the equation presented on the wiki:
    https://github.com/numenta/nupic/wiki/Anomaly-Score-Memo

    To put this in terms of the temporal pooler:
      A is the actual input array at a given timestep
      P is the predicted array that was produced from the previous timestep(s)
      [A - (A && P)] / [A]
    Rephrasing as questions:
      What bits are on in A that are not on in P?
      How does that compare to total on bits in A?

    Outputs 0 is there's no difference between P and A.
    Outputs 1 if P and A are totally distinct.

    Not a perfect metric - it doesn't credit proximity
    Next step: combine with a metric for a spatial pooler
    """
    combined = numpy.logical_and(actual, predicted)
    delta = numpy.logical_xor(actual,combined)
    delta_score = sum(delta)
    actual_score = float(sum(actual))
    return delta_score / actual_score


  def compareArray(self, actual, predicted):
    """
    Produce an array that compares the actual & predicted

    'A' - actual
    'P' - predicted
    'E' - expected (both actual & predicted
    ' ' - neither an input nor predicted
    """
    compare = []
    for i in range(actual.size):
      if actual[i] and predicted[i]:
        compare.append('E')
      elif actual[i]:
        compare.append('A')
      elif predicted[i]:
        compare.append('P')
      else:
        compare.append(' ')
    return compare


  def hashtagAnomaly(self, anomaly):
    """
    Basic printout method to visualize the anomaly score (scale: 1 - 50 #'s)
    """
    hashcount = '#'
    for i in range(int(anomaly / 0.02)):
      hashcount += '#'
    for j in range(int((1 - anomaly) / 0.02)):
      hashcount += '.'
    return hashcount


def learn_test():

    encoded_categories_dict, length_encoded_array = create_4_encoded_animal_categories()

    learning_iterations = 30
    number_of_things_to_learn = 4
    length_encoded_categories_dict = len(encoded_categories_dict)

    htm_object = HTM(number_of_things_to_learn, length_encoded_array,length_encoded_categories_dict)
    # We repeat the sequence 10 times
    for i in range(learning_iterations):

        # Send each letter in the sequence in order
        for _ in xrange(10):
            for key, element in encoded_categories_dict.items():

                htm_object.learning_algorith(element,key,debug=False)
                #htm_object.learning_algorith_withoutsp(element)
            # The reset command tells the TP that a sequence just ended and essentially
            # zeros out all the states. It is not strictly necessary but it's a bit
            # messier without resets, and the TP learns quicker with resets.
        htm_object.reset_tp()

    htm_object.print_ouput_dict()


def learn_test_with_error_index():
    rospy.init_node('learn_test_with_error_index', anonymous=True)

    encoded_categories_dict, length_encoded_array = create_4_encoded_animal_categories()

    learning_iterations = 30
    number_of_things_to_learn = 4
    length_encoded_categories_dict = len(encoded_categories_dict)
    errors_to_bare_in_mind = 30

    htm_object = HTM(number_of_things_to_learn, length_encoded_array,length_encoded_categories_dict,errors_to_bare_in_mind)
    # We repeat the sequence 10 times
    for i in range(learning_iterations):

        # Send each letter in the sequence in order
        for _ in xrange(10):
            for key, element in encoded_categories_dict.items():

                htm_object.learning_algorith(element,key,debug=False)
                if htm_object.average_error <= 0.05:
                    break
            # The reset command tells the TP that a sequence just ended and essentially
            # zeros out all the states. It is not strictly necessary but it's a bit
            # messier without resets, and the TP learns quicker with resets.
            if htm_object.average_error <= 0.05:
                break
        htm_object.reset_tp()
        if htm_object.average_error <= 0.05:
            break

    htm_object.print_ouput_dict()


def learn_and_predict_test():

    encoded_categories_dict, length_encoded_array = create_4_encoded_animal_categories()

    learning_iterations = 30
    number_of_things_to_learn = 4
    length_encoded_categories_dict = len(encoded_categories_dict)

    htm_object = HTM(number_of_things_to_learn, length_encoded_array,length_encoded_categories_dict,learning_iterations)
    raw_input("Continue...")
    # We repeat the sequence 10 times
    for i in range(learning_iterations):

        # Send each letter in the sequence in order
        for _ in xrange(10):
            for key, element in encoded_categories_dict.items():

                htm_object.learning_algorith(element,key,debug=False)
                #htm_object.learning_algorith_withoutsp(element)
            # The reset command tells the TP that a sequence just ended and essentially
            # zeros out all the states. It is not strictly necessary but it's a bit
            # messier without resets, and the TP learns quicker with resets.
        htm_object.reset_tp()

    htm_object.print_ouput_dict()

    for _ in xrange(3):
        for key, element in encoded_categories_dict.items():
            print "#### START PREDICTION ###"
            print key
            print element
            htm_object.predict(element)
            print "#### END PREDICTION ####"



def update_msg(msg, value):
    """
    Updates the values of the msg based on the value
    :param msg:
    :param ir_object:
    :return:
    """
    msg.y_value = value
    return msg


def start_error_ploting_engine(pub_lv1, pub_lv2):
    msg_error_lv1 = graph()
    msg_error_lv2 = graph()

    msg_error_lv1 = update_msg(msg=msg_error_lv1, value=0)
    msg_error_lv2 = update_msg(msg=msg_error_lv2, value=0)


    rate = rospy.Rate(RATE_INIT_PUBLISH)
    i = 0
    while not rospy.is_shutdown() and i<INIT_I:
        rospy.loginfo("Initialazing Plots of Error..."+str(i))
        msg_error_lv1 = update_msg(msg=msg_error_lv1, value=0)
        msg_error_lv2 = update_msg(msg=msg_error_lv2, value=0)
        pub_lv1.publish(msg_error_lv1)
        pub_lv2.publish(msg_error_lv2)
        rate.sleep()
        i += 1

    return msg_error_lv1, msg_error_lv2

def learnLV1_learnLV2_test(plot_error=False, plot_predictions=False):
    rospy.init_node('learn_test_with_error_index', anonymous=True)

    if plot_error:
        pub_lv1 = rospy.Publisher('graph_data_lv1', graph, queue_size=1)
        pub_lv2 = rospy.Publisher('graph_data_lv2', graph, queue_size=1)
        msg_error_lv1, msg_error_lv2 = start_error_ploting_engine(pub_lv1, pub_lv2)

    encoded_categories_dict, length_encoded_array = create_4_encoded_animal_categories()

    learning_iterations = 30
    number_of_things_to_learn = 4
    length_encoded_categories_dict = len(encoded_categories_dict)
    errors_to_bare_in_mind = 30

    htm_object = HTM(number_of_things_to_learn, length_encoded_array,length_encoded_categories_dict,errors_to_bare_in_mind)
    # We repeat the sequence 10 times
    for i in range(learning_iterations):

        # Send each letter in the sequence in order
        for _ in xrange(10):
            for key, element in encoded_categories_dict.items():

                htm_object.learning_algorith(element,key,debug=False)

                if plot_error:
                    update_msg(msg=msg_error_lv1, value=htm_object.average_error)
                    pub_lv1.publish(msg_error_lv1)

                if htm_object.average_error <= 0.05:
                    print "OUT of Loop 1"
                    break
            # The reset command tells the TP that a sequence just ended and essentially
            # zeros out all the states. It is not strictly necessary but it's a bit
            # messier without resets, and the TP learns quicker with resets.
            if htm_object.average_error <= 0.05:
                print "OUT of Loop 2"
                break
        htm_object.reset_tp()
        if htm_object.average_error <= 0.05:
            print "OUT of Loop 3"
            break

    htm_object.print_ouput_dict()

    encoded_categories_dict, length_encoded_array = create_4_encoded_animal_categories()
    length_encoded_array_lv2 = number_of_things_to_learn
    number_of_things_to_learn_lv2 = 2
    length_encoded_categories_dict_lv2 = number_of_things_to_learn_lv2

    htm_object_lv2 = HTM(number_of_things_to_learn=number_of_things_to_learn_lv2,
                         inputDimensionsSP=length_encoded_array_lv2,
                         numberOfColsTP=length_encoded_categories_dict_lv2,
                         errors_to_bare_in_mind=errors_to_bare_in_mind)

    raw_input("@@@@@                  Continue to fase 2...")

    # We repeat the sequence 10 times
    for i in range(learning_iterations):

        # Send each letter in the sequence in order
        for _ in xrange(10):
            for key, element in encoded_categories_dict.items():

                print "#### START PREDICTION LV 1 ###"
                print "KEY LV1 =="+str(key)
                print "INPUT LV1 =="+str(element)
                sp_output, predicted_data = htm_object.predict(element)
                print "#### END PREDICTION LV 1 ####"
                print "INPUT LV2 =="+str(sp_output)
                htm_object_lv2.learning_algorith(sp_output,key="Rubish",debug=False)

                if plot_error:
                    update_msg(msg=msg_error_lv2, value=htm_object_lv2.average_error)
                    pub_lv2.publish(msg_error_lv2)

                if htm_object_lv2.average_error <= 0.05:
                    print "OUT of Loop 1"
                    break
            # The reset command tells the TP that a sequence just ended and essentially
            # zeros out all the states. It is not strictly necessary but it's a bit
            # messier without resets, and the TP learns quicker with resets.
            if htm_object_lv2.average_error <= 0.05:
                print "OUT of Loop 2"
                break
        htm_object_lv2.reset_tp()
        if htm_object_lv2.average_error <= 0.05:
            print "OUT of Loop 3"
            break

    htm_object_lv2.print_ouput_dict()

    raw_input("@@@@   Prediction LV1+LV2  Continue to Final Fase...")
    for _ in xrange(3):
        for key, element in encoded_categories_dict.items():
            print "#### START PREDICTION LV 1 ###"
            print "KEY LV1 =="+str(key)
            print "INPUT LV1 =="+str(element)
            sp_data_lv1, predicted_data = htm_object.predict(element)
            print "PREDICTED LV1 =="+str(predicted_data)
            print "OUTPUT LV1 =="+str(sp_data_lv1)
            print "#### END PREDICTION LV 1 ####"
            print "INPUT LV2 =="+str(sp_data_lv1)
            sp_data_lv2, predicted_data = htm_object_lv2.predict(sp_data_lv1)
            print "PREDICTED LV2 =="+str(predicted_data)
            print "OUTPUT LV2 =="+str(sp_data_lv2)
            print "#### END PREDICTION ####"


def listener():

    rospy.init_node('learn_htm', anonymous=True)

    lst_songs = get_sound_list_dir(SOUND_DIR)
    assert len(lst_songs)>0, "Empty Sound Dir"
    pw = PlayWav(lst_songs)
    pw.play_next_song()
    start_time = time.time()

    def callback(data):
        rospy.loginfo(rospy.get_caller_id() + "SDR = %s", data.sdr_array)

        song_ended = pw.check_if_ended()
        print "SONG ENDED?=="+str(song_ended)

        if not song_ended:
            if pw.play_next_song():
                print "Playing Next Song"
            else:
                rospy.signal_shutdown("No more songs to learn")

        if time.time()-start_time> LEARNING_TIME:
            rospy.signal_shutdown("Passed the Record Time")


    rospy.Subscriber("sound_sdr", internal_sound_sdr, callback)
    rospy.loginfo("Start WRITING SDR to CSV file...")
    rospy.spin()


def generate_category_dict(number_of_things_to_learn):

    id = "ID-"
    categories = (id+str(1),)
    for i in range(2,number_of_things_to_learn+1):
        categories = categories + (id+str(i),)
    encoded_categories_dict = dict(zip(categories, categories))
    return encoded_categories_dict


def learner(number_of_things_to_learn):

    clean_saved_htm()

    rospy.init_node('learn_htm', anonymous=True)

    learning_time = time.time()


    msg = rospy.wait_for_message("sound_sdr", internal_sound_sdr)
    array_sdr = msg.sdr_array
    length_encoded_array = len(array_sdr)

    learning_iterations = 30

    encoded_categories_dict = generate_category_dict(number_of_things_to_learn)
    length_encoded_categories_dict = len(encoded_categories_dict)

    htm_object = HTM(number_of_things_to_learn, length_encoded_array, length_encoded_categories_dict)


    def callback(data):
        #rospy.loginfo(rospy.get_caller_id() + "SDR = %s", data.sdr_array)
        element = numpy.asarray(data.sdr_array)

        start_time = timeit.default_timer()
        htm_object.learning_algorith(element,key=str(time.time()))
        elapsed = timeit.default_timer() - start_time
        rospy.loginfo("Time for learn ==> "+str(elapsed))


        if time.time()-learning_time> LEARNING_TIME:
            rospy.loginfo("Time learning ==> "+str(time.time()-learning_time))
            htm_object.reset_tp()
            htm_object.print_ouput_dict()
            save_htm(htm_object)
            easygui.msgbox("Learning Time Ended", title="Kodama Learning")
            rospy.signal_shutdown("Passed the Record Time")


    rospy.Subscriber("sound_sdr", internal_sound_sdr, callback)
    rospy.loginfo("Start Learning...")
    rospy.spin()


def learner_with_sound_mng(number_of_things_to_learn, learning_data_dir, loop_data_times, reset=False):

    rospy.init_node('learn_htm', anonymous=True)

    lst_songs = get_sound_list_dir(learning_data_dir)
    assert len(lst_songs)>0, "Empty Sound Dir"
    pw = PlayWav(lst_songs)

    learning_time = time.time()

    msg = rospy.wait_for_message("sound_sdr", internal_sound_sdr)
    array_sdr = msg.sdr_array
    length_encoded_array = len(array_sdr)

    encoded_categories_dict = generate_category_dict(number_of_things_to_learn)
    length_encoded_categories_dict = len(encoded_categories_dict)

    if reset:
        rospy.loginfo("Erasing Memory HTM...")
        clean_saved_htm()
    else:
        rospy.loginfo("Retrieving Memory HTM...")
    htm_object = get_saved_htm()
    if not htm_object:
        # If ther isnt any data, we create a new one
        htm_object = HTM(number_of_things_to_learn, length_encoded_array, length_encoded_categories_dict)
    pw.play_next_song()

    def callback(data):
        #rospy.loginfo(rospy.get_caller_id() + "SDR = %s", data.sdr_array)
        element = numpy.asarray(data.sdr_array)

        htm_object.learning_algorith(element,key=str(time.time()))

        song_ended = pw.check_if_ended()
        print "SONG ENDED?=="+str(song_ended)

        if not song_ended:
            if pw.play_next_song():
                print "Playing Next Song"
            else:
                if pw.get_start_again_times() < loop_data_times:
                    pw.start_again()
                else:
                    htm_object.reset_tp()
                    htm_object.print_ouput_dict()
                    save_htm(htm_object)
                    easygui.msgbox("Start Predict Test", title="Kodama Learning")
                    htm_object.predict_test()
                    #save_htm_backup(htm_object)
                    easygui.msgbox("Learning Material Ended", title="Kodama Learning")
                    rospy.signal_shutdown("No more songs to learn")

        rospy.loginfo("Time learning ==> "+str(time.time()-learning_time))

        if time.time()-learning_time> LEARNING_TIME:
            rospy.loginfo("Time learning ==> "+str(time.time()-learning_time))
            htm_object.reset_tp()
            htm_object.print_ouput_dict()
            save_htm(htm_object)
            #save_htm_backup(htm_object)
            easygui.msgbox("Learning Time Ended", title="Kodama Learning")
            rospy.signal_shutdown("Passed the Record Time")


    rospy.Subscriber("sound_sdr", internal_sound_sdr, callback)
    rospy.loginfo("Start Learning...")
    rospy.spin()



def pickle_data_test(number_of_things_to_learn):

    rospy.init_node('learn_htm', anonymous=True)

    encoded_categories_dict, length_encoded_array = create_4_encoded_animal_categories()

    number_of_things_to_learn = 4
    length_encoded_categories_dict = len(encoded_categories_dict)
    errors_to_bare_in_mind = 30

    htm_object = HTM(number_of_things_to_learn, length_encoded_array, length_encoded_categories_dict, errors_to_bare_in_mind, "htm")

    raw_input("CLEAN PICLED DATA...NOW")
    htm_object.clean_saved_htm()
    raw_input("CHECK IF CLEANED PICLED DATA...")

    for _ in range(0,10):
        for key, element in encoded_categories_dict.items():
                    htm_object.learning_algorith(element, key, debug=False)

    print type(htm_object)
    print type(htm_object.sp)

    return htm_object.save_htm()


def get_nupic_pkg_path_to_pickle_data():
    # get an instance of RosPack with the default search paths
    rospack = rospkg.RosPack()
    # get the file path for rospy_tutorials
    pkg_path = rospack.get_path('nupic_pkg')
    path_to_htm_pickle = os.path.join(pkg_path, "src/pickle_files/")
    return path_to_htm_pickle

def get_saved_htm(htm_lvl_name):
    path_to_pickle = get_nupic_pkg_path_to_pickle_data()
    htm_file_path = os.path.join(path_to_pickle, str(htm_lvl_name)+".p")
    try:
        htm = pickle.load(open(htm_file_path, "rb"))
        rospy.loginfo("Pickle File FOUND, retrieving=="+str(htm_file_path))
        return htm
    except:
        rospy.loginfo("Pickle File not found=="+str(htm_file_path))
        open(htm_file_path, 'a').close()
        return None



def de_pickle_htm(pickl_path):

    file_name_ext = os.path.basename(pickl_path)
    file_name = file_name_ext.split(".")[0]
    print "FILE NAME ==>"+str(file_name)
    htm_object_new = get_saved_htm(file_name)
    print type(htm_object_new)
    print type(htm_object_new.sp)

    encoded_categories_dict, length_encoded_array = create_4_encoded_animal_categories()

    raw_input("@@@@   Prediction LV1+LV2  Continue to Final Fase...")
    for _ in xrange(3):
        for key, element in encoded_categories_dict.items():
            print "#### START PREDICTION LV 1 ###"
            print "KEY LV1 =="+str(key)
            print "INPUT LV1 =="+str(element)
            sp_data_lv1, predicted_data = htm_object_new.predict(element)

            print "#### END PREDICTION ####"

if __name__ == '__main__':
    #learner_with_sound_mng(number_of_things_to_learn= 15, learning_data_dir=SOUND_DIR, loop_data_times=0, reset=False)
    #learn_test()
    #learn_and_predict_test()
    #learn_test_with_error_index()
    #learnLV1_learnLV2_test()
    pickl_path = pickle_data_test(number_of_things_to_learn=4)
    print "DATA saved in =="+str(pickl_path)
    raw_input("FINISHED LEARNING, START PREDICTION")
    de_pickle_htm(pickl_path)
