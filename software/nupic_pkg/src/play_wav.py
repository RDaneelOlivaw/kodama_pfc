#!/usr/bin/env python

import wave
import pygame
import os

WAV_TEST = "/home/rdaneel/Kodama/Media/03a01Fa.wav"
SOUND_DIR = "/home/rdaneel/playground/sound_tests/sounds/"

class PlayWav(object):

    def __init__(self, song_list):
        self.voice = None
        self.sound = None
        self.next_song_index = 0
        self._start_again_counter = 0
        self._song_list = song_list

    def play_wav(self, file_path):

        f = wave.open(file_path, "rb")
        Ts = f.getframerate()
        channels = f.getnchannels()
        pygame.mixer.init(frequency=Ts, channels=channels, buffer=4096)
        # If you want more channels, change 8 to a desired number. 8 is the default number of channel
        pygame.mixer.set_num_channels(7)
        self.voice = pygame.mixer.Channel(2)
        # This is the sound channel
        self.sound = pygame.mixer.Sound(file_path)
        self.voice.play(self.sound)

    def check_if_ended(self):
        return not self.voice.get_busy()

    def play_next_song(self):
        if self.next_song_index < len(self._song_list):
            self.play_wav(self._song_list[self.next_song_index])
            self.next_song_index += 1
            return True
        else:
            return False

    def start_again(self):
        self._start_again_counter += 1
        self.next_song_index = 0
        self.play_next_song()

    def get_start_again_times(self):
        return self._start_again_counter

    def get_current_song_name(self):
        current_song_index = self.next_song_index - 1
        if current_song_index < 0:
            current_song_index = 0

        return self._song_list[current_song_index]

    def wait_till_finish_and_start_again(self):
        while not self.check_if_ended():
            None
        self.reset_index()
        self.play_next_song()

    def reset_index(self):
        self.next_song_index = 0
        self._start_again_counter = 0


def play_wav_till_end(file_path):

    f = wave.open(file_path,"rb")
    Ts = f.getframerate()
    channels = f.getnchannels()

    pygame.mixer.init(frequency=Ts, channels=channels, buffer=4096)

    # If you want more channels, change 8 to a desired number. 8 is the default number of channel
    pygame.mixer.set_num_channels(7)
    # This is the sound channel
    voice = pygame.mixer.Channel(2)
    sound = pygame.mixer.Sound(file_path)
    voice.play(sound)

    song_ended = voice.get_busy()
    print "SONG ENDED =="+str(song_ended)
    while song_ended:
        song_ended = voice.get_busy()

    print "SONG ENDED =="+str(song_ended)

    return True


def get_sound_list_dir(path_dir):
    from os import listdir
    from os.path import isfile, join

    onlyfiles = [ os.path.join(path_dir,f) for f in listdir(path_dir) if isfile(join(path_dir,f)) ]

    return onlyfiles

def play_sound_list(sound_list):

    for song in sound_list:
        while not play_wav_till_end(song):
            None

    print "List Ended"



if __name__ == "__main__":
    lst_songs = get_sound_list_dir(SOUND_DIR)
    print lst_songs
    pw = PlayWav(lst_songs)
    pw.play_next_song()
    song_ended = pw.check_if_ended()

    print "SONG ENDED?=="+str(song_ended)
    list_end = False

    while not list_end:
        song_ended = pw.check_if_ended()

        if song_ended:
            print "SONG ENDED == "+str(song_ended)
            if pw.play_next_song():
                print "Playing Next Song"
            else:

                print "No next songs"
                print "LIST ENDED"
                list_end = True
        else:
            None

    print "List ENDED...END"
    #play_sound_list(lst_songs)