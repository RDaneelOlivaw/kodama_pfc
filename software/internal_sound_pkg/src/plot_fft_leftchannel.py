#!/usr/bin/env python
"""
This python script will tell you the range in the FFT magnitud values of the left channel for
speed. It is usefull for knowing which is the range that needs to be set for the nupic encoding.

we write data in an excel with module xlsxwriter
https://xlsxwriter.readthedocs.org/examples.html


Use:
Start ROS
1)roscore
Start Internal sound recorder
2)rosrun internal_sound_pkg talker.py
Run fft range value
3)rosrun internal_sound_pkg fft_value_range_tester.py

This will write in the xlsx in the catkin dir the data registered
"""

import rospy
from internal_sound_pkg.msg import internal_sound_fft
from ui import plot_line_leftchannel
import pickle
from ui import PICKLE_XS_LEFT, PICKLE_YS_LEFT

def callback(data):

    assert len(data.fft_abs_left) == len(data.freq_left)
    #rospy.loginfo(rospy.get_caller_id() + "ABS LEFT = %s", data.fft_abs_left)
    #rospy.loginfo(rospy.get_caller_id() + "FREQ LEFT %s", data.freq_left)
    pickle.dump(data.freq_left, open(PICKLE_XS_LEFT, "wb"))
    pickle.dump(data.fft_abs_left, open(PICKLE_YS_LEFT, "wb"))

    average = sum(data.fft_abs_left) / float(len(data.fft_abs_left))
    rospy.loginfo("AVERAGE LEFT ==>"+str(average))
    rospy.loginfo("MAX BASED ON AVERAGE RANGE ==>"+str(average*2.5))


def listener():

    rospy.init_node('listener', anonymous=True)

    msg = rospy.wait_for_message("chatter", internal_sound_fft)
    last_value_freq = max(msg.freq_left)
    rospy.loginfo("MAX FREQ IN LIST => "+str(last_value_freq))

    rospy.Subscriber("chatter", internal_sound_fft, callback)
    rospy.loginfo("Start Plotting Left Channel Now...")
    plot_line_leftchannel(last_value_freq, height_values=20)
    rospy.spin()

if __name__ == '__main__':
    listener()

