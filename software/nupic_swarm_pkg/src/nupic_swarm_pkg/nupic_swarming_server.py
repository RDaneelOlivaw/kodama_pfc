#!/usr/bin/env python
import rospy
from nupic_pkg.srv import NupicSwarming, NupicSwarmingResponse
from nupic_swarming_tools import swarm_over_data


def nupic_swarming_server_cb(req):
    """
    It swarms over the csv data given and the json config file and outputs the best model.

    :param req:
    :return NupicSwarmingResponse:
    """
    swarming_config_json_file_path = req.swarming_config_json_file_path
    number_processing_cores_for_swarm = req.number_processing_cores_for_swarm
    overwrite_previous_models = req.overwrite_previous_models
    swarming_output_path = req.swarming_output_path
    swarming_working_path = req.swarming_working_path
    verbosity_level = req.verbosity_level


    print "SWARM CONFIG FILE PATH ==>"+swarming_config_json_file_path
    print "HOW many CORES for swarming ==>"+str(number_processing_cores_for_swarm)
    print "Want to overwrite previous models? ==>"+str(overwrite_previous_models)
    print "OUTPUT PATH ==>"+swarming_output_path
    print "WORKING PATH ==>"+swarming_working_path
    print "VERBOSITY LEVEL ==>"+str(verbosity_level)

    swarm_config_dict = dict(maxWorkers=number_processing_cores_for_swarm,
                             overwrite=overwrite_previous_models)
    print "SWARM CONFIG OPTION DICT ==>"+str(swarm_config_dict)

    try:
        model_params_path = swarm_over_data(swarming_config_json_file_name=swarming_config_json_file_path,
                                           swarm_option_dict=swarm_config_dict,
                                           model_out_dir=swarming_output_path,
                                           perm_work_dir=swarming_working_path,
                                           verbosity_level=verbosity_level)
        result = True

    except IOError:
        print "Config File Not Found, please try again."
        result = False
        model_params_path = None

    return NupicSwarmingResponse(result, model_params_path)


def nupic_swarming_server():
    rospy.init_node('nupic_swarming_server')
    s = rospy.Service('nupic_swarming', NupicSwarming, nupic_swarming_server_cb)
    print "Ready to Swarm."
    rospy.spin()

if __name__ == "__main__":
    nupic_swarming_server()