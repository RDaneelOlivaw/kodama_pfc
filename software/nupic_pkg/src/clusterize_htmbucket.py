#!/usr/bin/env python
from learn_htm_v2 import cluster_test

if __name__ == '__main__':
    htm_bucket_name = "htm_100_30_8_30sec"
    cluster_test(htm_bucket_name=htm_bucket_name, save_fig=False)