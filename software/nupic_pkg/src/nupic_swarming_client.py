#!/usr/bin/env python

import sys
import rospy
from nupic_pkg.srv import NupicSwarming

def nupic_swarming_client(x, y, z, x1, x2, x3):
    rospy.wait_for_service('nupic_swarming')
    try:
        swarm_data = rospy.ServiceProxy('nupic_swarming', NupicSwarming)
        response = swarm_data(x, y, z, x1, x2, x3)
        return response.finished_swarming, response.model_params_path
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e

def usage():
    return "%s [x y z x1 x2 x3]"%sys.argv[0]

if __name__ == "__main__":
    """
    reset;rosrun nupic_pkg nupic_swarming_client.py /home/rdaneel/catkin_ws/src/nupic_pkg/src/myswarm/sine_swarm_config_file.json 8 True /home/rdaneel/catkin_ws/src/nupic_pkg/src/myswarm/output /home/rdaneel/catkin_ws/src/nupic_pkg/src/myswarm/output 3
    reset;rosrun nupic_pkg nupic_swarming_client.py /home/rdaneel/catkin_ws/src/nupic_pkg/src/myswarm/sdr_swarm_config_file.json 8 True /home/rdaneel/catkin_ws/src/nupic_pkg/src/myswarm/output /home/rdaneel/catkin_ws/src/nupic_pkg/src/myswarm/output 3
    """

    if len(sys.argv) == 7:
        x = sys.argv[1]
        y = int(sys.argv[2])
        z = (sys.argv[3] == 'True')
        x1 = sys.argv[4]
        x2 = sys.argv[5]
        x3 = int(sys.argv[6])
    else:
        print usage()
        sys.exit(1)

    print "SWARM with json_config_file = %s, number_cores = %s, overwrite_previous_models = %s" % (x, y, z)
    print "OUTPUT DIR ==> %s, WORKING DIR ==> %s, Verbosity LEVEL ==> %s " % (x1, x2, x3)

    print "SWARMING DONE = %s, MODEL PARAMS MODULE PATH ==> %s" % (nupic_swarming_client(x, y, z, x1, x2, x3))