// This ino recieves comands for moving the Servo and executes it.

#include <Servo.h>
#include <TinkerKit.h>

// create servo object to control a servo 
// a maximum of eight servo objects can be created 
// This is works with python script sending_receiving_commands_PYSerial.py
//These limits are experimental. In Theory it should have 180degrees range but it only has 160 stable. The las 20
// are unstable and give vibrations.
Servo myservo;
Servo myservo_0;
Servo myservo_1;
Servo myservo_2;
Servo myservo_3;
Servo myservo_4;
Servo myservo_5;
int servo_position_array[] = {0,0,0,0,0,0};
int servo_desired_position_array[] = {0,0,0,0,0,0};
int servo_position = 0;

int INITIAL_POS = 0;
int i_MAX_POS = 160;
int i_MIN_POS = 0;
byte servo_name_array[] = {O0,O1,O2,O3,O4,O5};// O of Olav and Zero the number

void setup() {
Serial.begin(9600); // set the baud rate
//Serial.begin(115200);
Serial.println("Ready"); // print "Ready" once
myservo.attach(servo_name_array[5]);
myservo_0.attach(servo_name_array[0]);
myservo_1.attach(servo_name_array[1]);
myservo_2.attach(servo_name_array[2]);
myservo_3.attach(servo_name_array[3]);
myservo_4.attach(servo_name_array[4]);
myservo_5.attach(servo_name_array[5]);
// the servos when started go to this position
myservo.write(INITIAL_POS);
myservo_0.write(INITIAL_POS);
myservo_1.write(INITIAL_POS);
myservo_2.write(INITIAL_POS);
myservo_3.write(INITIAL_POS);
myservo_4.write(INITIAL_POS);
myservo_5.write(INITIAL_POS);
// Inital pos config.
servo_position_array[0] = INITIAL_POS;
servo_position_array[1] = INITIAL_POS;
servo_position_array[2] = INITIAL_POS;
servo_position_array[3] = INITIAL_POS;
servo_position_array[4] = INITIAL_POS;
servo_position_array[5] = INITIAL_POS;
servo_position = INITIAL_POS;
}

void loop() {
handleSerialCommunication();
}

void handleSerialCommunication(void){
  if (Serial.available())
  {
    char inByte = Serial.read();
    switch(inByte)
    {
    case 'r': //Data request
      serialPrintPos(numberFromSerial(1));
      break;

    case 'b': //Move Servo the to Pos requested
      MoveServo();
      break;
      
    case 'm': //Move Servo the to Pos requested
      MoveAllServos();
      break;

    case 't': //Move Servo To Pos requested in the Time requested
      MoveServoTime();
      break;
    
    case 'c': // Ask for config Data. For the moment only the MAX SERVO VALUE
      GiveConfigData();
      break;  
    
    default:
      Serial.println(inByte);
    }
    Serial.flush();
  }
}


void GiveConfigData(void)
{
  Serial.println(i_MAX_POS);
}

void MoveServoTime(void)
{
  long number_data;
  String datum;
  //int pos_time_array;
  int desired_pos;
  int desired_time;
  int current_pos;
  int pos;
  int difference;
  int des_delay;
  int number_dig;
  char charBufTime[10];

  number_data = numberFromSerial(7);
  desired_pos = number_data / 100000; // position where we want to move the servo to, not necesarilly the final one, because it could be out of Servo Range.
  desired_time = (number_data - (desired_pos*100000));// Time in MILIseconds that will take us to reach that position
  desired_pos = servoLimits(desired_pos);// Within Servo limits.

  current_pos = servo_position;
  
  Serial.println(desired_pos);
  Serial.println(desired_time);
  Serial.println(current_pos);
  
  difference = desired_pos - current_pos;
  // Singularity Processing
  if (difference == 0)
  {
    des_delay = 0;
  }
  else
  {
    des_delay = desired_time / abs(difference); // Worst case scenario, 1ms MAXerror for a 160 degreee--> Total time Error of 0.160seconds.
  }
  
 
  Serial.println("MOVING");
  if (difference >= 0) // Turning Right, encrease position
  {
    for(pos = current_pos; pos <= desired_pos; pos += 1)  // goes from current_pos degrees to desired_pos degrees 
    {
      MoveServoWithDelay(pos,des_delay);
    } 
  }
  else
  {
    for(pos = current_pos; pos >= desired_pos; pos -= 1)  // goes from current_pos degrees to desired_pos degrees 
    {
      MoveServoWithDelay(pos,des_delay);
    } 
  }
  
  Serial.println("OK");
}

void MoveServoWithDelay(int pos, int time)
{
  myservo.write(pos);              // tell servo to go to position in variable 'pos' 
  delay(time);                       // waits 1ms for the servo to reach the position 
  Serial.println(servo_position); // WARNING: THIS IS NOT THE REAL POSITION NECESARILY, ONLY REAL WHEN THE MOVEMENT IS SLOW ENOUGH
  servo_position = pos; // Update the position of the servo
}


// @@@ Move ALL Servos at the same time @@@
void MoveAllServos(void)
{
  int exponent = 15;
  int base = 10;
  int counter = 0;
  int divider;
  int data_cluster;
  
  numberFromSerialAll(18);
  // Data adquisition.  
  //while (counter <= 5)
  //{
  //  exponent = 3*(5-counter);
  //  divider = pow(base,exponent);
  //  servo_desired_position_array[counter] = data_cluster / divider;
  //  data_cluster = data_cluster - (servo_desired_position_array[counter]*divider) ;
  //  counter ++; 
  //}

  SelectSendAllServoPos();

}


void SelectSendAllServoPos(void)
{
    myservo_0.write(servoLimits(servo_desired_position_array[0]));
    myservo_1.write(servoLimits(servo_desired_position_array[1]));
    myservo_2.write(servoLimits(servo_desired_position_array[2]));
    myservo_3.write(servoLimits(servo_desired_position_array[3]));
    myservo_4.write(servoLimits(servo_desired_position_array[4]));
    myservo_5.write(servoLimits(servo_desired_position_array[5]));
    
    SendAllPosRecieved();
    
    servo_position_array = {servoLimits(servo_desired_position_array[0]),
                            servoLimits(servo_desired_position_array[1]),
                            servoLimits(servo_desired_position_array[2]),
                            servoLimits(servo_desired_position_array[3]),
                            servoLimits(servo_desired_position_array[4]),
                            servoLimits(servo_desired_position_array[5])};// Within Servo limits.
}

void SendAllPosRecieved(void)
{
  int i=0;
  while (i<=5)
  {
    Serial.println(servo_desired_position_array[i]);
    i ++;
  }
   
}


// @@@@@@@@@@@@@@@@@@ MOVE SERVO WITHOUT TIME LIMITATION @@@@@@@@@@@@@@@@@@@@@
void MoveServo(void)
{
  int pos_and_num_servo;
  int desired_pos;
  int desired_servo_num;
  
  pos_and_num_servo = numberFromSerial(3);
  desired_pos = pos_and_num_servo / 10; // position where we want to move the servo to.
  desired_servo_num = (pos_and_num_servo - (desired_pos*10));// number of the servo to move.
  
  Serial.println(desired_pos);
  Serial.println(desired_servo_num);
  
  Serial.println("MOVING");

  SelectSendServoPos(desired_pos,desired_servo_num);

}

void SelectSendServoPos(int pos, int servo_number)
{
  if (servo_number == 0)
  {
    myservo_0.write(servoLimits(pos));
    Serial.println("OK0");
  }
  else if (servo_number == 1)
  {
    myservo_1.write(servoLimits(pos));
    Serial.println("OK1");
  }
  else if (servo_number == 2)
  {
    myservo_2.write(servoLimits(pos));
    Serial.println("OK2");
  }
  else if (servo_number == 3)
  {
    myservo_3.write(servoLimits(pos));
    Serial.println("OK3");
  }
  else if (servo_number == 4)
  {
    myservo_4.write(servoLimits(pos));
    Serial.println("OK4");
  }
  else if (servo_number == 5)
  {
    myservo_5.write(servoLimits(pos));
    Serial.println("OK5");
  }
  else 
  {
   // Do nothing.   
  }
 
  servo_position_array[servo_number] = servoLimits(pos);// Within Servo limits.
}

long numberFromSerial(int num_of_digits_to_read)
{
  char numberString[9];
  unsigned char index=0;
  // char charBufAux[10];
  
  delay(10);
  while(Serial.available())
  {
    delay(10);
    numberString[index++]=Serial.read();
    if(index > num_of_digits_to_read)
    {
      break;
    }
  }
  numberString[index]=0;
  return atol(numberString);
}

// $$$$$$$$ Serial Read For All Servos $$$$$$$$$$$$$
void numberFromSerialAll(int num_of_digits_to_read)
{
  char numberString[20];
  unsigned char index=0;
  char charBufAux[3];
  int counter=0;
  
  delay(10);
  while(Serial.available())
  {
    delay(10);
    numberString[index++]=Serial.read();
    if(index > num_of_digits_to_read)
    {
      break;
    }
  }
  numberString[index]=0;
  
  while (counter < 6)
  {
    strncpy ( charBufAux, numberString + (3*counter), 3 ); // substring of a char type
    charBufAux[3] = '\0';
    servo_desired_position_array[counter] = atol(charBufAux);
    counter ++;
  }
  
  
  SendAllPosRecieved();
}
/// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

// @@@ Serial Print POS @@@
void serialPrintPos(int servo_number)
{
 Serial.println(servo_position_array[servo_number]);
   
}


int servoLimits(int i_number)
{
 int s_num;

 if (i_number > i_MAX_POS)
 {
   s_num = i_MAX_POS;
 }
 else if (i_number < i_MIN_POS)
 {
   s_num = i_MIN_POS;
 }
 else
 {
   s_num = i_number;
 }
 return s_num;
}
