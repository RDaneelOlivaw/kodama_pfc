/*      ***** PCBmotor Aps. May 2011 *****

PIC18F2420 program for TwinMotor project. 
V.1.0 May 2011 

Note:   Communication speed 19200 Baud (8b,1 stop, no parity, no flow control).
        In power failure the clock is 1MHz which is the limiting factor for the baudrate. 

Note:   In order to detect failure to write the new position into EEPROM after each motor action
the memory associated with Memory position#0 is used in the following way:
Value=0: Successful exit from last motor action with new position stored
Value=1: Error: Postion not written. Re-calibration is required
Value=2: Power failure while motor was running, but successful dump of new position 
Each time the current motor is activated the Value is set to 1, but cleared after proper exit.
After power failure interrupt (PWREN# ->1) a value of 2 is written.
Memory position#1 holds the number of the last active motor incl power up sweep.
It is the responsibility of the PC software to check the value when the program is started!!

USB Command structure 
-----------------------------------------

Each command is a variable length text string, max 90 chars, terminated by <CR> (ascii 13). 
The first char is the command char, i.e R for read EEPROM, followed by a value field (adddres 0 to 255 for the R command) 
and terminated by <CR>. The value field is different for the commands and the length will vary, but no padding with 0's 
is required. This means R1 and R001 are equally good and both means Read Memory Position 1 (one byte).

Multiple commands:  Use "," to separate commands and end with <CR>, e.g. M4,P+,S100,U-1000<CR> is legal and will:
Chose motor4, turn on power, run 100 CW steps and end with 1000 CCW micropulses.
Max command line length is 90 chars. Do not use white space between comma and next command.

Command
Char#1ValueDescription
--------------------------------
P+ or -P or P+ will power up the motor driver and do a sweep. P- will power down the driver
M0..4M1 .. M4 will select motor 1 .. 4 for the future step/pulse commands. M0 will deselect all motors
V0..5000 V2500 sets the motor voltage to 2500mV. Maximum is 5000mV, minimum is 0mV
InaMeasures current from the motor voltage. Used for calibration with resistor load on motor supply voltage
S+/-stepsSteps can be up to 999.999.999 with "-" for counterclockwise (CCW) rotation. S or S0 shows current position
G+/-position Goto the specified position up to +/-999.999.999. G or G0 will return the motor to the zero position.
U+/-pulsesMicropulses (upulses), max 999.999.999 with a leading "-" for CCW operation
P +/-pulses Run the specified number of pulses but stop if positive edge on sensor
N +/-pulses Run the specified number of pulses but stop if negative edge on sensor
A3 or 4Returns analog voltage from external sensors. Used to calibrate sensor LED drive. Not implemented yet
RadrReads EEPROM byte at address "adr" (0..255)
D adrReads EEPROM integer at addresses "adr" (LSB) & "adr+1": e.g RD12 returns 256*eeprom(adr+1) + eeprom(adr)
L adrReads EEPROM long at 4 addresses starting with "adr". 
Wval@adrWrites a byte ("val": 0..255) to address "adr" (0..255). "@" is required as separator after "val".
D val@adrWrites an integer ("val": +/-32768) to address "adr"+1 (0..255) with LSB in "adr".
N1 or 2Reads and resets the hardware counter N1 or N2 on the A sensor input for motor 1/3 or 2/4. Not implemented yet
L0,1 or 2L1 and L2 turns the sensor on for motor 1 respectively 2. L0 turns the sensor off. Not implemented yet. 
Z+/-positionSet current motor position to new value given by +/-position. Z0 will set the current position to zero.
DvalDelay in milliseconds, max 999.999.999 before executing next command. 
XvalRepeat the command line "val" times (max 999.999.999). Place anywhere in a string of commands 
Q(-)Quiet mode, i.e. only error messages and prompt ">" after completion of commands. Q- turns info back on. 

Emergency stop:Commands D,S,G & U will stop immediately if the LED input is pulled down during operation (HW stop) or
the signal ??? is set from the PC (SW stop). 
--------------------------------

 
EEPROM memory map (Each variable stored in 1,2 or 4 bytes as indicated, LSB in the lowest address):

System memory:
--------------------------------
Memory
0:0: Motor operation finished correctly
1: Motor operation failed without updating EEPROM
2: Power failure with active motor, but correctly updated EEPROM
1: Active motor number
 2+3:Maximum power dissipation (mW)
4+5: Length of micropulse ("U" command), default value 10. Actual width = (n+1)*5.4us
6+7: Interval between micropulses ("U" command), default value 2000. 
8+9: Not currently used
10+11: Flash cycle time for LED in ms, default 500 (250on,250off)
 12+13: Max steptime in ms, default 50ms. For larger steptime the motor is stopped with warning
14+15: Min steptime in us, default 2000us. Not currently used
16+17: VS: Driver voltage in mV max 5000mV, min 0mV

18: VU: Step up driver voltage in mV/step max 255mV, min 0mV, when step up is active i.e SU > 0
19:SU: Number of steps for ramping up driver voltage from VU.
20: VD: Step down driver voltage in mV/step max 255mV, min 0mV, when step down is active, i.e. SD > 0
21:SD: Number of steps for ramping down driver voltage from VS to VD.

 
       VS + SU*VU        ************************
                        *                        *
                       *                           *
                     *                               *
       VS  ********     |                       |       *        *************** VS
                  |     |                       |          *     *
                  |     |                       |             *  *
                  |     |                       |                *  VS+SU*VU - SD*VD
                  |     |                       |                |
                  |     |                       |                |
                  |     |                       |                |
     Steps:       0  SU                    STEPS-SD            STEPS 



22..25:Number of micropulses (starting with LSB) from the last "U" command.

25..47:Reserved

------------------------------------------------------------------------------------------------

Memory positions for Motor#1 to Motor#4 (Motor#1, 2, 3 & 4 starting in memory resp 48, 72, 96 & 120). 
Each motor uses consequtive memory positions shown below for motor#1: 


Memory motor#1
48..51:0..3Actual position for motor#1 in steps from zero position (starting with LSB)
52:4Sensor choice A,B or quadrature(dual sensor):
    0: Sensor A, half-step mode (2x resolution)
    1: Sensor A, full-step mode
    2: Sensor B, half-step mode (2x resolution)
    3: Sensor B, full-step mode
    4: A & B in quadrature mode (4x resolution)
53+54:5+6Steps (full-steps) per revolution for the codewheel 
55:7LED current mA (0..255) for sensor LED (factory calibration)
56: 8Voltage scaling (% of Vs 0..255) for CW operation in "S" and "G" command (no scaling i "U")
57: 9 Voltage scaling (% of Vs 0..255) for CCW operation in "S" and "G" command (no scaling i "U")
58+5910+11Maximum motor current in mA (factory calibration)
60..7112..23Reserved    

The memory postions after motor#4, i.e 144 up to 255 are free and can be programmed by the W & R commands

--------------------------------
--------------------------------

Update history:  

20-dec-2010 EJ: Modified "P-"command (driver off): INTCLKO from 32MHz to 1MHz, LED off, Motor#0 selected.
This brings supply current down to 20mA of which 15mA (as per datasheet) is used by FT232R. 
22-dec-2010 EJ: Added EEPROM flags for successful exit after motor action and active motor #
01-jan-2011 EJ: Global variables defined in "globals.h". Need to be "volatile" due to the interrupt routine.   
02-jan-2011 EJ: EEPROM Store of position @ Power failure and Memory 0 = 2 to indicate 
successful emergency shutdown while motor was runnning.

02-jan-2011 Micropulsing sets the position within a codewheel step for higher resolution: 
EEPROM Memory 4+5 stores length of a micropulse and Memory 6+7 stores time between micropulses.
Command "U" uses micropulses to move the indicated motor either the input no of micropulses
or until just past the next edge on the codewheel.

04-feb-2011 EJ: Memory 12+13 stores max steptime (ms) which sets timeout for the motor, default is 50ms.

07-feb-2011 EJ: Corrected EPROM write so negative numbers are stored correctly.

05-mar-2011 EJ: Only std codewheel supported, ie no special "home" position. MotorMemory 5+6 holds steps/rev.
"V" command to set driver voltage in mV. Memory 16+17 holds value, max 5000mV.
Support for measuring driver current.
"I" command to make an ADC conversion and read out the current in mA.
 ADC value found to be 1.194*current @ 800mA, measured with load resistor and "V" and "I" commands 

10-mar-2011 EJ: Shortened the timeunit for micropulses to one tenth in order to generate very short pulses. The default 
values have been incrased with a factor ten to keep the current timing as standard. 

10-mar-2011 EJ: Calculating stepTime as time/steps for each motoroperation. Scale factor calibrated with scope.

27-apr-2011 EJ: Command interface: All commands start with one character for the command followed by a value field 
up to 10 digits including sign +/- and terminated by <CR> (13). The length of the value field is variable,
i.e a command of V10 is equivalent to V0010.  

07-may-2011 EJ: Allowing multiple commands separated with comma and the line ending with <CR> 

08-may-2011 EJ: Yet to come:
L,N,A commands not implemented yet. 
No "emergency stop" from FTDI, but LED ready as emergency stop input
Quadrature sensor support

11-may-2011 EJ: Updated memory map. Added Delay and Xrepeat commands.

14-may-2011 EJ: Added memory positions for Voltage ramping for "soft" start/stop.

17-may-2011 EJ: Added "Q" to run in quit mode for long command strings and speed up command execution.
Added memory positions 22..25 for the number of micropulses from the last "U" command (max 999.999.999) 

19-may-2011 EJ: Added support for halfstep and fullstep operation in motor memory position 4 (see memory map) 

*/