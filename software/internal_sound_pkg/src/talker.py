#!/usr/bin/env python

import rospy
from internal_sound_pkg.msg import internal_sound_fft

from record_internal_sound import InternAlSoundRecorder


SOUND_SOURCE = "alsa_output.pci-0000_00_1b.0.analog-stereo"

def fill_array(out_ar, in_arr):

    out_ar = []
    for element in in_arr:
        out_ar.append(element)

    return out_ar


def update_msg(msg, ir_object):
    """
    Updates the values of the msg based on the values in IR
    :param msg:
    :param ir_object:
    :return:
    """

    msg.fft_abs_left = ir_object.fft_ydata_L
    msg.freq_left = ir_object.fft_xdata_L

    msg.fft_abs_right = ir_object.fft_ydata_R
    msg.freq_right = ir_object.fft_xdata_R

    return msg

def talker():
    pub = rospy.Publisher('chatter', internal_sound_fft, queue_size=1)
    rospy.init_node('talker', anonymous=True, log_level=rospy.WARN)
    # 10hz, more it make it really slow for new values
    rospy.logwarn("Init SoundRecorder...")
    rate = rospy.Rate(5)

    SIMPLE = False
    PRINT_DATA = False
    SAVE_DATA = False
    MAX_SAVE_DATA = 10000
    #RATE = 20000
    RATE = 10000

    IR = InternAlSoundRecorder(sink_name=SOUND_SOURCE,
                               rate=RATE,
                               simple_record=SIMPLE,
                               print_data=PRINT_DATA,
                               save_data=SAVE_DATA,
                               max_save_data=MAX_SAVE_DATA)
    IR.setup()
    IR.continuousStart()
    msg = internal_sound_fft()
    update_msg(msg, IR)



    while not rospy.is_shutdown():

        update_msg(msg, IR)
        rospy.loginfo(msg)
        pub.publish(msg)
        rate.sleep()

    IR.close()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
