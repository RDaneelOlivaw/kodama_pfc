

int incomingByte = 0;	// para el byte leido

void setup() {
	Serial.begin(115200);
        pinMode(13, OUTPUT);	// abre el puerto serie a 9600 bps
}

void loop() {

	// envia datos solamente cuando recibe datos
	if (Serial.available() > 0) {
		// lee el byte entrante:
		incomingByte = Serial.read();

		// dice lo que ha recibido:
                digitalWrite(13, HIGH);   // set the LED on
                delay(300);              // wait for a second
                digitalWrite(13, LOW);    // set the LED off
                delay(300);
                digitalWrite(13, HIGH);
                Serial.println("I blinked");
                delay(1000);                
		//Serial.println(incomingByte, DEC);
	}
}



