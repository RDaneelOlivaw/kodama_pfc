import time
import serial

COMMAND_1 = "Q,M3,S1,M4,S1,x100"
COMMAND_2 = "M4,P+,S100,U-1000"
COMMAND_3 = "M4,P+,S0"
COMMAND_4 = "M4,P+,G0"
COMMAND_5 = "M3,U1000,M4,U1000,M3,U1000,M4,U1000,M3,U1000,M4,U1000,M3,U1000,M4,U1000,M3,U1000,M4,U1000,M3,U1000,M4,U1000,M3,U1000,M4,U1000,M3,U1000,M4,U1000,M3,U1000,M4,U1000,M3,U1000,M4,U1000,M3,U1000,M4,U1000,M3,U1000,M4,U1000,M3,U1000,M4,U1000,M3,U1000,M4,U1000,M3,U1000,M4,U1000,M3,U1000,M4,U1000,M3,U1000,M4,U1000,M3,U1000,M4,U1000"
DEFAULT_COMMAND = COMMAND_3

CMD_MOVE_RIGHT_SEQ = "Q,M3,S-1,M4,S-1,x100"
CMD_MOVE_LEFT_SEQ = "Q,M3,S1,M4,S1,x100"
CMD_LOOK_SEQ_ARRAY = [CMD_MOVE_RIGHT_SEQ, CMD_MOVE_LEFT_SEQ]

CMD_MOVE_RIGHT_SIM = "M3,U-1000,M4,U-1000"
CMD_MOVE_LEFT_SIM = "M3,U1000,M4,U1000"
CMD_LOOK_SIMULT_ARRAY = [CMD_MOVE_RIGHT_SIM, CMD_MOVE_LEFT_SIM]

LOOKING_TIMES = 10

def sequential_look(serial_object, times=LOOKING_TIMES):
    """
    Looks side to side for times given
    :param seconds:
    :return:
    """

    movement = 0
    for i in range(times):
        if movement == 0:
            movement = 1
        else:
            movement = 0

        message = CMD_LOOK_SEQ_ARRAY[movement]

        ser.write(message)
        time.sleep(1)

def simultaneous_look(serial_object, times=LOOKING_TIMES):
    """
    Looks side to side for times given
    :param seconds:
    :return:
    """
    num_micros_steps = 5
    movement = 0
    for i in range(times):
        if movement == 0:
            movement = 1
        else:
            movement = 0

        message = CMD_LOOK_SIMULT_ARRAY[movement]
        for j in range(num_micros_steps):
            message = message+","+CMD_LOOK_SIMULT_ARRAY[movement]

        print "message simult=="+str(message)

        ser.write(message)
        time.sleep(1)

def send_msg(serial_object, message):
    """
    Sends message
    :param serial_object:
    :param message:
    :return:
    """
    # send the character to the device
    # (note that I happend a \r\n carriage return and line feed to the characters - this is requested by my device)
    serial_object.write(message)
    out = ''
    # let's wait one second before readM1,P+,S100,P-ing output (let's give device time to answer)
    time.sleep(1)
    while serial_object.inWaiting() > 0:
        out += serial_object.read(1)

    if out != '':
        print ">>" + out

# configure the serial connections (the parameters differs on the device you are connecting to)
#Communication speed 19200 Baud (8b,1 stop, no parity, no flow control).
ser = serial.Serial(
    port='/dev/ttyUSB0',
    baudrate=19200,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)

#ser.open()
ser.isOpen()

print 'Enter your commands below.\r\nInsert "exit" to leave the application.'
print 'These are the available commands:\r\n'
print "(1) For command " + COMMAND_1 + "--> moves the two motors simultaneusly 100 steps:\r\n"
print "(2) For command " + COMMAND_2 + " --> moves the Motors 4 100 steps and then 100 microsteps in the oposite dir:\r\n"
print "(3) For command " + COMMAND_3 + " --> Reads Current Position:\r\n"
print "(4) For command " + COMMAND_4 + " --> Goes to Position Zero:\r\n"
print "(5) For command " + COMMAND_5 + " -->:\r\n"
print "(6) For Sequential Look CMD -->:\r\n"
print "(7) For Simulatneous Look CMD -->:\r\n"
print "(0) For command custom command -->:\r\n"


input = 1
while 1:
    # get keyboard input
    input = raw_input(">> ")
        # Python 3 users
        # input = input(">> ")
    if input == 'exit':
        ser.close()
        exit()
    else:
        if input == "1":
            message = COMMAND_1 + "\r"
        elif input == "2":
            message = COMMAND_2 + "\r"
        elif input == "3":
            message = COMMAND_3 + "\r"
        elif input == "4":
            message = COMMAND_4 + "\r"
        elif input == "5":
            message = COMMAND_5 + "\r"
        elif input == "6":
            message = COMMAND_5 + "\r"
        elif input == "7":
            message = COMMAND_5 + "\r"
        elif input == "0":
            input = raw_input(">> ")
            message = input + "\r"
        else:
            print "Wrong Command, we will send default command " + DEFAULT_COMMAND
            message = DEFAULT_COMMAND + "\r"

        print "INPUT =="+str(input)
        if input == "6":
            print "In Sequential Look to send"
            sequential_look(serial_object=ser, times=LOOKING_TIMES)
        elif input == "7":
            print "In Sequential Look to send"
            simultaneous_look(serial_object=ser, times=LOOKING_TIMES)
        else:
            print "In Normal commands to send"
            send_msg(serial_object=ser, message=message)

