__author__ = "RDaneelOlivaw"

"""
From https://bitbucket.org/mjs0/peak-detect/src
From https://github.com/Valodim/python-pulseaudio

To list all available sound sinks and ports
$pacmd list-cards
This way we can see the SINK_NAME

U-Control External --> alsa_output.usb-Burr-Brown_from_TI_USB_Audio_CODEC-00-CODEC.analog-stereo
Internal Sound --> alsa_output.pci-0000_00_1b.0.analog-stereo
"""
import os
import sys
import numpy
from Queue import Queue
from ctypes import POINTER, c_ubyte, c_void_p, c_ulong, cast
from pulseaudio.lib_pulseaudio import *
import threading
import pylab
from dynamic_draw import DynamicUpdate
from write_data_in_excel import ExcelFileObject
import random
import time

TEST_EXCEL_PATH = "/home/rdaneel/Kodama/kodama_project/src/recording_internal_sound/peak-detect/data.xls"

class InternAlSoundRecorder(object):
    """Simple class to record from the internal sound."""

    def __init__(self, sink_name, rate, simple_record = False, print_data=False, save_data=False, max_save_data=0):
        self.sink_name = sink_name
        # Human main speech range 200 and 2000Hz --> 2000x2
        self.rate = rate
        self.newAudio = False
        self.buffersize = 2**8
        self._simple_rec = simple_record
        self._print_data = print_data
        self._save_data = save_data

        self.data_index_R = 0
        self.data_index_L = 0
        self._max_save_data = max_save_data
        self.left_channel_data = True


        # Wrap callback methods in appropriate ctypefunc instances so
        # that the Pulseaudio C API can call them
        self._context_notify_cb = pa_context_notify_cb_t(self.context_notify_cb)
        self._sink_info_cb = pa_sink_info_cb_t(self.sink_info_cb)
        self._stream_read_cb = pa_stream_request_cb_t(self.stream_read_cb)

        # stream_read_cb() puts peak samples into this Queue instance
        self._samples = Queue()

        # Set audio container for fft transform

        self.secToRecord = 1
        self.buffersToRecord = int(self.rate*self.secToRecord/self.buffersize)
        if self.buffersToRecord == 0:
            self.buffersToRecord = 1
        self.samplesToRecord = int(self.buffersize*self.buffersToRecord)
        self.chunksToRecord = int(self.samplesToRecord/self.buffersize)
        #self.audio = numpy.zeros(self.chunksToRecord*self.buffersize, dtype=numpy.int16)
        self.audio = 0
        #self.audiochunks = numpy.zeros((self.chunksToRecord*self.buffersize), dtype=numpy.int16)
        self.audiochunks_L = [0]*self.buffersize
        self.audiochunks_R = [0]*self.buffersize


        self.fft_xdata_L = None
        self.fft_ydata_L = None
        self.fft_xdata_R = None
        self.fft_ydata_R = None

        self.last_index_done_fft_R = 0
        self.last_index_done_fft_L = 0

        self.print_init_data()
        ###########################################

        # Create the mainloop thread and set our context_notify_cb
        # method to be called when there's updates relating to the
        # connection to Pulseaudio

        self._mainloop = pa_threaded_mainloop_new()


        # Save Audio in Excel Init
        self.e_o = ExcelFileObject(TEST_EXCEL_PATH)

    def setup(self):
        _mainloop_api = pa_threaded_mainloop_get_api(self._mainloop)
        context = pa_context_new(_mainloop_api, 'peak_demo')
        pa_context_set_state_callback(context, self._context_notify_cb, None)
        pa_context_connect(context, None, 0, None)
        pa_threaded_mainloop_start(self._mainloop)

    def close(self):
        # TODO: Use it when finishing program
        """cleanly back out and release sound card."""
        pa_threaded_mainloop_stop(self._mainloop)
        pa_threaded_mainloop_free(self._mainloop)
        os._exit(1)

    def __iter__(self):
        while True:
            yield self._samples.get()

    def print_init_data(self):
        print "---------------------------------------"
        print "Audio Container -->" + str(self.audio)
        print "chunksToRecord -->" + str(self.chunksToRecord)
        print "samplesToRecord -->" + str(self.samplesToRecord)
        print "buffersToRecord -->" + str(self.buffersToRecord)
        print "rate -->" + str(self.rate)
        print "sink_name -->" + str(self.sink_name)
        print "---------------------------------------"

    def context_notify_cb(self, context, _):
        state = pa_context_get_state(context)

        if state == PA_CONTEXT_READY:
            print "Pulseaudio connection ready..."
            # Connected to Pulseaudio. Now request that sink_info_cb
            # be called with information about the available sinks.
            o = pa_context_get_sink_info_list(context, self._sink_info_cb, None)
            pa_operation_unref(o)

        elif state == PA_CONTEXT_FAILED :
            print "Connection failed"

        elif state == PA_CONTEXT_TERMINATED:
            print "Connection terminated"

    def sink_info_cb(self, context, sink_info_p, _, __):
        if not sink_info_p:
            return

        sink_info = sink_info_p.contents
        print '-'* 60
        print 'index:', sink_info.index
        print 'name:', sink_info.name
        print 'description:', sink_info.description

        if sink_info.name == self.sink_name:
            # Found the sink we want to monitor for peak levels.
            # Tell PA to call stream_read_cb with peak samples.
            print
            print 'setting up peak recording using', sink_info.monitor_source_name
            print
            samplespec = pa_sample_spec()
            # TODO: Maybe this will have to be two because we generate sound in stereo.
            samplespec.channels = 2
            samplespec.format = PA_SAMPLE_S16LE
            samplespec.rate = self.rate

            pa_stream = pa_stream_new(context, "peak detect demo", samplespec, None)
            pa_stream_set_read_callback(pa_stream,
                                        self._stream_read_cb,
                                        sink_info.index)
            pa_stream_connect_record(pa_stream,
                                     sink_info.monitor_source_name,
                                     None,
                                     #PA_STREAM_PEAK_DETECT)
                                     PA_STREAM_NOFLAGS)

    def stream_read_cb(self, stream, length, index_incr):
        data = c_void_p()
        pa_stream_peek(stream, data, c_ulong(length))

        data = cast(data, POINTER(c_ubyte))
        for i in xrange(length):
            # When PA_SAMPLE_U8 is used, samples values range from 128
            # to 255 because the underlying audio data is signed but
            # it doesn't make sense to return signed peaks.
            #self._samples.put(data[i] - 128)
            self._samples.put(data[i])
        pa_stream_drop(stream)



    def getAudio(self):
        """get a single buffer size worth of audio."""
        #audioString = str(self.__iter__().next())
        #print "AUDIO STRING --> "+str(audioString)
        #return numpy.fromstring(audioString, dtype=numpy.int16, sep=',')
        return self.__iter__().next()

    def record(self, forever=True):
        """record secToRecord seconds of audio."""
        while True:

            if self._simple_rec:
                self.audio = self.getAudio()

                if self._save_data:
                    if self._print_data:
                        print "INDEX="+str(self.data_index_L)
                        print "SAVE REC="+str(self.audio)

                    # To Start saving after transitory
                    if self.data_index >= self._max_save_data/3:
                        self.e_o.write_data(x=self.data_index_L, y=self.audio, is_left=self.left_channel_data)

                    if not self.left_channel_data:
                        print "###RIGHT###"
                        self.data_index_R += 1
                        self.left_channel_data = True
                    else:
                        print "@@@LEFT@@@"
                        self.data_index_L += 1
                        self.left_channel_data = False

                    if self.data_index_R >= self._max_save_data:
                        self.e_o.save_data()
                        self.close()

                else:
                    if self._print_data:
                        print "REC="+str(self.audio)
                    self.data_index_L += 1

            else:
                self.audio = self.getAudio()
                if not self.left_channel_data:
                    # Right Channel Data
                    self.audiochunks_R.pop(0)
                    self.audiochunks_R.append(self.getAudio())
                    if (self.data_index_R - self.last_index_done_fft_R) >= self.buffersize:
                        self.fft_xdata_R, self.fft_ydata_R = self.fft_simple(in_data=self.audiochunks_R)
                        self.last_index_done_fft_R = self.data_index_R
                    self.left_channel_data = True
                    self.data_index_R += 1

                    if self._print_data:
                        print "REC="+str(self.audio)
                        print "FFT_X ==>"+str(self.fft_xdata_R)
                        print "FFT_Y ==>"+str(self.fft_ydata_R)


                else:
                    # Left Channel Data
                    self.audiochunks_L.pop(0)
                    self.audiochunks_L.append(self.getAudio())
                    if (self.data_index_L - self.last_index_done_fft_L) >= self.buffersize:
                        self.fft_xdata_L, self.fft_ydata_L = self.fft_simple(in_data=self.audiochunks_L)
                        self.last_index_done_fft_L = self.data_index_L
                    self.left_channel_data = False
                    self.data_index_L += 1

                    if self._print_data:
                        print "REC="+str(self.audio)
                        print "FFT_X ==>"+str(self.fft_xdata_L)
                        print "FFT_Y ==>"+str(self.fft_ydata_L)

                if self.data_index_R >= self._max_save_data and self._save_data:
                    self.save_excel_fft()


            self.newAudio=True
            if forever==False: break


    def print_there(self, x, y, text):
        sys.stdout.write("\x1b7\x1b[%d;%df%s\x1b8" % (x, y, text))
        sys.stdout.flush()

    def save_excel_fft(self):
        """
        Savein in excel the current fft data set
        :return:
        """
        print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@2START LEFT"
        if len(self.fft_ydata_L) == len(self.fft_xdata_L):
            i = 0
            print "#### LEFT ####"
            while i < len(self.fft_ydata_L):
                xdata = self.fft_xdata_L[i]
                ydata = self.fft_ydata_L[i]
                print "X="+str(xdata)
                print "Y="+str(ydata)
                self.e_o.write_data(x=xdata, y=ydata, is_left=True)
                i += 1
        else:
            assert False, "The length of x and y sets arent the same"


        print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@2START RIGHT"
        if len(self.fft_ydata_R) == len(self.fft_xdata_R):
            i = 0
            print "#### RIGHT ####"
            while i < len(self.fft_ydata_R):
                xdata = self.fft_xdata_R[i]
                ydata = self.fft_ydata_R[i]
                print "X="+str(xdata)
                print "Y="+str(ydata)
                self.e_o.write_data(x=xdata, y=ydata, is_left=True)
                i += 1
        else:
            assert False, "The length of x and y sets arent the same"

        self.e_o.write_data(x="LEFT FREQ", y="LEFT FFT", is_left=True)
        self.e_o.write_data(x="RIGHT FREQ", y="RIGHT FFT", is_left=False)
        print "Saving Data..."
        self.e_o.save_data()
        self.close()
        print "Saved File and closing Program..."



    def continuousStart(self):
        """CALL THIS to start running forever."""
        self.t = threading.Thread(target=self.record)
        self.t.start()

    def fft(self, in_data, trimBy=10, logScale=False, divBy=100):

        if in_data is None:
            return None, None

        data = numpy.array(in_data)
        left, right = numpy.split(numpy.abs(numpy.fft.fft(data)), 2)
        ys = numpy.add(left, right[::-1])

        if logScale:
            ys = numpy.multiply(20, numpy.log10(ys))

        xs = numpy.arange(self.buffersize/2, dtype=float)

        if trimBy:
            i = int((self.buffersize/2)/trimBy)
            ys = ys[:i]
            xs = xs[:i]*self.rate/self.buffersize

        if divBy:
            ys = ys / float(divBy)

        return xs, ys

    def fft_simple(self, in_data):

        # frequency of the signal ff
        y = numpy.array(in_data)
        # length of the signal
        n = len(y)

        k = numpy.arange(n)
        T = float(n)/float(self.rate)
        # two sides frequency range
        frq = k/T
        # one side frequency range
        frq = frq[range(n/2)]

        # fft computing and normalization
        #Y = numpy.fft.fft(y)/n
        Y = numpy.abs(numpy.fft.fft(y))/n
        Y = Y[range(n/2)]

        return frq.tolist(), Y.tolist()

    def fake_fft(self, data_array):
        """
        Fake just for speed data testing
        :return:
        """
        l = len(data_array)
        #x = random.random()
        #y = random.random()
        x = data_array[0]
        y = data_array[0]
        frq_fake = [x]
        y_fake = [y]

        return frq_fake, y_fake


    def plotAudio(self):
        """open a matplotlib popup window showing audio data."""
        pylab.plot(self.audio.flatten())
        pylab.show()

    def plotAudio2(self):
        """open a matplotlib popup window showing audio data."""
        d = DynamicUpdate(self)
        d()

    def plotAudioFFT(self):
        """open a matplotlib popup window showing audio data."""
        d = DynamicUpdate(self, is_fft=True)
        d()



if __name__ == "__main__":


    SIMPLE = False
    PRINT_DATA = True
    PLOT = False
    SAVE_DATA = False
    #MAX_SAVE_DATA = 60000
    MAX_SAVE_DATA = 10000
    # More than 20.000 starts having some lag
    RATE = 20000
    IR = InternAlSoundRecorder(sink_name='alsa_output.pci-0000_00_1b.0.analog-stereo',
                               rate=RATE,
                               simple_record=SIMPLE,
                               print_data=PRINT_DATA,
                               save_data=SAVE_DATA,
                               max_save_data=MAX_SAVE_DATA)
    #raw_input("Setup, PRESS A KEY\n")
    IR.setup()
    #raw_input("Start, PRESS A KEY\n")
    IR.continuousStart()
    if SIMPLE and PLOT:
        print "PlotAudio2------>"
        IR.plotAudio2()
    elif not SIMPLE and PLOT:
        None
        IR.plotAudioFFT()
    else:
        print "Not Plot"