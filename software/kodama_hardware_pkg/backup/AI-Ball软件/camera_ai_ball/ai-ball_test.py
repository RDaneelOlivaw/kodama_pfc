import cv2
print cv2.__version__
import numpy as np
import time
import urllib


AI_BALL_IP = "http://ai-ball.com/javascript.html"

def ocv_test():
    """
    Shows some ocv operation to test that all is allright
    :return:
    """
    img = cv2.imread('mo.jpg',0)
    print img
    cv2.imshow('image',img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def ocv_video_test():
    """
    Tests with videos
    :return:
    """

    cap = cv2.VideoCapture(0)
    t_now = time.clock()
    t_after = 0
    while(t_after - t_now <= 0.5):
        # Capture frame-by-frame
        ret, frame = cap.read()

        # Our operations on the frame come here
        #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Display the resulting frame
        cv2.imshow('frame',frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        t_after = time.clock()
        print str(t_after-t_now)

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()


def ip_cam_test():
    """
    Test for Ip_Camera streaming
    :return:
    """
    print "Opening URL"
    stream=urllib.urlopen(AI_BALL_IP)
    print "Stream created"
    bytes=''
    while True:
        print "Reading stream"
        bytes+=stream.read(16384)
        a = bytes.find('\xff\xd8')
        b = bytes.find('\xff\xd9')
        if a!=-1 and b!=-1:
            jpg = bytes[a:b+2]
            bytes= bytes[b+2:]
            i = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8),cv2.CV_LOAD_IMAGE_COLOR)
            cv2.imshow('i',i)
            if cv2.waitKey(1) ==27:
                exit(0)

def ip_cam_test2():

    print "Init"
    cap = cv2.VideoCapture()
    print "Open IP = "+AI_BALL_IP
    cap.open(AI_BALL_IP)

    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()

        # Our operations on the frame come here
        #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Display the resulting frame
        if frame != None:
            cv2.imshow('frame',frame)
        else:
            print "No image"

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()


def main():
    print "Starting Ai-Ball test"
    #ocv_test()
    #ocv_video_test()
    ip_cam_test2()
    print "Endedn Ai-Ball test"

if __name__ == '__main__':
    main()