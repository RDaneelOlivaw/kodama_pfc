"""
Simple test for UI_PLOT

tutorials:
http://zetcode.com/gui/pyqt4/firstprograms/

"""

import ui_plot
import sys
from PyQt4 import QtCore, QtGui
import PyQt4.Qwt5 as Qwt
import pickle
import random
import math

PICKLE_XS = "xs.p"
PICKLE_YS = "ys.p"
PICKLE_XS_RIGHT = "xs_right.p"
PICKLE_YS_RIGHT = "ys_right.p"
PICKLE_XS_LEFT = "xs_left.p"
PICKLE_YS_LEFT = "ys_left.p"
PICKLE_C = "c.p"
PICKLE_UIPLOT = "uiplot.p"


MAX_Y = 20
MAX_X = 5000

def test_void_plot():
    app = QtGui.QApplication(sys.argv)

    ### SET-UP WINDOWS
    
    # WINDOW plot
    win_plot = ui_plot.QtGui.QMainWindow()
    uiplot = ui_plot.Ui_win_plot()
    uiplot.setupUi(win_plot)

    ### DISPLAY WINDOWS
    win_plot.show()

    #WAIT UNTIL QT RETURNS EXIT CODE
    sys.exit(app.exec_())

def get_random_array():
    xs = range(0, 20000)
    ys = [1000*random.random() for _ in xrange(20000)]
    return xs, ys


def get_sinus_aray(range_size, f, fs):
    Ts = 1.0/fs
    xs = range(0, range_size)
    ys = [100*math.sin(2*math.pi*f*(t*Ts))+100 for t in range(range_size)]

    return xs, ys

def plotSomething():

    try:
        xs = pickle.load(open(PICKLE_XS, "rb"))
    except EOFError:
        xs = []
    except KeyError:
        xs = []

    try:
        ys = pickle.load(open(PICKLE_YS, "rb"))
    except EOFError:
        ys = []
    except KeyError:
        ys = []

    c.setData(xs, ys)
    uiplot.qwtPlot.replot()


def plotSomething_right():


    try:
        xs = pickle.load(open(PICKLE_XS_RIGHT, "rb"))
    except EOFError:
        xs = []
    except KeyError:
        xs = []

    try:
        ys = pickle.load(open(PICKLE_YS_RIGHT, "rb"))
    except EOFError:
        ys = []
    except KeyError:
        ys = []

    c.setData(xs, ys)
    uiplot.qwtPlot.replot()


def plotSomething_left():

    try:
        xs = pickle.load(open(PICKLE_XS_LEFT, "rb"))
    except EOFError:
        xs = []
    except KeyError:
        xs = []

    try:
        ys = pickle.load(open(PICKLE_YS_LEFT, "rb"))
    except EOFError:
        ys = []
    except KeyError:
        ys = []

    c.setData(xs, ys)
    uiplot.qwtPlot.replot()


def plot_setup(window_name, length_x_axis, length_y_axis):
    app = QtGui.QApplication(sys.argv)

    ### SET-UP WINDOWS

    # WINDOW plot
    win_plot = ui_plot.QtGui.QMainWindow()
    global uiplot
    uiplot = ui_plot.Ui_win_plot()
    uiplot.setupUi(win_plot, window_name)

    global c
    c = Qwt.QwtPlotCurve()
    c.attach(uiplot.qwtPlot)

    uiplot.qwtPlot.setAxisScale(uiplot.qwtPlot.yLeft, 0, length_y_axis)
    uiplot.qwtPlot.setAxisScale(uiplot.qwtPlot.xBottom, 0, length_x_axis)

    uiplot.timer = QtCore.QTimer()
    uiplot.timer.start(1.0)

    return app, win_plot, uiplot

def plot_line():
    app, win_plot, uiplot = plot_setup(window_name="Main")
    win_plot.connect(uiplot.timer, QtCore.SIGNAL('timeout()'), plotSomething)
    ### DISPLAY WINDOWS
    win_plot.show()
    #WAIT UNTIL QT RETURNS EXIT CODE
    sys.exit(app.exec_())

def plot_line_rightchannel():
    app, win_plot, uiplot = plot_setup(window_name="FFt Right Channel")
    win_plot.connect(uiplot.timer, QtCore.SIGNAL('timeout()'), plotSomething_right)
    ### DISPLAY WINDOWS
    win_plot.show()
    #WAIT UNTIL QT RETURNS EXIT CODE
    sys.exit(app.exec_())

def plot_line_leftchannel(length_freq, height_values):
    app, win_plot, uiplot = plot_setup(window_name="FFt Left Channel",
                                       length_x_axis=length_freq,
                                       length_y_axis=height_values)
    win_plot.connect(uiplot.timer, QtCore.SIGNAL('timeout()'), plotSomething_left)
    ### DISPLAY WINDOWS
    win_plot.show()
    #WAIT UNTIL QT RETURNS EXIT CODE
    sys.exit(app.exec_())