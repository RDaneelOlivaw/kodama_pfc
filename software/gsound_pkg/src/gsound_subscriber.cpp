#include <string>
#include <math.h>
#include <stdlib.h>
#include <gsound/GSound.h>
#include <portaudio.h>
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Pose.h"
#include "tf/transform_datatypes.h"
#include <tf/LinearMath/Quaternion.h>
#include <tf/LinearMath/Matrix3x3.h>
#include "turtlesim/Pose.h"

#define PI 3.14159265

using namespace gsound;
using gsound::util::Timer;

#define GetCurrentDir getcwd
const char AUDIO_SOURCE_FILE[]="Data/acoustics.wav";
char cCurrentPath[FILENAME_MAX];

int updateListenerPosition(SoundListener& listener);

// Load a box with the specified dimensions and material.
SoundMesh* loadBox( const AABB3& box, const SoundMaterial& material );

// Sets the current working directory to the one that contains the executable.
int setCurrentDirectory();

/***
  This subscriber reads the position and orientation of the listener topic
  and changes it accordingly. It firstly creates all the Gsound environment.
  Its a room with ONE sound source and ONE listener.
  TODO: Create a service that generates whatever desired.
  TODO: Create multiple sound sources or listeners.
 **/
float joint_pos_global;
turtlesim::Pose turtle_pose_global;

void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
    float n;
    ROS_INFO("I heard: [%s]", msg->data.c_str());
    n = atof (msg->data.c_str());
    std::cout << " Value of String to Float conversion ==" << n << " \n";
    joint_pos_global = n;
}

void turtleSimCallback(const turtlesim::Pose& msg)
{
    turtle_pose_global = msg;
}


// ################### MAIN ################################

int main(int argc, char **argv)
{

    ros::init(argc, argv, "listener");
    ros::NodeHandle n;
    ros::Subscriber sub;
    //sub = n.subscribe("chatter", 1000, chatterCallback);
    sub = n.subscribe("/turtle1/pose", 1000, turtleSimCallback);



    printf("Starting GSOUND...\n");
	// Make the current directory the same as the directory containing the executable.
	setCurrentDirectory();

	// The object which performs sound propagation.
	printf("The object which performs sound propagation...\n");
	SoundPropagator propagator;

	// Enable all types of propagation paths.
    propagator.setDirectSoundIsEnabled( true );
    propagator.setTransmissionIsEnabled( true );
    propagator.setReflectionIsEnabled( true );
    propagator.setDiffractionIsEnabled( true );
    propagator.setReverbIsEnabled( true );

	//***********************************************************************

	// The object which contains a collection of all sound objects,
	// sound sources, and listeners in the scene.
	SoundScene scene;

	// The object which specifies the location and orientation of
	// the sound reciever in the scene.
	SoundListener listener;

	// Set the listener's starting position, at head height and centered at the XZ origin.
    updateListenerPosition(listener);
    //listener.setPosition( Vector3( 0, 1.5, 0 ) );

	//***********************************************************************

	// Create a material object which will be the material for the box's surface.
	// The material is specified in terms of FrequencyResponse objects that dictate
	// how sound is affected when it undergoes the associated interaction.
	SoundMaterial defaultMaterial(
		// The reflection attenuation for the material.
		FrequencyResponse::getLinearHighRolloff(1000)*
		FrequencyResponse::getLinearLowRolloff(200)*0.9,
		// The transmisison attenuation per world unit for the material.
		FrequencyResponse::getQuadraticHighRolloff(800)*0.9,
		// The absorption attenuation for the material.
		FrequencyResponse::getLinearHighRolloff()*0.5 );


	// Create an axis-aligned box that is 4x8x3 meters, centered
	// at the origin that uses the default material.
	printf("Create an axis-aligned box...\n");

    //SoundMesh* box = loadBox( AABB3( -2, 2, -1.5, 1.5, -4, 4 ), defaultMaterial );
    SoundMesh* box = loadBox( AABB3( -5, 5, -5, 5, -4, 4 ), defaultMaterial );

	SoundObject* boxObject = new SoundObject( box );

	// Set the position of the box so that it is now 1.5 units higher along the Y-axis.
    boxObject->setPosition( Vector3( 5, 5, 0 ) );

	// Add the box to the scene
	scene.addObject( boxObject );

	//***********************************************************************

	SoundSource* source = new SoundSource();

	// Set the position of the source so that it is on one side of the box.
    source->setPosition( Vector3( 5, 5, 1 ) );

	// Set the source to have an intensity of 1.
	// This is the gain applied to the source's audio when there is no
	// distance attenuation.
	source->setIntensity( 1 );

	// Create a distance attenuation object which specifies how the source's
	// audio decreases in intensity with distance.
	SoundDistanceAttenuation attenuation( 1, // Constant attenuation of 1.
		1, // Linear attenuation of 1.
		0 ); // Quadratic attenuation of 0.

	// Set the distance attenuation for the source.
	source->setDistanceAttenuation( attenuation );

	// Set the reverb distance attenuation for the source to slightly less
	// than the normal distance attenuation.
	source->setDistanceAttenuation( SoundDistanceAttenuation( 1, 0.5, 0 ) );

	// Create a WAVE file decoder that will allow the source to use a WAVE file
	// as its source audio. Use the file "sound.wav" in the current working directory.
	printf("Create a WAVE file decoder...\n");
    //WaveDecoder* decoder = new WaveDecoder( AUDIO_SOURCE_FILE );

    std::string file_path = "/home/rdaneel/Escritorio/debbugging_gsound/gsound/Data/acoustics.wav";
    //std::string file_path = "/home/rdaneel/Escritorio/debbugging_gsound/gsound/Data/sinus1000hz.wav";

    std::cout << "Follow this command: " << file_path;
    WaveDecoder* decoder = new WaveDecoder(file_path);

	// Create a SoundPlayer object which handles sound playback from a seekable
	// stream of audio. A decoder for another format (such as OGG) can be substituted
	SoundPlayer* player = new SoundPlayer( decoder );

	// Configure the sound player to start playing as soon as audio is requested and to loop.
	player->setIsPlaying( true );
	player->setIsLooping( true );

	// Set the source of the sound source's audio.
	source->setSoundInput( player );

	// Add the sound source to the scene.
	scene.addSource( source );


	//***********************************************************************

	// Create a sound propagation renderer that renders to stereo.
	printf("Create a sound propagation renderer...\n");
	SoundPropagationRenderer* renderer = new SoundPropagationRenderer(
        SpeakerConfiguration::getStereo() );

	// Create a FrequencyPartition object which specifies how the audio
	// is split into frequency band when rendering. It is recommended to use
	// band numbers that are a multiple of 4 in order to take full advantage
	// of SIMD processing.
	FrequencyPartition frequencyPartition;

	// The four frequency bands will be: 0Hz to 250Hz, 250Hz to 1000Hz,
	// 1000Hz to 4000Hz, and 4000Hz to the Nyquist Frequency.
	frequencyPartition.addSplitFrequency( 250 );
	frequencyPartition.addSplitFrequency( 1000 );
	frequencyPartition.addSplitFrequency( 4000 );

	// Tell the propagation renderer to use this frequency partition when rendering.
	renderer->setFrequencyPartition( frequencyPartition );

    //@@@@@@@@@@@@@@@@@ Device Managing Objects @@@@@@@@@@@@@@@@@@@//

    SoundDeviceManager deviceManager;
    // We put the default device Id, that normaly is zero, but we wont use. Just to mantain structure
    printf("Fetching deviceID...");
    SoundDeviceID deviceID = deviceManager.getDefaultOutputDeviceID();
    printf("Creating SoundOutputDevice...");
    SoundOutputDevice* outputDevice = new SoundOutputDevice(deviceID);
    outputDevice->setInput( renderer );

	// Start outputing audio to the device.
    outputDevice->start();

    // Create a buffer to hold the output of the propagation system.
	SoundPropagationPathBuffer pathBuffer;
    Timer timer;
    //Matrix3 new_orientation = Matrix3::rotationX(3.1);
    //listener.setOrientation(new_orientation);

    printf("Perform sound propagation ...\n");
    //while ( true )
    //while ( timer.getElapsedTime() < 300.0 && ros::ok() )
    while ( ros::ok() )
	{
        // Perform sound propagation in the scene.
		propagator.propagateSound( scene, // The scene in which to perform propagation.
			listener, // The listener to use as the sound receiver.
			4, // The maximum depth of the rays shot from the listener.
			1000, // The number of rays to shoot from the listener,
				// influences the quality of the early reflection paths.
			4, // The maximum depth of the rays shot from each sound source.
			100, // The number of rays to shoot from each sound source,
				// influences reverb estimation quality.
			pathBuffer ); // The buffer in which to put the propagation paths.

        // Update the state of the sound propagation renderer.
        renderer->updatePropagationPaths( pathBuffer );
        updateListenerPosition(listener);

	}

	// Stop outputing audio to the device and destroy it.
    printf("Destroy the various objects used for propagation...\n");
    outputDevice->stop();
    delete outputDevice;
    // Give Time for the detructors to take effect.
    sleep(5);
    return 0;

}




//##    UpdateListener
/**
It updates the position and orientation of the given listener by reading
the listener topic.
**/

int updateListenerPosition(SoundListener& listener)
{
    ros::spinOnce();

    float corrected_yaw = turtle_pose_global.theta - PI/2;
    tf::Quaternion q =  tf::createQuaternionFromYaw	(corrected_yaw);

    std::cout << " UPDATE Pos X ==" << turtle_pose_global.x << " \n";
    std::cout << " UPDATE Pos Y ==" << turtle_pose_global.y << " \n";
    std::cout << " UPDATE Pos Z ==" << turtle_pose_global.theta << " \n";
    std::cout << " UPDATE Pos Z ==" << corrected_yaw << " \n";

    tf::Matrix3x3 matrix3 = tf::Matrix3x3(q);

    listener.setPosition( Vector3( turtle_pose_global.x, turtle_pose_global.y, 0.0 ) );


    float xx = matrix3.getColumn(0).getX();
    float xy = matrix3.getColumn(0).getY();
    float xz = matrix3.getColumn(0).getZ();

    float yx = matrix3.getColumn(1).getX();
    float yy = matrix3.getColumn(1).getY();
    float yz = matrix3.getColumn(1).getZ();

    float zx = matrix3.getColumn(2).getX();
    float zy = matrix3.getColumn(2).getY();
    float zz = matrix3.getColumn(2).getZ();

    //listener.setOrientation( Matrix3 ( Vector3(xx,xy,xz), Vector3(yx,yy,yz), Vector3(zx,zy,zz) ) );
    listener.setOrientation( Matrix3 ( xx,yx,zx,xy,yy,zy,xz,yz,zz ) );
    return 0;
}



//##########################################################################################
//##########################################################################################
//############
//############		Box Loading Method
//############
//##########################################################################################
//##########################################################################################




SoundMesh* loadBox( const AABB3& box, const SoundMaterial& material )
{
    std::cout << "LOADING BOX : ";
	ArrayList<SoundVertex> vertices;
	ArrayList<SoundTriangle> triangles;

	vertices.add( SoundVertex( box.min.x, box.min.y, box.min.z ) );
	vertices.add( SoundVertex( box.max.x, box.min.y, box.min.z ) );
	vertices.add( SoundVertex( box.max.x, box.max.y, box.min.z ) );
	vertices.add( SoundVertex( box.max.x, box.max.y, box.max.z ) );
	vertices.add( SoundVertex( box.min.x, box.min.y, box.max.z ) );
	vertices.add( SoundVertex( box.min.x, box.max.y, box.max.z ) );
	vertices.add( SoundVertex( box.min.x, box.max.y, box.min.z ) );
	vertices.add( SoundVertex( box.max.x, box.min.y, box.max.z ) );

	// negative z face
	triangles.add( SoundTriangle( 0, 1, 2, 0 ) );
	triangles.add( SoundTriangle( 0, 2, 6, 0 ) );

	// positive z face
	triangles.add( SoundTriangle( 3, 5, 4, 0 ) );
	triangles.add( SoundTriangle( 3, 7, 4, 0 ) );

	// positive y face
	triangles.add( SoundTriangle( 6, 3, 5, 0 ) );
	triangles.add( SoundTriangle( 6, 2, 3, 0 ) );

	// negative y face
	triangles.add( SoundTriangle( 0, 7, 4, 0 ) );
	triangles.add( SoundTriangle( 0, 1, 7, 0 ) );

	// negative x face
	triangles.add( SoundTriangle( 0, 4, 6, 0 ) );
	triangles.add( SoundTriangle( 4, 6, 5, 0 ) );

	// positive x face
	triangles.add( SoundTriangle( 1, 7, 2, 0 ) );
	triangles.add( SoundTriangle( 7, 2, 3, 0 ) );

	ArrayList<SoundMaterial> materials;
	materials.add( material );

	return new SoundMesh( vertices, triangles, materials );
}



//##########################################################################################
//##########################################################################################
//############
//############		Current Working Directory Set Method
//############
//##########################################################################################
//##########################################################################################

// Get the path to the current file
std::string get_selfpath() {
    char buff[PATH_MAX];
    ssize_t len = ::readlink("/proc/self/exe", buff, sizeof(buff)-1);
    if (len != -1) {
      buff[len] = '\0';
      return std::string(buff);
    }
}

std::string SplitFilename (const std::string& str)
{
  size_t found;
  std::cout << "Splitting: " << str << std::endl;
  found=str.find_last_of("/\\");
  std::cout << " folder: " << str.substr(0,found) << std::endl;
  std::cout << " file: " << str.substr(found+1) << std::endl;

  std::string return_string = str.substr(0,found);

  return return_string;
}


/// Set the current working directory to be the directory containing the executable.
int setCurrentDirectory()
{
    std::cout << "SET CURRENT DIR: ";

#if defined(GSOUND_PLATFORM_LINUX)

    std::string selfpath = get_selfpath();
    std::cout << selfpath << std::endl;

    selfpath = SplitFilename (selfpath);
    std::cout << selfpath << std::endl;

    const char * c = selfpath.c_str();
    chdir(c);

    if (!GetCurrentDir(cCurrentPath, sizeof(cCurrentPath)))
    {
       return 0;
    }

    printf ("The current working directory is %s \n", cCurrentPath);



#elif defined(GSOUND_PLATFORM_APPLE)
    Size bufferSize = 1024;
	char pathToExecutable[1024];
	_NSGetExecutablePath(pathToExecutable, (uint32_t*)&bufferSize);
	Index i;
	for ( i = 0; pathToExecutable[i] != '\0'; i++ );

	for ( ; i >= 1; i-- )
	{
	    if ( pathToExecutable[i] == '/' )
	    {
			pathToExecutable[i+1] = '\0';
			break;
		}
	}
	chdir( pathToExecutable );

#elif defined(GSOUND_PLATFORM_WINDOWS)
    Size bufferSize = 1024;
	TCHAR pathToExecutable[1024];
	bufferSize = GetModuleFileName(NULL, pathToExecutable, 1024);
	for ( i = 0; pathToExecutable[i] != '\0'; i++ );

	for ( ; i >= 1; i-- )
	{
	    if ( pathToExecutable[i] == '/' || pathToExecutable[i] == '\\' )
	    {
			pathToExecutable[i+1] = '\0';
			break;
		}
	}

	SetCurrentDirectory( pathToExecutable );

#endif
}




