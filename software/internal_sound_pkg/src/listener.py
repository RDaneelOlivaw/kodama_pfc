#!/usr/bin/env python

import rospy
from internal_sound_pkg.msg import internal_sound_fft

def callback(data):
    rospy.loginfo("LEFT 78 %s", data.fft_abs_left[0])
    rospy.loginfo( "LEFT 156 %s", data.fft_abs_left[1])

def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'talker' node so that multiple talkers can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber("chatter", internal_sound_fft, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
