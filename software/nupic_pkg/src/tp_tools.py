import copy
import numpy
from nupic.research.TP import TP
from encoding_tools import create_4_encoded_animal_categories
from sp_tools import print_connected_synapses
from nupic.research.spatial_pooler import SpatialPooler


# Utility routine for printing the input vector
def formatRow(x):
    s = ''
    for c in range(len(x)):
        if c > 0 and c % 10 == 0:
            s += ' '
        s += str(x[c])
    s += ' '
    return s


def temporal_pooling_example(learning_iterations):
    """
    Example of how a temporal pool can predict a sequence of 4 different patterns
    :return:
    """
    encoded_categories_dict, length_encoded_array = create_4_encoded_animal_categories()


    number_of_things_to_learn = 4
    sp = SpatialPooler(inputDimensions=(length_encoded_array,),
                   columnDimensions=(number_of_things_to_learn,),
                   potentialRadius=15,
                   numActiveColumnsPerInhArea=1,
                   globalInhibition=True,
                   synPermActiveInc=0.03,
                   potentialPct=1.0)

    # Now we see how each column is conected initialy in the SP.
    # we use xrange just for future efficiency in big ranges. Here we could use range.
    print_connected_synapses(sp, clear=True)
    #raw_input("Press any Key To Start Learning...>")



    # Step 1: create Temporal Pooler instance with appropriate parameters
    tp = TP(numberOfCols=len(encoded_categories_dict), cellsPerColumn=2,
            initialPerm=0.5, connectedPerm=0.5,
            minThreshold=3, newSynapseCount=10,
            permanenceInc=0.1, permanenceDec=0.0,
            activationThreshold=1,
            globalDecay=0, burnIn=1,
            checkSynapseConsistency=False,
            pamLength=10)

    """
    tp = TP(numberOfCols=len(encoded_categories_dict), cellsPerColumn=2,
            initialPerm=0.5, connectedPerm=0.5,
            minThreshold=10, newSynapseCount=10,
            permanenceInc=0.1, permanenceDec=0.0,
            activationThreshold=1,
            globalDecay=0, burnIn=1,
            checkSynapseConsistency=False,
            pamLength=10)
    """

    output = numpy.zeros((number_of_things_to_learn,), dtype="int")
    ouput_dict = {}
    # We repeat the sequence 10 times
    for i in range(learning_iterations):

        # Send each letter in the sequence in order
        for _ in xrange(10):
            for key, element in encoded_categories_dict.items():

                # First learn SP
                sp.compute(element, learn=True, activeArray=output)
                print_connected_synapses(sp, clear=False)

                ouput_dict[key] = copy.deepcopy(output)

                # The compute method performs one step of learning and/or inference. Note:
                # here we just perform learning but you can perform prediction/inference and
                # learning in the same step if you want (online learning).
                tp.compute(output, enableLearn=True, computeInfOutput=False)

                # This function prints the segments associated with every cell.$$$$
                # If you really want to understand the TP, uncomment this line. By following
                # every step you can get an excellent understanding for exactly how the TP
                # learns.
                #tp.printCells()

            # The reset command tells the TP that a sequence just ended and essentially
            # zeros out all the states. It is not strictly necessary but it's a bit
            # messier without resets, and the TP learns quicker with resets.
        tp.reset()

    print str(ouput_dict)

    for key, element in ouput_dict.items():
        print "\n\n--------"+str(key)+"-----------"
        print "Raw input vector\n", element


        # Send each vector to the TP, with learning turned off
        tp.compute(element, enableLearn=False, computeInfOutput=True)

        # This method prints out the active state of each cell followed by the
        # predicted state of each cell. For convenience the cells are grouped
        # 10 at a time. When there are multiple cells per column the printout
        # is arranged so the cells in a column are stacked together
        #
        # What you should notice is that the columns where active state is 1
        # represent the SDR for the current input pattern and the columns where
        # predicted state is 1 represent the SDR for the next expected pattern
        print "\nAll the active and predicted cells:"
        tp.printStates(printPrevious=False, printLearnState=False)

        # tp.getPredictedState() gets the predicted cells.
        # predictedCells[c][i] represents the state of the i'th cell in the c'th
        # column. To see if a column is predicted, we can simply take the OR
        # across all the cells in that column. In numpy we can do this by taking
        # the max along axis 1.
        print "\n\nThe following columns are predicted by the temporal pooler. This"
        print "should correspond to columns in the *next* item in the sequence."
        predictedCells = tp.getPredictedState()
        print formatRow(predictedCells.max(axis=1).nonzero())






