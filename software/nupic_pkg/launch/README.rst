To launch Basic systems:
roscore
rosrun internal_sound_pkg talker.py
rosrun internal_sound_pkg fft_coder_publisher.py

Plot and record:
rosrun nupic_pkg plot_htm_lvlx.py
rosrun nupic_pkg show_variable.py
rosrun nupic_pkg plot_error_lvlx.py

Learn , Predict and ClusterPlot:
rosrun nupic_pkg learn_htm_v2.py



[WARN] [WallTime: 1450263920.509114] ### LVL =1 ###
[WARN] [WallTime: 1450263920.510452] neo_val ==[[0 0 0 0]
 [1 0 0 0]
 [0 0 0 0]
 [0 0 0 0]]
[WARN] [WallTime: 1450263920.511490] neo_val ==[[1 1 1 1]
 [1 1 1 0]
 [0 0 0 0]
 [0 0 0 0]]
[WARN] [WallTime: 1450263920.512481] neo_val ==[[1 1 1 1]
 [1 0 0 0]
 [0 0 0 0]
 [0 0 0 0]]
[WARN] [WallTime: 1450263920.512897] ### LVL =2 ###
[WARN] [WallTime: 1450263920.513784] neo_val ==[[0 0 0]
 [0 1 0]
 [0 0 0]]
[WARN] [WallTime: 1450263920.514758] neo_val ==[[1 1 0]
 [1 1 1]
 [1 0 0]]
[WARN] [WallTime: 1450263920.515782] neo_val ==[[1 1 0]
 [0 0 1]
 [1 1 0]]
