"""
This module is for saving data in csv fomat, more precisely FFT data
to be able to read it afterwards for modeling the HTM with swarming.
"""
__author__ = 'rdaneel'

import csv
import math
import os

ROWS = 3000

INPUT_DATA_CSV_FILE_DEFAULT_NAME = "input_data.csv"
INPUT_DATA_DEFAULT_PATH = os.path.abspath("/home/rdaneel/.local/lib/python2.7/site-packages/nupic-0.3.0.dev0-py2.7-linux-x86_64.egg/nupic/datafiles/")


def run(experiment_name=INPUT_DATA_CSV_FILE_DEFAULT_NAME):
    """
    Generates the sine.csv data. It samples 100 parts of 2*pi and evaluates it in sinus.
    :return:
    """
    input_data_file_name = experiment_name + ".csv"
    print os.path.abspath(INPUT_DATA_DEFAULT_PATH)
    input_data_file_name_path = os.path.join(os.path.abspath(INPUT_DATA_DEFAULT_PATH), input_data_file_name)

    fileHandle = open(input_data_file_name_path, "w")
    writer = csv.writer(fileHandle)
    # Headers, variables
    writer.writerow(["angle", "sine"])
    # Type of the headers
    writer.writerow(["float", "float"])
    # Flags, special types for NUPIC, not necessary now
    writer.writerow(["", ""])

    for i in range(ROWS):
        # Portions of angle, one step pi/100, 100 samples for 2*pi
        angle = (i * math.pi)/ 50.0
        sine_value = math.sin(angle)
        writer.writerow([angle, sine_value])

    fileHandle.close()

if __name__ == "__main__":
    run("sine.csv")
