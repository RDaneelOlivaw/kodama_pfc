Kodama project is a complete project for the creation of a 3D printable cognitive robot.
The idea behind is the creation of a global community that improves the hardware to make the best simple and cheap cognitive robot. This will allow anyone to have a cognitive robot. Its design is modular to allow it to be adapted to the needs of each user. All the structure is 3D printable, it has blueprints for easy assembly and a list of all the materials needed.

As for the software, it was created to also make a community where all the artificial intelligence knowledge generated is shared and combined to make each iteration more and more intelligent.
This will can be saved through the knowledge stored in pickle files. 

It has hardware drivers, ARDUINO programs, a sound simulation system for Ubuntu and  a NUPIC ROSsification for HTM neural network AI system used for learning. 
The example made is for learning from sound, but it can be modified to be used for any kind of sensory input.

If you want to use it its your to use it, just MENTION the creators.
I would be delighted that a good community is created around this project and to hear from the applications that this humble project is put into.

If you want to contribute your are more than welcome ;).

I'll be changing the documentation and adding some extra stuff like the sound database used and result as soon as possible.

If you have any doubts or suggestion just let mi know at duckfrost@gmail.com.


Here you can find:

Software: 
Contains all the ROS packages, the ARDUINO programs and the GSound modified for ubuntu for the library generation.

Kodama: 
Here you can find all the SolidWorks Files, STL for 3D printing, Blueprints for Assembly and Gazebo Model.

Resultados:
( Not uploaded Yet for technical issues )
Here you will find the results of the Sound learning of 8 different emotional tones through non supervised learning with Hierarchical Temporal Memory ( HTM) useing a NUPIC ROSsification.

Memoria_Anexos: 
Here the Documents for the project.

Documentacion_Extra: 
Some extra Info for the users interes.

Bases_de_Datos_sonido: 
( not uploaded Yet for technical issues)
All the sound files used for learning.