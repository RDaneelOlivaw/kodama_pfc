#!/usr/bin/env python
import rospy
from internal_sound_pkg.msg import internal_sound_sdr
import os
import csv
import os.path
import rospkg
import time

# Time that will be inputing sound data encoded into csv file
RECORDING_TIME = 90
ROWS = 3000

INPUT_DATA_CSV_FILE_DEFAULT_NAME = "input_data"
INPUT_DATA_DEFAULT_PATH = os.path.abspath("./myswarm/data/")
PATH_TO_DATA_PKG = "src/myswarm/data/"
DEFAULT_DATA_TAG = "data_values"

class WriteCSV(object):
    def __init__(self, experiment_name=INPUT_DATA_CSV_FILE_DEFAULT_NAME, data_tag=DEFAULT_DATA_TAG):
        input_data_file_name = experiment_name + ".csv"
        print os.path.abspath(INPUT_DATA_DEFAULT_PATH)

        rospack = rospkg.RosPack()
        pkg_path = rospack.get_path('nupic_pkg')
        path_to_data_dir = os.path.join(pkg_path,PATH_TO_DATA_PKG)
        input_data_file_name_path = os.path.join(path_to_data_dir, input_data_file_name)

        self.prepare_file_system(input_data_file_name_path)

        assert os.path.isfile(input_data_file_name_path), "DOESNT EXIST The File "+str(input_data_file_name_path)


        self.fileHandle = open(input_data_file_name_path, "w")
        self.writer = csv.writer(self.fileHandle)
        # Headers, variables
        self.writer.writerow([data_tag])
        # Type of the headers
        self.writer.writerow(["string"])
        # Flags, special types for NUPIC, not necessary now
        self.writer.writerow([])


    def prepare_file_system(self, file_to_prepare):
        if os.path.isfile(file_to_prepare):
            os.remove(file_to_prepare)

        open(file_to_prepare, 'a').close()

    def add_data_row(self, value):
        self.writer.writerow([value])

    def close(self):
        self.fileHandle.close()

    def __del__(self):
        self.close()


def listener():

    rospy.init_node('sdr_to_csv', anonymous=True)

    write_csv_object = WriteCSV()

    start_time = time.time()

    def callback(data):
        str_data = ''.join(str(int(x)) for x in data.sdr_array)
        rospy.loginfo(rospy.get_caller_id() + "SDR NOW STRING = %s", str_data)
        write_csv_object.add_data_row(value=str_data)

        if time.time()-start_time> RECORDING_TIME:
            rospy.signal_shutdown("Passed the Record Time")



    rospy.Subscriber("sound_sdr", internal_sound_sdr, callback)
    rospy.loginfo("Start WRITING SDR to CSV file...")
    rospy.spin()

if __name__ == '__main__':
    listener()
