#!/usr/bin/env python
import rospy
from nupic_pkg.msg import variable_blackboard
import random
from collections import defaultdict


PUBLISH_RATE = 10
LETERS = ["a","b","c"]

class RandomVariablePublisher(object):
    def __init__(self):
        self.value = 0

        self.pub = rospy.Publisher('/variable_blackboard', variable_blackboard, queue_size=1)

    def run(self):
        r = rospy.Rate(PUBLISH_RATE)
        while not rospy.is_shutdown():
            self.value = LETERS[random.randint(0,2)]
            self.pub.publish(self.value)
            r.sleep()

    def publish_one_value(self,value):
        self.pub.publish(str(value))


class BlackboardVariablePublisher(object):
    def __init__(self):
        self._label_detection_dict = defaultdict(int)
        self._label_count = 0
        self._result_dict = {}
        self.pub = rospy.Publisher('/variable_blackboard', variable_blackboard, queue_size=1)

    def publish_value(self, value):
        self.update_results_data(str(value))
        self.pub.publish(str(value))

    def update_results_data(self,value):
        """
        It updates the average label detection
        :return:
        """
        self._label_detection_dict[value] += 1
        self._label_count += 1

    def extract_results(self):
        """
        Returns the average percentage of each label
        :return:
        """
        for label, times_detected in self._label_detection_dict.items():
            u_percentage = times_detected / float(self._label_count)
            self._result_dict.update({label: u_percentage})

        return self._result_dict


if __name__ == '__main__':
    rospy.init_node('variable_publisher_node')

    random_object = RandomVariablePublisher()
    random_object.run()