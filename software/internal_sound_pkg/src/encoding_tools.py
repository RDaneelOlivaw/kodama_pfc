import datetime
import pprint
import numpy
#Encoders Example
from nupic.encoders import ScalarEncoder
from nupic.encoders.random_distributed_scalar import RandomDistributedScalarEncoder
from nupic.encoders.date import DateEncoder
from nupic.encoders.category import CategoryEncoder
from nupic.encoders.multi import MultiEncoder
from nupic.data.dictutils import DictObj
import rospy



LEFTCHN_NAME = "LeftChn"
RIGHTCHN_NAME = "RightChn"
CHANNELS_LIST = [LEFTCHN_NAME, RIGHTCHN_NAME]


def create_4_encoded_animal_categories():
    """
    Create a four encoded anima categories dict.
    :return:
    """
    categories = ("cat", "dog", "monkey", "slow loris")
    # w is the number of bits that we want that encodes each category.
    # the bigger the number
    encoder = CategoryEncoder(w=3, categoryList=categories, forced=True)
    cat = encoder.encode("cat")
    dog = encoder.encode("dog")
    monkey = encoder.encode("monkey")
    loris = encoder.encode("slow loris")

    encoded_categories = (cat,)
    encoded_categories = encoded_categories + (dog,)
    encoded_categories = encoded_categories + (monkey,)
    encoded_categories = encoded_categories + (loris,)

    encoded_categories_dict = dict(zip(categories, encoded_categories))
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(encoded_categories_dict)

    print "cat =       ", encoded_categories_dict["cat"]
    print "dog =       ", encoded_categories_dict["dog"]
    print "monkey =    ", encoded_categories_dict["monkey"]
    print "slow loris =", encoded_categories_dict["slow loris"]

    length_encoded_array = len(encoded_categories_dict["cat"])

    return encoded_categories_dict, length_encoded_array


def encoders_examples():
    """
    Just prints some of the encoders available in NUPIC
    :return:None
    """
    # 22 bits with 3 active representing values 0 to 100
    # clipInput=True makes values >100 encode the same as 100 (instead of throwing a ValueError)
    # forced=True allows small values for `n` and `w`
    print "### SCALAR ENCODER ###"
    enc = ScalarEncoder(n=22, w=3, minval=2.5, maxval=97.5, clipInput=True, forced=True)
    print "3 =", enc.encode(3)
    print "4 =", enc.encode(4)
    print "5 =", enc.encode(5)
    # Encode maxval
    print "100  =", enc.encode(100)
    # See that any larger number gets the same encoding
    print "1000 =", enc.encode(1000)


    # 21 bits with 3 active with buckets of size 5
    print "### RANDOM ENCODER ###"
    rdse = RandomDistributedScalarEncoder(n=21, w=3, resolution=5, offset=2.5)
    print "3 =   ", rdse.encode(3)
    print "4 =   ", rdse.encode(4)
    print "5 =   ", rdse.encode(5)
    print
    print "100 = ", rdse.encode(100)
    print "1000 =", rdse.encode(1000)


    print "### DATE ENCODER ###"
    de = DateEncoder(season=5)
    now = datetime.datetime.strptime("2014-05-02 13:08:58", "%Y-%m-%d %H:%M:%S")
    print "now =       ", de.encode(now)
    nextMonth = datetime.datetime.strptime("2014-06-02 13:08:58", "%Y-%m-%d %H:%M:%S")
    print "next month =", de.encode(nextMonth)
    xmas = datetime.datetime.strptime("2014-12-25 13:08:58", "%Y-%m-%d %H:%M:%S")
    print "xmas =      ", de.encode(xmas)

    #Categories
    print "### CATEGORIES ENCODER ###"
    categories = ("cat", "dog", "monkey", "slow loris")
    encoder = CategoryEncoder(w=3, categoryList=categories, forced=True)
    cat = encoder.encode("cat")
    dog = encoder.encode("dog")
    monkey = encoder.encode("monkey")
    loris = encoder.encode("slow loris")
    print "cat =       ", cat
    print "dog =       ", dog
    print "monkey =    ", monkey
    print "slow loris =", loris
    print "None =", encoder.encode(None)
    print "Unknown =", encoder.encode("unknown")
    print "Decode Cat categorie =", encoder.decode(cat)

    catdog = numpy.array([0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0])
    print "CatDog =", encoder.decode(catdog)

    return None


def scalarencoderTests():
    print "### SCALAR ENCODER ###"
    enc = ScalarEncoder(w=3, resolution=1, minval=1, maxval=8, clipInput=True, forced=True)
    for elem in range(0,10):
        print "ELEM = ", elem
        print "NON PER ENCODED =", enc.encode(elem)

    enc = ScalarEncoder(w=3, resolution=1, minval=1, maxval=8,
                 periodic=True, name="day of week", forced=True)
    for elem in range(1,7):
        print "ELEM = ", elem
        print "PER ENCODED =", enc.encode(elem)

def encode_sound_examples():
    """
    Some examples of how to encode FFT sound conversion and also stereo
    We have left_FFT = [12, 3, 20, 21, 9] right_FFT = [12, 3, 20, 21, 9]
    We convert with ScalarEncoders each scalar and we join it with the others in the array
    and with the other channel
    :return:


    For example,
    fieldEncodings={
        'dateTime': dict(fieldname='dateTime', type='DateEncoder',
                         timeOfDay=(5,5)),
        'attendeeCount': dict(fieldname='attendeeCount', type='ScalarEncoder',
                              name='attendeeCount', minval=0, maxval=250,
                              clipInput=True, w=5, resolution=10),
        'consumption': dict(fieldname='consumption',type='ScalarEncoder',
                            name='consumption', minval=0,maxval=110,
                            clipInput=True, w=5, resolution=5),
    }


    """

    fieldEncodings = {'dow': dict(fieldname="dow", type='ScalarEncoder', w=3, resolution=1, minval=1, maxval=8,
                                periodic=True, name="day of week", forced=True),
                      'my_val': dict(fieldname="my_val", type='ScalarEncoder', w=5, resolution=1, minval=1, maxval=10,
                                   periodic=False, name="aux", forced=True),
                      }

    rospy.logdebug("FIELD Encoding == "+str(fieldEncodings))

    e = MultiEncoder(fieldEncodings)

    rospy.logdebug("Encoding Width == "+str(e.getWidth()))
    rospy.logdebug("Description == "+str(e.getDescription()))

    assert e.getWidth() == 21, "The Width should be 21"
    assert e.getDescription() == [("day of week", 0), ("aux", 7)], "Descriptions dont match"

    d = DictObj(dow=3, my_val=10)
    expected=numpy.array([0,1,1,1,0,0,0] + [0,0,0,0,0,0,0,0,0,1,1,1,1,1], dtype='uint8')
    output = e.encode(d)
    assert(expected == output).all()

    print output
    e.pprintHeader()
    e.pprint(output)

    # Check decoding
    decoded = e.decode(output)
    #print decoded
    assert len(decoded) == 2, "Length Of decoded diff from 2"
    (ranges, desc) = decoded[0]['aux']
    assert (len(ranges) == 1 and numpy.array_equal(ranges[0], [10, 10])), "Error in lengths"
    (ranges, desc) = decoded[0]['day of week']
    assert (len(ranges) == 1 and numpy.array_equal(ranges[0], [3, 3])), "Reanges error"

    print "decodedToStr=>", e.decodedToStr(decoded)



def generate_field_encoding_dict(frequency_array, max, min, resolution, width):
    """
    With an array where all the frequency batches are specified, we get the number of fields to
    encode.
    ex: freq = [0.0, 78.125, 156.25, 234.375, 312.5, 390.625, 468.75, 546.875, 625.0, 703.125, 781.25, 859.375, 937.5, 1015.625, 1093.75, 1171.875, 1250.0, 1328.125, 1406.25, 1484.375, 1562.5, 1640.625, 1718.75, 1796.875, 1875.0, 1953.125, 2031.25, 2109.375, 2187.5, 2265.625, 2343.75, 2421.875, 2500.0, 2578.125, 2656.25, 2734.375, 2812.5, 2890.625, 2968.75, 3046.875, 3125.0, 3203.125, 3281.25, 3359.375, 3437.5, 3515.625, 3593.75, 3671.875, 3750.0, 3828.125, 3906.25, 3984.375, 4062.5, 4140.625, 4218.75, 4296.875, 4375.0, 4453.125, 4531.25, 4609.375, 4687.5, 4765.625, 4843.75, 4921.875, 5000.0, 5078.125, 5156.25, 5234.375, 5312.5, 5390.625, 5468.75, 5546.875, 5625.0, 5703.125, 5781.25, 5859.375, 5937.5, 6015.625, 6093.75, 6171.875, 6250.0, 6328.125, 6406.25, 6484.375, 6562.5, 6640.625, 6718.75, 6796.875, 6875.0, 6953.125, 7031.25, 7109.375, 7187.5, 7265.625, 7343.75, 7421.875, 7500.0, 7578.125, 7656.25, 7734.375, 7812.5, 7890.625, 7968.75, 8046.875, 8125.0, 8203.125, 8281.25, 8359.375, 8437.5, 8515.625, 8593.75, 8671.875, 8750.0, 8828.125, 8906.25, 8984.375, 9062.5, 9140.625, 9218.75, 9296.875, 9375.0, 9453.125, 9531.25, 9609.375, 9687.5, 9765.625, 9843.75, 9921.875]
    ex: left_channel_fft_array = [125.9765625, 2.771609159522003, 4.160218536378973, 1.8707107377836383, 2.823320963199056, 1.7280633669814842, 1.1189562968645144, 1.4292290280539888, 0.7365914065130478, 0.472892876381329, 0.5884362039491997, 0.5300943406183437, 1.0482059597187636, 1.437211593705032, 0.25423946893653254, 0.44173607893464845, 0.3510936574212194, 2.023467074839109, 0.0762482390743354, 0.7011747878575469, 0.7472614860399044, 0.2191675405455787, 0.477769498944964, 0.4423493343463881, 0.6364369917943073, 2.66339880419387, 0.8626922184464602, 0.5789560452496476, 0.4419292918106204, 0.24247396450205497, 0.9244294276838946, 0.2947334752174958, 0.5677209884884735, 0.250144217068978, 1.346908053159175, 0.14129687220824622, 0.24654125996901236, 0.20640565976614886, 1.0725129449338129, 0.47399682523915354, 0.20452821161783818, 0.2180877582514502, 0.7742331393097351, 0.3090097732691459, 0.11502831989099392, 0.11253814411235283, 0.09926741471707412, 0.049826561856332635, 0.14545765848854184, 0.16823991557499363, 0.5490714091577452, 0.23444278557069764, 0.0685515084508388, 0.08182795416816604, 0.04135392555327018, 0.11199886422526273, 0.14297557986808546, 0.0821731351407186, 0.13874925968388335, 0.30944007150618846, 0.3048295209659889, 0.04787052152877434, 0.1551431973925371, 0.22504524820307328, 0.470374788234074, 0.1092463820962704, 0.029340622992498358, 0.1135525255337329, 0.25692283655388126, 0.18952842206974455, 0.1860976701261626, 0.09695156378584249, 0.09106609293255905, 0.13469719813126887, 0.18977434395668202, 0.28249151192036465, 0.03705822076106382, 0.29798050773150647, 0.0878248008106257, 0.014940702820057009, 0.036946176149752714, 0.030541791078995174, 0.04169349812421969, 0.03665527506328513, 0.09043380699140888, 0.1993794172148939, 0.11446577877752889, 0.25580357944972665, 0.19209000451327352, 0.32110599405396795, 0.38824368642567636, 0.3087758032790299, 0.0998616872832026, 0.03968652540110393, 0.024861688630955427, 0.04037346912732256, 0.02628547326320492, 0.01304738118040984, 0.04824853164991961, 0.10510552347973623, 0.05776823596708642, 0.056281598644654394, 0.2612018911300832, 0.026227705149050137, 0.061523886437762534, 0.12453631150047179, 0.2055517701308349, 0.051618910649128255, 0.06633779538723496, 0.03339836274544095, 0.0411441407062747, 0.0925421356065388, 0.07709534234331926, 0.029479413627892748, 0.11543208137491646, 0.0747863006843915, 0.06463043621093119, 0.06640913814944603, 0.08746517245438883, 0.04912428839074621, 0.04908925236157547, 0.10993497299200772, 0.0564714658713642, 0.006904896238234113, 0.03971591574483578, 0.06689237057664948, 0.02855087012616704, 0.014779881372426766]
    The w chosen will be based the concept that the same frequency has diferent magnitudes and when do we consider them
    the same, similar or different, based on the resolution of human ear to differenciate between dB.
    REF.: Kodama/DOCUMENTATION_RESOURCES/Aspects of Human Hearing.PDF

    :param frequency_array_LR:
    :return:
    """
    #resolution_now = 0.1
    #width = 3
    #min = 0
    #max = 10


    fieldEncodings = {}
    freq_names_dict = {}
    channels = CHANNELS_LIST
    for channel_name in channels:
        for freq in frequency_array:

            freq_name = channel_name+"_"+str(int(freq))
            #rospy.logdebug(freq_name)
            data_name = "fft magnitud of frequencies around "+str(freq)+" in channel "+str(channel_name)
            value = dict(fieldname=freq_name, type='ScalarEncoder',
                         w=width, resolution=resolution, minval=min,
                         maxval=max, periodic=False, name=data_name, forced=True)

            fieldEncodings.update({freq_name: value})
            freq_names_dict.update({freq_name: None})

    #rospy.logdebug("FieldEncodings == "+str(fieldEncodings))

    return fieldEncodings, freq_names_dict


def clip(lo, x, hi):
    """
    We use this to guarantee thet the input values never exceed the max/min values defined
    in the init of the encoding
    :param lo:
    :param x:
    :param hi:
    :return:
    """
    return max(lo, min(hi, x))

def fill_freq_data(freq_names_dict_empty, left_channel_fft_array, right_channel_fft_array, frequency_array_LR, max_val, min_val):
    """
    Fills the freq_names_dict_empty dictionary with the data given. We apply a filter based on the max
    and min values stated in the encode definition.
    :param freq_names_dict_empty:
    :param left_channel_fft_array:
    :param right_channel_fft_array:
    :param frequency_array_LR:
    :return:
    """
    fieldEncodings = {}
    freq_names_dict = {}
    channels = CHANNELS_LIST
    rospy.logdebug("LEFT DATA == "+str(left_channel_fft_array))
    rospy.logdebug("RIGHT DATA == "+str(right_channel_fft_array))
    for channel_name in channels:
        for i in range(0, len(frequency_array_LR)):
            freq = frequency_array_LR[i]
            if channel_name == LEFTCHN_NAME:
                #freq_magnitud = left_channel_fft_array[i]
                freq_magnitud = clip(min_val, left_channel_fft_array[i], max_val)
            else:
                #freq_magnitud = right_channel_fft_array[i]
                freq_magnitud = clip(min_val, right_channel_fft_array[i], max_val)

            freq_name = channel_name+"_"+str(int(freq))
            freq_names_dict_empty.update({freq_name: freq_magnitud})

    return freq_names_dict_empty


def encode_fft_to_sdr(left_channel_fft_array, right_channel_fft_array, frequency_array_LR):
    """
    It encodes both arrays into one sigle SDR output divided by frequency batches

    For the moment we wont filter any frequency, specialy zero which always has veeery high value
    :return:
    """
    # We have to test this to avoid problems.
    # TODO: Trying to emprove preformance
    #assert len(left_channel_fft_array) == len(frequency_array_LR), "The lenght of the LEFT arrays isn't the same."
    #assert len(right_channel_fft_array) == len(frequency_array_LR), "The lenght of the RIGHT arrays isn't the same."

    fieldEncodings, freq_names_dict_empty = generate_field_encoding_dict(frequency_array=frequency_array_LR)

    rospy.logdebug("FIELD Encoding == "+str(fieldEncodings))

    e = MultiEncoder(fieldEncodings)

    rospy.logdebug("Encoding Width == "+str(e.getWidth()))
    rospy.logdebug("Description == "+str(e.getDescription()))


    freq_names_dict_empty_object = DictObj(freq_names_dict_empty)
    # TODO: Do a max value filter to avoid problems when encoding
    freq_names_dict_full = fill_freq_data(freq_names_dict_empty_object, left_channel_fft_array, right_channel_fft_array, frequency_array_LR)
    rospy.logdebug("FREQ DICT FULL ==>\n"+str(freq_names_dict_full))

    # TESTING #
    i = 1
    freq = frequency_array_LR[i]
    left_value = left_channel_fft_array[i]
    freq_name = LEFTCHN_NAME+"_"+str(int(freq))

    assert freq_names_dict_full.get(freq_name) == left_value, "The values don't correspond"
    ###########


    output = e.encode(freq_names_dict_full)

    numpy.set_printoptions(threshold=numpy.nan)

    print output
    #e.pprintHeader()
    #e.pprint(output)
    """
    assert(expected == output).all()

    print output
    e.pprintHeader()
    e.pprint(output)

    # Check decoding
    decoded = e.decode(output)
    #print decoded
    assert len(decoded) == 2, "Length Of decoded diff from 2"
    (ranges, desc) = decoded[0]['aux']
    assert (len(ranges) == 1 and numpy.array_equal(ranges[0], [10, 10])), "Error in lengths"
    (ranges, desc) = decoded[0]['day of week']
    assert (len(ranges) == 1 and numpy.array_equal(ranges[0], [3, 3])), "Reanges error"

    print "decodedToStr=>", e.decodedToStr(decoded)
    """



class FFT_To_SDR_ECNODER(object):

    def __init__(self, frequency_array_LR, data_max_value=200, data_min_value=0, resolution=0.1, width=3):

        fieldEncodings, freq_names_dict_empty = generate_field_encoding_dict(frequency_array=frequency_array_LR,
                                                                             max=data_max_value,
                                                                             min=data_min_value,
                                                                             resolution=resolution,
                                                                             width=width)
        rospy.logdebug("FIELD Encoding == "+str(fieldEncodings))
        e = MultiEncoder(fieldEncodings)

        rospy.logdebug("Encoding Width == "+str(e.getWidth()))
        rospy.logdebug("Description == "+str(e.getDescription()))

        self.freq_names_dict_empty_object = DictObj(freq_names_dict_empty)
        self.frequency_array_LR = frequency_array_LR
        self.e = e

        self.max = data_max_value
        self.min = data_min_value
        self.resolution = resolution
        self.width = width



    def encode_fft_to_sdr(self, left_channel_fft_array, right_channel_fft_array):
        freq_names_dict_full = fill_freq_data(self.freq_names_dict_empty_object,
                                              left_channel_fft_array,
                                              right_channel_fft_array,
                                              self.frequency_array_LR,
                                              self.max,
                                              self.min)

        rospy.logdebug("FREQ DICT FULL ==>\n"+str(freq_names_dict_full))
        # TESTING #
        i = 1
        freq = self.frequency_array_LR[i]
        left_value = left_channel_fft_array[i]
        freq_name = LEFTCHN_NAME+"_"+str(int(freq))

        if not freq_names_dict_full.get(freq_name) == left_value:
            rospy.logdebug("The values don't correspond")
            rospy.logdebug("ORIGINAL == "+str(left_value))
            rospy.logdebug("NEW == "+str(freq_names_dict_full.get(freq_name)))

        output = self.e.encode(freq_names_dict_full)

        numpy.set_printoptions(threshold=numpy.nan)

        return output

    def get_frequency_array_LR(self):
        """
        Returns the value of variable frequency_array_LR
        :return:
        """
        return self.frequency_array_LR