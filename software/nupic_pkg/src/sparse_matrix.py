#!/usr/bin/env python
import copy
import rospy
import numpy as np
from scipy.sparse import csr_matrix
from sklearn.preprocessing import normalize

class ClusterPlot(object):
    def __init__(self):
        self._graph = []
        self._connection_weigh = []

    def graph_update(self, value):
        self._graph.append(value)

    def connection_weigh_update(self, value):
        self._connection_weigh.append(value)

    def get_graph(self):
        return self._graph

    def get_connection_weigh(self):
        return self._connection_weigh

    def print_cluster_plot(self):
        assert len(self._graph) == len(self._connection_weigh), "The lengths of graph and connection_weight dont coincide..."

        for i in range(len(self._graph)):
            rospy.logdebug(str(self._graph[i])+"____"+str(self._connection_weigh[i]))


def convert_csr_matrix_to_cluster_plot_class(csr_matrix, roundup_decimal=2):
    """
    Converts the given csr matrix into a ClusterPlot where we can easily access the connections
    and their corresponding weights
    :param csr_matrix:
    :return:
    """
    cluster_plot_object = ClusterPlot()
    csr_matrix_array = csr_matrix.toarray()

    for row in range(csr_matrix_array.shape[0]):
        for col in range(csr_matrix_array.shape[1]):

            weight = round(csr_matrix_array.item((row, col)), roundup_decimal)

            if weight > 0:
                cluster_plot_object.graph_update((row, col))
                cluster_plot_object.connection_weigh_update(weight)

    return cluster_plot_object


def create_connect_matrix(similarity_matrix, connectivity_matrix):
    """
    Similarity_matrix bares all the wights but is specular, while connectivity bares the correct connections
    but has unitary weights. We take both and create a combined connectivity matrix with the correct weights.
    It gets normalized before return.
    :param similarity_matrix:
    :param connectivity_matrix:
    :return: norm_connection_matrix
    """

    similarity_matrix_array = similarity_matrix.toarray()
    connectivity_matrix_array = connectivity_matrix.toarray()

    row = []
    col = []
    data = []

    row_dim = connectivity_matrix_array.shape[0]
    col_dim = connectivity_matrix_array.shape[1]

    for num_row in range(row_dim):
        for num_col in range(col_dim):

            connection = connectivity_matrix_array.item((num_row, num_col))

            if connection > 0:
                row.append(num_row)
                col.append(num_col)

                weight = similarity_matrix_array.item((num_row, num_col))
                data.append(weight)

    connection_matrix = csr_matrix((data, (row, col)), shape=(row_dim, col_dim))

    # Normalize through rows with norm l1, the only one that gives sum 1 by rows
    norm_connection_matrix = normalize(connection_matrix, norm='l1', axis=1)

    return norm_connection_matrix

if __name__ == "__main__":
    indptr = np.array([0, 2, 3, 6])
    indices = np.array([0, 2, 2, 0, 1, 2])
    data = np.array([1, 2, 3, 4, 5, 2])
    A = csr_matrix((data, indices, indptr), shape=(3, 3))
    print A

    cp = convert_csr_matrix_to_cluster_plot_class(A)

    cp.print_cluster_plot()
