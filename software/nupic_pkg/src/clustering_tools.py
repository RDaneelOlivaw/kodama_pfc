#!/usr/bin/env python

import rospy
import numpy as np
from scipy.sparse import csr_matrix
from sklearn.cluster import AgglomerativeClustering

SEPARATOR = "-"

"""
Print segment information for verbose messaging and debugging.
This uses the following format:

ID:54413 True 0.64801 (24/36) 101 [9,1]0.75 [10,1]0.75 [11,1]0.75

where:
  54413 - is the unique segment id
  True - is sequence segment
  0.64801 - moving average duty cycle
  (24/36) - (numPositiveActivations / numTotalActivations)
  101 - age, number of iterations since last activated
  [9,1]0.75 - synapse from column 9, cell #1, strength 0.75
  [10,1]0.75 - synapse from column 10, cell #1, strength 0.75
  [11,1]0.75 - synapse from column 11, cell #1, strength 0.75

  isActiveStr = "*" if isActive else " "
"""

def generate_patern_sequence_correspondance(lvlx_tp, n_clusters, synaptic_min = 0.0, segment_dutyCycle_value_min = 0.001):
    """
    Given a Tp object list, having info of all the HTM levels learned,
    we extract the dictionary that makes the correspondance of each levels paterns
    to the sequence which they belong to.
    :param tp_lvl_list:
    :return:
    """

    rospy.logdebug("<== Clustering Config Data ==>")
    rospy.logdebug("SIMETRIC SYNAPTIC MIN ="+str(synaptic_min))
    rospy.logdebug("segment_dutyCycle_value_min ="+str(segment_dutyCycle_value_min))
    rospy.logdebug("N Clusters ="+str(n_clusters))

    similarity_matrix_sim, connectivity_matrix_sim, working_cells_list, tp_general_data = get_tp_ell_data_clean(lvlx_tp, synaptic_min, segment_dutyCycle_value_min)

    rospy.logdebug("similarity_matrix ==>"+str(similarity_matrix_sim))
    rospy.logdebug("connectivity_matrix ==>"+str(connectivity_matrix_sim))
    rospy.logdebug("working_cells_list ==>"+str(working_cells_list))
    rospy.logdebug("TP DATA ==>"+str(tp_general_data))

    linkage = 'ward'
    rospy.logdebug("LINKAGE =="+str(linkage))

    model = AgglomerativeClustering(linkage=linkage,
                                    connectivity=connectivity_matrix_sim,
                                    n_clusters=n_clusters)
    rospy.logdebug("SIMILARITY DENSE==>\n"+str(similarity_matrix_sim.todense()))

    model.fit(similarity_matrix_sim.todense())

    rospy.logdebug(model.labels_)

    cluster_working_cell_correspondence_dict = cluster_correspondance_real(working_cell_list=working_cells_list,
                                                                           cluster_list=model.labels_)

    rospy.logdebug(cluster_working_cell_correspondence_dict)

    return cluster_working_cell_correspondence_dict, similarity_matrix_sim, connectivity_matrix_sim, working_cells_list


def cluster_correspondance_real(working_cell_list, cluster_list):
    """
    Returns the cluster which the patern with col_cell_id corresponds to.
    :param col_cell_id:
    :param working_cell_list:
    :param cluster_list:
    :return:
    """

    assert len(cluster_list) == len(working_cell_list), "Lengths of cluster list and working cells dont correspond."
    cluster_working_cell_correspondence_dict = {}

    for idx, cell_name in enumerate(working_cell_list):
        cluster_value = cluster_list[idx]
        cluster_working_cell_correspondence_dict.update({cell_name: cluster_value})

    return cluster_working_cell_correspondence_dict

def cluster_correspondance(col_cell_patern_correspondance, working_cell_list, cluster_list):
    """
    Returns the cluster which the patern with col_cell_id corresponds to.
    :param col_cell_id:
    :param working_cell_list:
    :param cluster_list:
    :return:
    """

    assert len(cluster_list) == len(working_cell_list), "Lengths of cluster list and working cells dont correspond."
    cluster_pattern_dict = {}

    for idx, cell_name in enumerate(working_cell_list):
        pattern_name = col_cell_patern_correspondance.get(cell_name)
        cluster_value = cluster_list[idx]
        cluster_pattern_dict.update({pattern_name:cluster_value})

    return cluster_pattern_dict


def generate_sparse_csr_connectivity_matrix(row_v, col_v, data_v, dimension):
    row = np.array(row_v)
    col = np.array(col_v)
    data = np.array(data_v)
    final_matrix = csr_matrix((data, (row, col)), shape=(dimension, dimension))
    return final_matrix


def generate_connectivity_matrix_simetric(working_cells_list, similarity_dict, dimension, synaptic_min):
    """
    Here we access the similarity_dict having each conection that leads to a certain cell
    with its corresponding synaptic strength.
    :param similarity_dict:
    :param dimension:
    :return:
    """
    row_v_conn = []
    col_v_conn = []
    data_bin_conn = []

    row_v_sim = []
    col_v_sim = []
    data_v_sim = []


    accepted_cells = []


    for landing_cell_name, synapse_to_landing_cell_connection_dict in similarity_dict.iteritems():
        rospy.logdebug("Landing Cell NAME ==>"+landing_cell_name)
        rospy.logdebug("Synapes ==>"+str(synapse_to_landing_cell_connection_dict))
        for takeoff_cell_name, connection_intensity in synapse_to_landing_cell_connection_dict.iteritems():

            rospy.logdebug("################################################")
            rospy.logdebug("takeoff_cell_name = "+str(takeoff_cell_name))
            rospy.logdebug("landing_cell_name = "+str(landing_cell_name))
            rospy.logdebug("ROW INDEX = "+str(working_cells_list.index(takeoff_cell_name)))
            rospy.logdebug("COL INDEX = "+str(working_cells_list.index(landing_cell_name)))
            rospy.logdebug("CONNECTION INTENSITY = "+str(connection_intensity))
            rospy.logdebug("################################################")


            if connection_intensity >= synaptic_min:

                # CONN
                row_v_conn.append(working_cells_list.index(takeoff_cell_name))
                col_v_conn.append(working_cells_list.index(landing_cell_name))
                data_bin_conn.append(int(connection_intensity > 0))
                # SIM
                row_v_sim.append(working_cells_list.index(takeoff_cell_name))
                col_v_sim.append(working_cells_list.index(landing_cell_name))
                data_v_sim.append(connection_intensity)

                # Simetric Copy
                row_v_sim.append(working_cells_list.index(landing_cell_name))
                col_v_sim.append(working_cells_list.index(takeoff_cell_name))
                data_v_sim.append(connection_intensity)

                if takeoff_cell_name not in accepted_cells:
                    accepted_cells.append(takeoff_cell_name)
                if landing_cell_name not in accepted_cells:
                    accepted_cells.append(landing_cell_name)

            else:
                rospy.logwarn("CONNECTION TOO WEAK,  NOT ACCEPTED")

    not_accepted_cells = [x for x in working_cells_list if x not in accepted_cells]

    rospy.logdebug("LIST OF ACCEPTED CELS ="+str(accepted_cells))
    rospy.logdebug("LIST OF NOT ACCEPTED CELS ="+str(not_accepted_cells))
    rospy.logdebug("LIST OF WORKCELL_LIST ="+str(working_cells_list))

    row_conn = np.array(row_v_conn)
    col_conn = np.array(col_v_conn)
    data_b_conn = np.array(data_bin_conn)
    dimension_conn = dimension


    row_sim = np.array(row_v_sim)
    col_sim = np.array(col_v_sim)
    data_sim = np.array(data_v_sim)
    dimension_sim = dimension

    similarity_matrix = generate_sparse_csr_connectivity_matrix(row_v=row_sim, col_v=col_sim, data_v=data_sim, dimension=dimension_sim)
    connectivity_matrix = generate_sparse_csr_connectivity_matrix(row_v=row_conn, col_v=col_conn, data_v=data_b_conn, dimension=dimension_conn)


    # TODO: Normalise MATRIXES BY ROWS
    # BARE IN MIND THAT THE WORKINGLIST LENGTH might be bigger than the connected nodes deppedning on the synaptic_min used

    return similarity_matrix, connectivity_matrix

def ExtractSegmentData(segment):
    """Print segment information for verbose messaging and debugging.
    This uses the following format:

     ID:54413 True 0.64801 (24/36) 101 [9,1]0.75 [10,1]0.75 [11,1]0.75

    where:
      54413 - is the unique segment id
      True - is sequence segment
      0.64801 - moving average duty cycle
      (24/36) - (numPositiveActivations / numTotalActivations)
      101 - age, number of iterations since last activated
      [9,1]0.75 - synapse from column 9, cell #1, strength 0.75
      [10,1]0.75 - synapse from column 10, cell #1, strength 0.75
      [11,1]0.75 - synapse from column 11, cell #1, strength 0.75
    """
    segment_data = {}
    # Segment ID
    segment_data.update({"ID": segment.segID})

    # Sequence segment or pooling segment
    segment_data.update({"isSequenceSeg": segment.isSequenceSeg})

    # Duty cycle
    segment_data.update({"dutyCycle": segment.dutyCycle(readOnly=True)})

    # numPositive/totalActivations
    segment_data.update({"positiveActivations": segment.positiveActivations})
    segment_data.update({"totalActivations": segment.totalActivations})
    # Age
    segment_data.update({"Age": segment.tp.lrnIterationIdx - segment.lastActiveIteration})

    # Print each synapses on this segment as: srcCellCol/srcCellIdx/perm
    # if the permanence is above connected, put [] around the synapse info
    segment_synapse = []

    sortedSyns = sorted(segment.syns)
    for _, synapse in enumerate(sortedSyns):
        rospy.logdebug("[%d,%d]%4.2f" % (synapse[0], synapse[1], synapse[2]),)
        segment_synapse.append(synapse)

    segment_data.update({"segment_synapse": segment_synapse})

    return segment_data

def convert_cell_name(c,i):
    return str(c)+SEPARATOR+str(i)

def get_tp_ell_data_clean(tp, synaptic_min, segment_dutyCycle_value_min):
    """
    Get Relevant Info from TP
    :param tp:
    :return:
    """
    tp_general_data = {}

    tp_general_data.update({"activationThreshold": tp.activationThreshold})
    tp_general_data.update({"minThreshold": tp.minThreshold})
    tp_general_data.update({"connectedPerm": tp.connectedPerm})
    tp_general_data.update({"numberOfCols": tp.numberOfCols})
    tp_general_data.update({"cellsPerColumn": tp.cellsPerColumn})

    landing_cells_list = set([])
    takeoff_cells_list = set([])
    similarity_dict = {}

    for c in xrange(tp.numberOfCols):
        for i in xrange(tp.cellsPerColumn):
            if len(tp.cells[c][i]) > 0:
                rospy.logdebug("Column="+str(c)+", Cell="+str(i)+" :")
                landing_cell = convert_cell_name(c, i)
                synapse_to_landing_cell_connection_dict = {}

                rospy.logdebug(str(len(tp.cells[c][i]))+" = segment(s)")
                landing_cell_number_relevant_segments = 0
                for j, segment in enumerate(tp.cells[c][i]):
                    rospy.logdebug("SEGMENT INFO ? ==>"+str(j))
                    segment_data = ExtractSegmentData(segment)
                    rospy.logdebug("SEGMENT DATA ==>"+str(segment_data))
                    segment_synapes_list = segment_data.get("segment_synapse")
                    segment_dutyCycle_value = segment_data.get("dutyCycle")
                    rospy.logdebug("SEGMENT SYNAPSE DATA ==>"+str(segment_synapes_list))

                    if segment_dutyCycle_value >= segment_dutyCycle_value_min:
                        rospy.logdebug("dutyCycle_value= "+str(segment_dutyCycle_value)+" > "+str(segment_dutyCycle_value)+", ACCEPTED ")
                        for seg_element in segment_synapes_list:
                            col = seg_element[0]
                            cell_num = seg_element[1]
                            connection_intensity = seg_element[2]
                            cell_name = convert_cell_name(col, cell_num)

                            synapse_to_landing_cell_connection_dict.update({cell_name:connection_intensity})
                            takeoff_cells_list.add(cell_name)

                            landing_cell_number_relevant_segments += 1
                    else:
                        rospy.logdebug("dutyCycle_value= "+str(segment_dutyCycle_value)+" > "+str(segment_dutyCycle_value)+", NOT ACCEPTED ")

                if landing_cell_number_relevant_segments > 0:
                    # The landing cell has at least one synapse that was relevant.
                    similarity_dict.update({landing_cell: synapse_to_landing_cell_connection_dict})
                    landing_cells_list.add(landing_cell)

    working_cells_list = list(landing_cells_list.union(takeoff_cells_list))


    sim_matrix_sim, conn_matrix_sim = generate_connectivity_matrix_simetric(working_cells_list=working_cells_list,
                                                                            similarity_dict=similarity_dict,
                                                                            dimension=len(working_cells_list),
                                                                            synaptic_min=synaptic_min)


    return sim_matrix_sim, conn_matrix_sim, working_cells_list, tp_general_data
