#include <sstream>
#include <string>
#include <math.h>
#include <gsound/GSound.h>
#include <portaudio.h>


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Vector3.h"
#include "geometry_msgs/Quaternion.h"


#include "gsound_pkg/Listeners_Array.h"
#include "gsound_pkg/Listener.h"

using namespace gsound;
using gsound::util::Timer;

const char AUDIO_SOURCE_FILE[]="Data/acoustics.wav";
char cCurrentPath[FILENAME_MAX];


/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */
int main(int argc, char **argv)
{

    SoundPropagator propagator;
    /**
    * The ros::init() function needs to see argc and argv so that it can perform
    * any ROS arguments and name remapping that were provided at the command line. For programmatic
    * remappings you can use a different version of init() which takes remappings
    * directly, but for most command-line programs, passing argc and argv is the easiest
    * way to do it.  The third argument to init() is the name of the node.
    *
    * You must call one of the versions of ros::init() before using any other
    * part of the ROS system.
    */
    ros::init(argc, argv, "talker");

    /**
    * NodeHandle is the main access point to communications with the ROS system.
    * The first NodeHandle constructed will fully initialize this node, and the last
    * NodeHandle destructed will close down the node.
    */
    ros::NodeHandle n;

    /**
    * The advertise() function is how you tell ROS that you want to
    * publish on a given topic name. This invokes a call to the ROS
    * master node, which keeps a registry of who is publishing and who
    * is subscribing. After this advertise() call is made, the master
    * node will notify anyone who is trying to subscribe to this topic name,
    * and they will in turn negotiate a peer-to-peer connection with this
    * node.  advertise() returns a Publisher object which allows you to
    * publish messages on that topic through a call to publish().  Once
    * all copies of the returned Publisher object are destroyed, the topic
    * will be automatically unadvertised.
    *
    * The second parameter to advertise() is the size of the message queue
    * used for publishing messages.  If messages are published more quickly
    * than we can send them, the number here specifies how many messages to
    * buffer up before throwing some away.
    */
    //ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);
    ros::Publisher chatter_pub = n.advertise<gsound_pkg::Listeners_Array>("chatter", 1000);


    ros::Rate loop_rate(10);

    /**
    * A count of how many messages we have sent. This is used to create
    * a unique string for each message.
    */
    int count = 0;
    while (ros::ok())
    {
        /**
         * This is a message object. You stuff it with data, and then publish it.
         */

        /**
        std_msgs::String msg;
        std::stringstream ss;
        ss << count;
        msg.data = ss.str();
        ROS_INFO("%s", msg.data.c_str());
        **/


        gsound_pkg::Listener msg1 ;
        gsound_pkg::Listeners_Array msg;
        printf("Create Geometry \n");
        geometry_msgs::Vector3 pos;
        pos.x = 1.0;
        pos.y = 0.0;
        pos.z = 0.0;
        geometry_msgs::Vector3 vel;
        vel.x = 1.0;
        vel.y = 0.0;
        vel.z = 0.0;
        geometry_msgs::Quaternion q;
        q.x = 1.0;
        q.y = 0.0;
        q.z = 0.0;
        q.w = 1.0;
        printf("Create Fill Message \n");
        msg1.position = pos;
        msg1.velocity = vel;
        msg1.orientation = q;
        std::vector<gsound_pkg::Listener> pp;
        pp.push_back(msg1);
        msg.listeners_data_array = pp;



        /**
         * The publish() function is how you send messages. The parameter
         * is the message object. The type of this object must agree with the type
         * given as a template parameter to the advertise<>() call, as was done
         * in the constructor above.
         */
        chatter_pub.publish(msg);

        ros::spinOnce();

        loop_rate.sleep();
        //++count;
    }


  return 0;
}
