import matplotlib.pyplot as plt
import time
plt.ion()
class DynamicUpdate():
    #Suppose we know the x range

    def __init__(self, sound_data_iterable=None, is_fft=False):

        self.min_x = 0
        self.max_x = 10000
        self.max_x_test = 10
        self.sound_data = sound_data_iterable
        self.x_increment = 10
        self._is_fft = is_fft


    def RateLimited(maxPerSecond):
        minInterval = 1.0 / float(maxPerSecond)
        def decorate(func):
            lastTimeCalled = [0.0]
            def rateLimitedFunction(*args,**kargs):
                elapsed = time.clock() - lastTimeCalled[0]
                leftToWait = minInterval - elapsed
                if leftToWait>0:
                    time.sleep(leftToWait)
                ret = func(*args,**kargs)
                lastTimeCalled[0] = time.clock()
                return ret
            return rateLimitedFunction
        return decorate

    def on_launch(self):
        #Set up plot
        self.figure, self.ax = plt.subplots()
        self.lines, = self.ax.plot([],[],"*")
        #Autoscale on unknown axis and known lims on the other
        self.ax.set_autoscaley_on(True)
        if self.sound_data:
            self.ax.set_xlim(self.min_x, self.max_x)
        else:
            self.ax.set_xlim(self.min_x, self.max_x_test)
        #Other stuff
        self.ax.grid()

    @RateLimited(maxPerSecond=300)
    def on_running(self, xdata, ydata):
        #Update data (with the new _and_ the old points)
        self.lines.set_xdata(xdata)
        self.lines.set_ydata(ydata)
        #Need both of these in order to rescale
        self.ax.relim()
        self.ax.autoscale_view()
        #We need to draw *and* flush
        self.figure.canvas.draw()
        self.figure.canvas.flush_events()

    #Example
    def __call__(self):
        import numpy as np
        import time
        self.on_launch()
        xdata = []
        ydata = []
        if not self.sound_data:
            for x in np.arange(0,10,0.5):
                xdata.append(x)
                ydata.append(np.exp(-x**2)+10*np.exp(-(x-7)**2))
                self.on_running(xdata, ydata)
                time.sleep(0.1)
        else:

            if self._is_fft:
                print "--- DRAWING FFT ---"
                self.on_running(self.sound_data.fft_xdata, self.sound_data.fft_ydata)
            else:
                for x in np.arange(0, self.max_x, self.x_increment):
                    xdata.append(x)
                    ydata.append(self.sound_data.audio)

                    lx = len(xdata)
                    ly = len(ydata)
                    if lx == ly:
                        self.on_running(xdata, ydata)
                    else:
                        print "LENGTH NOT THE SAME"

                i = xdata[-1]
                incr = 0
                while True:
                    xdata.pop(0)
                    ydata.pop(0)
                    i += self.x_increment
                    incr += self.x_increment
                    xdata.append(i)
                    ydata.append(self.sound_data.audio)
                    self.ax.set_xlim(self.min_x+incr, self.max_x+incr)
                    self.on_running(xdata, ydata)


        return xdata, ydata


if __name__ == "__main__":
    d = DynamicUpdate()
    d()