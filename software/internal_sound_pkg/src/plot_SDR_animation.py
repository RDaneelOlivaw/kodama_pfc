#!/usr/bin/env python
import rospy
from pylab import *
from internal_sound_pkg.msg import internal_sound_sdr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import math


class PlotSDR(object):
    def __init__(self, length_array):
        self.exact_col = math.sqrt(length_array)
        self.COL_N_ELEMENTS = int(self.exact_col)+1
        self.extra_length = pow(self.COL_N_ELEMENTS,2)-length_array
        self.extra_array = [0]*self.extra_length

        self.length_array = pow(self.COL_N_ELEMENTS,2)

        print len(self.extra_array)
        print "EXACT COL =="+str(self.exact_col)
        print "EXTRA COL =="+str(self.extra_length)

    def convert_sdr_to_matrix(self, data_array):
        fixed_data_array = data_array+self.extra_array

        assert len(fixed_data_array) == pow(self.COL_N_ELEMENTS,2), "Columns dont match with fixed array"

        np_array = np.asarray(fixed_data_array)
        #sdr_array = np.floor(np_array)
        sdr_matrix = reshape(np_array, (-1, self.COL_N_ELEMENTS))

        return sdr_matrix


def listener(video_file_path=None):

    FPS = 1.0/10.0

    rospy.init_node('sdr_plotter', anonymous=True)

    msg = rospy.wait_for_message("sound_sdr", internal_sound_sdr)
    array_sdr = msg.sdr_array

    plt_sdr_object = PlotSDR(length_array=len(array_sdr))

    SIZE = 100

    fig = plt.figure()

    def f(func_array_sdr):
        neo_val = plt_sdr_object.convert_sdr_to_matrix(func_array_sdr)
        return neo_val

    im = plt.imshow(f(array_sdr), cmap=plt.get_cmap('gnuplot2'))

    def updatefig(*args):

        msg = rospy.wait_for_message("sound_sdr", internal_sound_sdr)
        update_array_sdr = msg.sdr_array
        new_val = f(update_array_sdr)
        im.set_array(new_val)
        return im,

    ani = animation.FuncAnimation(fig, updatefig, interval=50, blit=True)
    #plt.show()

    if video_file_path:
        mywriter = animation.FFMpegWriter(bitrate=500)
        ani.save(video_file_path, fps=300, dpi=600, writer=mywriter, codec="libx264")
        rospy.loginfo("Recording Ended...")
    else:
        plt.show()



if __name__ == '__main__':
    video_file_path = None
    #video_file_path = '/media/rdaneel/Data/Kodama/Videos/sdr_test.mp4'
    listener(video_file_path=video_file_path)