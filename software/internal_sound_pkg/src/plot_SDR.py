#!/usr/bin/env python
import rospy
from pylab import *
import matplotlib.pyplot as plt
import numpy as np
import time
import random
from internal_sound_pkg.msg import internal_sound_sdr


class PlotSDR(object):
    def __init__(self, fps, length_array):
        self.FPS = fps
        self.exact_col = math.sqrt(length_array)
        self.COL_N_ELEMENTS = int(self.exact_col)+1
        self.extra_length = pow(self.COL_N_ELEMENTS,2)-length_array
        self.extra_array = [1]*self.extra_length

        self.length_array = pow(self.COL_N_ELEMENTS,2)

        ion()
        # for profiling
        self.tstart = time.time()
        print "FPS ==>"+str(self.FPS)
        print self.tstart
        print len(self.extra_array)
        print "EXACT COL =="+str(self.exact_col)

    def plot_1frame_sdr(self, data_array):
        if time.time() - self.tstart > self.FPS:
            self.tstart = time.time()
            fixed_data_array = data_array+self.extra_array

            assert len(fixed_data_array) == pow(self.COL_N_ELEMENTS,2), "Columns dont match with fixed array"

            np_array = np.asarray(fixed_data_array)
            sdr_array = np.floor(np_array)
            sdr_matrix = reshape(sdr_array, (-1, self.COL_N_ELEMENTS))
            # update the data
            plt.imshow(sdr_matrix, cmap='Greys',  interpolation='nearest')
            # redraw the canvas
            draw()


def plot_sdr_example():
    COL_N_ELEMENTS = 300
    length_array = pow(COL_N_ELEMENTS,2)
    init_array = [random.random()+0.5 for i in range(length_array)]



    plt_sdr_object = PlotSDR(fps=1.0/10.0, length_array=len(init_array))
    length_array = pow(COL_N_ELEMENTS,2)
    while True:
        data_array = [random.random()+0.5 for i in range(length_array)]
        plt_sdr_object.plot_1frame_sdr(data_array=data_array)


def callback(data):

    #rospy.loginfo(rospy.get_caller_id() + "SDR NOW = %s", data.sdr_array)
    plt_sdr_object.plot_1frame_sdr(data_array=data.sdr_array)


def listener():

    FPS = 1.0/10.0

    rospy.init_node('sdr_plotter', anonymous=True)

    msg = rospy.wait_for_message("sound_sdr", internal_sound_sdr)
    array_sdr = msg.sdr_array
    #rospy.loginfo("SDR ARRAY => "+str(array_sdr))

    global plt_sdr_object
    plt_sdr_object = PlotSDR(fps=FPS, length_array=len(array_sdr))

    rospy.Subscriber("sound_sdr", internal_sound_sdr, callback)
    rospy.loginfo("Start Plotting INTERNAL SOUND SDR Now...")
    rospy.spin()

if __name__ == '__main__':
    listener()