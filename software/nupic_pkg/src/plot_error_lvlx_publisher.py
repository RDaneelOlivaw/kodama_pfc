#!/usr/bin/env python
import rospy
from plot_error import publisher_error_lvlx


if __name__ == '__main__':
    while not rospy.is_shutdown():

        publish_rate = 20
        num_lvls = 5
        publisher_error_lvlx(publish_rate, num_lvls)