#!/usr/bin/env python
import rospy
from pylab import *
from internal_sound_pkg.msg import internal_sound_sdr
from nupic_pkg.msg import graph
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import math
import time
import random

class PlotGraph(object):
    def __init__(self, t_init):
        self._t_init = t_init
        self._t_now = 0

    def update_t_now(self, t_value):
        t_now_new = (t_value - self._t_init)
        print "T NEW NOW = "+str(t_now_new)
        self._t_now = t_now_new

    def get_t_now(self):
        return self._t_now


def plot_array(x_max, y_max, interval_t, t_steps, acceptable_error_limit, topic_name, node_name, line_colour, dot_colour, fig_subtitle, main_title, x_label, y_label):

    rospy.init_node(node_name, anonymous=True)

    msg = rospy.wait_for_message(topic_name, graph)
    #t_init = msg.t_value
    #t_init = time.time()
    t_init = rospy.get_time()
    plt_graph_object = PlotGraph(t_init)


    fig, ax = plt.subplots()

    fig.suptitle(fig_subtitle, fontsize=14, fontweight='bold')
    line, = ax.plot([], [], color=line_colour, lw=2)
    ax.set_ylim(0, y_max)
    ax.set_xlim(0, x_max)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.grid()
    xdata, ydata = [], []

    def update_t(t_value):
        return plt_graph_object.update_t_now(t_value)

    def run(*args):
        # update the data
        msg = rospy.wait_for_message(topic_name, graph)
        y = msg.y_value
        t_new = rospy.get_time()
        #t_new = time.time()

        print "Y = "+str(y)

        update_t(t_new)
        t = plt_graph_object.get_t_now()

        if y < acceptable_error_limit:
            ax.plot([t], [y], 'o', color=dot_colour)

        xdata.append(t)
        ydata.append(y)
        xmin, xmax = ax.get_xlim()
        if t >= xmax:
            new_xmin = xdata[1]
            new_xmax = xdata[len(xdata)-1] + t_steps
            ax.set_xlim(new_xmin, new_xmax)
            xdata.pop(0)
            ydata.pop(0)
            ax.figure.canvas.draw()

        line.set_data(xdata, ydata)

        return line,

    ani = animation.FuncAnimation(fig, run, blit=True, interval=interval_t, repeat=False)
    plt.title(main_title)
    plt.show()


if __name__ == '__main__':
    x_max = 10
    y_max = 1
    interval_t = 20
    t_steps = interval_t/1000.0
    acceptable_error_limit = 0.2
    topic_name = 'graph_data_lv1'
    node_name = 'error_plotter_LV1'
    line_colour = 'black'
    dot_colour = 'red'
    fig_subtitle = 'Error in the learning fase in level 1 of the HTM'
    main_title = ''
    x_label = 'time [seconds]'
    y_label = 'error'
    plot_array(x_max, y_max, interval_t, t_steps, acceptable_error_limit, topic_name,
               node_name, line_colour, dot_colour, fig_subtitle, main_title,
               x_label, y_label)