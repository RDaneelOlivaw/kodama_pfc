// Sweep
// by BARRAGAN <http://barraganstudio.com> 
// mod by RDaneelOlivaw duckfrost@gmail.com
// This example code is in the public domain.

#include <TinkerKit.h>
#include <Servo.h> 

const int max_repeat = 3; // maximum time we repeat a sweep
const int max_pos = 140;
const int min_pos = 30;

const int max_pos_array[3] = {60,110,110};
const int min_pos_array[3] = {30,70,70};

Servo myservo1;// finguer 30-60
Servo myservo2;// neck 80-100
Servo myservo3;// head 70-140
 
int pos = 0;    // variable to store the servo position 
int pos1 = 0;
int pos2 = 0;
int pos3 = 0;
int repetitions = 0; // Times a sweep is performed

int steps = 0;

void setup() 
{ 
  //Serial.begin(115200);
  myservo1.attach(O1);
  myservo2.attach(O2);
  myservo3.attach(O3);
} 
 
 
void loop() 
{ 
    //Serial.println("########### UP");
    //delay(1000);
  for(pos = min_pos; pos < max_pos; pos += 1)  // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
  
    pos1 = servoLimits(0,pos);
    pos2 = servoLimits(1,pos);
    pos3 = servoLimits(2,pos);    
    
    Serial.println(pos1);  
    myservo1.write(pos1);              // tell servo to go to position in variable 'pos' 
    myservo2.write(pos2);              // tell servo to go to position in variable 'pos' 
    myservo3.write(pos3);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  }
 
  //Serial.println("DOWN #################");
  //delay(1000);
  for(pos = max_pos; pos>min_pos; pos-=1)     // goes from 180 degrees to 0 degrees 
  {

    pos1 = servoLimits(0,pos);
    pos2 = servoLimits(1,pos);
    pos3 = servoLimits(2,pos);

    Serial.println(pos1); 
    myservo1.write(pos1);              // tell servo to go to position in variable 'pos' 
    myservo2.write(pos2);              // tell servo to go to position in variable 'pos' 
    myservo3.write(pos3);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  }
  repetitions += 1;
  if (repetitions >= max_repeat){
    delay(1000);
    repetitions = 0;
  }
} 


int servoLimits(int i_number, int servo_pos)
{
 int s_num;
 int i_MAX_POS = max_pos_array[i_number];
 int i_MIN_POS = min_pos_array[i_number];

 if (servo_pos > i_MAX_POS)
 {
   s_num = i_MAX_POS;
 }
 else if (servo_pos < i_MIN_POS)
 {
   s_num = i_MIN_POS;
 }
 else
 {
   s_num = servo_pos;
 }
 return s_num;
}
