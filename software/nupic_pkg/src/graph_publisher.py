#!/usr/bin/env python

import rospy
from nupic_pkg.msg import graph
import random
import time

PUBLISH_RATE = 10

def update_msg(msg):
    """
    Updates the values of the msg based on the values in IR
    :param msg:
    :param ir_object:
    :return:
    """

    msg.y_value = random.random()

    return msg

def talker():
    rospy.init_node('graph_publisher', anonymous=True)

    pub = rospy.Publisher('graph_data', graph, queue_size=1)

    rate = rospy.Rate(PUBLISH_RATE)

    msg = graph()
    update_msg(msg)

    while not rospy.is_shutdown():

        update_msg(msg)
        rospy.loginfo(msg)
        pub.publish(msg)
        rate.sleep()



if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass

