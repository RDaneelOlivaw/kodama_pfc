"""
This script works with ARDUINO script servo3_clean.ino
"""

from time import sleep
import serial

class ServoArduinoObject(object):

    def __init__(self):
        self._continue_program = True
        self._INITIAL_POS = 0
        self._position = self._INITIAL_POS
        self._confirmation = "OK"

        # Establish the connection on a specific port
        self._BAUD_SPEED = 9600
        self._dev_path = '/dev/ttyACM0'
        self._ser = serial.Serial(self._dev_path, self._BAUD_SPEED, timeout=2)

        arduino_ready_message = self._ser.readline()
        print "The ARDUINO says -->--> " + str(arduino_ready_message)

    def read_move_servo_commands(self):

        while self._continue_program:
            command = raw_input("(EXIT) to finish, (r) for requesting Position Servo, (b+Integer to make the servo move there)::>")
            if command[0] == "r":
                self.get_info_servo_pos_cmd(command)

            elif command[0] == "b" and command[1:].isdigit():
                servo_num = self.move_servo_cmd(command)
                # GET CURRENT POSITION
                info_command = "r"+servo_num
                self.get_info_servo_pos_cmd(info_command)

            elif command == "EXIT":
                self._continue_program = False

            else:
                print "Wrong command, please try again."

            sleep(.2)

        print "Finished program, have a nice day."


    def clean_servo_num(self, servo_num):
        s_servo_num = str(servo_num)
        if len(s_servo_num) > 1:
            s_servo_num = s_servo_num[0:1]
        elif len(s_servo_num) == 0:
            s_servo_num = "0"

        return s_servo_num

    def generate_info_servo_cmd(self, servo_num):
        s_servo_num = self.clean_servo_num(servo_num)

        cmd = "r"+s_servo_num
        print "GEN INFO CMD =="+str(cmd)
        return cmd

    def get_info_servo_pos(self, servo_num):

        cmd = self.generate_info_servo_cmd(servo_num)
        self.get_info_servo_pos_cmd(command=cmd)

    def get_info_servo_pos_cmd(self, command):
        if len(command[1:]) > 2:
            print "You are out of your mind!? Only ONE DIGIT SERVO NUM"
            command = command[0:3]

        self._ser.write(command)
        info = self._ser.readline()
        while info[0:4] != "INFO":
            print "Status of INFO --> #" + str(info[0:4]) + "#"
            info = self._ser.readline()

        position = self._ser.readline()
        print "Reached to the position --> " + str(position)

    def generate_move_servo_cmd(self, servo_num, servo_pos):

        s_servo_num = self.clean_servo_num(servo_num)

        s_servo_pos = str(servo_pos)
        if len(s_servo_pos) > 3:
            s_servo_pos = s_servo_pos[0:3]
        elif len(s_servo_pos) == 2:
            s_servo_pos = "0"+s_servo_pos
        elif len(s_servo_pos) == 1:
            s_servo_pos = "00"+s_servo_pos
        elif len(s_servo_pos) == 0:
            s_servo_pos = "000"

        cmd = "b"+s_servo_pos+s_servo_num
        print "GEN CMD =="+str(cmd)
        return cmd

    def move_servo_cmd(self, command):
        if len(command[1:]) > 4:
            print "You are out of your mind!? Only THREE DIGIT POSITIONS"
            command = command[0:5]

        servo_pos = command[1:4]
        servo_num = command[4:]

        print "Asked Servo num ="+str(servo_num)+", in position ="+str(servo_pos)
        print "Asked Servo to move to position ::> " + str(command[1:])
        #And give commands, in this case setting it a constant beer mode at a certain temperature:
        self._ser.write(command)
        # Read the newest output from the Arduino
        confirmation = self._ser.readline()
        print "BEFORE WHILE==> " + confirmation
        while confirmation[0:2] != "OK":
            print "Status of movement --> @" + str(confirmation) + "@"
            confirmation = self._ser.readline()

        return servo_num

    def move_servo_pos(self, servo_num, servo_pos):

        cmd = self.generate_move_servo_cmd(servo_num, servo_pos)
        self.move_servo_cmd(command=cmd)



class Gestures(object):
    def __init__(self, servo_o):
        self._servo_o = servo_o

    def wave(self, num_waves):
        for i in range(num_waves):
            self._servo_o.move_servo_pos(servo_num=0, servo_pos=30)
            sleep(0.5)
            self._servo_o.move_servo_pos(servo_num=0, servo_pos=50)
            sleep(0.5)

    def move_test1(self):
        self._servo_o.move_servo_pos(servo_num=0, servo_pos=30)
        self._servo_o.move_servo_pos(servo_num=1, servo_pos=110)
        self._servo_o.move_servo_pos(servo_num=2, servo_pos=110)
        sleep(3)
        self._servo_o.move_servo_pos(servo_num=0, servo_pos=60)
        self._servo_o.move_servo_pos(servo_num=1, servo_pos=80)
        self._servo_o.move_servo_pos(servo_num=2, servo_pos=80)

    def get_info_test1(self):
        servo_o.get_info_servo_pos(servo_num=0)
        sleep(3)
        servo_o.get_info_servo_pos(servo_num=1)

    def move_by_cmds(self):
        servo_o.read_move_servo_commands()


if __name__ == "__main__":
    servo_o = ServoArduinoObject()
    gestures = Gestures(servo_o)
    gestures.wave(num_waves=3)
    print "CMD INPUT"
    gestures.move_by_cmds()

