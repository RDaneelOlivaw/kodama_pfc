import csv
import random
import os

ROWS = 3000

INPUT_DATA_CSV_FILE_DEFAULT_NAME = "input_data.csv"
#INPUT_DATA_DEFAULT_PATH = os.path.abspath("/home/rdaneel/.local/lib/python2.7/site-packages/nupic-0.3.0.dev0-py2.7-linux-x86_64.egg/nupic/datafiles/")
INPUT_DATA_DEFAULT_PATH = os.path.abspath("./myswarm/data/")

def run(experiment_name=INPUT_DATA_CSV_FILE_DEFAULT_NAME):
    """
    Generates the sine.csv data. It samples 100 parts of 2*pi and evaluates it in sinus.
    :return:
    """
    input_data_file_name = experiment_name + ".csv"
    print os.path.abspath(INPUT_DATA_DEFAULT_PATH)
    input_data_file_name_path = os.path.join(os.path.abspath(INPUT_DATA_DEFAULT_PATH), input_data_file_name)

    fileHandle = open(input_data_file_name_path, "w")
    writer = csv.writer(fileHandle)
    # Headers, variables
    writer.writerow(["fft_R"])
    # Type of the headers
    writer.writerow(["list"])
    # Flags, special types for NUPIC, not necessary now
    writer.writerow([""])

    for i in range(ROWS):
        fft_value = random.random()
        writer.writerow([[fft_value]*10])

    fileHandle.close()

if __name__ == "__main__":
    run("fft")
