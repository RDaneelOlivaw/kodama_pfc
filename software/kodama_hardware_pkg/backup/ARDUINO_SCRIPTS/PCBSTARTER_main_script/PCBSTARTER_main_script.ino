// This is works with python script PCBSTARTER_main_script.py
// Each revolution are 400 steps. We use the motor as a servo
// therefore there will be limitations in the revolution.
int STB_PIN = 13; // Pin for the On OFF output signal for PCB, also conected to ARDUINO LED
int SEL_PIN = 12; // Pin for CW od CCW output signal
boolean pcb_active = false;
char CALIB_DIR = 'l';

int ENCODER_TO_ARD_PIN = 4;
int encoder_to_ard_pin_last; 
int steps_moved = 0;
int abs_position = 401 ; // Goes from -400::0::400, 401 is an imposible number (for debugging)
int n = LOW;
int ENCODER_STEPS_NUM = 400;

String servo_position = "0";
String MAX_POS = "400";
String MIN_POS = "-400";
int i_MAX_POS = 400;
int i_MIN_POS = -400;

int desired_position = 401; // Where goto will take us
int ERROR_POS_TOLERANCE = 1; // error tolerated minor than one step error.

unsigned long time;
int READ_ENCODER_MS_RATE = 100; // Publish at 10 HZ

void setup() {
time = millis();
pinMode (ENCODER_TO_ARD_PIN,INPUT);
pinMode(STB_PIN, OUTPUT);
digitalWrite(STB_PIN, HIGH); // We put the PCB on STANDBY inmediatelly.
pinMode(SEL_PIN, OUTPUT);
Serial.begin(19200); // set the baud rate
//Serial.begin(9600);
Serial.println("****PCB STARTER_KIT*****READY*****"); // print "Ready to Move PCB STARTER_KIT" once
encoder_to_ard_pin_last = digitalRead(ENCODER_TO_ARD_PIN); // We read the Init Value, read here to avoid false readings from movement in setup.

}

void loop() {
handleSerialCommunication();
}

void handleSerialCommunication(void){
  if (Serial.available())
  {
    char inByte = Serial.read();
    switch(inByte)
    {
    case 'c': // Calibrate
      CalibratePcb();
      break;

    case 'g': // Goto
      Serial.println("GOTO");
      numberFromSerial();
      Serial.println(desired_position,DEC);
      Serial.println(abs_position,DEC);
      GotoPos();
      break;
     
    case 'p': // Absolute Position request
      serialPrintAbsPos();
      break;
      
    case 'm': // Move as Motor
      MoveAsMotor();
      Serial.println("MOTOR MODE ENDED");
      break;

    case 's': // Put to StandBy PCB
      StopPCB();
      Serial.println("Put PCB on STANDBY");
      break;

    default:
      Serial.println(inByte);
    }
    Serial.flush();
  }
}


void MoveAsMotor(void)
{
  char command;
  delay(100);
  Serial.println("MOTOR READY");
  while(true)
  {
    delay(50);
    Serial.flush();
    command=Serial.read(); // r, l , z, x
    if (command == 'x')
    {
      StopPCB();
      break;
    }
    else if ((command == 'r') || (command == 'l') || (command == 'z'))
    {
     MovePcbStarter(command); 
    }
    else
    {
     //Do nothing 
    }
    Serial.println("NEXT");
  }
}


void serialPrintAbsPos(void)
{
 Serial.println(abs_position);
 delay(10);
   
}

void GotoPos(void)
{
 char direct;  
 while ( abs(desired_position-abs_position) >= ERROR_POS_TOLERANCE)
 {
   // Do while until it reaches destination
   direct = DecideDirMov();
   MovePcbStarter(direct);
   ReadEncoderPos(true, direct);
 }
 StopPCB();
 Serial.println("*REACHED COORDINATES*");
}

char DecideDirMov(void)
{
  char dir_mov;

  if ( (desired_position - abs_position) > 0 )
  {
   dir_mov = 'r';
  }
  else if ( (desired_position - abs_position) < 0 )
  {
   dir_mov = 'l';
  }
  else // if diference is zero
  {
   dir_mov = 'z';
  }

  return dir_mov; 
}

void numberFromSerial(void)
{
  char numberString[8];
  char cstr[8];
  unsigned char index=0;
  delay(100); // CAREFULL with this time, if too small it gets corrupt data. Depends on the Baud rate also. Smaller Bouds, bigger has to be the delay
  while(Serial.available())
  {
    delay(10);
    numberString[index++]=Serial.read();
    if(index > 4)
    {
      break;
    }
  }
  numberString[index]=0;
  Serial.println(numberString[0]);
  Serial.println(numberString[1]);
  Serial.println(numberString[2]);
  servo_position = servoLimits(atoi(numberString),numberString);
  servo_position.toCharArray(cstr,8);
  desired_position = atoi(cstr);
  //Serial.println(desired_position);
  //desired_position = atoi(numberString);
}


String servoLimits(int i_number, String s_init_num)
{
 String s_num;

 if (i_number > i_MAX_POS)
 {
   s_num = MAX_POS;
 }
 else if (i_number < i_MIN_POS)
 {
   s_num = MIN_POS;
 }
 else
 {
   s_num = s_init_num;
 }
 return s_num;
}

void CalibratePcb(void)
{
 // Get the Zero Pos of our Motor moving it and reading the encoder.
 Serial.println("***CALIBRATING***PLEASE_WAIT***");
 MovePcbStarter(CALIB_DIR);
 while ( steps_moved < ENCODER_STEPS_NUM) // This is temporary, it will be a touch sensor feedback.
 {
   // Do while until we read 100 steps
   ReadEncoderPos(false,CALIB_DIR);
 }
 StopPCB();
 abs_position = 0;
 Serial.println("*CALIBRATED*");
 
}

////////////////
void ReadEncoderPos(boolean add_to_abs, char dir ) { 
   n = digitalRead(ENCODER_TO_ARD_PIN);
   if ( ((encoder_to_ard_pin_last == LOW) && (n == HIGH)) || ((encoder_to_ard_pin_last == HIGH) && (n == LOW)) )
   { // We moved a step LOW to HIGH.
     if (add_to_abs)
     {
       ChangeAbsPos(dir);
       steps_moved++;
     }
     else
     {
       steps_moved++;
     }
     
   }
   PublishStuffRate(steps_moved, READ_ENCODER_MS_RATE);
   //Serial.println(steps_moved,DEC);
   //delay(10);
   encoder_to_ard_pin_last = n;
 }
///////////////////

void PublishStuffRate(int data, int rate)
{
 if (millis() > (time + rate ))
   {
    Serial.println(data,DEC);
    time = millis();
   }  
}

void ChangeAbsPos(char dir)
{
 if (dir == 'r')
 {
  abs_position++;
 }
 else if (dir == 'l')
 {
  abs_position--;
 }
 else
 {
  // do nothing
 } 
}

void MovePcbStarter(char sense_command)
{
  if (!pcb_active)
  {
   digitalWrite(STB_PIN, LOW);//ACTIVATE PCB, led ARDUINO TURNS OFF
   pcb_active = true; 
  }
  
  if (sense_command == 'r')
  {
    digitalWrite(SEL_PIN, LOW); // Move CW
    //Serial.println("MOVING_RIGHT");
  }
  else if (sense_command == 'l')
  {
    digitalWrite(SEL_PIN, HIGH); // Move CCW
    //Serial.println("MOVING_LEFT");
  }
  else // when sense_command == 'z'
  {
    StopPCB();
    //Serial.println("STOPPED_MOVING");
  }
  
  //Serial.println("MOVING");
}

void StopPCB()
{
  digitalWrite(STB_PIN, HIGH); //STANDBY PCB, led ARDUINO TURNS ON
  steps_moved = 0;
  pcb_active = false;
}
