"""
This script works with ARDUINO script sending_receiving_commands_PYSERIAL_real_msec_V3.ino
"""

from time import sleep
import serial

continue_program = True
INITIAL_POS = 0
position = INITIAL_POS
confirmation = "OK"

# Establish the connection on a specific port
ser = serial.Serial('/dev/ttyACM2', 115200, timeout=2)

arduino_ready_message = ser.readline()
print "The ARDUINO says -->--> " + str(arduino_ready_message)

while continue_program:
    command = raw_input("(EXIT) to finish, (r) for requesting Position Servo, (b+Integer to make the servo move there), (t+Pos Int+Time in milisec five digit)::>")
    if command == "r":

        #Request new data with:
        ser.write("r")
        # Read the newest output from the Arduino
        position = ser.readline()
        print "The position of the servo is ::> " + str(position)

    elif command[0] == "b" and command[1:].isdigit():

        if len(command[1:]) > 3:
            print "You are out of your mind!? Only THREE DIGIT POSITIONS"
            command = command[0:4]

        print "Asked Servo to move to position ::> " + str(command[1:])
        #And give commands, in this case setting it a constant beer mode at a certain temperature:
        ser.write(command)
        # Read the newest output from the Arduino
        confirmation = ser.readline()
        print "BEFORE WHILE==> " + confirmation
        while confirmation[0:2] != "OK":
            print "Status of movement --> @" + str(confirmation) + "@"
            confirmation = ser.readline()

        # Read the newest output from the Arduino
        ser.write("r")
        position = ser.readline()
        print "Reached to the position --> " + str(position)

    elif command[0] == "t" and command[1:].isdigit():

        if len(command[1:]) > 8:
            print "You are out of your mind!? Only THREE DIGIT POSITIONS AND OR FIVE digit time"
            command = command[0:6]

        print "Asked Servo to move to position ::> " + str(command[1:4] + "In " + str(command[4:]) + " miliseconds" )
        print "COMMAND SENT==>" + str(command)
        ser.write(command)

        des_pos = ser.readline()
        print "DESIRED POS ( CORRECTED IF NEEDED ) ==> " + str(des_pos)
        des_time = ser.readline()
        print "DESIRED TIME==> " + str(des_time)
        current_pos = ser.readline()
        print "CURRENT POS==> " + str(current_pos)


        # Read the newest output from the Arduino
        confirmation = ser.readline()
        print "BEFORE WHILE==> " + confirmation
        while confirmation[0:2] != "OK":
            print "Status of movement --> @" + str(confirmation) + "@"
            confirmation = ser.readline()

        # Read the newest output from the Arduino
        ser.write("r")
        position = ser.readline()
        print "Reached to the position --> " + str(position)

    elif command == "EXIT":

        continue_program = False

    else:

        print "Wrong command, please try again."

    sleep(.2)

print "Finished program, have a nice day."
