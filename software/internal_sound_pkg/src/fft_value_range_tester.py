#!/usr/bin/env python
"""
This python script will tell you the range in the FFT magnitud values of the left channel for
speed. It is usefull for knowing which is the range that needs to be set for the nupic encoding.

we write data in an excel with module xlsxwriter
https://xlsxwriter.readthedocs.org/examples.html


Use:
Start ROS
1)roscore
Start Internal sound recorder
2)rosrun internal_sound_pkg talker.py
Run fft range value
3)rosrun internal_sound_pkg fft_value_range_tester.py

This will write in the xlsx in the catkin dir the data registered
"""

import rospy
from internal_sound_pkg.msg import internal_sound_fft
import pickle
import xlsxwriter
import os

PICLE_RANGEDATA = "rangedata.p"
EXCEL_POS = "exceldata.p"
HISTORIC_DATA = "historic_data.p"
DEFAULT_RES = 0
EXCEL_DATA_PATH = "range_data.xlsx"

MAX_KEY_NAME = "max"
MIN_KEY_NAME = "min"
RES_KEY_NAME = "res"
CONSIDER_ZERO = 1.70394778252e-03

class RangeData(object):
    def __init__(self):
        self._max_value = None
        self._max_freq = None
        self._min_value = None
        self._min_freq = None
        self._resolution = None


    def set_max_val(self, value, freq):
        self._max_value = value
        self._max_freq = freq

    def set_min_val(self, value, freq):
        self._min_value = value
        self._min_freq = freq

    def set_resolution_val(self, value):
        self._resolution = value

    def get_max_val(self):
        return self._max_value
    def get_max_data(self):
        return self._max_value, self._max_freq

    def get_min_val(self):
        return self._min_value
    def get_min_data(self):
        return self._min_value, self._min_freq

    def get_resolution_val(self):
        return self._resolution

    def print_range(self):
        rospy.loginfo("MAX="+str(self.get_max_data()))
        rospy.loginfo("MIN="+str(self.get_min_data()))
        rospy.loginfo("RES="+str(self.get_resolution_val()))


def write_excel(file_path, historic_data):
    workbook = xlsxwriter.Workbook(file_path)
    worksheet = workbook.add_worksheet()
    for key,value in historic_data.iteritems():
        if key == MAX_KEY_NAME:
            col = 0
        elif key == MIN_KEY_NAME:
            col = 1
        elif key == RES_KEY_NAME:
            col = 2
        else:
            col = 3

        for row in range(0, len(value)):
            worksheet.write(row, col, value[row])
    workbook.close()


def callback(data):
    range_object = pickle.load(open(PICLE_RANGEDATA, "rb"))
    historic_data = pickle.load(open(HISTORIC_DATA, "rb"))

    assert len(data.fft_abs_left) == len(data.freq_left)

    # Get Current MAX, MIN and RESOLUTION
    max_value = max(data.fft_abs_left)
    max_index = data.fft_abs_left.index(max_value)
    max_value_freq = data.freq_left[max_index]

    min_value = min(data.fft_abs_left)
    min_index = data.fft_abs_left.index(min_value)
    min_value_freq = data.freq_left[min_index]

    abs_sorted = sorted(data.fft_abs_left)
    min_diference = min(y-x for x,y in zip(abs_sorted, abs_sorted[1:]))
    if min_diference < CONSIDER_ZERO:
        min_diference = 0.0

    # Decide if better than the ones we have
    if range_object.get_max_val():
        if max_value > range_object.get_max_val():
            range_object.set_max_val(max_value, max_value_freq)
    else:
        range_object.set_max_val(max_value, max_value_freq)

    if range_object.get_min_val():
        if min_value < range_object.get_min_val():
            range_object.set_min_val(min_value, min_value_freq)
    else:
        range_object.set_min_val(min_value, min_value_freq)

    if range_object.get_resolution_val() or range_object.get_resolution_val() == 0.0:
        if min_diference < range_object.get_resolution_val():
            range_object.set_resolution_val(min_diference)
    elif not range_object.get_resolution_val() and range_object.get_resolution_val() != 0.0:
        range_object.set_resolution_val(min_diference)

    # Print and save in excel
    range_object.print_range()
    historic_data[MAX_KEY_NAME].append(range_object.get_max_val())
    historic_data[MIN_KEY_NAME].append(range_object.get_min_val())
    historic_data[RES_KEY_NAME].append(range_object.get_resolution_val())
    #print historic_data
    write_excel(EXCEL_DATA_PATH, historic_data)

    pickle.dump(range_object, open(PICLE_RANGEDATA, "wb"))
    pickle.dump(historic_data, open(HISTORIC_DATA, "wb"))

def listener():
    range_object = RangeData()
    historic_data = {MAX_KEY_NAME: [MAX_KEY_NAME], MIN_KEY_NAME: [MIN_KEY_NAME], RES_KEY_NAME: [RES_KEY_NAME]}
    write_excel(EXCEL_DATA_PATH, historic_data)
    pickle.dump(range_object, open(PICLE_RANGEDATA, "wb"))
    pickle.dump(historic_data, open(HISTORIC_DATA, "wb"))

    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'talker' node so that multiple talkers can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber("chatter", internal_sound_fft, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
