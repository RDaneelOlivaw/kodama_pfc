#!/usr/bin/env python
import rospy
from pylab import *
from nupic_pkg.msg import htm_output
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import math
import random
import copy

class PlotSDR(object):
    def __init__(self, length_array):
        self.exact_col = math.sqrt(length_array)
        self.COL_N_ELEMENTS = int(self.exact_col)+1
        self.extra_length = pow(self.COL_N_ELEMENTS,2)-length_array
        self.extra_array = [0]*self.extra_length

        self.length_array = pow(self.COL_N_ELEMENTS,2)

        print len(self.extra_array)
        print "EXACT COL =="+str(self.exact_col)
        print "EXTRA COL =="+str(self.extra_length)

    def convert_sdr_to_matrix(self, data_array):
        fixed_data_array = data_array+self.extra_array

        assert len(fixed_data_array) == pow(self.COL_N_ELEMENTS,2), "Columns dont match with fixed array"

        np_array = np.asarray(fixed_data_array)
        sdr_matrix = reshape(np_array, (-1, self.COL_N_ELEMENTS))

        return sdr_matrix


def listener():

    rospy.init_node('htm_output_plotter', anonymous=True)

    msg = rospy.wait_for_message("/htm_output", htm_output)
    sp_output = msg.sp_output_array
    tp_prediction = msg.tp_prediction_array

    rospy.loginfo("SP = "+str(sp_output))
    rospy.loginfo("TP = "+str(tp_prediction))

    plt_sdr_object = PlotSDR(length_array=len(sp_output))

    fig = plt.figure()

    def f(sp_output):
        sp_neo_val = plt_sdr_object.convert_sdr_to_matrix(sp_output)
        return sp_neo_val

    im = plt.imshow(f(sp_output), cmap=plt.get_cmap('gnuplot2'))


    def updatefig(*args):

        msg = rospy.wait_for_message("/htm_output", htm_output)
        sp_output = msg.sp_output_array
        tp_prediction = msg.tp_prediction_array

        rospy.loginfo("SP = "+str(sp_output))
        rospy.loginfo("TP = "+str(tp_prediction))

        sp_output_new_val = f(sp_output)
        im.set_array(sp_output_new_val)
        return im,

    ani = animation.FuncAnimation(fig, updatefig, interval=50, blit=True)
    plt.show()


def listener_tp():

    rospy.init_node('htm_tp_output_plotter', anonymous=True)

    msg = rospy.wait_for_message("/htm_output", htm_output)
    tp_prediction = msg.tp_prediction_array

    rospy.loginfo("TP = "+str(tp_prediction))

    plt_sdr_object = PlotSDR(length_array=len(tp_prediction))

    fig = plt.figure()

    def f(tp_output):
        tp_neo_val = plt_sdr_object.convert_sdr_to_matrix(tp_output)
        return tp_neo_val

    im = plt.imshow(f(tp_prediction), cmap=plt.get_cmap('flag'))


    def updatefig(*args):

        msg = rospy.wait_for_message("/htm_output", htm_output)
        tp_prediction = msg.tp_prediction_array

        rospy.loginfo("TP = "+str(tp_prediction))

        tp_output_new_val = f(tp_prediction)
        im.set_array(tp_output_new_val)
        return im,

    ani = animation.FuncAnimation(fig, updatefig, interval=50, blit=True)
    plt.show()


def listener_thrise():

    rospy.init_node('htm_both_output_plotter', anonymous=True)

    msg = rospy.wait_for_message("/htm_output", htm_output)

    sp_output = msg.sp_output_array
    tp_prediction = msg.tp_prediction_array

    global tp_save
    tp_save = copy.deepcopy(tp_prediction)

    tp_past_prediction = [True]*len(tp_prediction)

    rospy.loginfo("SP = "+str(sp_output))
    rospy.loginfo("TP = "+str(tp_prediction))
    rospy.loginfo("TP PAST = "+str(tp_past_prediction))

    plt_sdr_object1 = PlotSDR(length_array=len(sp_output))
    plt_sdr_object2 = PlotSDR(length_array=len(tp_prediction))
    plt_sdr_object3 = PlotSDR(length_array=len(tp_past_prediction))

    fig = plt.figure()

    ax1=fig.add_subplot(1,3,1)
    ax2=fig.add_subplot(1,3,2)
    ax3=fig.add_subplot(1,3,3)

    ax1.set_title("SP Output")
    ax2.set_title("TP Predictions")
    ax3.set_title("TP PAST Predictions")

    ims=[]

    def f1(sp_output):
        sp_neo_val = plt_sdr_object1.convert_sdr_to_matrix(sp_output)
        return sp_neo_val

    def f2(tp_output):
        tp_neo_val = plt_sdr_object2.convert_sdr_to_matrix(tp_output)
        return tp_neo_val

    def f3(tp_past_prediction):
        tp_past_neo_val = plt_sdr_object3.convert_sdr_to_matrix(tp_past_prediction)
        return tp_past_neo_val

    im1 = ax1.imshow(f1(sp_output), cmap=plt.get_cmap('prism'))
    im2 = ax2.imshow(f2(tp_prediction), cmap=plt.get_cmap('flag'))
    im3 = ax3.imshow(f3(tp_past_prediction), cmap=plt.get_cmap('prism'))

    ims.append(im1)
    ims.append(im2)
    ims.append(im3)

    def updatefig(*args):

        msg = rospy.wait_for_message("/htm_output", htm_output)
        sp_output = msg.sp_output_array
        tp_prediction = msg.tp_prediction_array

        global tp_save
        """
        r_num = random.randint(0,1)
        r_b = bool(r_num)
        tp_past_prediction = [r_b]*len(tp_prediction)
        """

        rospy.loginfo("SP = "+str(sp_output))
        rospy.loginfo("TP = "+str(tp_prediction))
        rospy.loginfo("TP PAST = "+str(tp_save))

        sp_output_new_val = f1(sp_output)
        tp_output_new_val = f2(tp_prediction)
        tp_past_output_new_val = f3(tp_save)

        im1.set_array(sp_output_new_val)
        im2.set_array(tp_output_new_val)
        im3.set_array(tp_past_output_new_val)

        tp_save = copy.deepcopy(tp_prediction)

        return ims

    ani = animation.FuncAnimation(fig, updatefig, interval=50, blit=True)
    plt.show()


def listener_thrise_lvl(level_topic_name):

    rospy.init_node('htm_both_output_plotter', anonymous=True)

    msg = rospy.wait_for_message(level_topic_name, htm_output)

    sp_output = msg.sp_output_array
    tp_prediction = msg.tp_prediction_array

    global tp_save
    tp_save = copy.deepcopy(tp_prediction)

    tp_past_prediction = [True]*len(tp_prediction)

    rospy.loginfo("SP = "+str(sp_output))
    rospy.loginfo("TP = "+str(tp_prediction))
    rospy.loginfo("TP PAST = "+str(tp_past_prediction))

    plt_sdr_object1 = PlotSDR(length_array=len(sp_output))
    plt_sdr_object2 = PlotSDR(length_array=len(tp_prediction))
    plt_sdr_object3 = PlotSDR(length_array=len(tp_past_prediction))

    fig = plt.figure()

    ax1=fig.add_subplot(1,3,1)
    ax2=fig.add_subplot(1,3,2)
    ax3=fig.add_subplot(1,3,3)

    ax1.set_title("SP Output")
    ax2.set_title("TP Predictions")
    ax3.set_title("TP PAST Predictions")

    ims=[]

    def f1(sp_output):
        sp_neo_val = plt_sdr_object1.convert_sdr_to_matrix(sp_output)
        return sp_neo_val

    def f2(tp_output):
        tp_neo_val = plt_sdr_object2.convert_sdr_to_matrix(tp_output)
        return tp_neo_val

    def f3(tp_past_prediction):
        tp_past_neo_val = plt_sdr_object3.convert_sdr_to_matrix(tp_past_prediction)
        return tp_past_neo_val

    im1 = ax1.imshow(f1(sp_output), cmap=plt.get_cmap('prism'))
    im2 = ax2.imshow(f2(tp_prediction), cmap=plt.get_cmap('flag'))
    im3 = ax3.imshow(f3(tp_past_prediction), cmap=plt.get_cmap('prism'))

    ims.append(im1)
    ims.append(im2)
    ims.append(im3)

    def updatefig(*args):

        msg = rospy.wait_for_message(level_topic_name, htm_output)
        sp_output = msg.sp_output_array
        tp_prediction = msg.tp_prediction_array

        global tp_save

        rospy.loginfo("SP = "+str(sp_output))
        rospy.loginfo("TP = "+str(tp_prediction))
        rospy.loginfo("TP PAST = "+str(tp_save))

        sp_output_new_val = f1(sp_output)
        tp_output_new_val = f2(tp_prediction)
        tp_past_output_new_val = f3(tp_save)

        im1.set_array(sp_output_new_val)
        im2.set_array(tp_output_new_val)
        im3.set_array(tp_past_output_new_val)

        tp_save = copy.deepcopy(tp_prediction)

        return ims

    ani = animation.FuncAnimation(fig, updatefig, interval=200, blit=True)
    plt.show()



def listener_both_lvl1():

    rospy.init_node('htm_both_output_plotter', anonymous=True)

    msg = rospy.wait_for_message("/htm_output_lvl1", htm_output)

    sp_output = msg.sp_output_array
    tp_prediction = msg.tp_prediction_array

    rospy.loginfo("SP = "+str(sp_output))
    rospy.loginfo("TP = "+str(tp_prediction))

    plt_sdr_object1 = PlotSDR(length_array=len(sp_output))
    plt_sdr_object2 = PlotSDR(length_array=len(tp_prediction))

    fig = plt.figure()

    ax1=fig.add_subplot(2,1,1)
    ax2=fig.add_subplot(2,1,2)

    ax1.set_title("SP Output")
    ax2.set_title("TP Predictions")

    ims=[]

    def f1(sp_output):
        sp_neo_val = plt_sdr_object1.convert_sdr_to_matrix(sp_output)
        return sp_neo_val

    def f2(tp_output):
        tp_neo_val = plt_sdr_object2.convert_sdr_to_matrix(tp_output)
        return tp_neo_val

    im1 = ax1.imshow(f1(sp_output), cmap=plt.get_cmap('prism'))
    im2 = ax2.imshow(f2(tp_prediction), cmap=plt.get_cmap('flag'))

    ims.append(im1)
    ims.append(im2)

    def updatefig(*args):

        msg = rospy.wait_for_message("/htm_output_lvl1", htm_output)
        sp_output = msg.sp_output_array
        tp_prediction = msg.tp_prediction_array

        rospy.loginfo("SP = "+str(type(sp_output)))
        rospy.loginfo("TP = "+str(type(tp_prediction)))
        rospy.loginfo("SP = "+str(sp_output))
        rospy.loginfo("TP = "+str(tp_prediction))

        sp_output_new_val = f1(sp_output)
        tp_output_new_val = f2(tp_prediction)

        im1.set_array(sp_output_new_val)
        im2.set_array(tp_output_new_val)

        return ims

    ani = animation.FuncAnimation(fig, updatefig, interval=50, blit=True)
    plt.show()



if __name__ == '__main__':
    """
    Fpr testing
    rostopic echo /htm_output
    rostopic pub -r 1 htm_output nupic_pkg/htm_output [True,False] [True,True]
    """
    # TODO: We need to investigate why it doesnt work the first time round, we need to kill it and restart it again.
    while not rospy.is_shutdown():
        #listener_both()
        #listener_thrise_lvl1()
        listener_thrise_lvl(level_topic_name="/htm_output_lvl1")
        #listener_thrise_lvl(level_topic_name="/htm_output_lvl2")
        #listener_thrise()
        #listener_both_lvl1()