This package records the internal sound playing and makes the FFT and publishes the data in a topic
called /chatter

STEP 1
------
You need to put this package in your catkin_ws dorectory src
$cd catkin_ws
$catkin_make
$source devel/setup.bash

STEP 2
------
$rocore
$rosrun internal_sound_pkg talker.p

