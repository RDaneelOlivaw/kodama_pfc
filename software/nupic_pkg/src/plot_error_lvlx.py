#!/usr/bin/env python
import rospy
from plot_error import listener_error_lvlx
import math

def time_to_frames(r_time):
    """
    Concerts the seconds to frames based on experimentation
    100 Frames ==> 20 seconds recording
    1.000 Frames ==> 200 seconds recording
    10.000 Frames ==> 2.000 seconds recording
    :param time:
    :return:
    """
    time = r_time/2.0
    frames = int(math.ceil((time/20.0)*100))
    print frames

    return frames

if __name__ == '__main__':
    recording_ended = False
    while not rospy.is_shutdown() and not recording_ended:
        record_video = True
        # T for ONE level
        t_in_min = 15
        # 60 to go sec and 2 because we record 2 levels
        r_time = t_in_min*60*2
        anim_time = time_to_frames(r_time=r_time)
        video_file_path = '/media/rdaneel/Data/Kodama/Videos/error_plot_30min_200_40_8.mp4'
        recording_ended = listener_error_lvlx(record_video, anim_time, video_file_path)