#!/usr/bin/env python
from pylab import *
import operator
from matplotlib import pyplot as plt
from matplotlib import font_manager as fm
import rospy

EXPLODE_MOVE = 0.05
EXPLODE_SEP = 0.1

def extract_lable_percentage_from_dict(data_dict):
    """
    Extracts the labels and values to two lists
    :param data_dict:
    :return:
    """
    lables = []
    values = []
    for label,value in data_dict.items():
        lables.append(label)
        values.append(value)
    return lables, values


def generate_exploded(length):

    explode = []
    move = False
    for i in range(length):
        if move:
            explode.append(EXPLODE_MOVE)
            move = False
        else:
            explode.append(0)
            move = True

    return explode

def generate_pie_chart_test(title_name, lable_percentage_dict, save_path=None):

    # make a square figure and axes
    figure(1, figsize=(6,6))
    ax = axes([0.1, 0.1, 0.8, 0.8])

    # The slices will be ordered and plotted counter-clockwise.
    labels, fracs = extract_lable_percentage_from_dict(lable_percentage_dict)
    max_index, max_value = max(enumerate(fracs), key=operator.itemgetter(1))
    explode = generate_exploded(length= len(labels))
    print explode
    explode[max_index] = EXPLODE_SEP

    color_list = ['red','green','orange','blue', 'white', 'purple', 'darkcyan', 'skyblue']

    patches, texts, autotexts = pie(fracs, explode=explode, labels=labels,
                                    autopct='%1.1f%%', shadow=True, startangle=90, colors=color_list)
                                    # The default startangle is 0, which would start
                                    # the Frogs slice on the x-axis.  With startangle=90,
                                    # everything is rotated counter-clockwise by 90 degrees,
                                    # so the plotting starts on the positive y-axis.


    # http://matplotlib.org/1.5.0/examples/pylab_examples/fonts_demo.html
    proptease = fm.FontProperties()
    proptease.set_size('medium')
    proptease.set_family('monospace')
    plt.setp(autotexts, fontproperties=proptease)
    plt.setp(texts, fontproperties=proptease)


    title(title_name, bbox={'facecolor':'0.8', 'pad':5})

    if save_path:
        rospy.loginfo("Saving img in path =="+str(save_path))
        plt.savefig(save_path)
    else:
        show()

if __name__ == "__main__":
    title_name= "Test"
    lable_percentage_dict = {"Finn":0.8, "Jacke": 0.2, "Marceline":0.6, "BubleGum":0.5, "Penguin":0.03, "Mint":0.01, "Fiona":0.8, "Jackaline":0.5}
    generate_pie_chart_test(title_name, lable_percentage_dict)
