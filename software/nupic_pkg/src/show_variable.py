#!/usr/bin/env python
import rospy
import Tkinter
import tkFont
import random
from nupic_pkg.msg import variable_blackboard

REFRESH_PERIOD = 500

def show_variable():

    rospy.init_node("show_var", anonymous=True)

    root = Tkinter.Tk()
    helv36 = tkFont.Font(family="Helvetica",size=36,weight="bold")
    myvar = Tkinter.StringVar()
    myvar.set('')
    mywidget = Tkinter.Entry(root,textvariable=myvar,font=helv36)
    mywidget.pack()

    msg = rospy.wait_for_message("variable_blackboard", variable_blackboard)

    def task():
        msg = rospy.wait_for_message("variable_blackboard", variable_blackboard)
        myvar.set(msg.variable_value)
        root.after(REFRESH_PERIOD, task)  # reschedule event in 2 seconds

    root.after(REFRESH_PERIOD, task)

    root.mainloop()


if __name__ == '__main__':
    show_variable()
