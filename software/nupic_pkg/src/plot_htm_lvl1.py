#!/usr/bin/env python
import rospy
from plot_htm_predictions import listener_thrise_lvl


if __name__ == '__main__':
    while not rospy.is_shutdown():
        listener_thrise_lvl(level_topic_name="/htm_output_lvl1")