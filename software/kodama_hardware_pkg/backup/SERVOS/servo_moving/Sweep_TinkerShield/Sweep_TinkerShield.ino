// Sweep
// by BARRAGAN <http://barraganstudio.com> 
// mod by RDaneelOlivaw duckfrost@gmail.com
// This example code is in the public domain.

#include <TinkerKit.h>
#include <Servo.h> 

const int max_repeat = 3; // maximum time we repeat a sweep

Servo myservo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 
 
int pos = 0;    // variable to store the servo position 
int repetitions = 0; // Times a sweep is performed

void setup() 
{ 
  myservo.attach(O1);  // attaches the servo on pin 9 to the servo object 
} 
 
 
void loop() 
{ 
  for(pos = 0; pos < 90; pos += 1)  // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(1);                       // waits 15ms for the servo to reach the position 
  } 
  for(pos = 90; pos>=1; pos-=1)     // goes from 180 degrees to 0 degrees 
  {                                
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  }
  repetitions += 1;
  if (repetitions >= max_repeat){
    delay(1000);
    repetitions = 0;
  }
} 
