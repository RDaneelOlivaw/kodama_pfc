"""
This script works with ARDUINO script sending_receiving_commands_PYSERIAL_real_msec_V3.ino
"""

from time import sleep
import serial

continue_program = True
INITIAL_POS = 0
position = INITIAL_POS
confirmation = "OK"

# Establish the connection on a specific port
BAUD_SPEED = 9600
ser = serial.Serial('/dev/ttyACM0', BAUD_SPEED, timeout=2)

arduino_ready_message = ser.readline()
print "The ARDUINO says -->--> " + str(arduino_ready_message)

while continue_program:
    command = raw_input("(EXIT) to finish, (r) for requesting Position Servo, (b+Integer to make the servo move there), (t+Pos Int+Time in milisec five digit)::>")
    if  command[0] == "r":

        if len(command[1:]) > 2:
            print "You are out of your mind!? Only ONE DIGIT SERVO NUM"
            command = command[0:3]

        ser.write(command)
        info = ser.readline()
        while info[0:4] != "INFO":
            print "Status of INFO --> #" + str(info[0:4]) + "#"
            info = ser.readline()

        position = ser.readline()
        print "Reached to the position --> " + str(position)

    elif command[0] == "b" and command[1:].isdigit():


        if len(command[1:]) > 4:
            print "You are out of your mind!? Only THREE DIGIT POSITIONS"
            command = command[0:5]

        servo_pos = command[1:4]
        servo_num = command[4:]

        print "Asked Servo num ="+str(servo_num)+", in position ="+str(servo_pos)
        print "Asked Servo to move to position ::> " + str(command[1:])
        #And give commands, in this case setting it a constant beer mode at a certain temperature:
        ser.write(command)
        # Read the newest output from the Arduino
        confirmation = ser.readline()
        print "BEFORE WHILE==> " + confirmation
        while confirmation[0:2] != "OK":
            print "Status of movement --> @" + str(confirmation) + "@"
            confirmation = ser.readline()

        ############
        info_command = "r"+servo_num

        if len(info_command[1:]) > 2:
            print "You are out of your mind!? Only ONE DIGIT SERVO NUM"
            info_command = info_command[0:3]

        ser.write(info_command)
        info = ser.readline()
        while info[0:4] != "INFO":
            print "Status of INFO --> #" + str(info[0:4]) + "#"
            info = ser.readline()

        position = ser.readline()
        print "Reached to the position --> " + str(position)


    elif command[0] == "t" and command[1:].isdigit():

        if len(command[1:]) > 8:
            print "You are out of your mind!? Only THREE DIGIT POSITIONS AND OR FIVE digit time"
            command = command[0:6]

        print "Asked Servo to move to position ::> " + str(command[1:4] + "In " + str(command[4:]) + " miliseconds" )
        print "COMMAND SENT==>" + str(command)
        ser.write(command)

        des_pos = ser.readline()
        print "DESIRED POS ( CORRECTED IF NEEDED ) ==> " + str(des_pos)
        des_time = ser.readline()
        print "DESIRED TIME==> " + str(des_time)
        current_pos = ser.readline()
        print "CURRENT POS==> " + str(current_pos)


        # Read the newest output from the Arduino
        confirmation = ser.readline()
        print "BEFORE WHILE==> " + confirmation
        while confirmation[0:2] != "OK":
            print "Status of movement --> @" + str(confirmation) + "@"
            confirmation = ser.readline()

        # Read the newest output from the Arduino
        ser.write("r")
        position = ser.readline()
        print "Reached to the position --> " + str(position)

    elif command == "EXIT":

        continue_program = False

    else:

        print "Wrong command, please try again."

    sleep(.2)

print "Finished program, have a nice day."
