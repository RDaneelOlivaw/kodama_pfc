#!/usr/bin/env python
import rospy
from pylab import *
from nupic_pkg.msg import htm_output
import math
import copy
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from plot_error_lvlx import time_to_frames

class PlotSDR(object):
    def __init__(self, length_array):
        rospy.logdebug("--PlotSDR INIT--")

        self.exact_col = math.sqrt(length_array)
        self.COL_N_ELEMENTS = int(self.exact_col)+1
        self.extra_length = pow(self.COL_N_ELEMENTS,2)-length_array
        self.extra_array = [0]*self.extra_length

        self.length_array = pow(self.COL_N_ELEMENTS,2)


        rospy.logdebug(len(self.extra_array))
        rospy.logdebug("EXACT COL =="+str(self.exact_col))
        rospy.logdebug("EXTRA COL =="+str(self.extra_length))

    def convert_sdr_to_matrix(self, data_array):
        fixed_data_array = data_array+self.extra_array

        assert len(fixed_data_array) == pow(self.COL_N_ELEMENTS,2), "Columns dont match with fixed array"

        np_array = np.asarray(fixed_data_array)
        sdr_matrix = reshape(np_array, (-1, self.COL_N_ELEMENTS))

        return sdr_matrix


class PlotHtm(object):
    def __init__(self, level_topic_name):
        self._level_topic_name = level_topic_name
        self._sp_output = None
        self._tp_prediction = None
        self._tp_past_prediction = None
        self._tp_save = None
        self._plt_sdr_object_list = []
        self.wait_for_init_msg_data()
        self.print_plot_htm()
        self.create_plotsdr()

    def wait_for_init_msg_data(self):
        self.update_data(init=True)

    def print_plot_htm(self):
        rospy.loginfo("************ DATA **************")
        rospy.logwarn("TP = "+str(len(self._tp_prediction)))
        rospy.loginfo("SP = "+str(self._sp_output))
        rospy.loginfo("TP = "+str(self._tp_prediction))
        rospy.loginfo("TP PAST = "+str(self._tp_past_prediction))
        rospy.loginfo("TP SAVE DATA ="+str(self._tp_save))
        rospy.loginfo("************ END ***************")

    def create_plotsdr(self):
        rospy.logdebug("Creating PlotSDR objects for lvl="+str(self._level_topic_name))
        self._plt_sdr_object_list.append(PlotSDR(length_array=len(self._sp_output)))
        self._plt_sdr_object_list.append(PlotSDR(length_array=len(self._tp_prediction)))
        self._plt_sdr_object_list.append(PlotSDR(length_array=len(self._tp_past_prediction)))

        rospy.logdebug("PlotSDR objects list ="+str(self._plt_sdr_object_list))

    def get_plt_sdr_object(self, elem_idx):
        """
        elem_idx can be 0 = sp_otput, 1 = tp_pred , 2 = tp_past_pred
        :param elem_name:
        :return:
        """
        rospy.logdebug("ELEM IDX = "+str(elem_idx))
        rospy.logdebug("PLT SDR OBJECT LIST LENGTH = "+str(len(self._plt_sdr_object_list)))

        assert elem_idx < len(self._plt_sdr_object_list), "Index too big for list"

        return self._plt_sdr_object_list[elem_idx]

    def get_sp(self):
        return self._sp_output

    def get_tp(self):
        return self._tp_prediction

    def get_tp_past(self):
        return self._tp_past_prediction

    def get_tp_save(self):
        return self._tp_save

    def update_data(self, init=False):
        """
        Read from topic new data
        :return:
        """
        # TODO: See if deep copy is needed.
        msg = rospy.wait_for_message(self._level_topic_name, htm_output)
        self._sp_output = msg.sp_output_array
        self._tp_prediction = msg.tp_prediction_array


        if init:
            self._tp_past_prediction = [True]*len(self.get_tp())
        else:
            self._tp_past_prediction = copy.deepcopy(self.get_tp_save())

        self._tp_save = copy.deepcopy(self.get_tp())

class PlotHtmBucket(object):
    def __init__(self, levels_to_plot_list):

        self._htm_topic_root = "/htm_output_lvl"
        self._levels_to_plot_list = levels_to_plot_list
        self._num_levels = len(self._levels_to_plot_list)
        self._plot_htm_bucket_dict = {}
        self.fig = plt.figure()
        self._ax_lvl_dict = {}
        self._ims = []
        self._color_mapping = ['prism', 'flag', 'prism']

        self.init_plot_htm_bucket_dict()
        self.init_figure_elem()
        self.init_img_elem()

    def get_ims(self):
        return self._ims

    def update_plot(self):
        img_idx = 0
        rospy.logdebug("INIT img_idx =="+str(img_idx))

        assert len(self._ims) == len(self._plot_htm_bucket_dict)*3, "The ims object is not right"

        for lvl, plt_htm_object in self._plot_htm_bucket_dict.items():
            rospy.logdebug("### LVL ="+str(lvl)+" ###")
            sp_output = plt_htm_object.get_sp()
            tp_prediction = plt_htm_object.get_tp()
            tp_past_prediction = plt_htm_object.get_tp_past()

            input_list = [sp_output, tp_prediction, tp_past_prediction]


            for i in range(len(input_list)):
                rospy.logdebug("lvl ="+str(lvl))
                rospy.logdebug("i ="+str(i))
                rospy.logdebug("length ="+str(len(self._ims)))
                rospy.logdebug("img_idx =="+str(img_idx))

                input_value = input_list[i]
                neo_val = self.convert_sdr_to_matrix_input(input_value, plt_htm_object, i)
                rospy.logdebug("neo_val =="+str(neo_val))
                self._ims[img_idx].set_array(neo_val)
                img_idx += 1

    def print_htm_data(self):
        for lvl in self._levels_to_plot_list:
            rospy.loginfo("***** LVL = "+str(lvl)+" *****")
            plot_htm_object = self._plot_htm_bucket_dict.get(lvl)
            plot_htm_object.print_plot_htm()
            rospy.loginfo("***** ******* *****")

    def update_htm_data(self):
        """
        Update the data reading from topics of all the levels listed.
        :return:
        """
        for lvl in self._levels_to_plot_list:
            plot_htm_object = self._plot_htm_bucket_dict.get(lvl)
            plot_htm_object.update_data()

    def convert_sdr_to_matrix_input(self, input_data, plt_htm_object, idx):
        rospy.logdebug("-- IN CONVERT SDR TO MATRIX INPUT --")
        plt_sdr_object = plt_htm_object.get_plt_sdr_object(elem_idx=idx)
        neo_val = plt_sdr_object.convert_sdr_to_matrix(input_data)
        return neo_val

    def init_img_elem(self):

        rospy.logdebug("--- INIT IMG ELEM ---")
        rospy.logdebug("PLOT HTM BUCKET DICT =="+str(self._plot_htm_bucket_dict))
        # TODO: Hay un problema con el acceso cuando no es lvl1
        for lvl, plt_htm_object in self._plot_htm_bucket_dict.items():
            rospy.logdebug("lvl ="+str(lvl))
            rospy.logdebug("plt htm object =="+str(plt_htm_object))

            ax_lvl_3_data_list = self._ax_lvl_dict.get(lvl)

            rospy.logdebug("ax_lvl_3_data_list =="+str(ax_lvl_3_data_list))

            sp_output = plt_htm_object.get_sp()
            tp_prediction = plt_htm_object.get_tp()
            tp_past_prediction = plt_htm_object.get_tp_past()

            input_list = [sp_output, tp_prediction, tp_past_prediction]

            rospy.logdebug("input_list =="+str(input_list))
            rospy.logdebug("sp_output =="+str(sp_output))
            rospy.logdebug("tp_prediction =="+str(tp_prediction))
            rospy.logdebug("tp_past_prediction =="+str(tp_past_prediction))

            rospy.logdebug("** START INPUT CONVERT SDR MATRIX LOOP **")
            for i in range(len(input_list)):
                input_value = input_list[i]
                rospy.logdebug("input_value =="+str(input_value))
                neo_val = self.convert_sdr_to_matrix_input(input_value, plt_htm_object, i)
                color = self._color_mapping[i]

                rospy.logdebug("neo_val =="+str(neo_val))
                rospy.logdebug("color =="+str(color))
                im = ax_lvl_3_data_list[i].imshow(neo_val, cmap=plt.get_cmap(color))
                self._ims.append(im)

        rospy.logdebug("ims list =="+str(self._ims))
        rospy.logdebug("###### LENGTH ims list =="+str(len(self._ims)))


    def init_figure_elem(self):
        """
        Number of Rows = Number of levels to plot
        Number of Cols = Always 3, that correspond to sp Output, Tp Predictions and Tp Past Predictions
        :return:
        """
        rospy.logdebug("-- INIT FIGURE ELEM --")
        num_cols = 3
        number_of_plot_elements = self._num_levels*num_cols
        type_counter = 1
        lvl_counter = 0
        ax_list = []

        rospy.logdebug("NUM COLS =="+str(num_cols))
        rospy.logdebug("NUM ROWS =="+str(self._num_levels))
        rospy.logdebug("NUM number_of_plot_elements =="+str(number_of_plot_elements))

        for idx in range(1, number_of_plot_elements+1):
            ax = self.fig.add_subplot(self._num_levels, num_cols, idx)

            # Get the level name in position lvl_counter in the levels_to_plot_list
            lvl = self._levels_to_plot_list[lvl_counter]
            rospy.logdebug(" lvl_counter =="+str(lvl))
            rospy.logdebug(" lvl_counter =="+str(lvl_counter))
            rospy.logdebug(" type_counter =="+str(type_counter))
            rospy.logdebug(" idx =="+str(idx))

            if type_counter == 1:
                ax.set_title("SP Output lvl "+str(lvl))
                ax_list.append(ax)
                rospy.logdebug(" ax_list =="+str(ax_list))
                type_counter += 1
            elif type_counter == 2:
                ax.set_title("TP Predictions lvl "+str(lvl))
                ax_list.append(ax)
                rospy.logdebug(" ax_list =="+str(ax_list))
                type_counter += 1
            else:
                ax.set_title("TP PAST Pred lvl "+str(lvl))
                ax_list.append(ax)
                rospy.logdebug(" ax_list =="+str(ax_list))

                self._ax_lvl_dict.update({lvl: ax_list})
                # Reset counters
                rospy.logdebug("RESET COUNTERS ..")
                ax_list = []
                type_counter = 1
                lvl_counter += 1



        rospy.logdebug(" ax_lvl_dict =="+str(self._ax_lvl_dict))


    def get_lvl_topic_name(self, lvl):
        level_topic_name = self._htm_topic_root+str(lvl)
        return level_topic_name

    def init_plot_htm_bucket_dict(self):
        """
        Init all the PlotHtm objects clasified by Htm lvl
        :return:
        """
        rospy.logdebug("Init plot bucket Dict")
        for lvl in self._levels_to_plot_list:
            rospy.logdebug("Init plothtmobject of lvl ="+str(lvl))
            level_topic_name = self.get_lvl_topic_name(lvl)
            rospy.logdebug("lvl topic name ="+str(level_topic_name))
            plot_htm_object = PlotHtm(level_topic_name)
            self._plot_htm_bucket_dict.update({lvl: plot_htm_object})



def get_interval_frames_with_time(time, shooting_freq=25):
    """
    Return the frames necesary to record the time given with ShootingFreq
    time = frames*interval
    :param time: In seconds
    :param shooting_freq: in Herz
    :return:
    """
    interval_sec = 1.0/shooting_freq
    interval_ms = interval_sec*1000
    frames = None
    if time:
        # TODO: For some reason we need to double the frames to get more or les the time
        frames = int(math.ceil(time/interval_sec))

    rospy.logwarn("time sec = "+str(time))
    rospy.logwarn("interval ms = "+str(interval_ms))
    rospy.logwarn("frames = "+str(frames))

    return interval_ms, frames

def convert_sec_ms(t_sec, shooting_freq):
    interval_sec = 1.0/shooting_freq
    interval_ms = interval_sec*1000
    return interval_ms

def correct_time(rec_time):
    """
    It corrects for a Bug not solved that makes recording les time that what
    Observation gives -->
    Time 15 --> Rec Time 3
    Time 30 --> Rec Time 6
    Time 300 --> Rec Time 60
    :param rec_time:
    :return:
    """
    #cor_time = rec_time*5
    cor_time = rec_time
    rospy.logwarn("cor_time == "+str(cor_time))
    return cor_time

def listener_htm_lvlx(levels_to_plot_list, video_file_path=None, rec_time=None):
    """
    We plot only the htm_lvls given in the list , this way we only process the data that we need.
    :param level_topic_name_list:
    :return:
    """

    corrected_rec_time = rec_time

    rospy.init_node('htm_lvlx_plotter', log_level=rospy.INFO, anonymous=True)

    rospy.logdebug("levels to plot list == "+str(levels_to_plot_list))
    plot_htm_bucket_object = PlotHtmBucket(levels_to_plot_list=levels_to_plot_list)

    init_time = rospy.get_time()

    def updatefig(*args):
        plot_htm_bucket_object.update_htm_data()
        plot_htm_bucket_object.print_htm_data()
        plot_htm_bucket_object.update_plot()

        if video_file_path:
            now = rospy.get_time()
            time_dif = now - init_time
            rospy.logwarn(time_dif)
            if time_dif > corrected_rec_time:
                rospy.logwarn("Time is up")
                rospy.signal_shutdown("Time is Up")
            else:
                rospy.logwarn("Time passed ("+str(time_dif)+")< corrected_rec_time ("+str(corrected_rec_time)+")")


        return plot_htm_bucket_object.get_ims()

    # 25Hz
    interval_ms = convert_sec_ms(t_sec=corrected_rec_time, shooting_freq=25)
    frames = time_to_frames(corrected_rec_time)
    ani = animation.FuncAnimation(plot_htm_bucket_object.fig, updatefig, frames=frames, interval=interval_ms, blit=True)



    if video_file_path:
        mywriter = animation.FFMpegWriter(bitrate=None)
        ani.save(video_file_path, fps=None, dpi=600, writer=mywriter, codec="libx264")
        rospy.loginfo("Recording Ended...")

    else:
        plt.show()

    return True