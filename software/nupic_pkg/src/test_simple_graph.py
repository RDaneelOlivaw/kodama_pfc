import networkx as nx
import matplotlib.pyplot as plt
import random
from networkx import graphviz_layout


def create_dir_graph(graph, connection_weigh):


    G=nx.DiGraph()
    # add edges
    for idx, e in enumerate(graph):
        G.add_edges_from([e], weight=connection_weigh[idx])

    return G

def draw_graph(graph, connection_weigh, show):

    node_size = 1000
    linewidths = 3
    node_alpha = 1.0
    node_text_size=15
    labels=None
    edge_text_pos=0.3
    text_font = 'sans-serif'
    edge_tickness=5
    edge_alpha=1.0
    edge_color='green'

    G = create_dir_graph(graph, connection_weigh)
    # draw graph

    print("graph has %d nodes with %d edges"\
          %(nx.number_of_nodes(G.to_undirected()),nx.number_of_edges(G.to_undirected())))
    print(nx.number_connected_components(G.to_undirected()),"connected components")

    plt.figure(1,figsize=(10,8))
    pos = nx.graphviz_layout(G,prog="neato")


    elarge=[(u,v) for (u,v,d) in G.edges(data=True) if d['weight'] >0.5]
    esmall=[(u,v) for (u,v,d) in G.edges(data=True) if d['weight'] <=0.5]

    nx.draw_networkx_edges(G, pos, edgelist=elarge, width=edge_tickness,alpha=edge_alpha,edge_color="red",arrows=True,edge_cmap=plt.cm.Reds)
    nx.draw_networkx_edges(G, pos, edgelist=esmall, width=edge_tickness,alpha=edge_alpha,edge_color="green",arrows=True,edge_cmap=plt.cm.Reds)

    C=nx.connected_component_subgraphs(G.to_undirected())
    for g in C:

        c=[random.random()]*nx.number_of_nodes(g) # random color...

        nx.draw_networkx_nodes(g, pos, node_size=node_size, linewidths=linewidths, alpha=node_alpha, node_color=c, vmin=0.0, vmax=1.0,)
        nx.draw_networkx_labels(g, pos, font_size=node_text_size,font_family=text_font)


        if labels is None:
            labels = []
            for e in range(len(graph)):
                labels.append(random.randint(0,100))

        labels = connection_weigh
        edge_labels = dict(zip(graph, labels))
        nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels,label_pos=edge_text_pos)

    plt.axis('off')
    # show graph
    if show:
        plt.show()
    else:
        plt.savefig("simple_cluster.png", dpi=400)

# draw example
if __name__ == "__main__":
    graph = [(0, 1), (1, 5), (1, 7), (4, 5), (4, 8), (1, 6), (3, 7), (5, 9),(2, 4),(10, 11), (11, 10)]
    connection_weigh = [0.74, 0.5, 0.69, 0.47, 0.8, 0.3, 1.1, 0.3, 0.2, 0.1, 0.7]
    draw_graph(graph, connection_weigh, show=False)
