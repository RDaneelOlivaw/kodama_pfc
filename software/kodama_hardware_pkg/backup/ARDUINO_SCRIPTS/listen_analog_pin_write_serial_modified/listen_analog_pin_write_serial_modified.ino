/* RDaneelOlivaw
 * ----------------
 * We listen to an digital pin, sample the
 * signal and write it to the serial port.
 * Each revolution should be 400 steps.
 */
int ENCODER_TO_ARD_PIN = 4;
int encoder_to_ard_pin_last = LOW; 
int encoder0Pos = 0;
int n = LOW;
unsigned long time;
 
void setup() {
 time = millis();
 pinMode (ENCODER_TO_ARD_PIN,INPUT); // its a single sensor encoder. NO direction data.
 Serial.begin(9600);
 Serial.println("Ready to READ ANALOG PIN 5"); // print "Ready to Move PCB STARTER_KIT" once
}

void loop() { 
   n = digitalRead(ENCODER_TO_ARD_PIN);
   if ( ((encoder_to_ard_pin_last == LOW) && (n == HIGH)) || ((encoder_to_ard_pin_last == HIGH) && (n == LOW)) )
   { // We moved a step LOW to HIGH. or HIGH to LOW
     encoder0Pos++;
   }
   
   if (millis() > (time + 100 ))
   {
    Serial.println(encoder0Pos,DEC);
    time = millis();
   } 
   //Serial.println(encoder0Pos,DEC);
   //delay(100);
   encoder_to_ard_pin_last = n;
 }
