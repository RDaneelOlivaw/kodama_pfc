#!/usr/bin/env python
import rospy
from plot_graph import plot_cluster
import numpy as np
from scipy.sparse import csr_matrix


def htm_cluster_plotter(similarity_matrix, save_figure=False, figure_path=None):

    rospy.init_node('htm_cluster_plotter', anonymous=True)
    plot_cluster(similarity_matrix, show=True, figure_path=None)



if __name__ == '__main__':
    indptr = np.array([0, 2, 3, 6])
    indices = np.array([0, 2, 2, 0, 1, 2])
    data = np.array([1, 2, 3, 4, 5, 2])
    A = csr_matrix((data, indices, indptr), shape=(3, 3))
    print A
    #save_figure = False
    #figure_path = '/home/rdaneel/Pictures/cluster_test_1.jpg'
    htm_cluster_plotter(A)
