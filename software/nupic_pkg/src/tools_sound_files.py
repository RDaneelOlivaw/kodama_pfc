#!/usr/bin/env python
import os
import sys
import random
import shutil
import subprocess
import wave

OPTIONS_LIST = ['Anger/', 'Boredom/', 'Disgust/', 'Fear/', 'Happiness/', 'Neutral/', 'Sadness/']
OPTIONS_LIST_2 = ['Anger', 'Boredom', 'Disgust', 'Fear', 'Happiness', 'Neutral', 'Sadness', 'Surprise']


def get_args():
    total = len(sys.argv)
    cmdargs = str(sys.argv)
    print ("The total numbers of args passed to the script: %d " % total)
    print ("Args list: %s " % cmdargs)
    print ("Script name: %s" % str(sys.argv[0]))
    print ("First argument: %s" % str(sys.argv[1]))
    print ("Second argument: %s" % str(sys.argv[2]))

    return sys.argv[1], sys.argv[2]

def get_args_1():
    total = len(sys.argv)
    cmdargs = str(sys.argv)
    print ("The total numbers of args passed to the script: %d " % total)
    print ("Args list: %s " % cmdargs)
    print ("Script name: %s" % str(sys.argv[0]))
    print ("First argument: %s" % str(sys.argv[1]))

    return sys.argv[1]


def rename_files_in_dir():

    a1,a2 = get_args()

    dir_path = a1

    for filename in os.listdir(a1):
        if filename.endswith(".wav"):
            print "WAV file = "+str(filename)
            filename_path = os.path.join(dir_path,filename)
            filename_new = a2+"-"+filename
            filename_new_path = os.path.join(dir_path,filename_new)
            #os.rename(filename_path, filename_new_path)
            print "WAV file = "+str(filename_new)



def name_classifier(filename):
    name = filename.split('.')[0]

    result = None

    if "W" in name:
        result = OPTIONS_LIST[0]
    if "L" in name:
        result = OPTIONS_LIST[1]
    if "E" in name:
        result = OPTIONS_LIST[2]
    if "A" in name:
        result = OPTIONS_LIST[3]
    if "F" in name:
        result = OPTIONS_LIST[4]
    if "N" in name:
        result = OPTIONS_LIST[5]
    if "T" in name:
        result = OPTIONS_LIST[6]

    print result
    return result


def move_german_files():
    a1,a2 = get_args()

    origin_dir_path = a1
    destination_dir_path = a2

    for filename in os.listdir(origin_dir_path):
        if filename.endswith(".wav"):
            print "WAV file = "+str(filename)
            class_name = name_classifier(filename)

            filename_path = os.path.join(origin_dir_path,filename)

            classame_new_path = os.path.join(destination_dir_path,class_name)
            filename_new_path = os.path.join(classame_new_path,filename)

            print filename_new_path
            os.rename(filename_path, filename_new_path)

            print "WAV file new path = "+str(filename_new_path)


def move_percentage_files_learn_predict(percentage, obj_dir_name, base_dir_path):

    dir_to_move = obj_dir_name
    classified_path = os.path.join(base_dir_path, "Classified/")
    object_dir = os.path.join(classified_path, dir_to_move)

    learn_path = os.path.join(base_dir_path, "Learning/")
    predict_path = os.path.join(base_dir_path, "Predict/")

    number_files = len(os.listdir(object_dir))
    num_learn = int(number_files*percentage)
    num_predict = int(number_files - num_learn)

    print "TOTAL FILES ="+str(number_files)
    print "To LEARN ="+str(num_learn)
    print "To PREDICT ="+str(num_predict)
    i = 0

    for filename in os.listdir(object_dir):

        filename_path = os.path.join(object_dir,filename)

        if i < num_learn:
            print "MOVE TO LEARN =>"+str(filename)
            filename_new_path = os.path.join(learn_path, filename)
        else:
            print "MOVE TO PREDICT =>"+str(filename)
            filename_new_path = os.path.join(predict_path, filename)

        print filename_path
        print filename_new_path
        print "----------------"
        os.rename(filename_path, filename_new_path)
        i += 1


def move_percentage_files_main():
    a1 = get_args_1()
    percentage = 0.8
    for element in OPTIONS_LIST_2:
        print element
        move_percentage_files_learn_predict(percentage, element, a1)


def move_percentage_files_learn_predict_classified(percentage, obj_dir_name, base_dir_path, dry=True):

    dir_to_move = obj_dir_name
    classified_path = os.path.join(base_dir_path, "Classified/")
    object_dir = os.path.join(classified_path, dir_to_move)

    aux_learn_path = os.path.join(base_dir_path, "Learning/")
    learn_path = os.path.join(aux_learn_path, obj_dir_name)

    aux_predict_path = os.path.join(base_dir_path, "Predict/")
    predict_path = os.path.join(aux_predict_path, obj_dir_name)

    number_files = len(os.listdir(object_dir))
    num_learn = int(number_files*percentage)
    num_predict = int(number_files - num_learn)

    print "TOTAL FILES ="+str(number_files)
    print "To LEARN ="+str(num_learn)
    print "To PREDICT ="+str(num_predict)
    elements_in_learn = 0
    elements_in_predict = 0

    files_to_move_list = os.listdir(object_dir)

    while len(files_to_move_list)>0:

        filename = files_to_move_list[0]

        filename_path = os.path.join(object_dir,filename)

        rand_int = random.randint(0,1)
        if elements_in_learn < num_learn and rand_int == 1:
            print "MOVE TO LEARN =>"+str(filename)
            filename_new_path = os.path.join(learn_path, filename)
            elements_in_learn += 1
            files_to_move_list.pop(0)
        elif elements_in_predict < num_predict and rand_int == 0:
            print "MOVE TO PREDICT =>"+str(filename)
            filename_new_path = os.path.join(predict_path, filename)
            elements_in_predict += 1
            files_to_move_list.pop(0)
        elif elements_in_predict >= num_predict:
            print "MOVE TO LEARN =>"+str(filename)
            filename_new_path = os.path.join(learn_path, filename)
            elements_in_learn += 1
            files_to_move_list.pop(0)
        elif elements_in_learn >= num_learn:
            print "MOVE TO PREDICT =>"+str(filename)
            filename_new_path = os.path.join(predict_path, filename)
            elements_in_predict += 1
            files_to_move_list.pop(0)
        else:
            print "Nothing to be done...."

        print filename_path
        print filename_new_path
        print "----------------"
        if not dry:
            shutil.copy2(filename_path, filename_new_path)
            #os.rename(filename_path, filename_new_path)

    print "All files Moved"



def move_percentage_files_main_classified(dry):
    """
    Ex: a1 = /media/rdaneel/Data/Kodama_Project_Files/
    """
    a1 = get_args_1()
    percentage = 0.8
    for element in OPTIONS_LIST_2:
        print element
        move_percentage_files_learn_predict_classified(percentage, element, a1, dry)

def convert_monofile_to_44100hz(origin_wav_path, output_wav_path):
    from subprocess import call
    cmd = "sox"
    arguments = "-S "+origin_wav_path+" "+output_wav_path+" rate -L -s 44100"
    cmd2 = cmd+" "+arguments
    print cmd2
    os.system(cmd2)

def convert_to_44100hz_all_dir(input_dir_path, output_dir_path, dry=True):
    """
    input_dir_path = "/media/rdaneel/Data/Kodama_Project_Files/Classified/Sadness/Old"
    output_dir_path = "/media/rdaneel/Data/Kodama_Project_Files/Classified/Sadness/New"
    convert_to_44100hz_all_dir(input_dir_path, output_dir_path, dry)
    """
    file_list = os.listdir(input_dir_path)
    for file in file_list:
        print file
        input_file_path = os.path.join(input_dir_path, file)
        output_file_path = os.path.join(output_dir_path, file)
        print input_file_path
        print output_file_path
        if not dry:
            convert_monofile_to_44100hz(input_file_path, output_file_path)


def get_sound_file_info(infile):
    print infile
    w = wave.open(infile, 'rb')
    channels = w.getnchannels()
    rate = w.getframerate()

    return channels, rate

def get_sound_file_list_info(infiles_list):
    channels_list = []
    rates_list = []

    for infile in infiles_list:
        channels, rate = get_sound_file_info(infile)
        print "channels =="+str(channels)
        print "rate =="+str(rate)
        channels_list.append(channels)
        rates_list.append(rate)

    return channels_list, rates_list

def convert_mono_to_stereo(mono_in_file,stereo_out_file):
    """
    Ex: sox -M -c 1 08a05Fe.wav -c 1 08a05Fe.wav 08a05Fe_stereo.wav
    :param mono_in_file:
    :param stereo_out_file:
    :return:
    """
    subprocess.call(['sox'] + ['-M', '-c', '1'] + [mono_in_file]+['-c', '1']+[mono_in_file]+[stereo_out_file])

def convert_stereofile_to_44100hz(origin_wav_path, output_wav_path):
    """
    Ex: sox -S -c 2 gio-f2-l1.wav gio-f2-l1_out.wav rate -L -s 44100
    The input wav file HAS to be STEREO
    :param origin_wav_path:
    :param output_wav_path:
    :return:
    """
    subprocess.call(['sox'] + ['-S', '-c', '2'] + [origin_wav_path]+[output_wav_path]+['rate', '-L', '-s', '44100'])


def standarize_stereo_44100(origin_wav_path, output_wav_path):

    channels, rate = get_sound_file_info(origin_wav_path)

    stereo_tmp = "/tmp/stereo.wav"
    if channels == 1:
        if rate != 44100:
            convert_mono_to_stereo(origin_wav_path,stereo_tmp)
            convert_stereofile_to_44100hz(stereo_tmp, output_wav_path)
            print "Convert to Stereo and to 44100 = "+str(origin_wav_path)
        else:
            convert_mono_to_stereo(origin_wav_path,output_wav_path)
            print "Convert to Stereo = "+str(origin_wav_path)

    else:
        if rate != 44100:
            convert_stereofile_to_44100hz(origin_wav_path, output_wav_path)
            print "Convert to 44100 = "+str(origin_wav_path)
        else:
            print "Nothing to convert...= "+str(origin_wav_path)

    print "Standarized..."

def standarize_list(sound_list, base_dir_out_files=None):
    out_list = []
    for infile in sound_list:
        if not base_dir_out_files:
            base = os.path.dirname(infile)
            infile_name = os.path.basename(infile)
            name = os.path.splitext(infile_name)[0]
            ext = os.path.splitext(infile_name)[1]
            name_out = name+"_out"+ext
        else:
            base = base_dir_out_files
            infile_name = os.path.basename(infile)
            name_out = infile_name

        outfile = os.path.join(base, name_out)
        print outfile
        out_list.append(outfile)
        standarize_stereo_44100(infile, outfile)

    return out_list

def join_wav_files_list(wav_files_list, result_output_wav_path):
    print "Joining ="+str(wav_files_list)
    subprocess.call(['sox'] + wav_files_list + [result_output_wav_path])
    print "Joint wav files into ="+str(result_output_wav_path)


def standarize_and_join_wav_list(wav_files_list, result_output_wav_path, base_dir_out_files=None):
    """
    Standarizes the wav files to be able to join then into a single wav file
    :param wav_files_list:
    :param result_output_wav_path:
    :return:
    """
    out_wav_files_list = standarize_list(wav_files_list, base_dir_out_files)
    join_wav_files_list(out_wav_files_list, result_output_wav_path)

if __name__ == "__main__":
    dry = False
    move_percentage_files_main_classified(dry)