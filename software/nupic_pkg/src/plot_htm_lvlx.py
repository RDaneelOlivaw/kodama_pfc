#!/usr/bin/env python
import rospy
from plot_htm import listener_htm_lvlx
import matplotlib.animation as animation

if __name__ == '__main__':
    recording_end = False
    levels_to_plot_list = [1,2]
    #video_file_path = raw_input("video_file name =")

    #video_file_path = '/media/rdaneel/Data/Kodama/Videos/html_plot_200_40_8_60min_angry.mp4'
    #video_file_path = '/media/rdaneel/Data/Kodama/Videos/html_plot_200_40_8_60min_bored.mp4'
    #video_file_path = '/media/rdaneel/Data/Kodama/Videos/html_plot_200_40_8_60min_disgust.mp4'
    #video_file_path = '/media/rdaneel/Data/Kodama/Videos/html_plot_200_40_8_60min_fear.mp4'
    #video_file_path = '/media/rdaneel/Data/Kodama/Videos/html_plot_200_40_8_60min_happy.mp4'
    #video_file_path = '/media/rdaneel/Data/Kodama/Videos/html_plot_200_40_8_60min_neutral.mp4'
    #video_file_path = '/media/rdaneel/Data/Kodama/Videos/html_plot_200_40_8_60min_sad.mp4'
    video_file_path = '/media/rdaneel/Data/Kodama/Videos/html_plot_200_40_8_60min_surprise.mp4'

    # T for prediction recording
    t_in_min = 2
    # 60 to go sec and 2 because we record 2 levels
    r_time = t_in_min*60
    while not rospy.is_shutdown() and not recording_end:
        recording_end = listener_htm_lvlx(levels_to_plot_list, video_file_path=video_file_path, rec_time=r_time)

