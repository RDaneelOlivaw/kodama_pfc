// This ino recieves comands for moving the Servo and executes it.

#include <Servo.h>
#include <TinkerKit.h>

// create servo object to control a servo 
// a maximum of eight servo objects can be created 
// This is works with python script sending_receiving_commands_PYSerial.py
//These limits are experimental. In Theory it should have 180degrees range but it only has 160 stable. The last 20
// are unstable and give vibrations.
Servo myservo;
int servo_position = 0;
int INITIAL_POS = 0;
int i_MAX_POS = 160;
int i_MIN_POS = 0;
byte servo1_name = O0;

void setup() {
//Serial.begin(9600); // set the baud rate
Serial.begin(115200);
Serial.println("Ready"); // print "Ready" once
myservo.attach(servo1_name);
// the servos when started go to this position
myservo.write(INITIAL_POS);
servo_position = INITIAL_POS;
}

void loop() {
handleSerialCommunication();
}

void handleSerialCommunication(void){
  if (Serial.available())
  {
    char inByte = Serial.read();
    switch(inByte)
    {
    case 'r': //Data request
      serialPrintPos();
      break;

    case 'b': //Move Servo the to Pos requested
      MoveServo(numberFromSerial(2));
      break;

    case 't': //Move Servo To Pos requested in the Time requested
      MoveServoTime();
      break;  
    
    default:
      Serial.println(inByte);
    }
    Serial.flush();
  }
}


void MoveServoTime(void)
{
  long number_data;
  String datum;
  //int pos_time_array;
  int desired_pos;
  int desired_time;
  int current_pos;
  int pos;
  int difference;
  int des_delay;
  int number_dig;
  char charBufTime[10];

  number_data = numberFromSerial(7);
  desired_pos = number_data / 100000; // position where we want to move the servo to, not necesarilly the final one, because it could be out of Servo Range.
  desired_time = (number_data - (desired_pos*100000));// Time in MILIseconds that will take us to reach that position
  desired_pos = servoLimits(desired_pos);// Within Servo limits.

  current_pos = servo_position;
  
  Serial.println(desired_pos);
  Serial.println(desired_time);
  Serial.println(current_pos);
  
  difference = desired_pos - current_pos;
  // Singularity Processing
  if (difference == 0)
  {
    des_delay = 0;
  }
  else
  {
    des_delay = desired_time / abs(difference); // Worst case scenario, 1ms MAXerror for a 160 degreee--> Total time Error of 0.160seconds.
  }
  
 
  Serial.println("MOVING");
  if (difference >= 0) // Turning Right, encrease position
  {
    for(pos = current_pos; pos <= desired_pos; pos += 1)  // goes from current_pos degrees to desired_pos degrees 
    {
      MoveServoWithDelay(pos,des_delay);
    } 
  }
  else
  {
    for(pos = current_pos; pos >= desired_pos; pos -= 1)  // goes from current_pos degrees to desired_pos degrees 
    {
      MoveServoWithDelay(pos,des_delay);
    } 
  }
  
  Serial.println("OK");
}

void MoveServoWithDelay(int pos, int time)
{
  myservo.write(pos);              // tell servo to go to position in variable 'pos' 
  delay(time);                       // waits 1ms for the servo to reach the position 
  Serial.println(servo_position); // WARNING: THIS IS NOT THE REAL POSITION NECESARILY, ONLY REAL WHEN THE MOVEMENT IS SLOW ENOUGH
  servo_position = pos; // Update the position of the servo
}

void MoveServo(int pos)
{
  Serial.println("MOVING");
  myservo.write(servoLimits(pos));
  Serial.println("OK");
  servo_position = servoLimits(pos);// Within Servo limits.
}

long numberFromSerial(int num_of_digits_to_read)
{
  char numberString[9];
  unsigned char index=0;
  char charBufAux[10];
  
  delay(10);
  while(Serial.available())
  {
    delay(10);
    numberString[index++]=Serial.read();
    if(index > num_of_digits_to_read)
    {
      break;
    }
  }
  numberString[index]=0;
  return atol(numberString);
}


void serialPrintPos(void)
{
 Serial.println(servo_position);
   
}


int servoLimits(int i_number)
{
 int s_num;

 if (i_number > i_MAX_POS)
 {
   s_num = i_MAX_POS;
 }
 else if (i_number < i_MIN_POS)
 {
   s_num = i_MIN_POS;
 }
 else
 {
   s_num = i_number;
 }
 return s_num;
}
