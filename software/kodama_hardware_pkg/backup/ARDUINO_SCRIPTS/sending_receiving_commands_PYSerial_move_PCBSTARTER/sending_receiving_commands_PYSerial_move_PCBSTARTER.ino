// This is works with python script sending_receiving_commands_PYSerial.py
int STB_PIN = 13; // Pin for the On OFF output signal for PCB, also conected to ARDUINO LED
int SEL_PIN = 12; // Pin for CW od CCW output signal
boolean pcb_active = false;

void setup() {
pinMode(STB_PIN, OUTPUT);
pinMode(SEL_PIN, OUTPUT);
digitalWrite(STB_PIN, HIGH); // We put the PCB on STANDBY
Serial.begin(9600); // set the baud rate
Serial.println("Ready to Move PCB STARTER_KIT"); // print "Ready to Move PCB STARTER_KIT" once

}

void loop() {
handleSerialCommunication();
}

void handleSerialCommunication(void){
  if (Serial.available())
  {
    char inByte = Serial.read();
    switch(inByte)
    {
    case 'r': // Move CW
      MovePcbStarter('r');
      break;

    case 'l': // Move CCW
      MovePcbStarter('l');
      break;

    case 's': // Put to StandBy PCB
      digitalWrite(STB_PIN, HIGH); //STANDBY PCB, led ARDUINO TURNS ON
      Serial.println("Put PCB on STANDBY");
      break;

    default:
      Serial.println(inByte);
    }
    Serial.flush();
  }
}
 
void MovePcbStarter(char sense_command)
{
  if (!pcb_active)
  {
   digitalWrite(STB_PIN, LOW);//ACTIVATE PCB, led ARDUINO TURNS OFF 
  }
  
  if (sense_command == 'r')
  {
    digitalWrite(SEL_PIN, LOW); // Move CW
  }
  else
  {
    digitalWrite(SEL_PIN, HIGH); // Move CCW
  }
  
  Serial.println("MOVING");
}
