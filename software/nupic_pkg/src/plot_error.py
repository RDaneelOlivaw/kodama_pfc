#!/usr/bin/env python
import rospy
from pylab import *
from nupic_pkg.msg import error_array_lvx
from nupic_pkg.msg import graph
import math
import copy
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib.animation as animation
import random


ERROR_LVLX_TOPIC_NAME = "/htm_error_lvlx"

class PlotGraph(object):
    def __init__(self, t_len_limit):
        self._t_init = rospy.get_time()
        self._t_now = 0
        self.t_array = [0]
        self.y_array = [0]
        self._t_len_limit = t_len_limit

    def update_y_values(self, new_value):
        self.y_array.append(new_value)
        self.update_t_values()

        if len(self.t_array) > self._t_len_limit:
            self.t_array.pop(0)
            self.y_array.pop(0)


    def update_t_values(self):
        self.update_t_now()
        self.t_array.append(self.get_t_now())

    def update_t_now(self):
        t_value = rospy.get_time()
        self._t_now = (t_value - self._t_init)
        rospy.logdebug("T NEW NOW = "+str(self._t_now))

    def get_t_now(self):
        return self._t_now

    def get_t_y_arrays(self):
        return self.t_array, self.y_array


class SubplotAnimation(animation.TimedAnimation):
    def __init__(self, number_lvls, t_len_limit, y_limit, title, title_font_size, anim_time, data_topic_name):
        self._t_init = rospy.get_time()
        self.fig = plt.figure()
        self.fig.suptitle(title, fontsize=title_font_size)
        self.color_array = ['black', 'red', 'green']
        self.number_lvls = number_lvls
        self._t_len_limit = t_len_limit
        self._anim_time = anim_time
        self._y_limit = y_limit
        self._data_topic_name = data_topic_name

        self.ax_array = []
        self.line_array = []
        self.graph_array = []

        self._x_label = "time [s]"
        self._y_label = "Error [n.u.]"

        self._t_canvas_redraw = rospy.get_time()

        self.fill_ax_array()
        self.fill_line_array()

        self.ax_setup()

        self.graph_setup()

        animation.TimedAnimation.__init__(self, self.fig, interval=50, blit=False,  repeat=True)


    def graph_setup(self):
        for lvl in range(1,self.number_lvls+1):
            self.graph_array.append(PlotGraph(t_len_limit=self._t_len_limit))

    def graph_update(self, new_y_value_lvlx_array, learning_status_array):
        for idx, y_value in enumerate(new_y_value_lvlx_array):
            learning = learning_status_array[idx]
            if learning:
                self.graph_array[idx].update_y_values(y_value)

    def get_t_y_from_idx_graph(self, idx):
        return self.graph_array[idx].get_t_y_arrays()

    def fill_ax_array(self):
        for lvl in range(1,self.number_lvls+1):
            num_rows = int(math.ceil(self.number_lvls/3.0))
            num_cols = min(self.number_lvls, 3)
            position = lvl
            ax = self.fig.add_subplot(num_rows, num_cols, position)
            self.ax_array.append(ax)

    def fill_line_array(self):
        for lvl in range(1, self.number_lvls+1):
            color = self.color_array[random.randint(0, len(self.color_array)-1)]
            line = Line2D([], [], color=color)
            self.line_array.append(line)

    def ax_setup(self):
        for idx, ax in enumerate(self.ax_array):
            ax.set_xlabel('x')
            ax.set_ylabel('y')

            ax.set_title('LVL = '+str(idx+1))

            ax.add_line(self.line_array[idx])

            ax.set_xlim([0, self._t_len_limit])
            ax.set_ylim([0, self._y_limit])

            ax.set_xlabel(self._x_label)
            ax.set_ylabel(self._y_label)


    def _draw_frame(self, framedata):

        t_now = rospy.get_time()
        t_diff = t_now - self._t_init
        rospy.loginfo(t_diff)
        self._drawn_artists = []

        new_y_value_lvlx_array, learning_status_array = self.get_y_values_from_topic()

        self.graph_update(new_y_value_lvlx_array, learning_status_array)


        for idx, line in enumerate(self.line_array):

            t, y = self.get_t_y_from_idx_graph(idx)
            x = t
            y = y

            rospy.logdebug("len(x)"+str(len(x)))
            rospy.logdebug("x = "+str(x))
            if len(x) == 1:
                new_xmin = x[0]
                new_xmax = 1
            elif len(x) == 0:
                # Unitary range
                new_xmin = 0
                new_xmax = 1
            else:
                new_xmin = x[1]
                new_xmax = x[len(x)-1]

            self.ax_array[idx].set_xlim(new_xmin, new_xmax)

            rospy.logdebug("X ="+str(x))
            rospy.logdebug("Y ="+str(y))

            line.set_data(x, y)
            self._drawn_artists.append(line)


    def new_frame_seq(self):
        return iter(range(self._anim_time))


    def _init_draw(self):
        for l in self.line_array:
            l.set_data([], [])

    def get_y_random_values(self):

        y_values_array = []
        for lvl in range(1,self.number_lvls+1):
            r_num = random.randint(1,10)
            if r_num == 0:
                y_values_array.append(None)
            else:
                y_values_array.append(r_num)

        return y_values_array

    def get_y_values_from_topic(self):

        msg = rospy.wait_for_message(self._data_topic_name, error_array_lvx)
        error_lvlx_array = msg.error_lvlx
        learning_status_array = msg.learning_status

        rospy.logdebug("number levels = "+str(self.number_lvls))
        rospy.logdebug("len(learning_status_array) = "+str(len(learning_status_array)))

        assert len(error_lvlx_array) == self.number_lvls, "Number of lvls in ErrorArray don't coincide"
        assert len(learning_status_array) == self.number_lvls, "Number of lvls in LearningStatus don't coincide"

        y_new_values_array = []
        # We convert the msg into a standard array for better compatibility
        for element in error_lvlx_array:
            y_new_values_array.append(element.y_value)

        return y_new_values_array, learning_status_array


def listener_error_lvlx(record_video, anim_time=100, video_file_path=None):

    rospy.init_node('htm_error_lvlx_plotter', anonymous=True)

    level_topic_name = ERROR_LVLX_TOPIC_NAME
    msg = rospy.wait_for_message(level_topic_name, error_array_lvx)
    error_lvlx_array = msg.error_lvlx
    num_levels = len(error_lvlx_array)

    rospy.loginfo("Number of Levels = "+str(num_levels))
    rospy.loginfo("Error for each level = "+str(error_lvlx_array))

    t_range_limit = 50
    y_limit = 1.1
    main_title = "Errors in different HTM levels"
    title_font_size = 20

    data_topic_name = ERROR_LVLX_TOPIC_NAME
    ani = SubplotAnimation(num_levels, t_range_limit, y_limit, main_title, title_font_size, anim_time, data_topic_name)

    if record_video:
        mywriter = animation.FFMpegWriter(bitrate=500)
        ani.save(video_file_path, fps=300, dpi=600, writer=mywriter, codec="libx264")
        rospy.loginfo("Recording Ended...")
        return True
    else:
        plt.show()
        return False


class RandomErrorPublisher(object):
    def __init__(self, publish_rate, num_lvls):

        self.pub = rospy.Publisher(ERROR_LVLX_TOPIC_NAME, error_array_lvx, queue_size=1)
        self._publish_rate = publish_rate
        self._num_lvls = num_lvls
        self._r_l_status = [False]*self._num_lvls
        self._r_idx_status = 0
        self._init_t = rospy.get_time()
        self._init_t_diff = 15

    def run(self):
        r = rospy.Rate(self._publish_rate)
        graph_value = graph()
        while not rospy.is_shutdown():
            self.msg = error_array_lvx()

            for idx in range(1,self._num_lvls+1):
                r_num = random.random()
                graph_value = graph()
                graph_value.y_value = r_num
                self.msg.error_lvlx.append(copy.deepcopy(graph_value))


            t_now = rospy.get_time()
            t_diff = t_now - self._init_t
            if t_diff > self._init_t_diff:
                if self._r_idx_status < len(self._r_l_status):
                    self._r_l_status[self._r_idx_status] = True
                    self._r_idx_status += 1
                    self._init_t = rospy.get_time()
                else:
                    None

            self.msg.learning_status = self._r_l_status
            rospy.loginfo(str(self.msg.error_lvlx))
            rospy.loginfo(str(self.msg.learning_status))

            self.pub.publish(self.msg)
            r.sleep()

    def publish_one_value(self,value):
        self.pub.publish(str(value))


class ErrorPublisher(object):
    def __init__(self, publish_rate, num_lvls):

        self.pub = rospy.Publisher(ERROR_LVLX_TOPIC_NAME, error_array_lvx, queue_size=1)
        self._publish_rate = publish_rate
        self._num_lvls = num_lvls
        self._last_pub_time = rospy.get_time()
        self._publish_period = 1.0 / publish_rate
        self._init_error_value = 1

    def get_last_pub_elapsed_time(self):
        return rospy.get_time() - self._last_pub_time

    def update_last_time_pub(self):
        self._last_pub_time = rospy.get_time()

    def publish_new_error_lvlx_array(self, error_array, learning_status_array):

        time_elapsed_from_last_publish = self.get_last_pub_elapsed_time()
        rospy.loginfo("Time since last publish = "+str(time_elapsed_from_last_publish))

        if time_elapsed_from_last_publish >= self._publish_period:
            self.msg = error_array_lvx()

            for lvl_error in error_array:
                graph_value = graph()
                graph_value.y_value = lvl_error
                self.msg.error_lvlx.append(copy.deepcopy(graph_value))

            self.msg.learning_status = learning_status_array

            rospy.loginfo(str(self.msg.error_lvlx))
            rospy.loginfo(str(self.msg.learning_status))

            self.pub.publish(self.msg)
            self.update_last_time_pub()

    def init_publish(self, init_count):
        rate = rospy.Rate(self._publish_rate)
        counter = 0
        rospy.loginfo("INIT_I LIMIT ="+str(init_count))
        while not rospy.is_shutdown() and counter<init_count:
            rospy.loginfo("Initialazing Plots of Error..."+str(counter))
            mock_error_array = []
            for index in range(self._num_lvls):
                mock_error_array.append(self._init_error_value)

            mock_learn_status = [True]*self._num_lvls

            self.publish_new_error_lvlx_array(mock_error_array, mock_learn_status)

            rospy.logdebug("Sleep")
            rate.sleep()
            counter += 1
        rospy.loginfo("INIT_I LIMIT ="+str(init_count))

def publisher_error_lvlx(publish_rate, num_lvls):

    rospy.init_node('error_publisher_node')
    random_object = RandomErrorPublisher(publish_rate, num_lvls)
    random_object.run()