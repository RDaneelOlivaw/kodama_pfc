#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib.animation as animation
import random
import math
import rospy

# This example uses subclassing, but there is no reason that the proper
# function couldn't be set up and then use FuncAnimation. The code is long, but
# not really complex. The length is due solely to the fact that there are a
# total of 9 lines that need to be changed for the animation as well as 3
# subplots that need initial set up.
# Install for save http://fcorti.com/2014/04/22/ffmpeg-ubuntu-14-04-lts/
class PlotGraph(object):
    def __init__(self, t_len_limit):
        self._t_init = rospy.get_time()
        self._t_now = 0
        self.t_array = [0]
        self.y_array = [0]
        self._t_len_limit = t_len_limit

    def update_y_values(self, new_value):
        self.y_array.append(new_value)
        self.update_t_values()

        if len(self.t_array) > self._t_len_limit:
            self.t_array.pop(0)
            self.y_array.pop(0)


    def update_t_values(self):
        self.update_t_now()
        self.t_array.append(self.get_t_now())

    def update_t_now(self):
        t_value = rospy.get_time()
        self._t_now = (t_value - self._t_init)
        print "T NEW NOW = "+str(self._t_now)

    def get_t_now(self):
        return self._t_now

    def get_t_y_arrays(self):
        return self.t_array, self.y_array


class SubplotAnimation(animation.TimedAnimation):
    def __init__(self, number_lvls, t_len_limit, y_limit):
        self.fig = plt.figure()
        self.color_array = ['black', 'red', 'green']
        self.number_lvls = number_lvls
        self._t_len_limit = t_len_limit
        self._y_limit = y_limit
        self.ax_array = []
        self.line_array = []
        self.graph_array = []

        self._x_label = "time [s]"
        self._y_label = "Error [n.u.]"

        self._t_canvas_redraw = rospy.get_time()

        self.fill_ax_array()
        self.fill_line_array()

        self.ax_setup()

        self.graph_setup()

        animation.TimedAnimation.__init__(self, self.fig, interval=50, blit=True)


    def graph_setup(self):
        for lvl in range(1,self.number_lvls+1):
            self.graph_array.append(PlotGraph(t_len_limit=self._t_len_limit))

    def graph_update(self, new_y_value_lvlx_array):
        for idx, y_value in enumerate(new_y_value_lvlx_array):
            if y_value is not None:
                self.graph_array[idx].update_y_values(y_value)

    def get_t_y_from_idx_graph(self, idx):
        return self.graph_array[idx].get_t_y_arrays()

    def fill_ax_array(self):
        for lvl in range(1,self.number_lvls+1):
            num_rows = int(math.ceil(self.number_lvls/3.0))
            num_cols = min(self.number_lvls, 3)
            position = lvl
            ax = self.fig.add_subplot(num_rows, num_cols, position)
            self.ax_array.append(ax)

    def fill_line_array(self):
        for lvl in range(1, self.number_lvls+1):
            color = self.color_array[random.randint(0, len(self.color_array)-1)]
            line = Line2D([], [], color=color)
            self.line_array.append(line)

    def ax_setup(self):
        for idx, ax in enumerate(self.ax_array):
            ax.set_xlabel('x')
            ax.set_ylabel('y')

            ax.add_line(self.line_array[idx])

            ax.set_xlim(0, self._t_len_limit)
            ax.set_ylim([0, 15])

            # TODO: Why does it sometimes get axis units wrong?
            ax.set_xlabel(self._x_label)
            ax.set_ylabel(self._y_label)

            ax.set_aspect('equal', 'datalim')

    def _draw_frame(self, framedata):

        self._drawn_artists = []

        new_y_value_lvlx_array = self.get_y_values()

        self.graph_update(new_y_value_lvlx_array)


        for idx, line in enumerate(self.line_array):

            t, y = self.get_t_y_from_idx_graph(idx)
            x = t
            y = y

            new_xmin = x[1]
            new_xmax = x[len(x)-1]

            new_ymin = min(y)
            new_ymax = max(y)
            rospy.loginfo("new_ymin ="+str(new_ymin))
            rospy.loginfo("new_ymax ="+str(new_ymax))


            self.ax_array[idx].set_xlim(new_xmin, new_xmax)
            self.ax_array[idx].set_ylim(new_ymin, new_ymax)


            xmin, xmax = self.ax_array[idx].get_xlim()
            ymin, ymax = self.ax_array[idx].get_ylim()

            rospy.loginfo("xmin ="+str(xmin))
            rospy.loginfo("xmax ="+str(xmax))
            rospy.loginfo("ymin ="+str(ymin))
            rospy.loginfo("ymax ="+str(ymax))

            rospy.loginfo("X ="+str(x))
            rospy.loginfo("Y ="+str(y))


            line.set_data(x, y)
            self._drawn_artists.append(line)


    def new_frame_seq(self):
        return iter(range(self._t_len_limit))


    def _init_draw(self):
        for l in self.line_array:
            l.set_data([], [])

    def get_y_values(self):

        y_values_array = []
        for lvl in range(1,self.number_lvls+1):
            r_num = random.randint(1,10)
            if r_num == 0:
                y_values_array.append(None)
            else:
                y_values_array.append(r_num)

        return y_values_array

num_lvls = 1
t_range_limit = 50
y_limit = 20
rospy.init_node("multigraph_test", anonymous=True)
ani = SubplotAnimation(num_lvls,t_range_limit, y_limit)
#mywriter = animation.FFMpegWriter()
#ani.save('/home/rdaneel/Videos/test_sub.mp4',writer=mywriter)
plt.show()