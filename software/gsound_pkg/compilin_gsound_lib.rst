Commands to compile the library gsound
**************************************


$cd ~/gsound
$g++ -c  *.cpp
$ar rcs gsound.a *.o
$ranlib gsound.a


We test that we have compiled correctly
$nm gsound.a

Now we create a SDK for Linux by copying the structre of the MAC_OS one


gcc -o example0 -I/home/rdaneel/playground/UBUNTU_GSOUND_SDK/UBUNTU/include main.cpp /home/rdaneel/playground/gsound/*.o

gcc -o example0 -I/home/rdaneel/playground/UBUNTU_GSOUND_SDK/UBUNTU/include main.cpp -L/home/rdaneel/playground/UBUNTU_GSOUND_SDK/UBUNTU -lgsound

gcc -o example0 -I/home/rdaneel/playground/UBUNTU_GSOUND_SDK/UBUNTU/include -L/home/rdaneel/playground/UBUNTU_GSOUND_SDK/UBUNTU ../../UBUNTU/libgsound.a main.cpp


RONDA 2
-------
cd ~/playground/gsound
g++ -c *.cpp
reset;g++ -o example0 *.cpp

g++ -c *.cpp dsp/*.cpp internal/*.cpp math/*.cpp util/*.cpp
reset;g++ -o example0 *.cpp dsp/*.cpp internal/*.cpp util/*.cpp

Libraries Ubuntu doesnt have
****************************
	#undef __APPLE_ALTIVEC__
	#include <CoreServices/CoreServices.h>
	#include <CoreAudio/CoreAudio.h>
	#include <AudioUnit/AudioUnit.h>


FOR PORTAUDIO
*************

in cd ~/playground/c_plus_plus_test
sudo apt-get install libgcc_s
sudo updatedb
sudo apt-get install gcc-multilib
g++ test.cpp -Wl,-Bstatic -lportaudio -Wl,-Bdynamic -lrt -lasound -ljack -lpthread -o test