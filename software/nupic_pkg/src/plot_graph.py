#!/usr/bin/env python
import rospy
import copy
import networkx as nx
import matplotlib.pyplot as plt
import random
from networkx import graphviz_layout
from sparse_matrix import convert_csr_matrix_to_cluster_plot_class
import matplotlib.colors as colors
import matplotlib.cm as cmx

"""
Use example:
    graph = [(0, 1), (1, 5), (1, 7), (4, 5), (4, 8), (1, 6), (3, 7), (5, 9),(2, 4),(10, 11), (11, 10)]
    connection_weigh = [0.74, 0.5, 0.69, 0.47, 0.8, 0.3, 1.1, 0.3, 0.2, 0.1, 0.7]
    draw_graph(graph, connection_weigh, show=False)
"""


VMAX = 1.0
VMIN = 0.0

def create_dir_graph(graph, connection_weigh):


    G=nx.DiGraph()
    # add edges
    for idx, e in enumerate(graph):
        G.add_edges_from([e], weight=connection_weigh[idx])

    return G


def create_label_dict(graph, label_list):
    """
    It creates a dictionary that makse the correspondance between the node numbers and the labels listed in the
    label_list.
    :param pos:
    :param label_list:
    :return:
    """
    node_labels_correspondence_dict = {}

    rospy.logdebug("graph =="+str(graph))
    rospy.logdebug("label_list =="+str(label_list))

    list_node_num, number_nodes = get_number_of_diferent_nodes(graph)

    assert len(list_node_num) <= len(label_list), "There are more nodes than labels"
    assert max(list_node_num) < len(label_list), "The MaxNode number exceeds the label_list length"

    for node_num in list_node_num:
        node_labels_correspondence_dict.update({node_num: label_list[node_num]})

    return node_labels_correspondence_dict


def label_graph(labels, graph):

    label_graph_list = []

    for element in graph:
        take_off = element[0]
        landing = element[1]
        label_graph_list.append((labels[take_off],labels[landing]))

    return label_graph_list


def init_draw_graph_data_testing(graph, connection_weigh, labels, color_groups_label_correspondance_dict, graph_non_auto, graph_auto, non_auto_weight, auto_weight):
    """
    Tests all the init data
    :return:
    """
    rospy.logdebug("graph =====>"+str(graph))
    rospy.logdebug("connection_weigh =====>"+str(connection_weigh))
    rospy.logdebug("labels ======>"+str(labels))
    rospy.logdebug("color_groups_label_correspondance_dict ==>"+str(color_groups_label_correspondance_dict))
    l_graph = label_graph(labels, graph)
    rospy.logdebug("LABEL GRAPH ==>"+str(l_graph))

    rospy.logdebug("LEN GRAPH =="+str(len(graph)))
    rospy.logdebug("LEN labels =="+str(len(labels)))

    rospy.logdebug("LEN graph_non_auto =="+str(len(graph_non_auto))+", LEN graph_auto =="+str(len(graph_auto)))
    rospy.logdebug("LEN graph_non_auto+graph_auto == graph? =="+str(len(graph) == len(graph_non_auto)+len(graph_auto)))

    assert len(graph) == len(connection_weigh), "Wrong lengths graph+connection-weights"
    assert len(labels) == len(color_groups_label_correspondance_dict), "Wrong lengths labels+color_groups_label_correspondance_dict"
    assert len(graph) == len(graph_non_auto)+len(graph_auto), "LEN graph dont sum up"

    # We only graph non auto edges there fore the len of non_auto graph.
    num_edges = len(graph_non_auto)
    num_labeled_nodes = len(labels)

    return num_edges, num_labeled_nodes


def node_edge_draw_graph_data_testing(des_num_edges, des_num_nodes, number_nodes, G):
    """
    Tests if the number of nodes and edges are the same in G as the ones desired
    :param des_num_edges:
    :param des_num_nodes:
    :param G:
    :return:
    """
    num_nodes = nx.number_of_nodes(G.to_undirected())
    num_edges = nx.number_of_edges(G.to_undirected())

    rospy.logdebug("PLOT graph has %d nodes with %d edges" % (num_nodes, num_edges))
    rospy.logdebug("LABELED %d nodes with %d edges" % (des_num_nodes, des_num_edges))
    rospy.logdebug("Number of nodes in graph = %d" % (number_nodes))
    rospy.logdebug("connected components = "+str(nx.number_connected_components(G.to_undirected())))


def get_number_of_diferent_nodes(graph):
    """
    Extracts the list of number nodes in the graph
    :param graph:
    :return:
    """
    num_list = []
    for e in graph:
        x = e[0]
        if x not in num_list:
            num_list.append(x)
        y = e[1]
        if y not in num_list:
            num_list.append(y)


    rospy.logdebug("NUMLIST =="+str(num_list))
    rospy.logdebug("NUM =="+str(len(num_list)))

    return num_list, len(num_list)


def divide_auto_graph(graph, weights):
    """
    Divide the graph into auto graph Ex:(5,5) and non auto Ex:(5,2)
    :param graph:
    :return:
    """
    auto_graph = []
    non_auto_graph = []

    auto_weight = []
    non_auto_weight = []

    for idx, e in enumerate(graph):
        x = e[0]
        y = e[1]
        if x == y:
            auto_graph.append((x,y))
            auto_weight.append(weights[idx])
        else:
            non_auto_graph.append((x,y))
            non_auto_weight.append(weights[idx])

    rospy.logdebug("AUTO GRAPH =="+str(auto_graph))
    rospy.logdebug("NON AUTO GRAPH =="+str(non_auto_graph))

    rospy.logdebug("AUTO WEIGHT =="+str(auto_weight))
    rospy.logdebug("NON AUTO WEIGHT =="+str(non_auto_weight))

    return non_auto_graph, auto_graph, non_auto_weight, auto_weight


def label_node_correspondance(list_num,labels):
    """
    We compare the number of labels coincide with the number of nodes
    :param list_num:
    :param labels:
    :return:
    """

    rospy.logdebug("LIST OF DIFFERENT NODES = "+str(list_num))
    rospy.logdebug("LABELS FOR ALL THE NODES = "+str(labels))
    rospy.logdebug("LEN NODE LIST = %d, LEN LABEL LIST = %d" % (len(list_num), len(labels)))


def create_color_list(num_colors):
    """
    Generates a list of colors maximum difference possible.
    :param num_colors:
    :return:
    """
    rospy.logdebug("#### INIT COLOR LIST #####")
    color_list = []
    init_value = VMAX/ num_colors

    for i in range(1,num_colors+1):
        value = init_value*i
        rospy.logdebug("value = "+str(value))
        color_list.append(value)


    rospy.logdebug("COLOR LIST =="+str(color_list))
    rospy.logdebug("#### END COLOR LIST #####")

    return color_list


def create_node_color_list(number_nodes, node_labels_correspondence_dict, color_groups_label_correspondance_dict):
    """
    Generates a list of colors for each node depending on the cluster group specified in the
    color_groups_label_correspondance_dict.
    :param number_nodes:
    :param node_labels_correspondence_dict:
    :param color_groups_label_correspondance_dict:
    :return:
    """
    rospy.logdebug("number_nodes = "+str(number_nodes))
    node_color_list = [0]*number_nodes

    # We get the diferent groups there are
    different_groups_set = set(val for val in color_groups_label_correspondance_dict.values())
    color_list = create_color_list(num_colors=len(different_groups_set))

    rospy.logdebug("color_list = "+str(color_list))
    rospy.logdebug("color_list length = "+str(len(color_list)))
    rospy.logdebug("node_labels_correspondence_dict =="+str(node_labels_correspondence_dict))

    for node_number, node_label in node_labels_correspondence_dict.items():
        rospy.logdebug("******** START **********")
        rospy.logdebug("node_label = "+str(node_label))
        group_number = color_groups_label_correspondance_dict.get(node_label)
        rospy.logdebug("group_number = "+str(group_number))

        color_value = color_list[group_number]
        rospy.logdebug("color_value = "+str(color_value))

        rospy.logdebug("node_number = "+str(node_number))

        node_color_list[node_number] = color_value
        rospy.logdebug("******** END **********")

    rospy.logdebug("NODE COLOR LIST == "+str(node_color_list))

    return node_color_list, color_list

def create_nodelists_dict(node_color_list, color_list):
    """
    Creates a dictionary with key color number and value the list of nodes with that color
    :return:
    """

    color_nodelists_dict = {}

    for color in color_list:
        node_list = []
        for node, node_color in enumerate(node_color_list):
            if node_color == color:
                node_list.append(node)
        color_nodelists_dict.update({color:node_list})

    rospy.logdebug("color_nodelists_dict = "+str(color_nodelists_dict))

    return color_nodelists_dict


def create_color_legend(color_nodelists_dict):
    """
    Create a color label correspondance for legend
    :param color_nodelists_dict:
    :return:
    """
    color_legend = {}
    rgba_color_legend = {}
    # nipy_spectral gives the maximum color differentiation
    jet = plt.get_cmap('nipy_spectral')
    cNorm  = colors.Normalize(vmin=VMIN, vmax=VMAX)
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

    for idx, color in enumerate(color_nodelists_dict):
        group_name = "G-"+str(idx)
        key = color
        rgba_color_value = scalarMap.to_rgba(color)
        color_legend.update({key: group_name})
        rgba_color_legend.update({color: rgba_color_value})

    rospy.logdebug("color_legend = "+str(color_legend))
    rospy.logdebug("rgba_color_legend = "+str(rgba_color_legend))

    return color_legend, rgba_color_legend


def plot_external_node_text(pos, color_nodelists_dict, color_legend):
    """
    Plots text lables for node groups
    :return:
    """

    for color, node_list in color_nodelists_dict.items():
        for node in node_list:
            x = pos[node][0]
            y = pos[node][1]+30
            group_name = color_legend.get(color)
            plt.text(x, y, s=group_name, bbox=dict(facecolor='red', alpha=0.5),horizontalalignment='center')


def draw_graph(graph, connection_weigh, node_labels=None, color_groups_label_correspondance_dict=None, show=False, figure_save_path=None):

    node_size = 1000
    linewidths = 3
    node_alpha = 1.0
    node_text_size=15
    edge_text_pos=0.3
    text_font = 'sans-serif'
    edge_tickness=5
    edge_alpha=1.0



    list_num, number_nodes = get_number_of_diferent_nodes(graph)
    graph_non_auto, graph_auto, non_auto_weight, auto_weight = divide_auto_graph(graph, connection_weigh)

    des_num_edges, des_num_labeled_nodes = init_draw_graph_data_testing(graph,
                                                                        connection_weigh,
                                                                        node_labels,
                                                                        color_groups_label_correspondance_dict,
                                                                        graph_non_auto,
                                                                        graph_auto,
                                                                        non_auto_weight,
                                                                        auto_weight)

    G = create_dir_graph(graph, connection_weigh)

    node_edge_draw_graph_data_testing(des_num_edges, des_num_labeled_nodes, number_nodes, G)

    label_node_correspondance(list_num,node_labels)


    plt.figure(1,figsize=(10,8))
    pos = nx.graphviz_layout(G,prog="neato")

    weights = [0.2, 0.6]
    colors = ['green', 'orange', 'red']

    esmall = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] <= weights[0]]
    emedium = [(u, v) for (u, v, d) in G.edges(data=True) if weights[0] < d['weight'] <= weights[1]]
    elarge = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] > weights[1]]

    edge_list = []
    edge_list.append(esmall)
    edge_list.append(emedium)
    edge_list.append(elarge)

    rospy.logdebug("EDGESMALL =="+str(esmall))
    rospy.logdebug("EDGEMEDIUM =="+str(emedium))
    rospy.logdebug("EDGELARGE =="+str(elarge))

    for idx, color in enumerate(colors):
        edge_subset = edge_list[idx]
        nx.draw_networkx_edges(G, pos, edgelist=edge_subset, width=edge_tickness,alpha=edge_alpha,edge_color=color,arrows=True,edge_cmap=plt.cm.Reds)

    #C=nx.connected_component_subgraphs(G.to_undirected())
    #for g in C:
    g=G
    node_labels_correspondence_dict = create_label_dict(graph, node_labels)

    rospy.logdebug("### LENGTH OF NODE LIST =="+str(nx.number_of_nodes(g)))

    node_color_list, color_list = create_node_color_list(number_nodes=nx.number_of_nodes(g),
                                             node_labels_correspondence_dict=node_labels_correspondence_dict,
                                             color_groups_label_correspondance_dict=color_groups_label_correspondance_dict)

    color_nodelists_dict = create_nodelists_dict(node_color_list, color_list)

    color_legend, rgba_color_legend = create_color_legend(color_nodelists_dict)

    for color, node_list in color_nodelists_dict.items():

        # Color mapping
        node_group_name = color_legend.get(color)
        rgba_color = rgba_color_legend.get(color)

        nx.draw_networkx_nodes(G=g,
                               pos=pos,
                               nodelist=node_list,
                               node_size=node_size,
                               linewidths=linewidths,
                               alpha=node_alpha,
                               node_color=rgba_color,
                               vmin=VMIN,
                               vmax=VMAX,
                               label=node_group_name)



    nx.draw_networkx_labels(G=g,
                            pos=pos,
                            labels=node_labels_correspondence_dict,
                            font_size=node_text_size,
                            font_family=text_font)

    connections_labels = connection_weigh
    edge_labels = dict(zip(graph, connections_labels))

    rospy.logdebug("POS == "+str(pos))
    rospy.logdebug("EDGE LABELS =="+str(edge_labels))

    # Small function that extracts a subset of elements from a dictinary
    extract = lambda keys, dict: reduce(lambda x, y: x.update({y[0]:y[1]}) or x,
                                map(None, keys, map(dict.get, keys)), {})

    edge_labels_list = []
    edge_labels_list.append(extract(esmall, edge_labels))
    edge_labels_list.append(extract(emedium, edge_labels))
    edge_labels_list.append(extract(elarge, edge_labels))

    for idx, color in enumerate(colors):
        edge_labels_subset = edge_labels_list[idx]
        nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels_subset,label_pos=edge_text_pos, font_color=color)

    plot_external_node_text(pos, color_nodelists_dict, color_legend)


    plt.axis('off')
    plt.legend(numpoints = 1)
    # show graph, bare in mind that if we try to plot and save, the save just gets blanc data.
    if figure_save_path:
        rospy.loginfo("Saving img in path =="+str(figure_save_path))
        plt.savefig(figure_save_path, dpi=400, transparent=True)
        plt.clf()
    elif show and not figure_save_path:
        plt.show()
    else:
        rospy.logdebug("Not saving and not plotting...")


def plot_cluster(connection_matrix, working_cells_list, patern_sequence_correspondance, show=True, figure_path=None):

    cp = convert_csr_matrix_to_cluster_plot_class(connection_matrix)

    cp.print_cluster_plot()

    graph = cp.get_graph()
    connection_weigh = cp.get_connection_weigh()

    draw_graph(graph=graph,
               connection_weigh=connection_weigh,
               node_labels=working_cells_list,
               color_groups_label_correspondance_dict=patern_sequence_correspondance,
               show=show,
               figure_save_path=figure_path)