#!/usr/bin/env python
"""
This module codes the ffts generated by the recorder into a single
codded SDR ( Sparse Distributed Resesentation ). It will have to options of output
CSV files or topic publishing. CSV files are for swarming over the data. The topic publishing
will be used for the learning fase.
"""

__author__ = 'rdaneel'

import rospy
from internal_sound_pkg.msg import internal_sound_fft
from encoding_tools import FFT_To_SDR_ECNODER
import timeit
from internal_sound_pkg.msg import internal_sound_sdr


PUBLISH_RATE = 10


class FFTCoderPublisher(object):
    def __init__(self, freq_left):
        self.value = 0

        self.pub = rospy.Publisher('/sound_sdr', internal_sound_sdr, queue_size=1)
        rospy.Subscriber("chatter", internal_sound_fft, self.update_value)

        self.fft_enc_object = FFT_To_SDR_ECNODER(   frequency_array_LR=freq_left[1:],
                                                    data_max_value=10,
                                                    data_min_value=0,
                                                    resolution=0.1,
                                                    width=3)

    def update_value(self, msg):

        rospy.logdebug("INTERNAL OBJECT FREQ RANGE ==> "+str(self.fft_enc_object.get_frequency_array_LR()))

        left_channel_fft_array = msg.fft_abs_left[1:]
        right_channel_fft_array = msg.fft_abs_right[1:]

        start_time = timeit.default_timer()
        output = self.fft_enc_object.encode_fft_to_sdr(left_channel_fft_array, right_channel_fft_array)
        elapsed = timeit.default_timer() - start_time
        rospy.logdebug("Time for encode encoding ==> "+str(elapsed))
        rospy.logdebug("Value LENGTH of Result encoding Output==> "+str(len(output)))

        self.value = output

    def run(self):
        r = rospy.Rate(PUBLISH_RATE)
        while not rospy.is_shutdown():
            self.pub.publish(self.value)
            r.sleep()


if __name__ == '__main__':
    rospy.init_node('sdr_publisher_node')
    msg = rospy.wait_for_message("chatter", internal_sound_fft)
    rospy.loginfo("FREQ LIST => "+str(msg.freq_left))

    fft_coder_object = FFTCoderPublisher(msg.freq_left)
    fft_coder_object.run()

