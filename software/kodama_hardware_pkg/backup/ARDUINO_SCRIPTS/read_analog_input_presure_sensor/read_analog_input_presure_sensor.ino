int softPot = 0;    // this line selects the input pin a0 for the sensor
int ledPin = 13;      // this line selects the pin 13 for the LED output
int tempPot = 0;  // variable to store the value coming from the sensor

void setup() {
  // this line declares the ledPin as an OUTPUT:
  pinMode(ledPin, OUTPUT); 
}

void loop() {
  // read the value from the soft potentiometer
  tempPot = analogRead(softPot);   
  // it turns the LED on
  digitalWrite(ledPin, HIGH); 
  // stop the program for <tempPot> milliseconds:
  delay(tempPot);         
  // turn the LED off:       
  digitalWrite(ledPin, LOW);  
  // stop the program for for <tempPot> milliseconds:
  delay(tempPot);                 
}
