from sp_tools import spatial_pooler_example, learn_with_noise_example, input_noise_example
from encoding_tools import encoders_examples
from tp_tools import temporal_pooling_example

BEGGINER_GUIDE_NUPIC_MSG = "Press:\n(1)Encoders Examples :\n(2)SP Learn 4 Catergory :\n(3)Learn with noise example :\n" \
                           "(4)Input noise in categories test :\n(5)Input EXTREME noise in categories test :\n" \
                           "(6)TP example ordered sequence of 4 animals :\n(0)END"

# Start execution here!
if __name__ == '__main__':

    s = None

    while s != "0":
        print "############# START #############"
        print BEGGINER_GUIDE_NUPIC_MSG
        s = raw_input('--> ')
        if s == "1":
            encoders_examples()
        if s == "2":
            spatial_pooler_example()
        if s == "3":
            learn_with_noise_example()
        if s == "4":
            input_noise_example(rounds=1, extreme_noise=False)
        if s == "5":
            input_noise_example(rounds=1, extreme_noise=True)
        if s == "6":
            temporal_pooling_example(learning_iterations=100)

    print "Program Finished"
